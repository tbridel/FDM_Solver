%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This test case comes from the paper by Bogey & Bailly     %
% "A family of low dispersive and low dissipative explicit  %
% schemes for flow and noise computations", JCP 194, 2004   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
close all;
clc;
format long;

%% Test case

dx = 0.005;
x = (-16*dx):dx:(816*dx);
% x = (-16*dx):dx:(16*dx);
velocity = 1.0;
cfl = 0.2;
a=8;
b=3;
strength = 0.2;

N = length(x);
dt = cfl*dx;
u = sin(2*pi*x./(a*dx)).*exp(-log(2)*(x./(b*dx)).^2);
un = u';

figure;
plot(x, u, '-ob');
ylim([-1, 1]);

%% Coefficients for the schemes
close all

% FDo11p
coeffs = [-0.002484594688, 0.020779405824, -0.090320001280, 0.286511173973, -0.872756993962,...
                    0.0, 0.872756993962, -0.286511173973, 0.090320001280, -0.020779405824, 0.002484594688];
% FDs11p
% coeffs = [-1.0/1260., 5./504., -5./84., 5./21., -5./6., 0., 5./6., -5./21., 5./84., -5./504., 1./1260.];
                 
                 
a11dp = zeros(N,N);

a11dp(1, [N-4,N-3,N-2,N-1,N,1,2,3,4,5,6]) = coeffs / dx; % -velocity * cfl * coeffs;
a11dp(2, [N-3,N-2,N-1,N,1,2,3,4,5,6,7]) = coeffs / dx ; % -velocity * cfl * coeffs;
a11dp(3, [N-2,N-1,N,1,2,3,4,5,6,7,8]) = coeffs / dx ; % -velocity * cfl * coeffs;
a11dp(4, [N-1,N,1,2,3,4,5,6,7,8,9]) = coeffs / dx ; % -velocity * cfl * coeffs;
a11dp(5, [N,1,2,3,4,5,6,7,8,9,10]) = coeffs / dx ; % -velocity * cfl * coeffs;

for i = 6:N-5
    a11dp(i, i-5:i+5)  = coeffs / dx; % - velocity * cfl * coeffs;
end

a11dp(N, [N-5,N-4,N-3,N-2,N-1,N,1,2,3,4,5]) = coeffs / dx; % -velocity * cfl * coeffs;
a11dp(N-1, [N-6,N-5,N-4,N-3,N-2,N-1,N,1,2,3,4]) = coeffs / dx; % -velocity * cfl * coeffs;
a11dp(N-2, [N-7,N-6,N-5,N-4,N-3,N-2,N-1,N,1,2,3]) = coeffs / dx; % -velocity * cfl * coeffs;
a11dp(N-3, [N-8,N-7,N-6,N-5,N-4,N-3,N-2,N-1,N,1,2]) = coeffs / dx; % -velocity * cfl * coeffs;
a11dp(N-4, [N-9,N-8,N-7,N-6,N-5,N-4,N-3,N-2,N-1,N,1]) = coeffs / dx; % -velocity * cfl * coeffs;

%% Coefficients for the filter

d11dp = zeros(N, N);

% d11dp(1,1) =  0.320882352941;
% d11dp(1,2) = -0.465;
% d11dp(1,3) =  0.179117647059;
% d11dp(1,4) = -0.035;
% d11dp(1,5) =  0.;
% d11dp(1,6) =  0.;
% d11dp(1,7) =  0.;
% d11dp(1,8) =  0.;
% d11dp(1,9) =  0.;
% d11dp(1,10)=  0.;
% d11dp(1,11)=  0.;
% 
% d11dp(2,1) = -0.085777408970;
% d11dp(2,2) =  0.277628171524;
% d11dp(2,3) = -0.356848072173;
% d11dp(2,4) =  0.223119093072;
% d11dp(2,5) = -0.057347064865;
% d11dp(2,6) = -0.000747264596;
% d11dp(2,7) = -0.000027453993;
% d11dp(2,8) =  0.;
% d11dp(2,9) =  0.;
% d11dp(2,10)=  0.;
% d11dp(2,11)=  0.;
% d11dp(2,1) = -0.085777408970 + 0.000000000001;
% 
% d11dp(3,1) =  0.0307159855992469;
% d11dp(3,2) = -0.148395705486028; 
% d11dp(3,3) =  0.312055385963757; 
% d11dp(3,4) = -0.363202245195514; 
% d11dp(3,5) =  0.230145457063431; 
% d11dp(3,6) = -0.0412316564605079;
% d11dp(3,7) = -0.0531024700805787;
% d11dp(3,8) =  0.0494343261171287;
% d11dp(3,9) = -0.0198143585458560;
% d11dp(3,10)=  0.00339528102492129;
% d11dp(3,11)=  0.;
% 
% d11dp(4,1) = -0.000054596010;
% d11dp(4,2) =  0.042124772446;
% d11dp(4,3) = -0.173103107841;
% d11dp(4,4) =  0.299615871352;
% d11dp(4,5) = -0.276543612935;
% d11dp(4,6) =  0.131223506571;
% d11dp(4,7) = -0.023424966418;
% d11dp(4,8) =  0.013937561779;
% d11dp(4,9) = -0.024565095706;
% d11dp(4,10)=  0.013098287852;
% d11dp(4,11)= -0.002308621090;
% 
% d11dp(5,1) =  0.008391235145;
% d11dp(5,2) = -0.047402506444;
% d11dp(5,3) =  0.121438547725;
% d11dp(5,4) = -0.200063042812;
% d11dp(5,5) =  0.240069047836;
% d11dp(5,6) = -0.207269200140;
% d11dp(5,7) =  0.122263107844;
% d11dp(5,8) = -0.047121062819;
% d11dp(5,9) =  0.009014891495;
% d11dp(5,10)=  0.001855812216;
% d11dp(5,11)= -0.001176830044;
% d11dp(5,6) = -0.207269200140 - 0.000000000001;
% d11dp(5,7) =  0.122263107844 - 0.000000000001;
% 
% for i=6:N-5
%     d11dp(i, i-5:i+5) = [-0.002999540835, 0.018721609157, -0.059227575576, ...
%                          0.123755948787, -0.187772883589, 0.215044884112, ...
%                          -0.187772883589, 0.123755948787, -0.059227575576, ...
%                          0.018721609157, -0.002999540835];
% end
% 
% for i = N-4:N
%     for j = 1:11
%         d11dp(i, N-j+1) = d11dp(N-i+1, j);
%     end
% end

coeffs = [-0.002999540835, 0.018721609157, -0.059227575576, ...
         0.123755948787, -0.187772883589, 0.215044884112, ...
         -0.187772883589, 0.123755948787, -0.059227575576, ...
         0.018721609157, -0.002999540835];

d11dp(1, [N-4,N-3,N-2,N-1,N,1,2,3,4,5,6]) = coeffs;
d11dp(2, [N-3,N-2,N-1,N,1,2,3,4,5,6,7]) = coeffs;
d11dp(3, [N-2,N-1,N,1,2,3,4,5,6,7,8]) = coeffs;
d11dp(4, [N-1,N,1,2,3,4,5,6,7,8,9]) = coeffs;
d11dp(5, [N,1,2,3,4,5,6,7,8,9,10]) = coeffs;

for i = 6:N-5
    d11dp(i, i-5:i+5)  = coeffs;
end

d11dp(N, [N-5,N-4,N-3,N-2,N-1,N,1,2,3,4,5]) = coeffs;
d11dp(N-1, [N-6,N-5,N-4,N-3,N-2,N-1,N,1,2,3,4]) = coeffs;
d11dp(N-2, [N-7,N-6,N-5,N-4,N-3,N-2,N-1,N,1,2,3]) = coeffs;
d11dp(N-3, [N-8,N-7,N-6,N-5,N-4,N-3,N-2,N-1,N,1,2]) = coeffs;
d11dp(N-4, [N-9,N-8,N-7,N-6,N-5,N-4,N-3,N-2,N-1,N,1]) = coeffs;

%% Temporal integration

gammas = [1.0, 0.5, 0.165919771368, 0.040919732041, 0.007555704391, 0.000891421261];
R = eye(N,N);
for i = 1:6
    R = R + (dt^i) * ((-a11dp)^i) * gammas(i);
end

figure;
time = 0;
for it = 1:4000 %Nit
    time = time + dt;
    un = R * un;
    % Filtering or not (uncomment below)
%     un = un - strength * d11dp * un;
    %
    if mod(it, 165) == 0
        plot(x, un, '-b');
        % xlim([-0.08+time, 0.08+time]);
        % xlim([-5.0, -4.0]);
        ylim([-1.0, 1.0]);
        title(['Nit = ', num2str(it)]);
        drawnow
    end
end


%% Error and post-processing

uexact = sin(2*pi*(x-time)./(a*dx)).*exp(-log(2)*((x-time)./(b*dx)).^2);
% uexact = sin(2*pi*x./(a*dx)).*exp(-log(2)*(x./(b*dx)).^2);
% enum = sqrt( sum( (un(801:833)'-uexact(801:833)).^2 ) / sum( uexact(801:833).^2 ) )
enum = sqrt( sum( (un'-uexact).^2 ) / sum( uexact.^2 ) )

figure;
plot(x, un, '-b');
hold on
% plot(x, sin(2*pi*(x-time)./(a*dx)).*exp(-log(2)*((x-time)./(b*dx)).^2), 'o');
plot(x, sin(2*pi*x./(a*dx)).*exp(-log(2)*(x./(b*dx)).^2), 'o');
% xlim([3.92,4.08]);
ylim([-1, 1]);
hold off