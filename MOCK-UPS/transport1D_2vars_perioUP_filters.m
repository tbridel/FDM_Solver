%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This test case comes from the paper by Bogey & Bailly     %
% "A family of low dispersive and low dissipative explicit  %
% schemes for flow and noise computations", JCP 194, 2004   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
close all;
clc;

%% Test case

% dx = 1;
% x = -50:dx:50;
x = linspace(-50, 50, 33);
dx = x(2) - x(1);
velocity = 1.0;
cfl = 0.8;
a=8;
b=3;
strength = 0.2;
Nit = 250;

N = length(x);
dt = cfl*dx;
u = zeros(1,N);
p = cos(2*pi*x./8).*exp(-log(2)*(x./12).^2);
un = u';
pn = p';
uk = zeros(2*N, 1);
uk(1:N) = un;
uk(N+1:2*N) = pn;

%% Coefficients for the schemes

coeffs = [-0.002484594688, 0.020779405824, -0.090320001280, 0.286511173973, -0.872756993962,...
                    0.0, 0.872756993962, -0.286511173973, 0.090320001280, -0.020779405824, 0.002484594688];
                
a11dp = zeros(N,N);

a11dp(1, [N-4,N-3,N-2,N-1,N,1,2,3,4,5,6]) = -velocity * cfl * coeffs;
a11dp(2, [N-3,N-2,N-1,N,1,2,3,4,5,6,7]) = -velocity * cfl * coeffs;
a11dp(3, [N-2,N-1,N,1,2,3,4,5,6,7,8]) = -velocity * cfl * coeffs;
a11dp(4, [N-1,N,1,2,3,4,5,6,7,8,9]) = -velocity * cfl * coeffs;
a11dp(5, [N,1,2,3,4,5,6,7,8,9,10]) = -velocity * cfl * coeffs;

for i = 6:N-5
    a11dp(i, i-5:i+5)  = - velocity * cfl * coeffs;
end

a11dp(N, [N-5,N-4,N-3,N-2,N-1,N,1,2,3,4,5]) = -velocity * cfl * coeffs;
a11dp(N-1, [N-6,N-5,N-4,N-3,N-2,N-1,N,1,2,3,4]) = -velocity * cfl * coeffs;
a11dp(N-2, [N-7,N-6,N-5,N-4,N-3,N-2,N-1,N,1,2,3]) = -velocity * cfl * coeffs;
a11dp(N-3, [N-8,N-7,N-6,N-5,N-4,N-3,N-2,N-1,N,1,2]) = -velocity * cfl * coeffs;
a11dp(N-4, [N-9,N-8,N-7,N-6,N-5,N-4,N-3,N-2,N-1,N,1]) = -velocity * cfl * coeffs;

afull = zeros(2*N, 2*N);
afull(1:N, N+1:2*N) = a11dp;
afull(N+1:2*N, 1:N) = a11dp;

%% Coefficients for the filter

d11dp = zeros(N, N);

coeffs = [-0.002999540835, 0.018721609157, -0.059227575576, ...
         0.123755948787, -0.187772883589, 0.215044884112, ...
         -0.187772883589, 0.123755948787, -0.059227575576, ...
         0.018721609157, -0.002999540835];

d11dp(1, [N-4,N-3,N-2,N-1,N,1,2,3,4,5,6]) = coeffs;
d11dp(2, [N-3,N-2,N-1,N,1,2,3,4,5,6,7]) = coeffs;
d11dp(3, [N-2,N-1,N,1,2,3,4,5,6,7,8]) = coeffs;
d11dp(4, [N-1,N,1,2,3,4,5,6,7,8,9]) = coeffs;
d11dp(5, [N,1,2,3,4,5,6,7,8,9,10]) = coeffs;

for i = 6:N-5
    d11dp(i, i-5:i+5)  = coeffs;
end

d11dp(N, [N-5,N-4,N-3,N-2,N-1,N,1,2,3,4,5]) = coeffs;
d11dp(N-1, [N-6,N-5,N-4,N-3,N-2,N-1,N,1,2,3,4]) = coeffs;
d11dp(N-2, [N-7,N-6,N-5,N-4,N-3,N-2,N-1,N,1,2,3]) = coeffs;
d11dp(N-3, [N-8,N-7,N-6,N-5,N-4,N-3,N-2,N-1,N,1,2]) = coeffs;
d11dp(N-4, [N-9,N-8,N-7,N-6,N-5,N-4,N-3,N-2,N-1,N,1]) = coeffs;

dfull = zeros(2*N, 2*N);
dfull(1:N, 1:N) = d11dp;
dfull(N+1:2*N, N+1:2*N) = d11dp;

%% Temporal integration

gammas = [1.0, 0.5, 0.165919771368, 0.040919732041, 0.007555704391, 0.000891421261];
R = eye(2*N,2*N);
for i = 1:6
    R = R + (afull^i) * gammas(i);
end

figure;
time = 0;
for it = 1:254 
    time = time + dt;
    uk = R * uk;
    %
    uk = uk - strength * dfull * uk;
    %
    if mod(it, 1) == 0
        plot(x, uk(1:N), '-b');
        hold on;
        plot(x, uk(N+1:2*N), '--r');
        hold off;
        % xlim([-0.08+time, 0.08+time]);
        xlim([-50, 50]);
        ylim([-1, 1]);
        legend('u', 'p');
        drawnow
    end
end

figure;
plot(x, uk(1:N), '-b');
hold on
plot(x, uk(N+1:2*N), '--r');
plot(x, p, 'o');
xlim([-50,50]);
legend('u','p');
hold off