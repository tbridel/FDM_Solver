#! /usr/bin/env python

import h5py
import numpy as np
import os
import sys

# Acquire the dimension of the problem
ndim = 1
if len(sys.argv) == 1:
	pass
else:
	for arg in sys.argv[1:]:
		if '--dim' in arg:
			ndim = int(arg.split('=')[1])
		elif '--nx' in arg:
			nx = int(arg.split('=')[1])
		elif '--ny' in arg:
			ny = int(arg.split('=')[1])
		elif '--dt' in arg:
			dt = float(arg.split('=')[1])

# Read the files in consequence
if ndim == 1:
	lof = os.popen('ls sol_*.dat').read().split('\n')[:-1]
else:
	lof = os.popen('ls sol_*.h5').read().split('\n')[:-1]

# Get the numbers of the files for reordering and safe keeping
numbers = [int(val.split('.')[0].split('_')[-1]) for val in lof]
order = np.argsort(numbers)
lof = np.asarray(lof)[order]
numbers = np.asarray(numbers)[order]

# For each file, load the data, and recreate a clean csv file
if ndim == 1:
	for idx, fname in enumerate(lof):
		counter = 0
		r_vector = []
		with open(fname, 'r') as fid:
			for line in fid:
				if '%' in line or '[' in line:
					continue
				if '%' in line or '[' in line or ']' in line:
					continue
				counter += 1
				r_vector.append(float(line.split('\n')[0]))
		nvars = counter // nx - 1
		# Extract the coordinates and the data
		coords = np.array(r_vector[:nx])
		unk = []; unk_names = []
		for jdx in range(nvars):
			unk.append(np.array(r_vector[nx*(jdx+1):nx*(jdx+2)]))
			unk_names.append('Uk%d'%(jdx+1))
		# Store them in a file in CSV format
		unk.insert(0, coords)
		np.savetxt('sol_%06d.csv'%(numbers[idx]), np.array(unk).T, delimiter=',')
		# Prepend the names of the variables at the top of the file
		with open('sol_%06d.csv'%(numbers[idx]), 'r+') as fid:
			content = fid.read()
			fid.seek(0, 0)
			fid.write(('x,'+','.join(unk_names)).rstrip('\r\n') + '\n' + content)
	# Then destroy the original files
	for fname in lof:
		os.remove(fname)
elif ndim == 2:
	# Get the data in a new better organized hdf file
	fout = h5py.File("sol.h5", 'w')
	gmesh = fout.create_group("Mesh")
	ginstant = fout.create_group("Instants")
	for idx, fname in enumerate(lof):
		# Open the C-written hdf file
		fin = h5py.File(fname, 'r')
		keys = fin.keys()
		# Get the coordinates (only once, assume they are all the same)
		if not idx:
			gridX = np.reshape(fin[keys[0]][:], (ny, nx))
			gridY = np.reshape(fin[keys[1]][:], (ny, nx))
		# Get the data
		if (len(keys) == 3):
			nvars = fin[keys[2]][:].size // (nx * ny)
			unk = []
			for jdx in range(nvars):
				unk.append(np.reshape(fin[keys[2]][:][nx*ny*jdx:nx*ny*(jdx+1)], (ny, nx)))
		else:
			raise NotImplementedError
		# Close the C-written hdf file
		fin.close()
		# Store the coordinate once in the new hdf file
		if not idx:
			gmesh.create_dataset("CoordX", shape=gridX.shape, dtype=gridX.dtype, data=gridX)
			gmesh.create_dataset("CoordY", shape=gridY.shape, dtype=gridY.dtype, data=gridY)
		# Store the data in the appropriate instant group
		if (len(keys) == 3):
			currentinstant = ginstant.create_group("%05d"%idx)
			for jdx in range(nvars):
				currentinstant.create_dataset("Uk%d"%(jdx+1), shape=unk[jdx].shape, dtype=unk[jdx].dtype, data=unk[jdx])
		else:
			raise NotImplementedError
	# Close the new hdf file
	fout.close()
	# Write an XDMF file
	# Prepare a variable to store all the text to print it in one slab later
	global tab_n
	global xmf_txt
	xmf_txt = '<?xml version="1.0" ?>\n'
	tab_n = 0
	# Prepare a function to strip and properply prepare a string to be printed in the xmf file
	def xmf(txt):
		global tab_n
		global xmf_txt
		tab = '    '
		txt = txt.strip()
		if txt[:2] == '</':
			tab_n -= 1
		xmf_txt += tab * tab_n + txt + '\n'
		if txt[0] == '<' and txt[1] != '/':
			tab_n += 1
		if txt[-2:] == '/>':
			tab_n -= 1
	# Go for the xmf file
	xmf('<Xdmf Version="2.0" xmlns:xi="http://www.w3.org/2001/XInclude">')
	xmf('<Domain>')
	zone_tree = 'sol.h5:'
	xmf('<Grid Name="Mesh" GridType="Collection" CollectionType="Temporal">')  # instants collection
	for idx in xrange(len(lof)):
		instant_tree = '%s/Instants/%05d'%(zone_tree, idx)
		xmf('<Grid Name="Mesh">')
		xmf('<Time Value="%s" />'%(idx*dt))
		# Connectivity
		node_dimensions = ' '.join(map(str, (ny, nx)))
		cell_dimensions = ' '.join(map(str, (ny-1, nx-1)))
		xmf('<Topology Type="2DSMesh" Dimensions="%s" />' % node_dimensions)
		# Mesh
		nb_nodes = nx * ny
		xmf('<Geometry Type="X_Y">')
		xmf('<DataItem Format="HDF" ItemType="Uniform" Precision="%i" NumberType="Float" Dimensions="%s">'%(gridX.dtype.itemsize, node_dimensions))
		xmf('%s/Mesh/CoordX'%(zone_tree))
		xmf('</DataItem>')
		xmf('<DataItem Format="HDF" ItemType="Uniform" Precision="%i" NumberType="Float" Dimensions="%s">'%(gridY.dtype.itemsize, node_dimensions))
		xmf('%s/Mesh/CoordY'%(zone_tree))
		xmf('</DataItem>')
		xmf('</Geometry>')
		# Variables
		for jdx in range(nvars):
			xmf('<Attribute Name="Uk%d" Center="node" AttributeType="Scalar">'%(jdx+1))
			xmf('<DataItem Format="HDF" ItemType="Uniform" Precision="%i" NumberType="Float" Dimensions="%s">'%(unk[jdx].dtype.itemsize, node_dimensions))
			xmf('%s/Uk%d'%(instant_tree,jdx+1))
			xmf('</DataItem>')
			xmf('</Attribute>')
		xmf('</Grid>')
	xmf('</Grid>')
	xmf('</Domain>')
	xmf('</Xdmf>')
	# Write the content in an actual file
	file_w = open('sol.xmf', 'w')
	file_w.write(xmf_txt)
	file_w.close()
else:
	raise NotImplementedError
