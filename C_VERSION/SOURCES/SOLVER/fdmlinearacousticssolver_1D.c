/* This file contains the methods handling the one-dimensional solver for
   the linear acoustics solver 1D
   @author : Thibault Bridel-Bertomeu
   @license: MIT
   @status : development
*/

#include "fdmsolver.h"

/* This method initializes a solver for the linear acoustics equations in 1D
*/
LinearAcousticsSolver1D* init_linearAcousticsSolver1D(PetscInt nequation_args, PetscChar equation_arguments[TWENTY*2][WORDSZ], Grid1D *grid)
{
	// Declare the linear acoustics solver entity
	LinearAcousticsSolver1D *las = (LinearAcousticsSolver1D*) malloc(sizeof(LinearAcousticsSolver1D));

	// Create child transport solver no. 1
	las->ierr = PetscPrintf(PETSC_COMM_WORLD, "\tInitializing child transport solver no. 1...\n");
	las->ts_1 = init_transportSolver1D(nequation_args, equation_arguments, grid, las, 1);

	// Create child transport solver no. 2
	las->ierr = PetscPrintf(PETSC_COMM_WORLD, "\n\tInitializing child transport solver no. 2...\n");
	las->ts_2 = init_transportSolver1D(nequation_args, equation_arguments, grid, las, 2);

	// Call the routine to build the RHS matrix based on numerical scheme
	get_las1D_rhs(las, grid);

	// Call the routine to build the filter matrix based on numerical scheme
	if ((las->ts_1->parent_solver->hasFilter) ||
		(las->ts_2->parent_solver->hasFilter)) {
		get_las1D_filter(las, grid);
	}

	// Return the solver
	return las;
}

/* This method yields the RHS matrix for a one dimensional linear acoustics solver based
   on the name of the scheme.
*/
void get_las1D_rhs(LinearAcousticsSolver1D* las, Grid1D* grid)
{
	PetscInt nlocal, vecsize;

	// Get parallel layout of the grid->unknowns vector
	las->ierr = VecGetSize(grid->unknowns, &vecsize);
	las->ierr = VecGetLocalSize(grid->unknowns, &nlocal);

	// Declare the RHS matrix
	las->ierr = MatCreate(PETSC_COMM_WORLD, &(las->rhs));
	las->ierr = MatSetSizes(las->rhs, nlocal, nlocal, vecsize, vecsize);
	las->ierr = MatSetType(las->rhs, MATAIJ);
	las->ierr = MatSetUp(las->rhs);

	// Call the two subroutines
	PetscPrintf(PETSC_COMM_WORLD, "\tFilling the diagonals of the RHS matrix ...\n");
	// Child no.1
	get_ts1D_rhs(las->ts_1, grid);
	// Child no.2
	get_ts1D_rhs(las->ts_2, grid);
}

/* This method is used to apply all kind of boundary condition onto
   the matrix(ces) of a 1D linear-acoustics solver
*/
PetscErrorCode applyBocos_las1D(PetscInt nboco_arguments, PetscChar boco_arguments[2*TWENTY][WORDSZ], Grid1D *grid, LinearAcousticsSolver1D *las)
{
	PetscErrorCode ierr = 0;
	PetscChar bocoU[2*TWENTY][WORDSZ] = {""}, bocoP[2*TWENTY][WORDSZ] = {""};
	PetscInt nbocoU = 0, nbocoP = 0, vecsize, nlocal;

	// Acquire the length of the unknown vector (for the neumanns realloc)
	ierr = VecGetSize(grid->unknowns, &vecsize); CHKERRQ(ierr);
	ierr = VecGetLocalSize(grid->unknowns, &nlocal); CHKERRQ(ierr);

	// Get the names of the temporal, convection and diffusion schemes
	// Also acquire the convection velocity and conductivity string
	for (int i=0; i < nboco_arguments; i++) {
		if (strstr(boco_arguments[i], "type_u") != NULL) {
			strcpy(bocoU[nbocoU], boco_arguments[i]);
			nbocoU ++;
		} else if (strstr(boco_arguments[i], "type_p") != NULL) {
			strcpy(bocoP[nbocoP], boco_arguments[i]);
			nbocoP ++;
		}
	}

	// Apply the boundary conditions to child transport solver no.1
	ierr = applyBocos_ts1D(nbocoU, bocoU, grid, las->ts_1); CHKERRQ(ierr);

	// Apply the boundary conditions to child transport solver no.2
	ierr = applyBocos_ts1D(nbocoP, bocoP, grid, las->ts_2); CHKERRQ(ierr);

	// Assemble the RHS matrix
	ierr = PetscPrintf(PETSC_COMM_WORLD, "\tAssembling the RHS matrix ...\n"); CHKERRQ(ierr);
	ierr = MatAssemblyBegin(las->rhs, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
	ierr = MatAssemblyEnd(las->rhs, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

	// Correct the indices of the neumann and/or dirichlet rows if present in ts_2
	if (las->ts_2->parent_solver->ndir_rows > 0){
		for (int i=0; i<(las->ts_2->parent_solver->ndir_rows+1); i++) {
			las->ts_2->parent_solver->dir_rows[i] += grid->nvtx;
		}
	}
	for (int i=0; i<DIM2; i++) {
		if (las->ts_2->neumanns[i] != NULL) {
			las->ts_2->parent_solver->neu_rows[i] += grid->nvtx;
		}
		{ // Append a NULL to ts_1 neumanns vectors
			if (las->ts_1->neumanns[i] != NULL) {
				PetscScalar *v;
				PetscInt *idx, rs, re, nl;
				ierr = VecGetOwnershipRange(las->ts_1->neumanns[i], &rs, &re); CHKERRQ(ierr);
				ierr = VecGetLocalSize(las->ts_1->neumanns[i], &nl); CHKERRQ(ierr);
				v = (PetscScalar*) malloc(nl * sizeof(PetscScalar));
				idx = (PetscInt*) malloc(nl * sizeof(PetscInt));
				for (int i=rs; i<re; i++) {
					idx[i-rs] = i;
				}
				ierr = VecGetValues(las->ts_1->neumanns[i], nl, idx, v); CHKERRQ(ierr);
				ierr = VecDestroy(&(las->ts_1->neumanns[i])); CHKERRQ(ierr);
				ierr = VecCreate(PETSC_COMM_WORLD, &(las->ts_1->neumanns[i])); CHKERRQ(ierr);
				ierr = VecSetSizes(las->ts_1->neumanns[i], nlocal, vecsize); CHKERRQ(ierr);
				ierr = VecSetUp(las->ts_1->neumanns[i]); CHKERRQ(ierr);
				ierr = VecSetValues(las->ts_1->neumanns[i],nl,idx,v,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(las->ts_1->neumanns[i]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(las->ts_1->neumanns[i]); CHKERRQ(ierr);
				//
				free(v);
				free(idx);
			}
		}
		{ // Prepend a NULL to ts_2 neumanns vectors
			if (las->ts_2->neumanns[i] != NULL) {
				PetscScalar *v;
				PetscInt *idx, rs, re, nl;
				ierr = VecGetOwnershipRange(las->ts_2->neumanns[i], &rs, &re); CHKERRQ(ierr);
				ierr = VecGetLocalSize(las->ts_2->neumanns[i], &nl); CHKERRQ(ierr);
				v = (PetscScalar*) malloc(nl * sizeof(PetscScalar));
				idx = (PetscInt*) malloc(nl * sizeof(PetscInt));
				for (int i=rs; i<re; i++) {
					idx[i-rs] = i;
				}
				ierr = VecGetValues(las->ts_2->neumanns[i], nl, idx, v); CHKERRQ(ierr);
				ierr = VecDestroy(&(las->ts_2->neumanns[i])); CHKERRQ(ierr);
				ierr = VecCreate(PETSC_COMM_WORLD, &(las->ts_2->neumanns[i])); CHKERRQ(ierr);
				ierr = VecSetSizes(las->ts_2->neumanns[i], nlocal, vecsize); CHKERRQ(ierr);
				ierr = VecSetUp(las->ts_2->neumanns[i]); CHKERRQ(ierr);
				for (int i=rs; i<re; i++) {
					idx[i-rs] = i + grid->nvtx;
				}
				ierr = VecSetValues(las->ts_2->neumanns[i],nl,idx,v,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(las->ts_2->neumanns[i]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(las->ts_2->neumanns[i]); CHKERRQ(ierr);
				//
				free(v);
				free(idx);
			}
		}
	}

	// Assemble the RHS matrix
	ierr = PetscPrintf(PETSC_COMM_WORLD, "\tAssembling the RHS matrix ...\n"); CHKERRQ(ierr);
	ierr = MatAssemblyBegin(las->rhs, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
	ierr = MatAssemblyEnd(las->rhs, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

	// Assemble the filter matrix
	if ((las->ts_1->parent_solver->hasFilter) ||
		(las->ts_2->parent_solver->hasFilter)) {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "\tAssembling the filter matrix ...\n"); CHKERRQ(ierr);
		ierr = MatAssemblyBegin(las->filter, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
		ierr = MatAssemblyEnd(las->filter, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
	}

	return ierr;
}

/* Method for the execution of a one dimensional linear-acoustics solver
*/
PetscErrorCode execute_las1D(LinearAcousticsSolver1D *las, Grid1D* grid)
{
	Mat identity, R, mlist1, mlist2;
	PetscErrorCode ierr = 0;
	PetscScalar factor = 0.0, *ukmax, *ukmin, *exvalues;
	PetscScalar *nL2diff=NULL, *nL2sol=NULL, *errorL2=NULL;
	PetscScalar neuscal = 0.0;
	PetscInt step, idx, rstart, rend, nlocal, vecsize, ninit_values, *indices;
	Vec b, c;
	PetscMPIInt size, rank;

	// Get info on the MPI layout
	ierr = MPI_Comm_size(PETSC_COMM_WORLD, &size); CHKERRQ(ierr);
	ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank); CHKERRQ(ierr);

	// Get parallel layout of the grid->unknowns vector
	ierr = VecGetSize(grid->unknowns, &vecsize); CHKERRQ(ierr);
	ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend); CHKERRQ(ierr);
	ierr = VecGetLocalSize(grid->unknowns, &nlocal); CHKERRQ(ierr);

	// Prepare the maxima and minima vector for user information
	ninit_values = (PetscInt) (vecsize / grid->nvtx);
	ukmax = (PetscScalar*) malloc(ninit_values * sizeof(PetscScalar));
	ukmin = (PetscScalar*) malloc(ninit_values * sizeof(PetscScalar));
	exvalues = (PetscScalar*) malloc(nlocal * sizeof(PetscScalar));
	indices = (PetscInt*) malloc(nlocal * sizeof(PetscInt));
	for (int i=0; i<ninit_values; i++) {
		ukmax[i] = 0;
		ukmin[i] = (PetscScalar) 1e+12;
	}
	for (int i=rstart; i<rend; i++) {
		indices[i-rstart] = i;
	}

	// Create an identity matrix, it can come handy
	ierr = MatCreate(PETSC_COMM_WORLD, &identity); CHKERRQ(ierr);
	ierr = MatSetSizes(identity, nlocal, nlocal, vecsize, vecsize); CHKERRQ(ierr);
	ierr = MatSetType(identity, MATAIJ); CHKERRQ(ierr);
	ierr = MatSetUp(identity); CHKERRQ(ierr);
	for (idx = rstart; idx < rend ; idx++) {
		ierr = MatSetValue(identity, idx, idx, 1.0, INSERT_VALUES); CHKERRQ(ierr);
	}
	ierr = MatAssemblyBegin(identity, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
	ierr = MatAssemblyEnd(identity, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

	// Prepare a few other objects before the big loop
	// First correctly scale the two solvers and pass them actually to the right hand side
	ierr = MatScale(las->rhs, -1.0); CHKERRQ(ierr);
	// Then handle the preprocessing for the RK matrix
	// (assume that both transport solvers have received the same RK scheme information)
	if (las->ts_1->parent_solver->rk_params->has_gammas) {
		PetscPrintf(PETSC_COMM_WORLD, "\tPreparing Runge-Kutta matrix ...\n");
		// Create the matrix and copy the values from the RHS and assemble
		ierr = MatDuplicate(las->rhs, MAT_COPY_VALUES, &R); CHKERRQ(ierr);
		ierr = MatScale(R, las->ts_1->parent_solver->dt); CHKERRQ(ierr);
		ierr = MatAssemblyBegin(R, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
		ierr = MatAssemblyEnd(R, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
		// First element of mlist is the RHS matrix
		ierr = MatDuplicate(las->rhs, MAT_COPY_VALUES, &mlist1); CHKERRQ(ierr);
		// Now loop over the remaining RK stages to simulate matrix power
		for (idx = 1; idx < las->ts_1->parent_solver->rk_params->steps; idx++) {
			factor = PetscPowScalar(las->ts_1->parent_solver->dt, idx+1)*las->ts_1->parent_solver->rk_params->gammas[idx];
			ierr = MatMatMult(las->rhs, mlist1, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &mlist2); CHKERRQ(ierr);
			ierr = MatDestroy(&mlist1); CHKERRQ(ierr);
			ierr = MatAXPY(R, factor, mlist2, DIFFERENT_NONZERO_PATTERN); CHKERRQ(ierr);
			ierr = MatDuplicate(mlist2, MAT_COPY_VALUES, &mlist1); CHKERRQ(ierr);
			ierr = MatDestroy(&mlist2); CHKERRQ(ierr);
		}
		// Destroy the mlist object
		MatDestroy(&mlist1);
		MatDestroy(&mlist2);
		// Add identity to R
		ierr = MatAXPY(R, 1.0, identity, DIFFERENT_NONZERO_PATTERN); CHKERRQ(ierr);
		// Change R structure in case of dirichlet boco
		// For transport solver no.1
		{
			PetscInt *rows = (PetscInt*) malloc(sizeof(PetscInt) * (las->ts_1->parent_solver->ndir_rows+1));
			for (int i=0; i<(las->ts_1->parent_solver->ndir_rows+1); i++) {
				rows[i] = las->ts_1->parent_solver->dir_rows[i];
			}
			ierr = MatZeroRowsColumns(R, las->ts_1->parent_solver->ndir_rows+1, rows, 0.0, 0, 0); CHKERRQ(ierr);
			if ((las->ts_1->parent_solver->hasFilter) ||
				(las->ts_2->parent_solver->hasFilter)) {
				ierr = MatZeroRowsColumns(las->filter, las->ts_1->parent_solver->ndir_rows+1, rows, 0.0, 0, 0); CHKERRQ(ierr);
			}
			free(rows);
		}
		// For transport solver no.2
		{
			PetscInt *rows = (PetscInt*) malloc(sizeof(PetscInt) * (las->ts_2->parent_solver->ndir_rows+1));
			for (int i=0; i<(las->ts_2->parent_solver->ndir_rows+1); i++) {
				rows[i] = las->ts_2->parent_solver->dir_rows[i];
			}
			ierr = MatZeroRowsColumns(R, las->ts_2->parent_solver->ndir_rows+1, rows, 0.0, 0, 0); CHKERRQ(ierr);
			if ((las->ts_1->parent_solver->hasFilter) ||
				(las->ts_2->parent_solver->hasFilter)) {
				ierr = MatZeroRowsColumns(las->filter, las->ts_2->parent_solver->ndir_rows+1, rows, 0.0, 0, 0); CHKERRQ(ierr);
			}
			free(rows);
		}
		// Change filter structure in case of Neumann boco
		if ((las->ts_1->parent_solver->hasFilter) ||
		    (las->ts_2->parent_solver->hasFilter)) {
			for (int i=0; i<DIM2; i++) {
				if (las->ts_1->neumanns[i] != NULL) {
					ierr = MatZeroRows(las->filter, 1, las->ts_1->parent_solver->neu_rows + i, 0.0, 0, 0); CHKERRQ(ierr);
				}
				if (las->ts_2->neumanns[i] != NULL) {
					ierr = MatZeroRows(las->filter, 1, las->ts_2->parent_solver->neu_rows + i, 0.0, 0, 0); CHKERRQ(ierr);
				}
			}
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown definition of the Runge-Kutta scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}
	// Scale the filter by the strength input by the user
	if ((las->ts_1->parent_solver->hasFilter) ||
		(las->ts_2->parent_solver->hasFilter)) {
		ierr = MatScale(las->filter, las->ts_1->parent_solver->filter_strength); CHKERRQ(ierr);
	}

	// Export the initial solution
	{
		step = 0;
		// Create the viewers and save the vectors
		PetscViewer Viewer = NULL;
		PetscChar filename[WORDSZ];
		if (grid->parent_grid->ndim == DIM2) {
			sprintf(filename, "sol_%06d.h5", step);
			ierr = PetscViewerHDF5Open(PETSC_COMM_WORLD, filename, FILE_MODE_WRITE, &Viewer); CHKERRQ(ierr);
		} else if (grid->parent_grid->ndim == DIM1) {
			sprintf(filename, "sol_%06d.dat", step);
			ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, filename, &Viewer); CHKERRQ(ierr);
			ierr = PetscViewerPushFormat(Viewer, PETSC_VIEWER_ASCII_MATLAB); CHKERRQ(ierr);
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Wrong dimension for solution output.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
		ierr = VecView(grid->x, Viewer); CHKERRQ(ierr);
		ierr = VecView(grid->unknowns, Viewer); CHKERRQ(ierr);
		ierr = PetscViewerDestroy(&Viewer); CHKERRQ(ierr);
	}

	// Loop on iterations
	ierr = PetscPrintf(PETSC_COMM_WORLD, "\tMarching the equation in time ...\n"); CHKERRQ(ierr);
	for (step = 1; step <= las->ts_1->parent_solver->nit; step ++) {
		// Implementation differs function of the way the RK scheme is made
		if (las->ts_1->parent_solver->rk_params->has_gammas) {
			// Get an auxiliary vector
			ierr = VecDuplicate(grid->unknowns, &b); CHKERRQ(ierr);
			// Apply the temporal scheme
			ierr = MatMult(R, grid->unknowns, b); CHKERRQ(ierr);
			// Save the result
			ierr = VecCopy(b, grid->unknowns); CHKERRQ(ierr);
			// Destroy b
			ierr = VecDestroy(&b); CHKERRQ(ierr);
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown definition of the Runge-Kutta scheme.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
		// Apply Neumann boundary conditions
		for (int i=0; i<DIM2; i++) {
			if (las->ts_1->neumanns[i] != NULL) {
				ierr = VecDot(grid->unknowns, las->ts_1->neumanns[i], &neuscal); CHKERRQ(ierr);
				ierr = VecSetValue(grid->unknowns, las->ts_1->parent_solver->neu_rows[i], neuscal, INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(grid->unknowns); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(grid->unknowns); CHKERRQ(ierr);
			}
			if (las->ts_2->neumanns[i] != NULL) {
				ierr = VecDot(grid->unknowns, las->ts_2->neumanns[i], &neuscal); CHKERRQ(ierr);
				ierr = VecSetValue(grid->unknowns, las->ts_2->parent_solver->neu_rows[i], neuscal, INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(grid->unknowns); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(grid->unknowns); CHKERRQ(ierr);
			}
		}
		// Filter the signal
		if ((las->ts_1->parent_solver->hasFilter) ||
			(las->ts_2->parent_solver->hasFilter)) {
			ierr = VecDuplicate(grid->unknowns, &b); CHKERRQ(ierr);
			ierr = MatMult(las->filter, grid->unknowns, b); CHKERRQ(ierr);
			ierr = VecAXPY(grid->unknowns, -1.0, b); CHKERRQ(ierr);
			ierr = VecDestroy(&b); CHKERRQ(ierr);
		}
		// Tell a few things to the user
		{
			PetscScalar *ukmax_red, *ukmin_red;
			ukmax_red = (PetscScalar*) malloc(ninit_values * sizeof(PetscScalar));
			ukmin_red = (PetscScalar*) malloc(ninit_values * sizeof(PetscScalar));
			// Re-initialize the minima and maxima
			for (int i=0; i<ninit_values; i++) {
				ukmax[i] = 0;
				ukmin[i] = (PetscScalar) 1e+12;
			}
			// Get the values
			ierr = VecGetValues(grid->unknowns, nlocal, indices, exvalues); CHKERRQ(ierr);
			for (int i=0; i<nlocal; i++) {
				if (indices[i] < grid->nvtx) {
					if (exvalues[i] > ukmax[0]) {
						ukmax[0] = exvalues[i];
					}
					if (exvalues[i] < ukmin[0]) {
						ukmin[0] = exvalues[i];
					}
				} else {
					if (exvalues[i] > ukmax[1]) {
						ukmax[1] = exvalues[i];
					}
					if (exvalues[i] < ukmin[1]) {
						ukmin[1] = exvalues[i];
					}
				}
			}
			for (int i=0; i<ninit_values; i++) {
				ierr = MPI_Reduce(&(ukmax[i]), &(ukmax_red[i]), 1, MPI_DOUBLE, MPI_MAX, 0, PETSC_COMM_WORLD); CHKERRQ(ierr);
				ierr = MPI_Reduce(&(ukmin[i]), &(ukmin_red[i]), 1, MPI_DOUBLE, MPI_MIN, 0, PETSC_COMM_WORLD); CHKERRQ(ierr);
			}
			ierr = PetscPrintf(PETSC_COMM_WORLD,
						"Iter. %05d\t Time %.6e\t Min/Max uk1 (%.4e,%.4e)\t Min/Max uk2 (%.4e,%.4e)\t\n",
						step, step*las->ts_1->parent_solver->dt, ukmin_red[0], ukmax_red[0], ukmin_red[1], ukmax_red[1]); CHKERRQ(ierr);
			free(ukmax_red);
			free(ukmin_red);
		}
		// Export the data
		if (las->ts_1->parent_solver->export_frequency > 0) {
			if ((step % las->ts_1->parent_solver->export_frequency) == 0) {
				// Create the viewers and save the vectors
				PetscViewer Viewer = NULL;
				PetscChar filename[WORDSZ];
				if (grid->parent_grid->ndim == DIM2) {
					sprintf(filename, "sol_%06d.h5", step);
					ierr = PetscViewerHDF5Open(PETSC_COMM_WORLD, filename, FILE_MODE_WRITE, &Viewer); CHKERRQ(ierr);
				} else if (grid->parent_grid->ndim == DIM1) {
					sprintf(filename, "sol_%06d.dat", step);
					ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, filename, &Viewer); CHKERRQ(ierr);
					ierr = PetscViewerPushFormat(Viewer, PETSC_VIEWER_ASCII_MATLAB); CHKERRQ(ierr);
				} else {
					ierr = PetscPrintf(PETSC_COMM_WORLD, "Wrong dimension for solution output.\n"); CHKERRQ(ierr);
					ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
				}
				ierr = VecView(grid->x, Viewer); CHKERRQ(ierr);
				ierr = VecView(grid->unknowns, Viewer); CHKERRQ(ierr);
				ierr = PetscViewerDestroy(&Viewer); CHKERRQ(ierr);
				// Compute at this frequency too an L2 error if an exact solution exists
				if (grid->hasExact) {
					// Allocate memory for storing the errors
					nL2diff = (PetscScalar*) malloc(ninit_values * sizeof(PetscScalar));
					nL2sol = (PetscScalar*) malloc(ninit_values * sizeof(PetscScalar));
					errorL2 = (PetscScalar*) malloc(ninit_values * sizeof(PetscScalar));
					PetscScalar *nL2diff_red = (PetscScalar*) malloc(ninit_values * sizeof(PetscScalar));
					PetscScalar *nL2sol_red = (PetscScalar*) malloc(ninit_values * sizeof(PetscScalar));
					// Initialize them
					for (int i=0; i<ninit_values; i++) {
						nL2sol[i] = 0.0;
						nL2diff[i] = 0.0;
						errorL2[i] = 0.0;
					}
					// Create the vector for the difference between sol an exact
					ierr = VecDuplicate(grid->unknowns, &c); CHKERRQ(ierr);
					ierr = VecCopy(grid->unknowns, c); CHKERRQ(ierr);
					ierr = VecAXPY(c, -1.0, grid->exact); CHKERRQ(ierr);
					// Get values from sol
					ierr = VecGetValues(grid->unknowns, nlocal, indices, exvalues); CHKERRQ(ierr);
					for (int i=0; i<nlocal; i++) {
						if (indices[i] < grid->nvtx) {
							nL2sol[0] += exvalues[i] * exvalues[i];
						} else {
							nL2sol[1] += exvalues[i] * exvalues[i];
						}
					}
					for (int i=0; i<ninit_values; i++) {
						nL2sol[i] = PetscSqrtScalar(nL2sol[i]);
						ierr = MPI_Reduce(&(nL2sol[i]), &(nL2sol_red[i]), 1, MPI_DOUBLE, MPI_MAX, 0, PETSC_COMM_WORLD); CHKERRQ(ierr);
					}
					// Get values from diff
					ierr = VecGetValues(c, nlocal, indices, exvalues); CHKERRQ(ierr);
					for (int i=0; i<nlocal; i++) {
						if (indices[i] < grid->nvtx) {
							nL2diff[0] += exvalues[i] * exvalues[i];
						} else {
							nL2diff[1] += exvalues[i] * exvalues[i];
						}
					}
					for (int i=0; i<ninit_values; i++) {
						nL2diff[i] = PetscSqrtScalar(nL2diff[i]);
						ierr = MPI_Reduce(&(nL2diff[i]), &(nL2diff_red[i]), 1, MPI_DOUBLE, MPI_MAX, 0, PETSC_COMM_WORLD); CHKERRQ(ierr);
					}
					if (rank == 0) {
						for (int i=0; i<ninit_values; i++) {
							errorL2[i] = nL2diff_red[i] / nL2sol_red[i];
							ierr = PetscPrintf(PETSC_COMM_WORLD, "\t --> At this time, error L2 for uk%d is %.2e\t\n", i+1, errorL2[i]); CHKERRQ(ierr);
						}
					}
					// Destroy everything in this scope
					ierr = VecDestroy(&c); CHKERRQ(ierr);
					free(nL2diff); nL2diff = NULL;
					free(nL2sol); nL2sol = NULL;
					free(errorL2); errorL2 = NULL;
					free(nL2diff_red); nL2diff_red = NULL;
					free(nL2sol_red); nL2sol_red = NULL;
				} // end of if(grid->hasExact)
			} // end of if ((step % las->ts_1->parent_solver->export_frequency) == 0)
		} // end of if (las->ts_1->parent_solver->export_frequency > 0)
	} // end of loop on iterations

	// Free malloc'd stuff
	free(ukmax);
	free(ukmin);
	free(exvalues);
	free(indices);
	if (nL2diff != NULL) free(nL2diff);
	if (nL2sol != NULL) free(nL2sol);
	if (errorL2 != NULL) free(errorL2);

	ierr = MatDestroy(&R); CHKERRQ(ierr);
	ierr = MatDestroy(&identity); CHKERRQ(ierr);

	return ierr;
}

/* Method to destroy, i.e. deallocate, a one-dimensional linear-acoustics solver
*/
void destroy_linearAcousticsSolver1D(LinearAcousticsSolver1D *las)
{
	if ((las->ts_1->parent_solver->hasFilter) ||
		(las->ts_2->parent_solver->hasFilter)) {
		MatDestroy(&(las->filter));
	}
	destroy_transportSolver1D(las->ts_1);
	destroy_transportSolver1D(las->ts_2);
	MatDestroy(&(las->rhs));
	free(las);
}
