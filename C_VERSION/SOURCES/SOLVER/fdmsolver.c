/* This file contains the methods handling the one-dimensional solver
   @author : Thibault Bridel-Bertomeu
   @license: MIT
   @status : development
*/

#include "fdmsolver.h"

/* This method initializes the parent solver
*/
Solver* init_Solver(PetscInt nequation_args, PetscChar equation_arguments[TWENTY][WORDSZ], PetscInt nBocosPnts)
{
	int eq_index;
	Solver *solver = (Solver*) malloc(sizeof(Solver));

	// Initialize some booleans for easy access
	solver->hasFilter = false;

	// Store all the execute loop informations
	for (int i=0; i < nequation_args; i++) {
		if (strstr(equation_arguments[i], "timestep") != NULL) {
			eq_index = ( strchr(equation_arguments[i], '=') - equation_arguments[i] ) + 1;
			solver->dt = (PetscScalar) atof(equation_arguments[i] + (eq_index + 1));
		} else if (strstr(equation_arguments[i], "niterations") != NULL) {
			eq_index = ( strchr(equation_arguments[i], '=') - equation_arguments[i] ) + 1;
			solver->nit = (PetscInt) atoi(equation_arguments[i] + (eq_index + 1));
		} else if (strstr(equation_arguments[i], "exportevery") != NULL) {
			eq_index = ( strchr(equation_arguments[i], '=') - equation_arguments[i] ) + 1;
			solver->export_frequency = (PetscInt) atoi(equation_arguments[i] + (eq_index + 1));
		} else if (strstr(equation_arguments[i], "scheme") != NULL) {
			eq_index = ( strchr(equation_arguments[i], '=') - equation_arguments[i] ) + 1;
			strcpy(solver->scheme_name, equation_arguments[i] + (eq_index + 1));
		} else if (strstr(equation_arguments[i], "filter_name") != NULL) {
			eq_index = ( strchr(equation_arguments[i], '=') - equation_arguments[i] ) + 1;
			strcpy(solver->filter_name, equation_arguments[i] + (eq_index + 1));
			if (strcmp(solver->filter_name, "None") != 0) {
				solver->hasFilter = true;
			}
		} else if (strstr(equation_arguments[i], "filter_strength") != NULL) {
			eq_index = ( strchr(equation_arguments[i], '=') - equation_arguments[i] ) + 1;
			solver->filter_strength = (PetscScalar) atof(equation_arguments[i] + (eq_index + 1));
		} else {
			continue;
		}
	}

	// Generate the parameters of the Runge-Kutta scheme
	solver->rk_params = prep_rk(solver->scheme_name);
	solver->ierr = solver->rk_params->ierr;

	// Initialize the counters for Dirichlet and Neumann bocos and Robin bocos
	solver->ndir_rows = -1;
	solver->dir_rows = (PetscInt*) malloc(sizeof(PetscInt) * nBocosPnts);
	//
	solver->nneu_rows = -1;
	solver->neu_rows = (PetscInt*) malloc(sizeof(PetscInt) * nBocosPnts);
	//
	solver->nrob_rows = -1;
	solver->rob_rows = (PetscInt*) malloc(sizeof(PetscInt) * nBocosPnts);

	// Return the solver
	return solver;
}
