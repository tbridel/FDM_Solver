/* This file contains the methods handling the one-dimensional solver for
   an advection-diffusion equation
   @author : Thibault Bridel-Bertomeu
   @license: MIT
   @status : development
*/

#include "fdmsolver.h"

/* This method initializes a solver for the advection-diffusion equation in 1D
*/
AdvectionDiffusionSolver1D* init_advectionDiffusionSolver1D(PetscInt nequation_args, PetscChar equation_arguments[TWENTY*2][WORDSZ], Grid1D *grid)
{
	int eq_index;
	PetscChar *equationTemporal, *equationConvection, *equationDiffusion;
	PetscScalar cvel = 0.0, cond = 0.0;
	AdvectionDiffusionSolver1D *ads = (AdvectionDiffusionSolver1D*) malloc(sizeof(AdvectionDiffusionSolver1D));

	// Get the names of the temporal, convection and diffusion schemes
	// Also acquire the convection velocity and conductivity string
	for (int i=0; i < nequation_args; i++) {
		if (strstr(equation_arguments[i], "equation_temporal") != NULL) {
			eq_index = ( strchr(equation_arguments[i], '=') - equation_arguments[i] ) + 1;
			equationTemporal = equation_arguments[i] + (eq_index + 1);
		} else if (strstr(equation_arguments[i], "equation_convection") != NULL) {
			eq_index = ( strchr(equation_arguments[i], '=') - equation_arguments[i] ) + 1;
			equationConvection = equation_arguments[i] + (eq_index + 1);
		} else if (strstr(equation_arguments[i], "equation_diffusion") != NULL) {
			eq_index = ( strchr(equation_arguments[i], '=') - equation_arguments[i] ) + 1;
			equationDiffusion = equation_arguments[i] + (eq_index + 1);
		} else if (strstr(equation_arguments[i], "cvel") != NULL) {
			eq_index = ( strchr(equation_arguments[i], '=') - equation_arguments[i] ) + 1;
			cvel = (PetscScalar) atof(equation_arguments[i] + (eq_index + 1));
		} else if (strstr(equation_arguments[i], "cond") != NULL) {
			eq_index = ( strchr(equation_arguments[i], '=') - equation_arguments[i] ) + 1;
			cond = (PetscScalar) atof(equation_arguments[i] + (eq_index + 1));
		}
	}

	// Initialize the transport solver
	// Create the name of the full scheme
	sprintf(equation_arguments[nequation_args], "equation_scheme = %s_%s\0", equationTemporal, equationConvection);
	nequation_args++;
	// Add the convection velocity
	sprintf(equation_arguments[nequation_args], "equation_parameters = [%f,]", cvel);
	nequation_args++;
	// Then create transport solver
	ads->ierr = PetscPrintf(PETSC_COMM_WORLD, "\tInitializing child transport solver ...\n");
	ads->transport_solver = init_transportSolver1D(nequation_args, equation_arguments, grid, NULL, 0);

	// Initialize the diffusion solver
	// Create the name of the full scheme
	nequation_args-=2;
	sprintf(equation_arguments[nequation_args], "equation_scheme = %s_%s\0", equationTemporal, equationDiffusion);
	nequation_args++;
	// Add the conduction
	sprintf(equation_arguments[nequation_args], "equation_parameters = [%f,]", cond);
	nequation_args++;
	// Then create diffusion solver
	ads->ierr = PetscPrintf(PETSC_COMM_WORLD, "\n\tInitializing child diffusion solver ...\n");
	ads->diffusion_solver = init_diffusionSolver1D(nequation_args, equation_arguments, grid);

	// Return the solver
	return ads;
}

/* This method is used to apply all kind of boundary condition onto
   the matrix(ces) of a 1D advection-diffusion solver
*/
PetscErrorCode applyBocos_ads1D(PetscInt nboco_arguments, PetscChar boco_arguments[2*TWENTY][WORDSZ], Grid1D *grid, AdvectionDiffusionSolver1D *ads)
{
	int eq_index;
	PetscErrorCode ierr = 0;
	PetscChar bocotype[WORDSZ] = "";

	// Browse the boundary conditions
	// If user inputs "insulation", got to turn in into the right Robin boundary condition before calling the children solvers
	for (int i=0; i < nboco_arguments; i++) {
		if (strstr(boco_arguments[i], "xmin") != NULL) {
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else if (strstr(boco_arguments[i], "xmax") != NULL) {
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown boundary assignement.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
		if (strcmp(bocotype, "insulation") == 0) {
			PetscChar dest[WORDSZ] = "";
			strncpy(dest, boco_arguments[i], eq_index+1);
			sprintf(dest, "%srobin,%lf,%lf", dest, ads->transport_solver->tv[0], -ads->diffusion_solver->cond[0]);
			strncpy(boco_arguments[i], dest, strlen(dest));
		}
	}

	// Apply the boundary conditions to the transport solver
	ierr = applyBocos_ts1D(nboco_arguments, boco_arguments, grid, ads->transport_solver); CHKERRQ(ierr);

	// Apply the boundary conditions to the diffusion solver
	ierr = applyBocos_ds1D(nboco_arguments, boco_arguments, grid, ads->diffusion_solver); CHKERRQ(ierr);

	return ierr;
}

/* Method for the execution of a one dimensional advection-diffusion solver
*/
PetscErrorCode execute_ads1D(AdvectionDiffusionSolver1D *ads, Grid1D* grid)
{
	Mat identity, R, mlist1, mlist2;
	PetscErrorCode ierr = 0;
	PetscScalar factor = 0.0, ukmax = 0.0, ukmin = 0.0;
	PetscScalar nL2diff = 0.0, nL2sol = 0.0, errorL2 = 0.0;
	PetscScalar neuscal = 0.0;
	PetscInt step, idx, rstart, rend, nlocal, vecsize;
	Vec b, c;
	PetscMPIInt size;

	// Get info on the MPI layout
	ierr = MPI_Comm_size(PETSC_COMM_WORLD, &size); CHKERRQ(ierr);

	// Get parallel layout of the grid->unknowns vector
	ierr = VecGetSize(grid->unknowns, &vecsize); CHKERRQ(ierr);
	if (vecsize == grid->nvtx) {
		ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend); CHKERRQ(ierr);
		ierr = VecGetLocalSize(grid->unknowns, &nlocal); CHKERRQ(ierr);
	} else {
		ierr = MatGetOwnershipRange(ads->transport_solver->rhs, &rstart, &rend); CHKERRQ(ierr);
		ierr = MatGetLocalSize(ads->transport_solver->rhs, &nlocal, &nlocal); CHKERRQ(ierr);
	}

	// Create an identity matrix, it can come handy
	ierr = MatCreate(PETSC_COMM_WORLD, &identity); CHKERRQ(ierr);
	ierr = MatSetSizes(identity, nlocal, nlocal, grid->nvtx, grid->nvtx); CHKERRQ(ierr);
	ierr = MatSetType(identity, MATAIJ); CHKERRQ(ierr);
	ierr = MatSetUp(identity); CHKERRQ(ierr);
	for (idx = rstart; idx < rend ; idx++) {
		ierr = MatSetValue(identity, idx, idx, 1.0, INSERT_VALUES); CHKERRQ(ierr);
	}
	ierr = MatAssemblyBegin(identity, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
	ierr = MatAssemblyEnd(identity, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

	// Prepare a few other objects before the big loop
	// First correctly scale the two solvers and pass them actually to the right hand side
	ierr = MatScale(ads->transport_solver->rhs, -ads->transport_solver->tv[0]/grid->parent_grid->dx); CHKERRQ(ierr);
	ierr = MatScale(ads->diffusion_solver->rhs, ads->diffusion_solver->cond[0]/PetscPowScalar(grid->parent_grid->dx, 2)); CHKERRQ(ierr);
	// Then add both matrices
	ierr = MatDuplicate(ads->transport_solver->rhs, MAT_COPY_VALUES, &(ads->rhs)); CHKERRQ(ierr);
	ierr = MatAssemblyBegin(ads->rhs, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
	ierr = MatAssemblyEnd(ads->rhs, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
	ierr = MatAXPY(ads->rhs, 1.0, ads->diffusion_solver->rhs, DIFFERENT_NONZERO_PATTERN); CHKERRQ(ierr);
	// Then handle the preprocessing for the RK matrix
	// (assume that both t and d solvers have received the same RK scheme information)
	if (ads->transport_solver->parent_solver->rk_params->has_gammas) {
		PetscPrintf(PETSC_COMM_WORLD, "\tPreparing Runge-Kutta matrix ...\n");
		// Create the matrix and copy the values from the RHS and assemble
		ierr = MatDuplicate(ads->rhs, MAT_COPY_VALUES, &R); CHKERRQ(ierr);
		ierr = MatScale(R, ads->transport_solver->parent_solver->dt); CHKERRQ(ierr);
		ierr = MatAssemblyBegin(R, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
		ierr = MatAssemblyEnd(R, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
		// First element of mlist is the RHS matrix
		ierr = MatDuplicate(ads->rhs, MAT_COPY_VALUES, &mlist1); CHKERRQ(ierr);
		// Now loop over the remaining RK stages to simulate matrix power
		for (idx = 1; idx < ads->transport_solver->parent_solver->rk_params->steps; idx++) {
			factor = PetscPowScalar(ads->transport_solver->parent_solver->dt, idx+1)*ads->transport_solver->parent_solver->rk_params->gammas[idx];
			ierr = MatMatMult(ads->rhs, mlist1, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &mlist2); CHKERRQ(ierr);
			ierr = MatDestroy(&mlist1); CHKERRQ(ierr);
			ierr = MatAXPY(R, factor, mlist2, DIFFERENT_NONZERO_PATTERN); CHKERRQ(ierr);
			ierr = MatDuplicate(mlist2, MAT_COPY_VALUES, &mlist1); CHKERRQ(ierr);
			ierr = MatDestroy(&mlist2); CHKERRQ(ierr);
		}
		// Destroy the mlist object
		MatDestroy(&mlist1);
		MatDestroy(&mlist2);
		// Add identity to R
		ierr = MatAXPY(R, 1.0, identity, DIFFERENT_NONZERO_PATTERN); CHKERRQ(ierr);
		// Change R structure in case of dirichlet boco
		{
			PetscInt *rows = (PetscInt*) malloc(sizeof(PetscInt) * (ads->transport_solver->parent_solver->ndir_rows+1));
			for (int i=0; i<(ads->transport_solver->parent_solver->ndir_rows+1); i++) {
				rows[i] = ads->transport_solver->parent_solver->dir_rows[i];
			}
			ierr = MatZeroRowsColumns(R, ads->transport_solver->parent_solver->ndir_rows+1, rows, 0.0, 0, 0); CHKERRQ(ierr);
			if (ads->transport_solver->parent_solver->hasFilter) {
				ierr = MatZeroRowsColumns(ads->transport_solver->filter, ads->transport_solver->parent_solver->ndir_rows+1, rows, 0.0, 0, 0); CHKERRQ(ierr);
			}
			free(rows);
		}
		// Change filter structure in case of Neumann boco
		if (ads->transport_solver->parent_solver->hasFilter) {
			for (int i=0; i<DIM2; i++) {
				if ((ads->transport_solver->neumanns[i] != NULL) &&
					(ads->diffusion_solver->neumanns[i] != NULL)) {
					ierr = MatZeroRows(ads->transport_solver->filter, 1, ads->transport_solver->parent_solver->neu_rows + i, 0.0, 0, 0); CHKERRQ(ierr);
				}
			}
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown definition of the Runge-Kutta scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}
	// Scale the filter by the strength input by the user
	if (ads->transport_solver->parent_solver->hasFilter) {
		ierr = MatScale(ads->transport_solver->filter, ads->transport_solver->parent_solver->filter_strength); CHKERRQ(ierr);
	}

	// Export the initial solution
	{
		step = 0;
		// Create the viewers and save the vectors
		PetscViewer Viewer = NULL;
		PetscChar filename[WORDSZ];
		if (grid->parent_grid->ndim == DIM2) {
			sprintf(filename, "sol_%06d.h5", step);
			ierr = PetscViewerHDF5Open(PETSC_COMM_WORLD, filename, FILE_MODE_WRITE, &Viewer); CHKERRQ(ierr);
		} else if (grid->parent_grid->ndim == DIM1) {
			sprintf(filename, "sol_%06d.dat", step);
			ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, filename, &Viewer); CHKERRQ(ierr);
			ierr = PetscViewerPushFormat(Viewer, PETSC_VIEWER_ASCII_MATLAB); CHKERRQ(ierr);
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Wrong dimension for solution output.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
		ierr = VecView(grid->x, Viewer); CHKERRQ(ierr);
		ierr = VecView(grid->unknowns, Viewer); CHKERRQ(ierr);
		ierr = PetscViewerDestroy(&Viewer); CHKERRQ(ierr);
	}

	// Loop on iterations
	ierr = PetscPrintf(PETSC_COMM_WORLD, "\tMarching the equation in time ...\n"); CHKERRQ(ierr);
	for (step = 1; step <= ads->transport_solver->parent_solver->nit; step ++) {
		// Implementation differs function of the way the RK scheme is made
		if (ads->transport_solver->parent_solver->rk_params->has_gammas) {
			// Get an auxiliary vector
			ierr = VecDuplicate(grid->unknowns, &b); CHKERRQ(ierr);
			// Apply the temporal scheme
			ierr = MatMult(R, grid->unknowns, b); CHKERRQ(ierr);
			// Save the result
			ierr = VecCopy(b, grid->unknowns); CHKERRQ(ierr);
			// Destroy b
			ierr = VecDestroy(&b); CHKERRQ(ierr);
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown definition of the Runge-Kutta scheme.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
		// Apply Neumann boundary conditions
		for (int i=0; i<DIM2; i++) {
			if ((ads->transport_solver->neumanns[i] != NULL) &&
				(ads->diffusion_solver->neumanns[i] != NULL)) {
				// Better use the operator from the first order derivative present in the equation
				ierr = VecDot(grid->unknowns, ads->transport_solver->neumanns[i], &neuscal); CHKERRQ(ierr);
				ierr = VecSetValue(grid->unknowns, ads->transport_solver->parent_solver->neu_rows[i], neuscal, INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(grid->unknowns); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(grid->unknowns); CHKERRQ(ierr);
			}
		}
		// Filter the signal
		if (ads->transport_solver->parent_solver->hasFilter) {
			ierr = VecDuplicate(grid->unknowns, &b); CHKERRQ(ierr);
			ierr = MatMult(ads->transport_solver->filter, grid->unknowns, b); CHKERRQ(ierr);
			ierr = VecAXPY(grid->unknowns, -1.0, b); CHKERRQ(ierr);
			ierr = VecDestroy(&b); CHKERRQ(ierr);
		}
		// Tell a few things to the user
		ierr = VecMax(grid->unknowns, NULL, &ukmax); CHKERRQ(ierr);
		ierr = VecMin(grid->unknowns, NULL, &ukmin); CHKERRQ(ierr);
		ierr = PetscPrintf(PETSC_COMM_WORLD,
					"Iter. %05d\t Time %.6e\t Min/Max uk (%.4e,%.4e)\t\n",
					step, step*ads->transport_solver->parent_solver->dt, ukmin, ukmax); CHKERRQ(ierr);
		// Export the data
		if (ads->transport_solver->parent_solver->export_frequency > 0) {
			if ((step % ads->transport_solver->parent_solver->export_frequency) == 0) {
				// Create the viewers and save the vectors
				PetscViewer Viewer = NULL;
				PetscChar filename[WORDSZ];
				if (grid->parent_grid->ndim == DIM2) {
					sprintf(filename, "sol_%06d.h5", step);
					ierr = PetscViewerHDF5Open(PETSC_COMM_WORLD, filename, FILE_MODE_WRITE, &Viewer); CHKERRQ(ierr);
				} else if (grid->parent_grid->ndim == DIM1) {
					sprintf(filename, "sol_%06d.dat", step);
					ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, filename, &Viewer); CHKERRQ(ierr);
					ierr = PetscViewerPushFormat(Viewer, PETSC_VIEWER_ASCII_MATLAB); CHKERRQ(ierr);
				} else {
					ierr = PetscPrintf(PETSC_COMM_WORLD, "Wrong dimension for solution output.\n"); CHKERRQ(ierr);
					ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
				}
				ierr = VecView(grid->x, Viewer); CHKERRQ(ierr);
				ierr = VecView(grid->unknowns, Viewer); CHKERRQ(ierr);
				ierr = PetscViewerDestroy(&Viewer); CHKERRQ(ierr);
				// Compute at this frequency too an L2 error if an exact solution exists
				if (grid->hasExact) {
					ierr = VecDuplicate(grid->unknowns, &c); CHKERRQ(ierr);
					ierr = VecCopy(grid->unknowns, c); CHKERRQ(ierr);
					ierr = VecAXPY(c, -1.0, grid->exact); CHKERRQ(ierr);
					ierr = VecNorm(c, NORM_2, &nL2diff); CHKERRQ(ierr);
					ierr = VecNorm(grid->unknowns, NORM_2, &nL2sol); CHKERRQ(ierr);
					errorL2 = nL2diff / nL2sol;
					ierr = PetscPrintf(PETSC_COMM_WORLD, "\t --> At this time, error L2 is %.2e\t\n", errorL2); CHKERRQ(ierr);
					ierr = VecDestroy(&c); CHKERRQ(ierr);
				}
			}
		}
	} // end of loop on iterations

	ierr = MatDestroy(&R); CHKERRQ(ierr);
	ierr = MatDestroy(&identity); CHKERRQ(ierr);

	return ierr;
}

/* Method to destroy, i.e. deallocate, a one-dimensional advection-diffusion solver
*/
void destroy_advectionDiffusionSolver1D(AdvectionDiffusionSolver1D *ads)
{
	destroy_transportSolver1D(ads->transport_solver);
	destroy_diffusionSolver1D(ads->diffusion_solver);
	MatDestroy(&(ads->rhs));
	free(ads);
}
