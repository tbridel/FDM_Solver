/* Header file for the fdmsolver.c methods file
   @author : Thibault Bridel-Bertomeu
   @license: MIT
   @status : development
*/

#if !defined(__FDMSOLVER_H)
#define __FDMSOLVER_H

#include "../GRID/fdmgrid.h"
#include "../UTILITARIES/utils.h"
#include <petscsys.h>
#include <petscmat.h>
#include <petscmath.h>
#include <petscviewerhdf5.h>

typedef struct Solver Solver;
typedef struct TransportSolver1D TransportSolver1D;
typedef struct TransportSolver2D TransportSolver2D;
typedef struct DiffusionSolver1D DiffusionSolver1D;
typedef struct DiffusionSolver2D DiffusionSolver2D;
typedef struct AdvectionDiffusionSolver1D AdvectionDiffusionSolver1D;
typedef struct AdvectionDiffusionSolver2D AdvectionDiffusionSolver2D;
typedef struct LinearAcousticsSolver1D LinearAcousticsSolver1D;
typedef struct LinearAcousticsSolver2D LinearAcousticsSolver2D;

/********************************************/
/*****************BASE SOLVER****************/
/********************************************/
struct Solver {
	PetscErrorCode ierr;
	PetscScalar dt;						// timestep
	PetscInt nit;						// number of iterations
	PetscInt export_frequency;			// frequency at which to export solutions
	PetscInt ndir_rows;					// number of dirichlet bocos
	PetscInt *dir_rows;					// rows matching dirichlet bocos
	PetscInt nneu_rows;					// number of neumann bocos
	PetscInt *neu_rows;					// rows matching neumann bocos
	PetscInt nrob_rows;					// number of robin bocos
	PetscInt *rob_rows;					// rows matching robin bocos
	PetscChar scheme_name[WORDSZ];		// name of the temporal+spatial scheme
	PetscBool hasFilter;				// whether it is to be filtered
	PetscChar filter_name[WORDSZ];		// name of the filtering scheme
	PetscScalar filter_strength;		// strength of the filter
	RKParams *rk_params;				// structure holding the parameters of the RungeKutta scheme
};

Solver* init_Solver(PetscInt nequation_args, PetscChar equation_arguments[TWENTY][WORDSZ], PetscInt nBocosPnts);

/********************************************/
/***************TRANSPORT SOLVER*************/
/********************************************/
// Transport Solver 1D
struct TransportSolver1D {
	LinearAcousticsSolver1D *super_las; // pointer to eventual superior linear acoustics solver
	PetscInt mylasnumber; 				// in case of a superior las, number of the present ts1D
	Solver *parent_solver;				// pointer to the parent solver 1D
	PetscErrorCode ierr;
	PetscScalar tv[DIM1];				// vector holding the transport velocities
	PetscScalar cfl[DIM1];				// vector holding the convective CFL
	Mat rhs;							// RHS matrix of the problem built with the spatial scheme
	Mat filter;							// Filter matrix if results are to be filtered
	Vec *neumanns;						// Coefficients vector to solve the neumann problems
	Vec *robins;						// Coefficients vector to solve the robin problems
};

TransportSolver1D* init_transportSolver1D(PetscInt nequation_args, PetscChar equation_arguments[TWENTY*2][WORDSZ], Grid1D *grid, LinearAcousticsSolver1D *super_las, PetscInt mylasnumber);
void get_ts1D_rhs(TransportSolver1D* ts, Grid1D* grid);
PetscErrorCode applyBocos_ts1D(PetscInt nboco_arguments, PetscChar boco_arguments[2*TWENTY][WORDSZ], Grid1D *grid, TransportSolver1D *ts);
PetscErrorCode set_periodic_ts1D(TransportSolver1D* ts, Grid1D* grid);
PetscErrorCode set_open_ts1D(TransportSolver1D* ts, Grid1D* grid, PetscBool ismin);
PetscErrorCode set_dirichlet_ts1D(TransportSolver1D* ts, Grid1D* grid, PetscBool ismin);
PetscErrorCode set_neumann_ts1D(TransportSolver1D* ts, Grid1D* grid, PetscBool ismin);
PetscErrorCode set_robin_ts1D(TransportSolver1D* ts, Grid1D* grid, PetscBool ismin, PetscScalar robA, PetscScalar robB);
PetscErrorCode execute_ts1D(TransportSolver1D* ts, Grid1D* grid);
void destroy_transportSolver1D(TransportSolver1D *ts);

// Transport Solver 1D Filters
void get_ts1D_filter(TransportSolver1D* ts, Grid1D* grid);
PetscErrorCode set_periodic_filter_ts1D(TransportSolver1D* ts, Grid1D* grid);
PetscErrorCode set_open_filter_ts1D(TransportSolver1D* ts, Grid1D* grid, PetscBool ismin);

// Transport Solver 2D
struct TransportSolver2D {
	LinearAcousticsSolver2D *super_las; // pointer to eventual superior linear acoustics solver
	PetscInt mylasnumber;				// in case of a superiori las, number of the present ts2D
	Solver *parent_solver;				// pointer to the parent solver 2D
	PetscErrorCode ierr;
	PetscScalar tv[DIM2];				// vector holding the transport velocities
	PetscScalar tv_norm;
	PetscScalar cfl[DIM2];				// vector holding the convective CFL
	Mat rhs;							// RHS matrix of the problem built with the spatial scheme
	Mat filter;							// Filter matrix if results are to be filtered
	Vec *neumanns;						// Coefficients vector to solve the neumann problems
	Vec *robins;						// Coefficients vector to solve the robin problems
};

TransportSolver2D* init_transportSolver2D(PetscInt nequation_args, PetscChar equation_arguments[TWENTY*2][WORDSZ], Grid2D *grid, LinearAcousticsSolver2D *super_las, PetscInt mylasnumber);
void get_ts2D_rhs(TransportSolver2D* ts, Grid2D* grid);
PetscErrorCode applyBocos_ts2D(PetscInt nboco_arguments, PetscChar boco_arguments[2*TWENTY][WORDSZ], Grid2D *grid, TransportSolver2D *ts);
PetscErrorCode set_periodic_ts2D(TransportSolver2D* ts, Grid2D* grid, PetscBool isx);
PetscErrorCode set_open_ts2D(TransportSolver2D* ts, Grid2D* grid, PetscBool ismin, PetscBool isx);
PetscErrorCode set_dirichlet_ts2D(TransportSolver2D* ts, Grid2D* grid, PetscBool ismin, PetscBool isx);
PetscErrorCode set_neumann_ts2D(TransportSolver2D* ts, Grid2D* grid, PetscBool ismin, PetscBool isx);
PetscErrorCode set_robin_ts2D(TransportSolver2D* ts, Grid2D* grid, PetscBool ismin, PetscBool isx, PetscScalar robA, PetscScalar robB);
PetscErrorCode execute_ts2D(TransportSolver2D* ts, Grid2D* grid);
void destroy_transportSolver2D(TransportSolver2D *ts, Grid2D* grid);

// Transport Solver 2D Filters
void get_ts2D_filter(TransportSolver2D* ts, Grid2D* grid);
PetscErrorCode set_periodic_filter_ts2D(TransportSolver2D* ts, Grid2D* grid, PetscBool isx, PetscBool bothdirections);
PetscErrorCode set_open_filter_ts2D(TransportSolver2D* ts, Grid2D* grid, PetscBool ismin, PetscBool isx);

/********************************************/
/***************DIFFUSION SOLVER*************/
/********************************************/
// Diffusion Solver 1D
struct DiffusionSolver1D {
	Solver *parent_solver;
	PetscErrorCode ierr;
	PetscScalar cond[DIM1];				// vector holding the conductivities
	PetscScalar cfl[DIM1];				// vector holding the diffusive CFL (Peclet number)
	Mat rhs;
	Mat filter;
	Vec *neumanns;
	Vec *robins;
};

DiffusionSolver1D* init_diffusionSolver1D(PetscInt nequation_args, PetscChar equation_arguments[TWENTY*2][WORDSZ], Grid1D *grid);
void get_ds1D_rhs(DiffusionSolver1D* ds, Grid1D* grid);
PetscErrorCode applyBocos_ds1D(PetscInt nboco_arguments, PetscChar boco_arguments[2*TWENTY][WORDSZ], Grid1D *grid, DiffusionSolver1D *ds);
PetscErrorCode set_periodic_ds1D(DiffusionSolver1D* ds, Grid1D* grid);
PetscErrorCode set_open_ds1D(DiffusionSolver1D* ds, Grid1D* grid, PetscBool ismin);
PetscErrorCode set_dirichlet_ds1D(DiffusionSolver1D* ds, Grid1D* grid, PetscBool ismin);
PetscErrorCode set_neumann_ds1D(DiffusionSolver1D* ds, Grid1D* grid, PetscBool ismin);
PetscErrorCode set_robin_ds1D(DiffusionSolver1D* ds, Grid1D* grid, PetscBool ismin, PetscScalar robA, PetscScalar robB);
PetscErrorCode execute_ds1D(DiffusionSolver1D* ds, Grid1D* grid);
void destroy_diffusionSolver1D(DiffusionSolver1D *ds);

// Diffusion Solver 1D Filters
void get_ds1D_filter(DiffusionSolver1D* ds, Grid1D* grid); /** DUD **/
PetscErrorCode set_periodic_filter_ds1D(DiffusionSolver1D* ds, Grid1D* grid); /** DUD **/
PetscErrorCode set_open_filter_ds1D(DiffusionSolver1D* ds, Grid1D* grid, PetscBool ismin); /** DUD **/

// Diffusion Solver 2D
struct DiffusionSolver2D {
	Solver *parent_solver;
	PetscErrorCode ierr;
	PetscScalar cond[DIM2];				// vector holding the conductivities
	PetscScalar cond_norm;
	PetscScalar cfl[DIM2];				// vector holding the diffusive CFL (Peclet number)
	Mat rhs;
	Mat filter;
	Vec *neumanns;
	Vec *robins;
};

DiffusionSolver2D* init_diffusionSolver2D(PetscInt nequation_args, PetscChar equation_arguments[TWENTY*2][WORDSZ], Grid2D *grid);
void get_ds2D_rhs(DiffusionSolver2D* ds, Grid2D* grid);
PetscErrorCode applyBocos_ds2D(PetscInt nboco_arguments, PetscChar boco_arguments[2*TWENTY][WORDSZ], Grid2D *grid, DiffusionSolver2D *ds);
PetscErrorCode set_periodic_ds2D(DiffusionSolver2D* ds, Grid2D* grid, PetscBool isx);
PetscErrorCode set_open_ds2D(DiffusionSolver2D* ds, Grid2D* grid, PetscBool ismin, PetscBool isx);
PetscErrorCode set_dirichlet_ds2D(DiffusionSolver2D* ds, Grid2D* grid, PetscBool ismin, PetscBool isx);
PetscErrorCode set_neumann_ds2D(DiffusionSolver2D* ds, Grid2D* grid, PetscBool ismin, PetscBool isx);
PetscErrorCode set_robin_ds2D(DiffusionSolver2D* ds, Grid2D* grid, PetscBool ismin, PetscBool isx, PetscScalar robA, PetscScalar robB);
PetscErrorCode execute_ds2D(DiffusionSolver2D* ds, Grid2D* grid);
void destroy_diffusionSolver2D(DiffusionSolver2D *ds, Grid2D* grid);

// Diffusion Solver 2D Filters
void get_ds2D_filter(DiffusionSolver2D* ds, Grid2D* grid); /** DUD **/
PetscErrorCode set_periodic_filter_ds2D(DiffusionSolver2D* ds, Grid2D* grid); /** DUD **/
PetscErrorCode set_open_filter_ds2D(DiffusionSolver2D* ds, Grid2D* grid, PetscBool ismin); /** DUD **/

/********************************************/
/********ADVECTION DIFFUSION SOLVER**********/
/********************************************/
// Advection Diffusion Solver 1D
struct AdvectionDiffusionSolver1D {
	PetscErrorCode ierr;
	TransportSolver1D *transport_solver;// pointer to the transport solver 1D
	DiffusionSolver1D *diffusion_solver;// pointer to the diffusion solver 1D
	Mat rhs;
};

AdvectionDiffusionSolver1D* init_advectionDiffusionSolver1D(PetscInt nequation_args, PetscChar equation_arguments[TWENTY*2][WORDSZ], Grid1D *grid);
PetscErrorCode applyBocos_ads1D(PetscInt nboco_arguments, PetscChar boco_arguments[2*TWENTY][WORDSZ], Grid1D *grid, AdvectionDiffusionSolver1D *ads);
PetscErrorCode execute_ads1D(AdvectionDiffusionSolver1D *ads, Grid1D* grid);
void destroy_advectionDiffusionSolver1D(AdvectionDiffusionSolver1D *ads);

// Advection Diffusion Solver 2D
struct AdvectionDiffusionSolver2D {
	PetscErrorCode ierr;
	TransportSolver2D *transport_solver;// pointer to the transport solver 2D
	DiffusionSolver2D *diffusion_solver;// pointer to the diffusion solver 2D
	Mat rhs;
};

AdvectionDiffusionSolver2D* init_advectionDiffusionSolver2D(PetscInt nequation_args, PetscChar equation_arguments[TWENTY*2][WORDSZ], Grid2D *grid);
PetscErrorCode applyBocos_ads2D(PetscInt nboco_arguments, PetscChar boco_arguments[2*TWENTY][WORDSZ], Grid2D *grid, AdvectionDiffusionSolver2D *ads);
PetscErrorCode execute_ads2D(AdvectionDiffusionSolver2D *ads, Grid2D* grid);
void destroy_advectionDiffusionSolver2D(AdvectionDiffusionSolver2D *ads, Grid2D* grid);

/********************************************/
/***********LINEAR ACOUSTICS SOLVER**********/
/********************************************/
// Linear Acoustics Solver 1D
struct LinearAcousticsSolver1D {
	PetscErrorCode ierr;
	TransportSolver1D *ts_1;			// pointer to the first transport solver 1D
	TransportSolver1D *ts_2;			// pointer to the second transport solver 1D
	Mat rhs;
	Mat filter;
};

LinearAcousticsSolver1D* init_linearAcousticsSolver1D(PetscInt nequation_args, PetscChar equation_arguments[TWENTY*2][WORDSZ], Grid1D *grid);
void get_las1D_rhs(LinearAcousticsSolver1D* ts, Grid1D* grid);
PetscErrorCode applyBocos_las1D(PetscInt nboco_arguments, PetscChar boco_arguments[2*TWENTY][WORDSZ], Grid1D *grid, LinearAcousticsSolver1D *las);
PetscErrorCode execute_las1D(LinearAcousticsSolver1D *las, Grid1D* grid);
void destroy_linearAcousticsSolver1D(LinearAcousticsSolver1D *las);

// Linear Acoustics Solver 1D Filters
void get_las1D_filter(LinearAcousticsSolver1D* ts, Grid1D* grid);

// Linear Acoustics Solver 2D
struct LinearAcousticsSolver2D {
	PetscErrorCode ierr;
	TransportSolver2D *ts_1;			// pointer to the first transport solver 2D
	TransportSolver2D *ts_2;			// pointer to the second transport solver 2D
	Mat rhs;
	Mat filter;
};

LinearAcousticsSolver2D* init_linearAcousticsSolver2D(PetscInt nequation_args, PetscChar equation_arguments[TWENTY*2][WORDSZ], Grid2D *grid);
void get_las2D_rhs(LinearAcousticsSolver2D* ts, Grid2D* grid);
PetscErrorCode applyBocos_las2D(PetscInt nboco_arguments, PetscChar boco_arguments[2*TWENTY][WORDSZ], Grid2D *grid, LinearAcousticsSolver2D *las);
PetscErrorCode execute_las2D(LinearAcousticsSolver2D *las, Grid2D* grid);
void destroy_linearAcousticsSolver2D(LinearAcousticsSolver2D *las, Grid2D* grid);

// Linear Acoustics Solver 2D Filters
void get_las2D_filter(LinearAcousticsSolver2D* ts, Grid2D* grid);

#endif
