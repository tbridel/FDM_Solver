/* This file contains the methods handling the two-dimensional solver for a transport equation
   @author : Thibault Bridel-Bertomeu
   @license: MIT
   @status : development
*/

#include "fdmsolver.h"
#ifdef LARGEST_STENCIL
#undef LARGEST_STENCIL
#endif
#define LARGEST_STENCIL 21

/* This method initializes a solver for the transport equation in 2D
*/
TransportSolver2D* init_transportSolver2D(PetscInt nequation_args, PetscChar equation_arguments[TWENTY*2][WORDSZ], Grid2D *grid, LinearAcousticsSolver2D *super_las, PetscInt mylasnumber)
{
	int eq_index;
	PetscChar tv_string[WORDSZ] = "";
	PetscInt nBocosPnts = 0;
	TransportSolver2D *ts = (TransportSolver2D*) malloc(sizeof(TransportSolver2D));

	// Initialize the parent
	PetscPrintf(PETSC_COMM_WORLD, "\tInitializing parent solver ...\n");
	nBocosPnts = 2 * grid->parent_grid->nx + 2 * grid->ny;
	ts->parent_solver = init_Solver(nequation_args, equation_arguments, nBocosPnts);
	ts->ierr = ts->parent_solver->ierr;

	// Initialize the vectors for the Neumann boundary conditions
	ts->neumanns = (Vec*) malloc(nBocosPnts * sizeof(Vec));
	for (int i = 0; i < nBocosPnts; i++) {
		ts->neumanns[i] = NULL;
	}

	// Initialize the vectors for the Robin boundary conditions
	ts->robins = (Vec*) malloc(nBocosPnts * sizeof(Vec));
	for (int i = 0; i < nBocosPnts; i++) {
		ts->robins[i] = NULL;
	}

	// Get the transport velocity from the user inputs
	PetscPrintf(PETSC_COMM_WORLD, "\tAcquiring transport velocity ...\n");
	for (int i=0; i < nequation_args; i++) {
		if (strstr(equation_arguments[i], "parameters") != NULL) {
			eq_index = ( strchr(equation_arguments[i], '=') - equation_arguments[i] ) + 1;
			strcpy(tv_string, equation_arguments[i] + (eq_index + 1));
		}
	}
	if (sscanf(tv_string, "[%lf,%lf]", &(ts->tv[0]), &(ts->tv[1])) != 2) {
		PetscPrintf(PETSC_COMM_WORLD, "For a 2D transport equation, a 2-elements transport velocity vector such as [X,Y] must be input.\n");
		ts->ierr = PETSC_ERR_ARG_WRONG;
	}

	// Store the norm of the transport velocity
	ts->tv_norm = PetscSqrtScalar( PetscPowScalar(ts->tv[0], 2) + PetscPowScalar(ts->tv[1], 2) );

	// Store the CFL number
	ts->cfl[0] = ts->tv[0] * ts->parent_solver->dt / grid->parent_grid->dx;
	ts->cfl[1] = ts->tv[1] * ts->parent_solver->dt / grid->dy;

	if (super_las == NULL) {
		// Initialize a few things to be duds
		ts->super_las = NULL;
		ts->mylasnumber = 1;

		// Call the routine to build the RHS matrix based on numerical scheme
		get_ts2D_rhs(ts, grid);

		// Call the routine to build the filter matrix based on numerical scheme
		if (ts->parent_solver->hasFilter) {
			get_ts2D_filter(ts, grid);
		}

	} else {
		ts->super_las = super_las;
		ts->mylasnumber = mylasnumber;
	}

	// Return the solver
	return ts;
}

/* This method yields the RHS matrix for a one dimensional transport solver based
   on the name of the scheme.
*/
void get_ts2D_rhs(TransportSolver2D *ts, Grid2D *grid)
{
	PetscInt idx, jdx, rstart, rend, nlocal;
	PetscInt *cols = NULL, vecsize = 0;
	PetscInt nx = grid->parent_grid->nx, ny = grid->ny;
	PetscInt *result = (PetscInt*) malloc(2*sizeof(PetscInt)), ii, jj;
	PetscMPIInt size;

	// Get info on the MPI layout
	ts->ierr = MPI_Comm_size(PETSC_COMM_WORLD, &size);

	// Get parallel layout of the grid->unknowns vector
	ts->ierr = VecGetSize(grid->unknowns, &vecsize);
	if ((vecsize == grid->nvtx) ||
	    (ts->super_las != NULL)) {
		ts->ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend);
		ts->ierr = VecGetLocalSize(grid->unknowns, &nlocal);
	}

	// Prepare the RHS matrix
	if (ts->super_las == NULL) {
		PetscPrintf(PETSC_COMM_WORLD, "\tCreating RHS distributed AIJ matrix ...\n");
		ts->ierr = MatCreate(PETSC_COMM_WORLD, &(ts->rhs));
		if (vecsize == grid->nvtx) {
			ts->ierr = MatSetSizes(ts->rhs, nlocal, nlocal, grid->nvtx, grid->nvtx);
		} else {
			ts->ierr = MatSetSizes(ts->rhs, PETSC_DECIDE, PETSC_DECIDE, grid->nvtx, grid->nvtx);
		}
		ts->ierr = MatSetType(ts->rhs, MATAIJ);
	}

	// Condition on the scheme now
	if (ts->super_las == NULL) PetscPrintf(PETSC_COMM_WORLD, "\tFilling the diagonals of the RHS matrix ...\n");
	if ((strcmp(ts->parent_solver->scheme_name, "RK4o6s_CSo11p") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4CSo11p") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4_CSo11p") == 0)) {
		// Complete the matrix setup
		if (ts->super_las == NULL) {
			if (size==1) {
				ts->ierr = MatSeqAIJSetPreallocation(ts->rhs, LARGEST_STENCIL, NULL);
			} else {
				ts->ierr = MatMPIAIJSetPreallocation(ts->rhs, LARGEST_STENCIL, NULL, LARGEST_STENCIL, NULL);
			}
			if (vecsize != grid->nvtx) {
				ts->ierr = MatGetOwnershipRange(ts->rhs, &rstart, &rend);
			}
		}
		// Get the shift in case this ts is embedded into an ads or a las
		PetscInt shift = 0;
		if (ts->super_las != NULL) shift = (ts->mylasnumber==1)?grid->nvtx:-grid->nvtx;
		// Now loop on the inner points, skip the boundaries for now
		for (idx = rstart; idx < rend; idx ++){
			if (idx <= ts->mylasnumber*grid->nvtx-1) {
				jdx = idx;
				if ((idx >= grid->nvtx) && (ts->super_las != NULL)) {
					jdx = idx - grid->nvtx;
				}
				unravel_index(jdx, nx, result); ii = result[0]; jj = result[1];
				//
				if ((ii-5 >= 0) && (ii+5 <= nx-1) &&
					(jj-5 >= 0) && (jj+5 <= ny-1)) {
					PetscScalar values[LARGEST_STENCIL] = {-0.002484594688*ts->cfl[1], 0.020779405824*ts->cfl[1], -0.090320001280*ts->cfl[1], 0.286511173973*ts->cfl[1], -0.872756993962*ts->cfl[1],
									  		  			   -0.002484594688*ts->cfl[0], 0.020779405824*ts->cfl[0], -0.090320001280*ts->cfl[0], 0.286511173973*ts->cfl[0], -0.872756993962*ts->cfl[0],
									  		  			   0.0,
									  		  			   0.872756993962*ts->cfl[0], -0.286511173973*ts->cfl[0], 0.090320001280*ts->cfl[0], -0.020779405824*ts->cfl[0], 0.002484594688*ts->cfl[0],
									  		  			   0.872756993962*ts->cfl[1], -0.286511173973*ts->cfl[1], 0.090320001280*ts->cfl[1], -0.020779405824*ts->cfl[1], 0.002484594688*ts->cfl[1]};
					//
					cols = (PetscInt*) malloc(LARGEST_STENCIL * sizeof(PetscInt));
					cols[0]=idx-5*nx+shift; cols[1]=idx-4*nx+shift; cols[2]=idx-3*nx+shift; cols[3]=idx-2*nx+shift; cols[4]=idx-1*nx+shift;
					cols[5]=idx-5+shift; cols[6]=idx-4+shift; cols[7]=idx-3+shift; cols[8]=idx-2+shift; cols[9]=idx-1+shift;
					cols[10]=idx+shift;
					cols[11]=idx+1+shift; cols[12]=idx+2+shift; cols[13]=idx+3+shift; cols[14]=idx+4+shift; cols[15]=idx+5+shift;
					cols[16]=idx+1*nx+shift; cols[17]=idx+2*nx+shift; cols[18]=idx+3*nx+shift; cols[19]=idx+4*nx+shift; cols[20]=idx+5*nx+shift;
					//
					if (ts->super_las != NULL) {
						ts->super_las->ierr = MatSetValues(ts->super_las->rhs, 1, &idx, LARGEST_STENCIL, cols, values, INSERT_VALUES);
					} else {
						ts->ierr = MatSetValues(ts->rhs, 1, &idx, LARGEST_STENCIL, cols, values, INSERT_VALUES);
					}
					//
					free(cols); cols = NULL;
				} else if ((jj-5 >= 0) && (jj+5 <= ny-1)) {
					PetscScalar values[11] = {-0.002484594688*ts->cfl[1], 0.020779405824*ts->cfl[1], -0.090320001280*ts->cfl[1], 0.286511173973*ts->cfl[1], -0.872756993962*ts->cfl[1],
									  		  0.0,
									  		  0.872756993962*ts->cfl[1], -0.286511173973*ts->cfl[1], 0.090320001280*ts->cfl[1], -0.020779405824*ts->cfl[1], 0.002484594688*ts->cfl[1]};
					//
					cols = (PetscInt*) malloc(11 * sizeof(PetscInt));
					cols[0]=idx-5*nx+shift; cols[1]=idx-4*nx+shift; cols[2]=idx-3*nx+shift; cols[3]=idx-2*nx+shift; cols[4]=idx-1*nx+shift;
					cols[5]=idx+shift;
					cols[6]=idx+1*nx+shift; cols[7]=idx+2*nx+shift; cols[8]=idx+3*nx+shift; cols[9]=idx+4*nx+shift; cols[10]=idx+5*nx+shift;
					//
					if (ts->super_las != NULL) {
						ts->super_las->ierr = MatSetValues(ts->super_las->rhs, 1, &idx, 11, cols, values, INSERT_VALUES);
					} else {
						ts->ierr = MatSetValues(ts->rhs, 1, &idx, 11, cols, values, INSERT_VALUES);
					}
					//
					free(cols); cols = NULL;
				} else if ((ii-5 >= 0) && (ii+5 <= nx-1)) {
					PetscScalar values[11] = {-0.002484594688*ts->cfl[0], 0.020779405824*ts->cfl[0], -0.090320001280*ts->cfl[0], 0.286511173973*ts->cfl[0], -0.872756993962*ts->cfl[0],
									  		  0.0,
									  		  0.872756993962*ts->cfl[0], -0.286511173973*ts->cfl[0], 0.090320001280*ts->cfl[0], -0.020779405824*ts->cfl[0], 0.002484594688*ts->cfl[0]};
					//
					cols = (PetscInt*) malloc(11 * sizeof(PetscInt));
					cols[0]=idx-5+shift; cols[1]=idx-4+shift; cols[2]=idx-3+shift; cols[3]=idx-2+shift; cols[4]=idx-1+shift;
					cols[5]=idx+shift;
					cols[6]=idx+1+shift; cols[7]=idx+2+shift; cols[8]=idx+3+shift; cols[9]=idx+4+shift; cols[10]=idx+5+shift;
					//
					if (ts->super_las != NULL) {
						ts->super_las->ierr = MatSetValues(ts->super_las->rhs, 1, &idx, 11, cols, values, INSERT_VALUES);
					} else {
						ts->ierr = MatSetValues(ts->rhs, 1, &idx, 11, cols, values, INSERT_VALUES);
					}
					//
					free(cols); cols = NULL;
				}
			}
		} // end of for(idx = rstart; idx < rend; i++)
	} else if ((strcmp(ts->parent_solver->scheme_name, "RK4o6s_CS") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4CS") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4_CS") == 0)) {
		// Complete the matrix setup
		if (ts->super_las == NULL) {
			if (size==1) {
				ts->ierr = MatSeqAIJSetPreallocation(ts->rhs, 5, NULL);
			} else {
				ts->ierr = MatMPIAIJSetPreallocation(ts->rhs, 5, NULL, 5, NULL);
			}
			if (vecsize != grid->nvtx) {
				ts->ierr = MatGetOwnershipRange(ts->rhs, &rstart, &rend);
			}
		}
		// Get the shift in case this ts is embedded into an ads or a las
		PetscInt shift = 0;
		if (ts->super_las != NULL) shift = (ts->mylasnumber==1)?grid->nvtx:-grid->nvtx;
		// Now loop on the inner points, skip the boundaries for now
		for (idx = rstart; idx < rend; idx ++){
			if (idx <= ts->mylasnumber*grid->nvtx-1) {
				jdx = idx;
				if ((idx >= grid->nvtx) && (ts->super_las != NULL)) {
					jdx = idx - grid->nvtx;
				}
				unravel_index(jdx, nx, result); ii = result[0]; jj = result[1];
				//
				if ((ii-1 >= 0) && (ii+1 <= nx-1) &&
					(jj-1 >= 0) && (jj+1 <= ny-1)) {
					PetscScalar values[5] = {-0.5*ts->cfl[1], -0.5*ts->cfl[0],
									 		 0.0,
									 		 0.5*ts->cfl[0], 0.5*ts->cfl[1]};
					//
					cols = (PetscInt*) malloc(5 * sizeof(PetscInt));
					cols[0]=idx-grid->parent_grid->nx+shift; cols[1]=idx-1+shift;
					cols[2]=idx+shift;
					cols[3]=idx+1+shift; cols[4]=idx+grid->parent_grid->nx+shift;
					//
					if (ts->super_las != NULL) {
						ts->super_las->ierr = MatSetValues(ts->super_las->rhs, 1, &idx, 5, cols, values, INSERT_VALUES);
					} else {
						ts->ierr = MatSetValues(ts->rhs, 1, &idx, 5, cols, values, INSERT_VALUES);
					}
					//
					free(cols); cols=NULL;
				} else if ((jj-1 >= 0) && (jj+1 <= ny-1)) {
					PetscScalar values[3] = {-0.5*ts->cfl[1], 0.0, 0.5*ts->cfl[1]};
					//
					cols = (PetscInt*) malloc(3 * sizeof(PetscInt));
					cols[0]=idx-grid->parent_grid->nx+shift; cols[1]=idx+shift; cols[2]=idx+grid->parent_grid->nx+shift;
					//
					if (ts->super_las != NULL) {
						ts->super_las->ierr = MatSetValues(ts->super_las->rhs, 1, &idx, 3, cols, values, INSERT_VALUES);
					} else {
						ts->ierr = MatSetValues(ts->rhs, 1, &idx, 3, cols, values, INSERT_VALUES);
					}
					//
					free(cols); cols=NULL;
				} else if ((ii-1 >= 0) && (ii+1 <= nx-1)) {
					PetscScalar values[3] = {-0.5*ts->cfl[0], 0.0, 0.5*ts->cfl[0]};
					//
					cols = (PetscInt*) malloc(3 * sizeof(PetscInt));
					cols[0]=idx-1+shift; cols[1]=idx+shift; cols[2]=idx+1+shift;
					//
					if (ts->super_las != NULL) {
						ts->super_las->ierr = MatSetValues(ts->super_las->rhs, 1, &idx, 3, cols, values, INSERT_VALUES);
					} else {
						ts->ierr = MatSetValues(ts->rhs, 1, &idx, 3, cols, values, INSERT_VALUES);
					}
					//
					free(cols); cols=NULL;
				}
			}
		} // end of for(idx = rstart; idx < rend; i++)
	} else {
		PetscPrintf(PETSC_COMM_WORLD, "Unknown spatial scheme.\n");
		ts->ierr = PETSC_ERR_ARG_WRONG;
	}

	if (cols != NULL) free(cols);
	free(result);
}

/* This method is used to apply all kind of boundary condition onto
   the matrix(ces) of a 2D transport solver
*/
PetscErrorCode applyBocos_ts2D(PetscInt nboco_arguments, PetscChar boco_arguments[2*TWENTY][WORDSZ], Grid2D *grid, TransportSolver2D *ts)
{
	int eq_index;
	PetscBool ismin = true, isx = true, dox = false, doy = false;
	PetscChar bocotype[WORDSZ] = "";
	PetscErrorCode ierr = 0;

	// Parse the boundary condition arguments for proper assignment -- only for perio first
	for (int i=0; i < nboco_arguments; i++) {
		// Get the type of boundary condition for the current boundary
		if (strstr(boco_arguments[i], "xmin") != NULL) {
			ismin = true; isx = true;
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else if (strstr(boco_arguments[i], "xmax") != NULL) {
			ismin = false; isx = true;
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else if (strstr(boco_arguments[i], "ymin") != NULL) {
			ismin = true; isx = false;
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else if (strstr(boco_arguments[i], "ymax") != NULL) {
			ismin = false; isx = false;
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown boundary assignement.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
		// Switch on the type of boundary condition required by the user
		if (strcmp(bocotype, "periodic") == 0) {
			if (isx)
				dox = true;
			else
				doy = true;
			ierr = set_periodic_ts2D(ts, grid, isx); CHKERRQ(ierr);
			if (ts->parent_solver->hasFilter) {
				if (dox && doy) {
					ierr = set_periodic_filter_ts2D(ts, grid, isx, true); CHKERRQ(ierr);
				} else {
					ierr = set_periodic_filter_ts2D(ts, grid, isx, false); CHKERRQ(ierr);
				}
			}
		} else if (strcmp(bocotype, "dirichlet") == 0) {
			continue;
		} else if (strcmp(bocotype, "neumann") == 0) {
			continue;
		} else if (startsWith(bocotype, "robin")) {
			continue;
		} else if (strcmp(bocotype, "open") == 0) {
			continue;
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown boundary condition type.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
	}

	// Parse the boundary condition arguments for proper assignment -- all but perio
	for (int i=0; i < nboco_arguments; i++) {
		// Get the type of boundary condition for the current boundary
		if (strstr(boco_arguments[i], "xmin") != NULL) {
			ismin = true; isx = true;
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else if (strstr(boco_arguments[i], "xmax") != NULL) {
			ismin = false; isx = true;
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else if (strstr(boco_arguments[i], "ymin") != NULL) {
			ismin = true; isx = false;
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else if (strstr(boco_arguments[i], "ymax") != NULL) {
			ismin = false; isx = false;
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown boundary assignement.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
		// Switch on the type of boundary condition required by the user
		if (strcmp(bocotype, "periodic") == 0) {
			continue;
		} else if (strcmp(bocotype, "dirichlet") == 0) {
			ierr = set_dirichlet_ts2D(ts, grid, ismin, isx); CHKERRQ(ierr);
			if (ts->parent_solver->hasFilter) {
				ierr = set_open_filter_ts2D(ts, grid, ismin, isx); CHKERRQ(ierr);
			}
		} else if (strcmp(bocotype, "neumann") == 0) {
			ierr = set_neumann_ts2D(ts, grid, ismin, isx); CHKERRQ(ierr);
			if (ts->parent_solver->hasFilter) {
				ierr = set_open_filter_ts2D(ts, grid, ismin, isx); CHKERRQ(ierr);
			}
		} else if (startsWith(bocotype, "robin")) {
			PetscScalar robA = 0.0, robB = 0.0;
			sscanf(bocotype, "robin,%lf,%lf", &robA, &robB);
			ierr = set_robin_ts2D(ts, grid, ismin, isx, robA, robB); CHKERRQ(ierr);
			if (ts->parent_solver->hasFilter) {
				ierr = set_open_filter_ts2D(ts, grid, ismin, isx); CHKERRQ(ierr);
			}
		} else if (strcmp(bocotype, "open") == 0) {
			ierr = set_open_ts2D(ts, grid, ismin, isx); CHKERRQ(ierr);
			if (ts->parent_solver->hasFilter) {
				ierr = set_open_filter_ts2D(ts, grid, ismin, isx); CHKERRQ(ierr);
			}
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown boundary condition type.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
	}

	if (ts->super_las == NULL) {
		// Assemble the RHS matrix
		ierr = PetscPrintf(PETSC_COMM_WORLD, "\tAssembling the RHS matrix ...\n"); CHKERRQ(ierr);
		ierr = MatAssemblyBegin(ts->rhs, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
		ierr = MatAssemblyEnd(ts->rhs, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

		// Assemble the filter matrix
		if (ts->parent_solver->hasFilter) {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "\tAssembling the filter matrix ...\n"); CHKERRQ(ierr);
			ierr = MatAssemblyBegin(ts->filter, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
			ierr = MatAssemblyEnd(ts->filter, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
		}
	}

	return ierr;
}

/* Method to set the RHS matrix of a 2D transport solver for periodicity
*/
PetscErrorCode set_periodic_ts2D(TransportSolver2D* ts, Grid2D* grid, PetscBool isx)
{
	PetscErrorCode ierr = 0;
	PetscInt idx, jdx, rstart, rend;
	PetscInt nx = grid->parent_grid->nx, ny = grid->ny;
	PetscInt *cols = NULL, vecsize = 0;
	PetscInt *result = (PetscInt*)malloc(2*sizeof(PetscInt)), ii, jj;

	// Get parallel layout of the grid->unknowns vector
	ierr = VecGetSize(grid->unknowns, &vecsize); CHKERRQ(ierr);
	if ((vecsize == grid->nvtx) ||
		(ts->super_las != NULL)) {
		ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend); CHKERRQ(ierr);
	} else {
		ierr = MatGetOwnershipRange(ts->rhs, &rstart, &rend); CHKERRQ(ierr);
	}

	ierr = PetscPrintf(PETSC_COMM_WORLD, "\tImposing periodicity conditions ...\n"); CHKERRQ(ierr);
	if ((strcmp(ts->parent_solver->scheme_name, "RK4o6s_CSo11p") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4CSo11p") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4_CSo11p") == 0)) {
		// Allocate some memory for the vector containing the columns indices
		cols = (PetscInt*) malloc(11 * sizeof(PetscInt));
		// Get the shift in case this ts is embedded in an ads or a las
		PetscInt shift = 0;
		if (ts->super_las != NULL) shift = (ts->mylasnumber==1)?grid->nvtx:-grid->nvtx;
		// Now loop on the points closest to the boundaries
		for (idx = rstart; idx < rend; idx ++){
			if (idx <= ts->mylasnumber*grid->nvtx-1) {
				jdx = idx;
				if ((idx >= grid->nvtx) && (ts->super_las != NULL)) {
					jdx = idx - grid->nvtx;
				}
				unravel_index(jdx, nx, result); ii = result[0]; jj = result[1];
				// Default assignement columns
				if (isx) {
					cols[0]=idx-5+shift; cols[1]=idx-4+shift; cols[2]=idx-3+shift; cols[3]=idx-2+shift; cols[4]=idx-1+shift;
					cols[5]=idx+shift;
					cols[6]=idx+1+shift; cols[7]=idx+2+shift; cols[8]=idx+3+shift; cols[9]=idx+4+shift; cols[10]=idx+5+shift;
				} else {
					cols[0]=idx-5*nx+shift; cols[1]=idx-4*nx+shift; cols[2]=idx-3*nx+shift; cols[3]=idx-2*nx+shift; cols[4]=idx-1*nx+shift;
					cols[5]=idx+shift;
					cols[6]=idx+1*nx+shift; cols[7]=idx+2*nx+shift; cols[8]=idx+3*nx+shift; cols[9]=idx+4*nx+shift; cols[10]=idx+5*nx+shift;
				}
				// Change function of the location
				if (isx && (ii < 5)) {
					if (ii == 0) {
						cols[0]=idx+(nx-5)+shift; cols[1]=idx+(nx-4)+shift; cols[2]=idx+(nx-3)+shift; cols[3]=idx+(nx-2)+shift; cols[4]=idx+(nx-1)+shift;
					} else if (ii == 1) {
						cols[0]=idx+(nx-5)+shift; cols[1]=idx+(nx-4)+shift; cols[2]=idx+(nx-3)+shift; cols[3]=idx+(nx-2)+shift;
					} else if (ii == 2) {
						cols[0]=idx+(nx-5)+shift; cols[1]=idx+(nx-4)+shift; cols[2]=idx+(nx-3)+shift;
					} else if (ii == 3) {
						cols[0]=idx+(nx-5)+shift; cols[1]=idx+(nx-4)+shift;
					} else if (ii == 4) {
						cols[0]=idx+(nx-5)+shift;
					}
				}
				if (isx && (ii > nx-6)) {
					if (ii == nx-1) {
						cols[6]=idx-(nx-1)+shift; cols[7]=idx-(nx-2)+shift; cols[8]=idx-(nx-3)+shift; cols[9]=idx-(nx-4)+shift; cols[10]=idx-(nx-5)+shift;
					} else if (ii == nx-2) {
						cols[7]=idx-(nx-2)+shift; cols[8]=idx-(nx-3)+shift; cols[9]=idx-(nx-4)+shift; cols[10]=idx-(nx-5)+shift;
					} else if (ii == nx-3) {
						cols[8]=idx-(nx-3)+shift; cols[9]=idx-(nx-4)+shift; cols[10]=idx-(nx-5)+shift;
					} else if (ii == nx-4) {
						cols[9]=idx-(nx-4)+shift; cols[10]=idx-(nx-5)+shift;
					} else if (ii == nx-5) {
						cols[10]=idx-(nx-5)+shift;
					}
				}
				if (!isx && (jj < 5)) {
					if (jj == 0) {
						cols[0]=idx+(ny-5)*nx+shift; cols[1]=idx+(ny-4)*nx+shift; cols[2]=idx+(ny-3)*nx+shift; cols[3]=idx+(ny-2)*nx+shift; cols[4]=idx+(ny-1)*nx+shift;
					} else if (jj == 1) {
						cols[0]=idx+(ny-5)*nx+shift; cols[1]=idx+(ny-4)*nx+shift; cols[2]=idx+(ny-3)*nx+shift; cols[3]=idx+(ny-2)*nx+shift;
					} else if (jj == 2) {
						cols[0]=idx+(ny-5)*nx+shift; cols[1]=idx+(ny-4)*nx+shift; cols[2]=idx+(ny-3)*nx+shift;
					} else if (jj == 3) {
						cols[0]=idx+(ny-5)*nx+shift; cols[1]=idx+(ny-4)*nx+shift;
					} else if (jj == 4) {
						cols[0]=idx+(ny-5)*nx+shift;
					}
				}
				if (!isx && (jj > ny-6)) {
					if (jj == ny-1) {
						cols[6]=idx+(1-ny)*nx+shift; cols[7]=idx+(2-ny)*nx+shift; cols[8]=idx+(3-ny)*nx+shift; cols[9]=idx+(4-ny)*nx+shift; cols[10]=idx+(5-ny)*nx+shift;
					} else if (jj == ny-2) {
						cols[7]=idx+(2-ny)*nx+shift; cols[8]=idx+(3-ny)*nx+shift; cols[9]=idx+(4-ny)*nx+shift; cols[10]=idx+(5-ny)*nx+shift;
					} else if (jj == ny-3) {
						cols[8]=idx+(3-ny)*nx+shift; cols[9]=idx+(4-ny)*nx+shift; cols[10]=idx+(5-ny)*nx+shift;
					} else if (jj == ny-4) {
						cols[9]=idx+(4-ny)*nx+shift; cols[10]=idx+(5-ny)*nx+shift;
					} else if (jj == ny-5) {
						cols[10]=idx+(5-ny)*nx+shift;
					}
				}
				// Get the vector of coefficients and fill the matrix row(s)
				if (isx) {
					PetscScalar values[11] = {-0.002484594688*ts->cfl[0], 0.020779405824*ts->cfl[0], -0.090320001280*ts->cfl[0], 0.286511173973*ts->cfl[0], -0.872756993962*ts->cfl[0],
									  	  	  0.0,
									  	  	  0.872756993962*ts->cfl[0], -0.286511173973*ts->cfl[0], 0.090320001280*ts->cfl[0], -0.020779405824*ts->cfl[0], 0.002484594688*ts->cfl[0]};
					if (ts->super_las != NULL) {
						ierr = MatSetValues(ts->super_las->rhs, 1, &idx, 11, cols, values, INSERT_VALUES); CHKERRQ(ierr);
					} else {
						ierr = MatSetValues(ts->rhs, 1, &idx, 11, cols, values, INSERT_VALUES); CHKERRQ(ierr);
					}
				} else {
					PetscScalar values[11] = {-0.002484594688*ts->cfl[1], 0.020779405824*ts->cfl[1], -0.090320001280*ts->cfl[1], 0.286511173973*ts->cfl[1], -0.872756993962*ts->cfl[1],
									  	  	  0.0,
									  	  	  0.872756993962*ts->cfl[1], -0.286511173973*ts->cfl[1], 0.090320001280*ts->cfl[1], -0.020779405824*ts->cfl[1], 0.002484594688*ts->cfl[1]};
					if (ts->super_las != NULL) {
						ierr = MatSetValues(ts->super_las->rhs, 1, &idx, 11, cols, values, INSERT_VALUES); CHKERRQ(ierr);
					} else {
						ierr = MatSetValues(ts->rhs, 1, &idx, 11, cols, values, INSERT_VALUES); CHKERRQ(ierr);
					}
				}
			}
		}
	} else if ((strcmp(ts->parent_solver->scheme_name, "RK4o6s_CS") == 0) ||
	    	   (strcmp(ts->parent_solver->scheme_name, "RK4CS") == 0) ||
	    	   (strcmp(ts->parent_solver->scheme_name, "RK4_CS") == 0)) {
		// Fill the vector of coefficients
		cols = (PetscInt*) malloc(3 * sizeof(PetscInt));
		// The shift
		PetscInt shift = 0;
		if (ts->super_las != NULL) shift = (ts->mylasnumber==1)?grid->nvtx:-grid->nvtx;
		// Now loop on the points closest to the boundaries
		for (idx = rstart; idx < rend; idx ++){
			if (idx <= ts->mylasnumber*grid->nvtx-1) {
				jdx = idx;
				if ((idx >= grid->nvtx) && (ts->super_las != NULL)) {
					jdx = idx - grid->nvtx;
				}
				unravel_index(jdx, nx, result); ii = result[0]; jj = result[1];
				// Default assignement columns
				if (isx) {
					cols[0] = idx-1+shift; cols[1] = idx+shift; cols[2] = idx+1+shift;
				} else {
					cols[0] = idx-1*nx+shift; cols[1] = idx+shift; cols[2] = idx+1*nx+shift;
				}
				// Change function of the location
				if (isx && (ii==0)) {
					cols[0] = idx+(nx-1)+shift;
				}
				if (isx && (ii==nx-1)) {
					cols[2] = idx-(nx-1)+shift;
				}
				if (!isx && (jj==0)) {
					cols[0] = idx+(ny-1)*nx+shift;
				}
				if (!isx && (jj==ny-1)) {
					cols[2] = idx-(ny-1)*nx+shift;
				}
				//
				if (isx) {
					PetscScalar values[3] = {-0.5*ts->cfl[0], 0.0, 0.5*ts->cfl[0]};
					if (ts->super_las != NULL) {
						ierr = MatSetValues(ts->super_las->rhs, 1, &idx, 3, cols, values, INSERT_VALUES); CHKERRQ(ierr);
					} else {
						ierr = MatSetValues(ts->rhs, 1, &idx, 3, cols, values, INSERT_VALUES); CHKERRQ(ierr);
					}
				} else {
					PetscScalar values[3] = {-0.5*ts->cfl[1], 0.0, 0.5*ts->cfl[1]};
					if (ts->super_las != NULL) {
						ierr = MatSetValues(ts->super_las->rhs, 1, &idx, 3, cols, values, INSERT_VALUES); CHKERRQ(ierr);
					} else {
						ierr = MatSetValues(ts->rhs, 1, &idx, 3, cols, values, INSERT_VALUES); CHKERRQ(ierr);
					}
				}
			}
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown spatial scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}

	free(cols);
	free(result);

	return ierr;
}

/* Method to set the RHS matrix of a 2D transport solver for open boundary condition
*/
PetscErrorCode set_open_ts2D(TransportSolver2D* ts, Grid2D* grid, PetscBool ismin, PetscBool isx)
{
	PetscErrorCode ierr = 0;
	PetscInt idx, jdx, rstart, rend;
	PetscInt *cols = NULL, vecsize = 0;
	PetscScalar *values = NULL;
	PetscInt *result = (PetscInt*)malloc(2*sizeof(PetscInt)), ii, jj;
	PetscInt nx = grid->parent_grid->nx, ny = grid->ny;

	// Get parallel layout of the grid->unknowns vector
	ierr = VecGetSize(grid->unknowns, &vecsize); CHKERRQ(ierr);
	if ((vecsize == grid->nvtx) ||
		(ts->super_las != NULL)) {
		ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend); CHKERRQ(ierr);
	} else {
		ierr = MatGetOwnershipRange(ts->rhs, &rstart, &rend); CHKERRQ(ierr);
	}

	ierr = PetscPrintf(PETSC_COMM_WORLD, "\tImposing open conditions ...\n"); CHKERRQ(ierr);
	if ((strcmp(ts->parent_solver->scheme_name, "RK4o6s_CSo11p") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4CSo11p") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4_CSo11p") == 0)) {
		// Fill the vector of coefficients
		cols = (PetscInt*) malloc(11 * sizeof(PetscInt));
		// Get the shift in case this ts is embedded in an ads or a las
		PetscInt shift = 0;
		if (ts->super_las != NULL) shift = (ts->mylasnumber==1)?grid->nvtx:-grid->nvtx;
		// Now loop on the points and stop only if close to the right boundary
		for (idx = rstart; idx < rend; idx ++) {
			if (idx <= ts->mylasnumber*grid->nvtx-1) {
				jdx = idx;
				if ((idx >= grid->nvtx) && (ts->super_las != NULL)) {
					jdx = idx - grid->nvtx;
				}
				unravel_index(jdx, nx, result); ii = result[0]; jj = result[1];
				if (ismin && isx && (ii < 5)) {
					if (ii == 0) {
						for (int i=0; i<11; i++)
							cols[i] = idx+i+shift;
						values = (PetscScalar [11]) {-2.391602219538, 5.832490322294, (-7.650218001182+0.000000000001), 7.907810563576, -5.922599052629,  3.071037015445, -1.014956769726, 0.170022256519, 0.002819958377, -0.004791009708, -0.000013063429};
					} else if (ii == 1) {
						for (int i=0; i<11; i++)
							cols[i] = idx+(i-1)+shift;
						values = (PetscScalar [11]) {-0.180022054228, -1.237550583044, 2.484731692990, -1.810320814061, 1.112990048440, -0.481086916514, 0.126598690230, -0.015510730165, (0.000024609059-0.000003), (0.000156447571-0.000000000001), -0.000007390277};
					} else if (ii == 2) {
						for (int i=0; i<11; i++)
							cols[i] = idx+(i-2)+shift;
						values = (PetscScalar [11]) {0.057982271137, -0.536135360383, (-0.264089548967+0.000000000002), (0.917445877606-0.000000000002), -0.169688364841, -0.029716326170, 0.029681617641, -0.005222483773, -0.000118806260, -0.000118806260, -0.000020069730};
					} else if (ii == 3) {
						for (int i=0; i<11; i++)
							cols[i] = idx+(i-3)+shift;
						values = (PetscScalar [11]) {-0.013277273810, 0.115976072920, -0.617479187931, (-0.274113948206+0.000000000002), (1.086208764655-0.000000000002), -0.402951626982, 0.131066986242, -0.028154858354, 0.002596328316, 0.000128743150, 0.};
					} else if (ii == 4) {
						for (int i=0; i<11; i++)
							cols[i] = idx+(i-4)+shift;
						values = (PetscScalar [11]) {0.016756572303, -0.117478455239, 0.411034935097, -1.130286765151, (0.341435872100-0.000000000001), 0.556396830543, -0.082525734207, 0.003565834658, 0.001173034777, (-0.000071772671+0.000000000064), -0.000000352273};
					}
					//
					for (int i=0; i<11; i++){
						values[i] = values[i] * ts->cfl[0];
					}
				} else if (!ismin && isx && (ii > nx-6)) {
					if (ii == nx-1) {
						for (int i=0; i<11; i++)
							cols[i] = idx-i+shift;
						values = (PetscScalar [11]) {-2.391602219538, 5.832490322294, -7.650218001182+0.000000000001, 7.907810563576, -5.922599052629,  3.071037015445, -1.014956769726, 0.170022256519, 0.002819958377, -0.004791009708, -0.000013063429};
					} else if (ii == nx-2) {
						for (int i=0; i<11; i++)
							cols[i] = idx-(i-1)+shift;
						values = (PetscScalar [11]) {-0.180022054228, -1.237550583044, 2.484731692990, -1.810320814061, 1.112990048440, -0.481086916514, 0.126598690230, -0.015510730165, 0.000024609059-0.000003, 0.000156447571-0.000000000001, -0.000007390277};
					} else if (ii == nx-3) {
						for (int i=0; i<11; i++)
							cols[i] = idx-(i-2)+shift;
						values = (PetscScalar [11]) {0.057982271137, -0.536135360383, -0.264089548967+0.000000000002, 0.917445877606-0.000000000002, -0.169688364841, -0.029716326170, 0.029681617641, -0.005222483773, -0.000118806260, -0.000118806260, -0.000020069730};
					} else if (ii == nx-4) {
						for (int i=0; i<11; i++)
							cols[i] = idx-(i-3)+shift;
						values = (PetscScalar [11]) {-0.013277273810, 0.115976072920, -0.617479187931, -0.274113948206+0.000000000002, 1.086208764655-0.000000000002, -0.402951626982, 0.131066986242, -0.028154858354, 0.002596328316, 0.000128743150, 0.};
					} else if (ii == nx-5) {
						for (int i=0; i<11; i++)
							cols[i] = idx-(i-4)+shift;
						values = (PetscScalar [11]) {0.016756572303, -0.117478455239, 0.411034935097, -1.130286765151, 0.341435872100-0.000000000001, 0.556396830543, -0.082525734207, 0.003565834658, 0.001173034777, -0.000071772671+0.000000000064, -0.000000352273};
					}
					//
					for (int i=0; i<11; i++)
						values[i] = -values[i] * ts->cfl[0];
				} else if (ismin && !isx && (jj < 5)) {
					if (jj == 0) {
						for (int i=0; i<11; i++)
							cols[i] = idx+i*nx+shift;
						values = (PetscScalar [11]) {-2.391602219538, 5.832490322294, -7.650218001182+0.000000000001, 7.907810563576, -5.922599052629,  3.071037015445, -1.014956769726, 0.170022256519, 0.002819958377, -0.004791009708, -0.000013063429};
					} else if (jj == 1) {
						for (int i=0; i<11; i++)
							cols[i] = idx+(i-1)*nx+shift;
						values = (PetscScalar [11]) {-0.180022054228, -1.237550583044, 2.484731692990, -1.810320814061, 1.112990048440, -0.481086916514, 0.126598690230, -0.015510730165, 0.000024609059-0.000003, 0.000156447571-0.000000000001, -0.000007390277};
					} else if (jj == 2) {
						for (int i=0; i<11; i++)
							cols[i] = idx+(i-2)*nx+shift;
						values = (PetscScalar [11]) {0.057982271137, -0.536135360383, -0.264089548967+0.000000000002, 0.917445877606-0.000000000002, -0.169688364841, -0.029716326170, 0.029681617641, -0.005222483773, -0.000118806260, -0.000118806260, -0.000020069730};
					} else if (jj == 3) {
						for (int i=0; i<11; i++)
							cols[i] = idx+(i-3)*nx+shift;
						values = (PetscScalar [11]) {-0.013277273810, 0.115976072920, -0.617479187931, -0.274113948206+0.000000000002, 1.086208764655-0.000000000002, -0.402951626982, 0.131066986242, -0.028154858354, 0.002596328316, 0.000128743150, 0.};
					} else if (jj == 4) {
						for (int i=0; i<11; i++)
							cols[i] = idx+(i-4)*nx+shift;
						values = (PetscScalar [11]) {0.016756572303, -0.117478455239, 0.411034935097, -1.130286765151, 0.341435872100-0.000000000001, 0.556396830543, -0.082525734207, 0.003565834658, 0.001173034777, -0.000071772671+0.000000000064, -0.000000352273};
					}
					//
					for (int i=0; i<11; i++){
						values[i] = values[i] * ts->cfl[1];
					}
				} else if (!ismin && !isx && (jj > ny-6)) {
					if (jj == ny-1) {
						for (int i=0; i<11; i++)
							cols[i] = idx-i*nx+shift;
						values = (PetscScalar [11]) {-2.391602219538, 5.832490322294, -7.650218001182+0.000000000001, 7.907810563576, -5.922599052629,  3.071037015445, -1.014956769726, 0.170022256519, 0.002819958377, -0.004791009708, -0.000013063429};
					} else if (jj == ny-2) {
						for (int i=0; i<11; i++)
							cols[i] = idx-(i-1)*nx+shift;
						values = (PetscScalar [11]) {-0.180022054228, -1.237550583044, 2.484731692990, -1.810320814061, 1.112990048440, -0.481086916514, 0.126598690230, -0.015510730165, 0.000024609059-0.000003, 0.000156447571-0.000000000001, -0.000007390277};
					} else if (jj == ny-3) {
						for (int i=0; i<11; i++)
							cols[i] = idx-(i-2)*nx+shift;
						values = (PetscScalar [11]) {0.057982271137, -0.536135360383, -0.264089548967+0.000000000002, 0.917445877606-0.000000000002, -0.169688364841, -0.029716326170, 0.029681617641, -0.005222483773, -0.000118806260, -0.000118806260, -0.000020069730};
					} else if (jj == ny-4) {
						for (int i=0; i<11; i++)
							cols[i] = idx-(i-3)*nx+shift;
						values = (PetscScalar [11]) {-0.013277273810, 0.115976072920, -0.617479187931, -0.274113948206+0.000000000002, 1.086208764655-0.000000000002, -0.402951626982, 0.131066986242, -0.028154858354, 0.002596328316, 0.000128743150, 0.};
					} else if (jj == ny-5) {
						for (int i=0; i<11; i++)
							cols[i] = idx-(i-4)*nx+shift;
						values = (PetscScalar [11]) {0.016756572303, -0.117478455239, 0.411034935097, -1.130286765151, 0.341435872100-0.000000000001, 0.556396830543, -0.082525734207, 0.003565834658, 0.001173034777, -0.000071772671+0.000000000064, -0.000000352273};
					}
					//
					for (int i=0; i<11; i++)
						values[i] = -values[i] * ts->cfl[1];
				} else {
					continue;
				}
				if (ts->super_las != NULL) {
					ierr = MatSetValues(ts->super_las->rhs, 1, &idx, 11, cols, values, ADD_VALUES); CHKERRQ(ierr);
				} else {
					ierr = MatSetValues(ts->rhs, 1, &idx, 11, cols, values, ADD_VALUES); CHKERRQ(ierr);
				}
			}
		}
	} else if ((strcmp(ts->parent_solver->scheme_name, "RK4o6s_CS") == 0) ||
	    	   (strcmp(ts->parent_solver->scheme_name, "RK4CS") == 0) ||
	    	   (strcmp(ts->parent_solver->scheme_name, "RK4_CS") == 0)) {
		// Fill the vector of coefficients
		cols = (PetscInt*) malloc(2 * sizeof(PetscInt));
		// Now loop on the points and stop only if close to the right boundary
		PetscInt shift = 0;
		if (ts->super_las != NULL) shift = (ts->mylasnumber==1)?grid->nvtx:-grid->nvtx;
		for (idx = rstart; idx < rend; idx ++){
			if (idx <= ts->mylasnumber*grid->nvtx-1) {
				jdx = idx;
				if ((idx >= grid->nvtx) && (ts->super_las != NULL)) {
					jdx = idx - grid->nvtx;
				}
				unravel_index(jdx, nx, result); ii = result[0]; jj = result[1];
				if (ismin && isx && (ii == 0)) {
					for (int i=0; i<2; i++)
						cols[i] = idx+i+shift;
					//
					values = (PetscScalar [2]) {-1.0*ts->cfl[0], 1.0*ts->cfl[0]};
				} else if (!ismin && isx && (ii == nx-1)) {
					for (int i=0; i<2; i++)
						cols[i] = idx-i+shift;
					//
					values = (PetscScalar [2]) {-1.0*ts->cfl[0], 1.0*ts->cfl[0]};
					//
					for (int i=0; i<2; i++)
						values[i] = -values[i];
				} else if (ismin && !isx && (jj == 0)) {
					for (int i=0; i<2; i++)
						cols[i] = idx+i*nx+shift;
					//
					values = (PetscScalar [2]) {-1.0*ts->cfl[1], 1.0*ts->cfl[1]};
				} else if (!ismin && !isx && (jj == ny-1)) {
					for (int i=0; i<2; i++)
						cols[i] = idx-i*nx+shift;
					//
					values = (PetscScalar [2]) {-1.0*ts->cfl[1], 1.0*ts->cfl[1]};
					//
					for (int i=0; i<2; i++)
						values[i] = -values[i];
				} else {
					continue;
				}
				if (ts->super_las != NULL) {
					ierr = MatSetValues(ts->super_las->rhs, 1, &idx, 2, cols, values, ADD_VALUES); CHKERRQ(ierr);
				} else {
					ierr = MatSetValues(ts->rhs, 1, &idx, 2, cols, values, ADD_VALUES); CHKERRQ(ierr);
				}
			}
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown spatial scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}

	free(cols);
	free(result);

	return ierr;
}

/* Method to set the RHS matrix of a 2D transport solver for an
   homogeneous dirichlet boundary condition
*/
PetscErrorCode set_dirichlet_ts2D(TransportSolver2D* ts, Grid2D* grid, PetscBool ismin, PetscBool isx)
{
	PetscErrorCode ierr = 0;
	PetscInt nx = grid->parent_grid->nx, ny = grid->ny;

	// Start by applying an open boundary condition to have the
	// right schemes close to the boundary points
	ierr = set_open_ts2D(ts, grid, ismin, isx);

	// Increment the number of rows to zero later during execution
	if (ismin && isx) {
		for (int i=0; i<ny; i++) {
			ts->parent_solver->ndir_rows ++;
			ts->parent_solver->dir_rows[ts->parent_solver->ndir_rows] = grid->ixmin[i];
		}
	} else if (!ismin && isx) {
		for (int i=0; i<ny; i++) {
			ts->parent_solver->ndir_rows ++;
			ts->parent_solver->dir_rows[ts->parent_solver->ndir_rows] = grid->ixmax[i];
		}
	} else if (ismin && !isx) {
		for (int i=0; i<nx; i++) {
			ts->parent_solver->ndir_rows ++;
			ts->parent_solver->dir_rows[ts->parent_solver->ndir_rows] = grid->iymin[i];
		}
	} else if (!ismin && !isx) {
		for (int i=0; i<nx; i++) {
			ts->parent_solver->ndir_rows ++;
			ts->parent_solver->dir_rows[ts->parent_solver->ndir_rows] = grid->iymax[i];
		}
	}

	return ierr;
}

/* Method to set the RHS matrix of a 2D transport solver for an
   homogeneous neumann boundary condition
*/
PetscErrorCode set_neumann_ts2D(TransportSolver2D* ts, Grid2D* grid, PetscBool ismin, PetscBool isx)
{
	PetscInt *rows = NULL;
	PetscErrorCode ierr = 0;
	PetscScalar *values = NULL;
	PetscInt nx = grid->parent_grid->nx, ny = grid->ny;

	// Start by applying an open boundary condition to have the
	// right schemes close to the boundary points
	ierr = set_open_ts2D(ts, grid, ismin, isx);

	// Condition on the scheme for the proper computation of homogeneous Neumann corrections
	if ((strcmp(ts->parent_solver->scheme_name, "RK4o6s_CSo11p") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4CSo11p") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4_CSo11p") == 0)) {
		// Fill the vector of values
		values = (PetscScalar [11]) {-2.391602219538, 5.832490322294, -7.650218001182+0.000000000001, 7.907810563576, -5.922599052629,  3.071037015445, -1.014956769726, 0.170022256519, 0.002819958377, -0.004791009708, -0.000013063429};
		for (int i=1; i<11; i++) {
			values[i] = - values[i] / values[0];
		}
		values[0] = 0.0;
		// Fill the vector of coefficients
		rows = (PetscInt*) malloc(11 * sizeof(PetscInt));
		// Now fix the correct boundary
		if (ismin && isx) {
			for (int i=0; i<ny; i++) {
				ts->parent_solver->nneu_rows ++;
				ts->parent_solver->neu_rows[ts->parent_solver->nneu_rows] = grid->ixmin[i];
				//
				for (int j=0; j<11; j++)
					rows[j] = grid->ixmin[i]+j;
				//
				ierr = VecCreate(PETSC_COMM_WORLD, &(ts->neumanns[ts->parent_solver->nneu_rows])); CHKERRQ(ierr);
				ierr = VecSetSizes(ts->neumanns[ts->parent_solver->nneu_rows], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
				ierr = VecSetUp(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecSet(ts->neumanns[ts->parent_solver->nneu_rows], 0.0); CHKERRQ(ierr);
				ierr = VecSetValues(ts->neumanns[ts->parent_solver->nneu_rows],11,rows,values,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
			}
		} else if (!ismin && isx) {
			for (int i=0; i<ny; i++) {
				ts->parent_solver->nneu_rows ++;
				ts->parent_solver->neu_rows[ts->parent_solver->nneu_rows] = grid->ixmax[i];
				//
				for (int j=0; j<11; j++)
					rows[j] = grid->ixmax[i]-j;
				//
				ierr = VecCreate(PETSC_COMM_WORLD, &(ts->neumanns[ts->parent_solver->nneu_rows])); CHKERRQ(ierr);
				ierr = VecSetSizes(ts->neumanns[ts->parent_solver->nneu_rows], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
				ierr = VecSetUp(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecSet(ts->neumanns[ts->parent_solver->nneu_rows], 0.0); CHKERRQ(ierr);
				ierr = VecSetValues(ts->neumanns[ts->parent_solver->nneu_rows],11,rows,values,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
			}
		} else if (ismin && !isx) {
			for (int i=0; i<nx; i++) {
				ts->parent_solver->nneu_rows ++;
				ts->parent_solver->neu_rows[ts->parent_solver->nneu_rows] = grid->iymin[i];
				//
				for (int j=0; j<11; j++)
					rows[j] = grid->iymin[i]+j*nx;
				//
				ierr = VecCreate(PETSC_COMM_WORLD, &(ts->neumanns[ts->parent_solver->nneu_rows])); CHKERRQ(ierr);
				ierr = VecSetSizes(ts->neumanns[ts->parent_solver->nneu_rows], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
				ierr = VecSetUp(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecSet(ts->neumanns[ts->parent_solver->nneu_rows], 0.0); CHKERRQ(ierr);
				ierr = VecSetValues(ts->neumanns[ts->parent_solver->nneu_rows],11,rows,values,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
			}
		} else if (!ismin && !isx) {
			for (int i=0; i<nx; i++) {
				ts->parent_solver->nneu_rows ++;
				ts->parent_solver->neu_rows[ts->parent_solver->nneu_rows] = grid->iymax[i];
				//
				for (int j=0; j<11; j++)
					rows[j] = grid->iymax[i]-j*nx;
				//
				ierr = VecCreate(PETSC_COMM_WORLD, &(ts->neumanns[ts->parent_solver->nneu_rows])); CHKERRQ(ierr);
				ierr = VecSetSizes(ts->neumanns[ts->parent_solver->nneu_rows], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
				ierr = VecSetUp(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecSet(ts->neumanns[ts->parent_solver->nneu_rows], 0.0); CHKERRQ(ierr);
				ierr = VecSetValues(ts->neumanns[ts->parent_solver->nneu_rows],11,rows,values,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
			}
		}
	} else if ((strcmp(ts->parent_solver->scheme_name, "RK4o6s_CS") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4CS") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4_CS") == 0)) {
		// Fill the vector of values
		values = (PetscScalar [2]) {-1.0, 1.0};
		for (int i=1; i<2; i++) {
				values[i] = - values[i] / values[0];
		}
		values[0] = 0.0;
		// Fill the vector of coefficients
		rows = (PetscInt*) malloc(2 * sizeof(PetscInt));
		// Now fix the right boundary
		if (ismin && isx) {
			for (int i=0; i<ny; i++) {
				ts->parent_solver->nneu_rows ++;
				ts->parent_solver->neu_rows[ts->parent_solver->nneu_rows] = grid->ixmin[i];
				//
				for (int j=0; j<2; j++)
					rows[j] = grid->ixmin[i]+j;
				//
				ierr = VecCreate(PETSC_COMM_WORLD, &(ts->neumanns[ts->parent_solver->nneu_rows])); CHKERRQ(ierr);
				ierr = VecSetSizes(ts->neumanns[ts->parent_solver->nneu_rows], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
				ierr = VecSetUp(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecSet(ts->neumanns[ts->parent_solver->nneu_rows], 0.0); CHKERRQ(ierr);
				ierr = VecSetValues(ts->neumanns[ts->parent_solver->nneu_rows],2,rows,values,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
			}
		} else if (!ismin && isx) {
			for (int i=0; i<ny; i++) {
				ts->parent_solver->nneu_rows ++;
				ts->parent_solver->neu_rows[ts->parent_solver->nneu_rows] = grid->ixmax[i];
				//
				for (int j=0; j<2; j++)
					rows[j] = grid->ixmax[i]-j;
				//
				ierr = VecCreate(PETSC_COMM_WORLD, &(ts->neumanns[ts->parent_solver->nneu_rows])); CHKERRQ(ierr);
				ierr = VecSetSizes(ts->neumanns[ts->parent_solver->nneu_rows], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
				ierr = VecSetUp(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecSet(ts->neumanns[ts->parent_solver->nneu_rows], 0.0); CHKERRQ(ierr);
				ierr = VecSetValues(ts->neumanns[ts->parent_solver->nneu_rows],2,rows,values,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
			}
		} else if (ismin && !isx) {
			for (int i=0; i<nx; i++) {
				ts->parent_solver->nneu_rows ++;
				ts->parent_solver->neu_rows[ts->parent_solver->nneu_rows] = grid->iymin[i];
				//
				for (int j=0; j<2; j++)
					rows[j] = grid->iymin[i]+j*nx;
				//
				ierr = VecCreate(PETSC_COMM_WORLD, &(ts->neumanns[ts->parent_solver->nneu_rows])); CHKERRQ(ierr);
				ierr = VecSetSizes(ts->neumanns[ts->parent_solver->nneu_rows], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
				ierr = VecSetUp(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecSet(ts->neumanns[ts->parent_solver->nneu_rows], 0.0); CHKERRQ(ierr);
				ierr = VecSetValues(ts->neumanns[ts->parent_solver->nneu_rows],2,rows,values,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
			}
		} else if (!ismin && !isx) {
			for (int i=0; i<nx; i++) {
				ts->parent_solver->nneu_rows ++;
				ts->parent_solver->neu_rows[ts->parent_solver->nneu_rows] = grid->iymax[i];
				//
				for (int j=0; j<2; j++)
					rows[j] = grid->iymax[i]-j*nx;
				//
				ierr = VecCreate(PETSC_COMM_WORLD, &(ts->neumanns[ts->parent_solver->nneu_rows])); CHKERRQ(ierr);
				ierr = VecSetSizes(ts->neumanns[ts->parent_solver->nneu_rows], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
				ierr = VecSetUp(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecSet(ts->neumanns[ts->parent_solver->nneu_rows], 0.0); CHKERRQ(ierr);
				ierr = VecSetValues(ts->neumanns[ts->parent_solver->nneu_rows],2,rows,values,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(ts->neumanns[ts->parent_solver->nneu_rows]); CHKERRQ(ierr);
			}
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown spatial scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}

	free(rows);

	return ierr;
}

/* Method to set the RHS matrix of a 2D transport solver for an
   homogeneous robin boundary condition
*/
PetscErrorCode set_robin_ts2D(TransportSolver2D* ts, Grid2D* grid, PetscBool ismin, PetscBool isx, PetscScalar robA, PetscScalar robB)
{
	PetscInt *rows = NULL;
	PetscErrorCode ierr = 0;
	PetscScalar *values = NULL;
	PetscInt nx = grid->parent_grid->nx, ny = grid->ny;

	// Start by applying an open boundary condition to have the
	// right schemes close to the boundary points
	ierr = set_open_ts2D(ts, grid, ismin, isx);

	// Condition on the scheme for the proper computation of homogeneous Neumann corrections
	if ((strcmp(ts->parent_solver->scheme_name, "RK4o6s_CSo11p") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4CSo11p") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4_CSo11p") == 0)) {
		// Fill the vector of values
		values = (PetscScalar [11]) {-2.391602219538, 5.832490322294, -7.650218001182+0.000000000001, 7.907810563576, -5.922599052629,  3.071037015445, -1.014956769726, 0.170022256519, 0.002819958377, -0.004791009708, -0.000013063429};
		for (int i=0; i<11; i++)
			values[i] *= robB;
		if (isx) {
			values[0] = values[0] + grid->parent_grid->dx * robA;
		} else {
			values[0] = values[0] + grid->dy * robA;
		}
		for (int i=1; i<11; i++) {
			values[i] = - values[i] / values[0];
		}
		values[0] = 0.0;
		// Fill the vector of coefficients
		rows = (PetscInt*) malloc(11 * sizeof(PetscInt));
		// Now fix the correct boundary
		if (ismin && isx) {
			for (int i=0; i<ny; i++) {
				ts->parent_solver->nrob_rows ++;
				ts->parent_solver->rob_rows[ts->parent_solver->nrob_rows] = grid->ixmin[i];
				//
				for (int j=0; j<11; j++)
					rows[j] = grid->ixmin[i]+j;
				//
				ierr = VecCreate(PETSC_COMM_WORLD, &(ts->robins[ts->parent_solver->nrob_rows])); CHKERRQ(ierr);
				ierr = VecSetSizes(ts->robins[ts->parent_solver->nrob_rows], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
				ierr = VecSetUp(ts->robins[ts->parent_solver->nrob_rows]); CHKERRQ(ierr);
				ierr = VecSet(ts->robins[ts->parent_solver->nrob_rows], 0.0); CHKERRQ(ierr);
				ierr = VecSetValues(ts->robins[ts->parent_solver->nrob_rows],11,rows,values,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(ts->robins[ts->parent_solver->nrob_rows]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(ts->robins[ts->parent_solver->nrob_rows]); CHKERRQ(ierr);
			}
		} else if (!ismin && isx) {
			for (int i=0; i<ny; i++) {
				ts->parent_solver->nrob_rows ++;
				ts->parent_solver->rob_rows[ts->parent_solver->nrob_rows] = grid->ixmax[i];
				//
				for (int j=0; j<11; j++)
					rows[j] = grid->ixmax[i]-j;
				//
				ierr = VecCreate(PETSC_COMM_WORLD, &(ts->robins[ts->parent_solver->nrob_rows])); CHKERRQ(ierr);
				ierr = VecSetSizes(ts->robins[ts->parent_solver->nrob_rows], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
				ierr = VecSetUp(ts->robins[ts->parent_solver->nrob_rows]); CHKERRQ(ierr);
				ierr = VecSet(ts->robins[ts->parent_solver->nrob_rows], 0.0); CHKERRQ(ierr);
				ierr = VecSetValues(ts->robins[ts->parent_solver->nrob_rows],11,rows,values,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(ts->robins[ts->parent_solver->nrob_rows]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(ts->robins[ts->parent_solver->nrob_rows]); CHKERRQ(ierr);
			}
		} else if (ismin && !isx) {
			for (int i=0; i<nx; i++) {
				ts->parent_solver->nrob_rows ++;
				ts->parent_solver->rob_rows[ts->parent_solver->nrob_rows] = grid->iymin[i];
				//
				for (int j=0; j<11; j++)
					rows[j] = grid->iymin[i]+j*nx;
				//
				ierr = VecCreate(PETSC_COMM_WORLD, &(ts->robins[ts->parent_solver->nrob_rows])); CHKERRQ(ierr);
				ierr = VecSetSizes(ts->robins[ts->parent_solver->nrob_rows], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
				ierr = VecSetUp(ts->robins[ts->parent_solver->nrob_rows]); CHKERRQ(ierr);
				ierr = VecSet(ts->robins[ts->parent_solver->nrob_rows], 0.0); CHKERRQ(ierr);
				ierr = VecSetValues(ts->robins[ts->parent_solver->nrob_rows],11,rows,values,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(ts->robins[ts->parent_solver->nrob_rows]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(ts->robins[ts->parent_solver->nrob_rows]); CHKERRQ(ierr);
			}
		} else if (!ismin && !isx) {
			for (int i=0; i<nx; i++) {
				ts->parent_solver->nrob_rows ++;
				ts->parent_solver->rob_rows[ts->parent_solver->nrob_rows] = grid->iymax[i];
				//
				for (int j=0; j<11; j++)
					rows[j] = grid->iymax[i]-j*nx;
				//
				ierr = VecCreate(PETSC_COMM_WORLD, &(ts->robins[ts->parent_solver->nrob_rows])); CHKERRQ(ierr);
				ierr = VecSetSizes(ts->robins[ts->parent_solver->nrob_rows], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
				ierr = VecSetUp(ts->robins[ts->parent_solver->nrob_rows]); CHKERRQ(ierr);
				ierr = VecSet(ts->robins[ts->parent_solver->nrob_rows], 0.0); CHKERRQ(ierr);
				ierr = VecSetValues(ts->robins[ts->parent_solver->nrob_rows],11,rows,values,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(ts->robins[ts->parent_solver->nrob_rows]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(ts->robins[ts->parent_solver->nrob_rows]); CHKERRQ(ierr);
			}
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown spatial scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}

	free(rows);

	return ierr;
}

/* Method for the execution of a one dimensional transport solver
*/
PetscErrorCode execute_ts2D(TransportSolver2D* ts, Grid2D* grid)
{
	Mat identity, R, mlist1, mlist2;
	PetscErrorCode ierr = 0;
	PetscScalar factor = 0.0, ukmax = 0.0, ukmin = 0.0;
	PetscScalar nL2diff = 0.0, nL2sol = 0.0, errorL2 = 0.0;
	PetscScalar neuscal = 0.0, robscal = 0.0;
	PetscInt step, idx, rstart, rend, nlocal, vecsize;
	Vec b, c;

	// Get parallel layout of the grid->unknowns vector
	ierr = VecGetSize(grid->unknowns, &vecsize); CHKERRQ(ierr);
	if (vecsize == grid->nvtx) {
		ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend); CHKERRQ(ierr);
		ierr = VecGetLocalSize(grid->unknowns, &nlocal); CHKERRQ(ierr);
	} else {
		ierr = MatGetOwnershipRange(ts->rhs, &rstart, &rend); CHKERRQ(ierr);
		ierr = MatGetLocalSize(ts->rhs, &nlocal, &nlocal); CHKERRQ(ierr);
	}

	// Create an identity matrix, it can come handy
	ierr = MatCreate(PETSC_COMM_WORLD, &identity); CHKERRQ(ierr);
	ierr = MatSetSizes(identity, nlocal, nlocal, grid->nvtx, grid->nvtx); CHKERRQ(ierr);
	ierr = MatSetType(identity, MATAIJ); CHKERRQ(ierr);
	ierr = MatSetUp(identity); CHKERRQ(ierr);
	for (idx = rstart; idx < rend ; idx++) {
		ierr = MatSetValue(identity, idx, idx, 1.0, INSERT_VALUES); CHKERRQ(ierr);
	}
	ierr = MatAssemblyBegin(identity, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
	ierr = MatAssemblyEnd(identity, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

	// Prepare a few other objects before the big loop
	// First pass the RHS actually on the right hand side and scale it by the space step
	// Advection velocities have already been included in the matrix creation
	ierr = MatScale(ts->rhs, -1.0/ts->parent_solver->dt); CHKERRQ(ierr);
	// Then handle the preprocessing for the RK matrix
	if (ts->parent_solver->rk_params->has_gammas) {
		PetscPrintf(PETSC_COMM_WORLD, "\tPreparing Runge-Kutta matrix ...\n");
		// Create the matrix and copy the values from the RHS and assemble
		ierr = MatDuplicate(ts->rhs, MAT_COPY_VALUES, &R); CHKERRQ(ierr);
		ierr = MatScale(R, ts->parent_solver->dt); CHKERRQ(ierr);
		ierr = MatAssemblyBegin(R, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
		ierr = MatAssemblyEnd(R, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
		// First element of mlist is the RHS matrix
		ierr = MatDuplicate(ts->rhs, MAT_COPY_VALUES, &mlist1); CHKERRQ(ierr);
		// Now loop over the remaining RK stages to simulate matrix power
		for (idx = 1; idx < ts->parent_solver->rk_params->steps; idx++) {
			factor = PetscPowScalar(ts->parent_solver->dt, idx+1)*ts->parent_solver->rk_params->gammas[idx];
			ierr = MatMatMult(ts->rhs, mlist1, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &mlist2); CHKERRQ(ierr);
			ierr = MatDestroy(&mlist1); CHKERRQ(ierr);
			ierr = MatAXPY(R, factor, mlist2, DIFFERENT_NONZERO_PATTERN); CHKERRQ(ierr);
			ierr = MatDuplicate(mlist2, MAT_COPY_VALUES, &mlist1); CHKERRQ(ierr);
			ierr = MatDestroy(&mlist2); CHKERRQ(ierr);
		}
		// Destroy the mlist object
		MatDestroy(&mlist1);
		MatDestroy(&mlist2);
		// Add identity to R
		ierr = MatAXPY(R, 1.0, identity, DIFFERENT_NONZERO_PATTERN); CHKERRQ(ierr);
		// Change R structure in case of dirichlet boco
		{
			PetscInt *rows = (PetscInt*) malloc(sizeof(PetscInt) * (ts->parent_solver->ndir_rows+1));
			for (int i=0; i<(ts->parent_solver->ndir_rows+1); i++) {
				rows[i] = ts->parent_solver->dir_rows[i];
			}
			ierr = MatZeroRowsColumns(R, ts->parent_solver->ndir_rows+1, rows, 0.0, 0, 0); CHKERRQ(ierr);
			if (ts->parent_solver->hasFilter) {
				ierr = MatZeroRowsColumns(ts->filter, ts->parent_solver->ndir_rows+1, rows, 0.0, 0, 0); CHKERRQ(ierr);
			}
			free(rows);
		}
		// Change filter structure in case of Neumann boco or Robin boco
		if (ts->parent_solver->hasFilter) {
			for (int i=0; i<(ts->parent_solver->nneu_rows+1); i++) {
				if (ts->neumanns[i] != NULL) {
					ierr = MatZeroRows(ts->filter, 1, ts->parent_solver->neu_rows + i, 0.0, 0, 0); CHKERRQ(ierr);
				}
			}
			for (int i=0; i<(ts->parent_solver->nrob_rows+1); i++) {
				if (ts->robins[i] != NULL) {
					ierr = MatZeroRows(ts->filter, 1, ts->parent_solver->rob_rows + i, 0.0, 0, 0); CHKERRQ(ierr);
				}
			}
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown definition of the Runge-Kutta scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}
	// Scale the filter by the strength input by the user
	if (ts->parent_solver->hasFilter) {
		ierr = MatScale(ts->filter, ts->parent_solver->filter_strength); CHKERRQ(ierr);
	}

	// Export the initial solution
	{
		step = 0;
		// Create the viewers and save the vectors
		PetscViewer Viewer = NULL;
		PetscChar filename[WORDSZ];
		if (grid->parent_grid->ndim == DIM2) {
			sprintf(filename, "sol_%06d.h5", step);
			ierr = PetscViewerHDF5Open(PETSC_COMM_WORLD, filename, FILE_MODE_WRITE, &Viewer); CHKERRQ(ierr);
		} else if (grid->parent_grid->ndim == DIM1) {
			sprintf(filename, "sol_%06d.dat", step);
			ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, filename, &Viewer); CHKERRQ(ierr);
			ierr = PetscViewerPushFormat(Viewer, PETSC_VIEWER_ASCII_MATLAB); CHKERRQ(ierr);
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Wrong dimension for solution output.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
		ierr = VecView(grid->x, Viewer); CHKERRQ(ierr);
		ierr = VecView(grid->y, Viewer); CHKERRQ(ierr);
		ierr = VecView(grid->unknowns, Viewer); CHKERRQ(ierr);
		ierr = PetscViewerDestroy(&Viewer); CHKERRQ(ierr);
	}

	// Loop on iterations
	ierr = PetscPrintf(PETSC_COMM_WORLD, "\tMarching the equation in time ...\n"); CHKERRQ(ierr);
	for (step = 1; step <= ts->parent_solver->nit; step ++) {
		// Implementation differs function of the way the RK scheme is made
		if (ts->parent_solver->rk_params->has_gammas) {
			// Get an auxiliary vector
			ierr = VecDuplicate(grid->unknowns, &b); CHKERRQ(ierr);
			// Apply the temporal scheme
			ierr = MatMult(R, grid->unknowns, b); CHKERRQ(ierr);
			// Save the result
			ierr = VecCopy(b, grid->unknowns); CHKERRQ(ierr);
			// Destroy b
			ierr = VecDestroy(&b); CHKERRQ(ierr);
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown definition of the Runge-Kutta scheme.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
		// Apply Neumann boundary conditions
		for (int i=0; i<(ts->parent_solver->nneu_rows+1); i++) {
			if (ts->neumanns[i] != NULL) {
				ierr = VecDot(grid->unknowns, ts->neumanns[i], &neuscal); CHKERRQ(ierr);
				ierr = VecSetValue(grid->unknowns, ts->parent_solver->neu_rows[i], neuscal, INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(grid->unknowns); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(grid->unknowns); CHKERRQ(ierr);
			}
		}
		// Apply Robin boundary conditions
		for (int i=0; i<(ts->parent_solver->nrob_rows+1); i++) {
			if (ts->robins[i] != NULL) {
				ierr = VecDot(grid->unknowns, ts->robins[i], &robscal); CHKERRQ(ierr);
				ierr = VecSetValue(grid->unknowns, ts->parent_solver->rob_rows[i], robscal, INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(grid->unknowns); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(grid->unknowns); CHKERRQ(ierr);
			}
		}
		// Filter the signal
		if (ts->parent_solver->hasFilter) {
			ierr = VecDuplicate(grid->unknowns, &b); CHKERRQ(ierr);
			ierr = MatMult(ts->filter, grid->unknowns, b); CHKERRQ(ierr);
			ierr = VecAXPY(grid->unknowns, -1.0, b); CHKERRQ(ierr);
			ierr = VecDestroy(&b); CHKERRQ(ierr);
		}
		// Tell a few things to the user
		ierr = VecMax(grid->unknowns, NULL, &ukmax); CHKERRQ(ierr);
		ierr = VecMin(grid->unknowns, NULL, &ukmin); CHKERRQ(ierr);
		ierr = PetscPrintf(PETSC_COMM_WORLD,
					"Iter. %05d\t Time %.6e\t Min/Max uk (%.4e,%.4e)\t\n",
					step, step*ts->parent_solver->dt, ukmin, ukmax); CHKERRQ(ierr);
		// Export the data
		if (ts->parent_solver->export_frequency > 0) {
			if ((step % ts->parent_solver->export_frequency) == 0) {
				// Create the viewers and save the vectors
				PetscViewer Viewer = NULL;
				PetscChar filename[WORDSZ];
				if (grid->parent_grid->ndim == DIM2) {
					sprintf(filename, "sol_%06d.h5", step);
					ierr = PetscViewerHDF5Open(PETSC_COMM_WORLD, filename, FILE_MODE_WRITE, &Viewer); CHKERRQ(ierr);
				} else if (grid->parent_grid->ndim == DIM1) {
					sprintf(filename, "sol_%06d.dat", step);
					ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, filename, &Viewer); CHKERRQ(ierr);
					ierr = PetscViewerPushFormat(Viewer, PETSC_VIEWER_ASCII_MATLAB); CHKERRQ(ierr);
				} else {
					ierr = PetscPrintf(PETSC_COMM_WORLD, "Wrong dimension for solution output.\n"); CHKERRQ(ierr);
					ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
				}
				ierr = VecView(grid->x, Viewer); CHKERRQ(ierr);
				ierr = VecView(grid->y, Viewer); CHKERRQ(ierr);
				ierr = VecView(grid->unknowns, Viewer); CHKERRQ(ierr);
				ierr = PetscViewerDestroy(&Viewer); CHKERRQ(ierr);
				// Compute at this frequency too an L2 error if an exact solution exists
				if (grid->hasExact) {
					ierr = VecDuplicate(grid->unknowns, &c); CHKERRQ(ierr);
					ierr = VecCopy(grid->unknowns, c); CHKERRQ(ierr);
					ierr = VecAXPY(c, -1.0, grid->exact); CHKERRQ(ierr);
					ierr = VecNorm(c, NORM_2, &nL2diff); CHKERRQ(ierr);
					ierr = VecNorm(grid->unknowns, NORM_2, &nL2sol); CHKERRQ(ierr);
					errorL2 = nL2diff / nL2sol;
					ierr = PetscPrintf(PETSC_COMM_WORLD, "\t --> At this time, error L2 is %.2e\t\n", errorL2); CHKERRQ(ierr);
					ierr = VecDestroy(&c); CHKERRQ(ierr);
				}
			}
		}
	} // end of loop on iterations

	ierr = MatDestroy(&R); CHKERRQ(ierr);
	ierr = MatDestroy(&identity); CHKERRQ(ierr);

	return ierr;
}

/* Method to destroy, i.e. deallocate, a one-dimensional transport solver
*/
void destroy_transportSolver2D(TransportSolver2D *ts, Grid2D *grid)
{
	PetscInt nBocosPnts = 2 * grid->parent_grid->nx + 2 * grid->ny;

	destroy_rkParams(ts->parent_solver->rk_params);
	free(ts->parent_solver->dir_rows);
	free(ts->parent_solver->neu_rows);
	if ((ts->parent_solver->hasFilter) &&
		(ts->super_las == NULL)) {
		MatDestroy(&(ts->filter));
	}
	free(ts->parent_solver);
	for (int i=0; i<nBocosPnts; i++) {
		if (ts->neumanns[i] != NULL) {
			VecDestroy(&(ts->neumanns[i]));
		}
	}
	free(ts->neumanns);
	for (int i=0; i<nBocosPnts; i++) {
		if (ts->robins[i] != NULL) {
			VecDestroy(&(ts->robins[i]));
		}
	}
	free(ts->robins);
	if (ts->super_las == NULL) MatDestroy(&(ts->rhs));
	free(ts);
}
