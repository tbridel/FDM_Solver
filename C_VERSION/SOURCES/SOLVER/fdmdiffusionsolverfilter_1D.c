/* This file contains the methods handling the filters for a
   one-dimensional solver for a diffusion equation
   @author : Thibault Bridel-Bertomeu
   @license: MIT
   @status : development
*/

#include "fdmsolver.h"

/* This method yields the filter matrix for a one dimensional diffusion solver based
   on the name of the filter scheme.
   ** DUD **
*/
void get_ds1D_filter(DiffusionSolver1D *ds, Grid1D *grid)
{

}

/* Method to set the filter matrix of a 1D diffusion solver for periodicity
   ** DUD **
*/
PetscErrorCode set_periodic_filter_ds1D(DiffusionSolver1D *ds, Grid1D* grid)
{
	PetscErrorCode ierr = 0;

	return ierr;
}

/* Method to set the filter matrix of a 1D diffusion solver for open boundary condition
   ** DUD **
*/
PetscErrorCode set_open_filter_ds1D(DiffusionSolver1D *ds, Grid1D* grid, PetscBool ismin)
{
	PetscErrorCode ierr = 0;

	return ierr;
}
