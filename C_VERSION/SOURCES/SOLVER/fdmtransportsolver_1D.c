/* This file contains the methods handling the one-dimensional solver for a transport equation
   @author : Thibault Bridel-Bertomeu
   @license: MIT
   @status : development
*/

#include "fdmsolver.h"
#ifdef LARGEST_STENCIL
#undef LARGEST_STENCIL
#endif
#define LARGEST_STENCIL 11

/* This method initializes a solver for the transport equation in 1D
*/
TransportSolver1D* init_transportSolver1D(PetscInt nequation_args, PetscChar equation_arguments[TWENTY*2][WORDSZ], Grid1D *grid, LinearAcousticsSolver1D *super_las, PetscInt mylasnumber)
{
	int eq_index;
	PetscChar tv_string[WORDSZ] = "";
	TransportSolver1D *ts = (TransportSolver1D*) malloc(sizeof(TransportSolver1D));

	// Initialize the parent
	PetscPrintf(PETSC_COMM_WORLD, "\tInitializing parent solver ...\n");
	ts->parent_solver = init_Solver(nequation_args, equation_arguments, (PetscInt)2);
	ts->ierr = ts->parent_solver->ierr;

	// Initialize the vectors for the Neumann boundary conditions
	ts->neumanns = (Vec*) malloc(DIM2 * sizeof(Vec));
	ts->neumanns[0] = NULL; ts->neumanns[1] = NULL;

	// Initialize the vectors for the Robin boundary conditions
	ts->robins = (Vec*) malloc(DIM2 * sizeof(Vec));
	ts->robins[0] = NULL; ts->robins[1] = NULL;

	// Get the transport velocity from the user inputs
	PetscPrintf(PETSC_COMM_WORLD, "\tAcquiring transport velocity ...\n");
	for (int i=0; i < nequation_args; i++) {
		if (strstr(equation_arguments[i], "parameters") != NULL) {
			eq_index = ( strchr(equation_arguments[i], '=') - equation_arguments[i] ) + 1;
			strcpy(tv_string, equation_arguments[i] + (eq_index + 1));
		}
	}
	if (sscanf(tv_string, "[%lf,]", ts->tv) != 1) {
		PetscPrintf(PETSC_COMM_WORLD, "For a 1D transport equation, a 1-elements transport velocity vector such as [X,] must be input.\n");
		ts->ierr = PETSC_ERR_ARG_WRONG;
	}

	// Store the CFL number
	ts->cfl[0] = ts->tv[0] * ts->parent_solver->dt / grid->parent_grid->dx;

	if (super_las == NULL) {
		// Initialize a few things to be duds
		ts->super_las = NULL;
		ts->mylasnumber = 1;

		// Call the routine to build the RHS matrix based on numerical scheme
		get_ts1D_rhs(ts, grid);

		// Call the routine to build the filter matrix based on numerical scheme
		if (ts->parent_solver->hasFilter) {
			get_ts1D_filter(ts, grid);
		}
	} else {
		ts->super_las = super_las;
		ts->mylasnumber = mylasnumber;
	}

	// Return the solver
	return ts;
}

/* This method yields the RHS matrix for a one dimensional transport solver based
   on the name of the scheme.
*/
void get_ts1D_rhs(TransportSolver1D *ts, Grid1D *grid)
{
	PetscInt idx, rstart, rend, nlocal;
	PetscInt *cols = NULL, vecsize = 0;
	PetscMPIInt size;

	// Get info on the MPI layout
	ts->ierr = MPI_Comm_size(PETSC_COMM_WORLD, &size);

	// Get parallel layout of the grid->unknowns vector
	ts->ierr = VecGetSize(grid->unknowns, &vecsize);
	if ((vecsize == grid->nvtx) ||
		(ts->super_las != NULL)) {
		ts->ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend);
		ts->ierr = VecGetLocalSize(grid->unknowns, &nlocal);
	}

	// Prepare the RHS matrix
	if (ts->super_las == NULL) {
		PetscPrintf(PETSC_COMM_WORLD, "\tCreating RHS distributed AIJ matrix ...\n");
		ts->ierr = MatCreate(PETSC_COMM_WORLD, &(ts->rhs));
		if (vecsize == grid->nvtx) {
			ts->ierr = MatSetSizes(ts->rhs, nlocal, nlocal, grid->nvtx, grid->nvtx);
		} else {
			ts->ierr = MatSetSizes(ts->rhs, PETSC_DECIDE, PETSC_DECIDE, grid->nvtx, grid->nvtx);
		}
		ts->ierr = MatSetType(ts->rhs, MATAIJ);
		if (size==1) {
			ts->ierr = MatSeqAIJSetPreallocation(ts->rhs, LARGEST_STENCIL, NULL);
		} else {
			ts->ierr = MatMPIAIJSetPreallocation(ts->rhs, LARGEST_STENCIL, NULL, LARGEST_STENCIL, NULL);
		}
		if (vecsize != grid->nvtx) {
			ts->ierr = MatGetOwnershipRange(ts->rhs, &rstart, &rend);
		}
	}

	// Condition on the scheme now
	if (ts->super_las == NULL) PetscPrintf(PETSC_COMM_WORLD, "\tFilling the diagonals of the RHS matrix ...\n");
	if ((strcmp(ts->parent_solver->scheme_name, "RK4o6s_CSo11p") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4CSo11p") == 0)) {
		// Fill the vector of coefficients
		cols = (PetscInt*) malloc(11 * sizeof(PetscInt));
		PetscScalar values[11] = {-0.002484594688, 0.020779405824, -0.090320001280, 0.286511173973, -0.872756993962,
								  0.0,
								  0.872756993962, -0.286511173973, 0.090320001280, -0.020779405824, 0.002484594688};
		if (ts->super_las != NULL) {
			for (int i=0; i<11; i++) {
				values[i] = values[i] * ts->tv[0] / grid->parent_grid->dx;
			}
		}
		// Now loop on the inner points, skip the boundaries for now
		for (idx = rstart; idx < rend; idx ++){
			if (ts->super_las != NULL) {
				if ((idx-5 >= (ts->mylasnumber-1)*grid->nvtx) && (idx+5 <= ts->mylasnumber*grid->nvtx-1)) {
					PetscInt shift = (ts->mylasnumber==1)?grid->nvtx:-grid->nvtx;
					//
					cols[0]=idx-5+shift; cols[1]=idx-4+shift; cols[2]=idx-3+shift; cols[3]=idx-2+shift; cols[4]=idx-1+shift;
					cols[5]=idx+shift;
					cols[6]=idx+1+shift; cols[7]=idx+2+shift; cols[8]=idx+3+shift; cols[9]=idx+4+shift; cols[10]=idx+5+shift;
					//
					ts->super_las->ierr = MatSetValues(ts->super_las->rhs,1,&idx,11,cols,values,INSERT_VALUES);
				}
			} else {
				if ((idx-5 >= 0) && (idx+5 <= grid->nvtx-1)) {
					cols[0]=idx-5; cols[1]=idx-4; cols[2]=idx-3; cols[3]=idx-2; cols[4]=idx-1;
					cols[5]=idx;
					cols[6]=idx+1; cols[7]=idx+2; cols[8]=idx+3; cols[9]=idx+4; cols[10]=idx+5;
					ts->ierr = MatSetValues(ts->rhs,1,&idx,11,cols,values,INSERT_VALUES);
				}
			}
		}
	} else if ((strcmp(ts->parent_solver->scheme_name, "RK4o6s_CS") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4CS") == 0)) {
		// Fill the vector of coefficients
		cols = (PetscInt*) malloc(3 * sizeof(PetscInt));
		PetscScalar values[3] = {-0.5,
								 0.0,
								 0.5};
		if (ts->super_las != NULL) {
			for (int i=0; i<3; i++) {
				values[i] = values[i] * ts->tv[0] / grid->parent_grid->dx;
			}
		}
		// Now loop on the inner points, skip the boundaries for now
		for (idx = rstart; idx < rend; idx ++){
			if (ts->super_las != NULL) {
				if ((idx-1 >= (ts->mylasnumber-1)*grid->nvtx) && (idx+1 <= ts->mylasnumber*grid->nvtx-1)) {
					PetscInt shift = (ts->mylasnumber==1)?grid->nvtx:-grid->nvtx;
					//
					cols[0]=idx-1+shift;
					cols[1]=idx+shift;
					cols[2]=idx+1+shift;
					//
					ts->super_las->ierr = MatSetValues(ts->super_las->rhs,1,&idx,3,cols,values,INSERT_VALUES);
				}
			} else {
				if ((idx-1 >= 0) && (idx+1 <= grid->nvtx-1)) {
					cols[0]=idx-1;
					cols[1]=idx;
					cols[2]=idx+1;
					ts->ierr = MatSetValues(ts->rhs,1,&idx,3,cols,values,INSERT_VALUES);
				}
			}
		}
	} else {
		PetscPrintf(PETSC_COMM_WORLD, "Unknown spatial scheme.\n");
		ts->ierr = PETSC_ERR_ARG_WRONG;
	}

	free(cols);
}

/* This method is used to apply all kind of boundary condition onto
   the matrix(ces) of a 1D transport solver
*/
PetscErrorCode applyBocos_ts1D(PetscInt nboco_arguments, PetscChar boco_arguments[2*TWENTY][WORDSZ], Grid1D *grid, TransportSolver1D *ts)
{
	int eq_index;
	PetscBool ismin = true;
	PetscChar bocotype[WORDSZ] = "";
	PetscErrorCode ierr = 0;

	// Parse the boundary condition arguments for proper assignment
	for (int i=0; i < nboco_arguments; i++) {
		// Get the type of boundary condition for the current boundary
		if (strstr(boco_arguments[i], "xmin") != NULL) {
			ismin = true;
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else if (strstr(boco_arguments[i], "xmax") != NULL) {
			ismin = false;
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown boundary assignement.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
		// Switch on the type of boundary condition required by the user
		if (strcmp(bocotype, "periodic") == 0) {
			ierr = set_periodic_ts1D(ts, grid); CHKERRQ(ierr);
			if (ts->parent_solver->hasFilter) {
				ierr = set_periodic_filter_ts1D(ts, grid); CHKERRQ(ierr);
			}
			// Once periodic has been called, can break the for loop
			break;
		} else if (strcmp(bocotype, "dirichlet") == 0) {
			ierr = set_dirichlet_ts1D(ts, grid, ismin); CHKERRQ(ierr);
			if (ts->parent_solver->hasFilter) {
				ierr = set_open_filter_ts1D(ts, grid, ismin); CHKERRQ(ierr);
			}
		} else if (strcmp(bocotype, "neumann") == 0) {
			ierr = set_neumann_ts1D(ts, grid, ismin); CHKERRQ(ierr);
			if (ts->parent_solver->hasFilter) {
				ierr = set_open_filter_ts1D(ts, grid, ismin); CHKERRQ(ierr);
			}
		} else if (startsWith(bocotype, "robin")) {
			PetscScalar robA = 0.0, robB = 0.0;
			sscanf(bocotype, "robin,%lf,%lf", &robA, &robB);
			ierr = set_robin_ts1D(ts, grid, ismin, robA, robB); CHKERRQ(ierr);
			if (ts->parent_solver->hasFilter) {
				ierr = set_open_filter_ts1D(ts, grid, ismin); CHKERRQ(ierr);
			}
		} else if (strcmp(bocotype, "open") == 0) {
			ierr = set_open_ts1D(ts, grid, ismin); CHKERRQ(ierr);
			if (ts->parent_solver->hasFilter) {
				ierr = set_open_filter_ts1D(ts, grid, ismin); CHKERRQ(ierr);
			}
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown boundary condition type.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
	}

	if (ts->super_las == NULL) {
		// Assemble the RHS matrix
		ierr = PetscPrintf(PETSC_COMM_WORLD, "\tAssembling the RHS matrix ...\n"); CHKERRQ(ierr);
		ierr = MatAssemblyBegin(ts->rhs, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
		ierr = MatAssemblyEnd(ts->rhs, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

		// Assemble the filter matrix
		if (ts->parent_solver->hasFilter) {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "\tAssembling the filter matrix ...\n"); CHKERRQ(ierr);
			ierr = MatAssemblyBegin(ts->filter, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
			ierr = MatAssemblyEnd(ts->filter, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
		}
	}

	return ierr;
}

/* Method to set the RHS matrix of a 1D transport solver for periodicity
*/
PetscErrorCode set_periodic_ts1D(TransportSolver1D* ts, Grid1D* grid)
{
	PetscErrorCode ierr = 0;
	PetscInt idx, rstart, rend;
	PetscInt *cols = NULL, vecsize = 0;

	// Get parallel layout of the grid->unknowns vector
	ierr = VecGetSize(grid->unknowns, &vecsize); CHKERRQ(ierr);
	if ((vecsize == grid->nvtx) ||
		(ts->super_las != NULL)) {
		ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend); CHKERRQ(ierr);
	} else {
		ierr = MatGetOwnershipRange(ts->rhs, &rstart, &rend); CHKERRQ(ierr);
	}

	ierr = PetscPrintf(PETSC_COMM_WORLD, "\tImposing periodicity conditions ...\n"); CHKERRQ(ierr);
	if ((strcmp(ts->parent_solver->scheme_name, "RK4o6s_CSo11p") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4CSo11p") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4_CSo11p") == 0)) {
		// Fill the vector of coefficients
		cols = (PetscInt*) malloc(11 * sizeof(PetscInt));
		PetscScalar values[11] = {-0.002484594688, 0.020779405824, -0.090320001280, 0.286511173973, -0.872756993962,
								  0.0,
								  0.872756993962, -0.286511173973, 0.090320001280, -0.020779405824, 0.002484594688};
		if (ts->super_las != NULL) {
			for (int i=0; i<LARGEST_STENCIL; i++) {
				values[i] = values[i] * ts->tv[0] / grid->parent_grid->dx;
			}
		}
		// Now loop on the points closest to the boundaries
		PetscInt shift = 0;
		if (ts->super_las != NULL) shift = (ts->mylasnumber==1)?grid->nvtx:-grid->nvtx;
		for (idx = rstart; idx < rend; idx ++){
			if ((idx-5 < (ts->mylasnumber-1)*grid->nvtx) &&
				(idx >= (ts->mylasnumber-1)*grid->nvtx)) {
				if (idx == 0+(ts->mylasnumber-1)*grid->nvtx){
					cols[0]=ts->mylasnumber*grid->nvtx-5+shift; cols[1]=ts->mylasnumber*grid->nvtx-4+shift; cols[2]=ts->mylasnumber*grid->nvtx-3+shift; cols[3]=ts->mylasnumber*grid->nvtx-2+shift; cols[4]=ts->mylasnumber*grid->nvtx-1+shift;
				} else if (idx == 1+(ts->mylasnumber-1)*grid->nvtx){
					cols[0]=ts->mylasnumber*grid->nvtx-4+shift; cols[1]=ts->mylasnumber*grid->nvtx-3+shift; cols[2]=ts->mylasnumber*grid->nvtx-2+shift; cols[3]=ts->mylasnumber*grid->nvtx-1+shift; cols[4]=idx-1+shift;
				} else if (idx == 2+(ts->mylasnumber-1)*grid->nvtx){
					cols[0]=ts->mylasnumber*grid->nvtx-3+shift; cols[1]=ts->mylasnumber*grid->nvtx-2+shift; cols[2]=ts->mylasnumber*grid->nvtx-1+shift; cols[3]=idx-2+shift; cols[4]=idx-1+shift;
				} else if (idx == 3+(ts->mylasnumber-1)*grid->nvtx){
					cols[0]=ts->mylasnumber*grid->nvtx-2+shift; cols[1]=ts->mylasnumber*grid->nvtx-1+shift; cols[2]=idx-3+shift; cols[3]=idx-2+shift; cols[4]=idx-1+shift;
				} else if (idx == 4+(ts->mylasnumber-1)*grid->nvtx){
					cols[0]=ts->mylasnumber*grid->nvtx-1+shift; cols[1]=idx-4+shift; cols[2]=idx-3+shift; cols[3]=idx-2+shift; cols[4]=idx-1+shift;
				}
				cols[5]=idx+shift;
				cols[6]=idx+1+shift; cols[7]=idx+2+shift; cols[8]=idx+3+shift; cols[9]=idx+4+shift; cols[10]=idx+5+shift;
			} else if ((idx+5 > ts->mylasnumber*grid->nvtx-1) &&
					   (idx <= ts->mylasnumber*grid->nvtx-1)) {
				cols[0]=idx-5+shift; cols[1]=idx-4+shift; cols[2]=idx-3+shift; cols[3]=idx-2+shift; cols[4]=idx-1+shift;
				cols[5]=idx+shift;
				if (idx==(ts->mylasnumber*grid->nvtx-5)){
					cols[6]=idx+1+shift; cols[7]=idx+2+shift; cols[8]=idx+3+shift; cols[9]=idx+4+shift; cols[10]=(ts->mylasnumber-1)*grid->nvtx+0+shift;
				} else if (idx==(ts->mylasnumber*grid->nvtx-4)){
					cols[6]=idx+1+shift; cols[7]=idx+2+shift; cols[8]=idx+3+shift; cols[9]=(ts->mylasnumber-1)*grid->nvtx+0+shift; cols[10]=(ts->mylasnumber-1)*grid->nvtx+1+shift;
				} else if (idx==(ts->mylasnumber*grid->nvtx-3)){
					cols[6]=idx+1+shift; cols[7]=idx+2+shift; cols[8]=(ts->mylasnumber-1)*grid->nvtx+0+shift; cols[9]=(ts->mylasnumber-1)*grid->nvtx+1+shift; cols[10]=(ts->mylasnumber-1)*grid->nvtx+2+shift;
				} else if (idx==(ts->mylasnumber*grid->nvtx-2)){
					cols[6]=idx+1+shift; cols[7]=(ts->mylasnumber-1)*grid->nvtx+0+shift; cols[8]=(ts->mylasnumber-1)*grid->nvtx+1+shift; cols[9]=(ts->mylasnumber-1)*grid->nvtx+2+shift; cols[10]=(ts->mylasnumber-1)*grid->nvtx+3+shift;
				} else if (idx==(ts->mylasnumber*grid->nvtx-1)){
					cols[6]=(ts->mylasnumber-1)*grid->nvtx+0+shift; cols[7]=(ts->mylasnumber-1)*grid->nvtx+1+shift; cols[8]=(ts->mylasnumber-1)*grid->nvtx+2+shift; cols[9]=(ts->mylasnumber-1)*grid->nvtx+3+shift; cols[10]=(ts->mylasnumber-1)*grid->nvtx+4+shift;
				}
			} else {
				continue;
			}
			if (ts->super_las != NULL) {
				ierr = MatSetValues(ts->super_las->rhs,1,&idx,11,cols,values,INSERT_VALUES); CHKERRQ(ierr);
			} else {
				ierr = MatSetValues(ts->rhs,1,&idx,11,cols,values,INSERT_VALUES); CHKERRQ(ierr);
			}
		}
		free(cols);
	} else if ((strcmp(ts->parent_solver->scheme_name, "RK4o6s_CS") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4CS") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4_CS") == 0)) {
		// Fill the vector of coefficients
		cols = (PetscInt*) malloc(3 * sizeof(PetscInt));
		PetscScalar values[3] = {-0.5,
								 0.0,
								 0.5};
		if (ts->super_las != NULL) {
			for (int i=0; i<3; i++) {
				values[i] = values[i] * ts->tv[0] / grid->parent_grid->dx;
			}
		}
		// Now loop on the points closest to the boundaries
		PetscInt shift = 0;
		if (ts->super_las != NULL) shift = (ts->mylasnumber==1)?grid->nvtx:-grid->nvtx;
		for (idx = rstart; idx < rend; idx ++){
			if ((idx-1 < (ts->mylasnumber-1)*grid->nvtx) &&
				(idx >= (ts->mylasnumber-1)*grid->nvtx)) {
				cols[0]=ts->mylasnumber*grid->nvtx-1+shift;
				cols[1]=idx+shift;
				cols[2]=idx+1+shift;
			} else if ((idx+1 > ts->mylasnumber*grid->nvtx-1) &&
					   (idx <= ts->mylasnumber*grid->nvtx-1)) {
				cols[0]=idx-1+shift;
				cols[1]=idx+shift;
				cols[2]=(ts->mylasnumber-1)*grid->nvtx+0+shift;
			} else {
				continue;
			}
			if (ts->super_las != NULL) {
				ierr = MatSetValues(ts->super_las->rhs,1,&idx,3,cols,values,INSERT_VALUES); CHKERRQ(ierr);
			} else {
				ierr = MatSetValues(ts->rhs,1,&idx,3,cols,values,INSERT_VALUES); CHKERRQ(ierr);
			}
		}
		free(cols);
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown spatial scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}

	return ierr;
}

/* Method to set the RHS matrix of a 1D transport solver for open boundary condition
*/
PetscErrorCode set_open_ts1D(TransportSolver1D* ts, Grid1D* grid, PetscBool ismin)
{
	PetscErrorCode ierr = 0;
	PetscInt idx, rstart, rend;
	PetscInt *cols = NULL, vecsize = 0;
	PetscScalar *values = NULL;

	// Get parallel layout of the grid->unknowns vector
	ierr = VecGetSize(grid->unknowns, &vecsize); CHKERRQ(ierr);
	if ((vecsize == grid->nvtx) ||
	    (ts->super_las != NULL)) {
		ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend); CHKERRQ(ierr);
	} else {
		ierr = MatGetOwnershipRange(ts->rhs, &rstart, &rend); CHKERRQ(ierr);
	}

	ierr = PetscPrintf(PETSC_COMM_WORLD, "\tImposing open conditions ...\n"); CHKERRQ(ierr);
	if ((strcmp(ts->parent_solver->scheme_name, "RK4o6s_CSo11p") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4CSo11p") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4_CSo11p") == 0)) {
		// Fill the vector of coefficients
		cols = (PetscInt*) malloc(11 * sizeof(PetscInt));
		// Now loop on the points and stop only if close to the right boundary
		PetscInt shift = 0;
		if (ts->super_las != NULL) shift = (ts->mylasnumber==1)?grid->nvtx:-grid->nvtx;
		for (idx = rstart; idx < rend; idx ++){
			if (ismin && ((idx-5 < (ts->mylasnumber-1)*grid->nvtx) &&
						  (idx >= (ts->mylasnumber-1)*grid->nvtx))) {
				for (int i=0; i<11; i++)
					cols[i] = (ts->mylasnumber-1)*grid->nvtx+i+shift;
				//
				if (idx == 0+(ts->mylasnumber-1)*grid->nvtx){
					values = (PetscScalar [11]) {-2.391602219538, 5.832490322294, -7.650218001182+0.000000000001, 7.907810563576, -5.922599052629,  3.071037015445, -1.014956769726, 0.170022256519, 0.002819958377, -0.004791009708, -0.000013063429};
				} else if (idx == 1+(ts->mylasnumber-1)*grid->nvtx){
					values = (PetscScalar [11]) {-0.180022054228, -1.237550583044, 2.484731692990, -1.810320814061, 1.112990048440, -0.481086916514, 0.126598690230, -0.015510730165, 0.000024609059-0.000003, 0.000156447571-0.000000000001, -0.000007390277};
				} else if (idx == 2+(ts->mylasnumber-1)*grid->nvtx){
					values = (PetscScalar [11]) {0.057982271137, -0.536135360383, -0.264089548967+0.000000000002, 0.917445877606-0.000000000002, -0.169688364841, -0.029716326170, 0.029681617641, -0.005222483773, -0.000118806260, -0.000118806260, -0.000020069730};
				} else if (idx == 3+(ts->mylasnumber-1)*grid->nvtx){
					values = (PetscScalar [11]) {-0.013277273810, 0.115976072920, -0.617479187931, -0.274113948206+0.000000000002, 1.086208764655-0.000000000002, -0.402951626982, 0.131066986242, -0.028154858354, 0.002596328316, 0.000128743150, 0.};
				} else if (idx == 4+(ts->mylasnumber-1)*grid->nvtx){
					values = (PetscScalar [11]) {0.016756572303, -0.117478455239, 0.411034935097, -1.130286765151, 0.341435872100-0.000000000001, 0.556396830543, -0.082525734207, 0.003565834658, 0.001173034777, -0.000071772671+0.000000000064, -0.000000352273};
				}
				if (ts->super_las != NULL) {
					for (int i=0; i<11; i++) {
						values[i] = values[i] * ts->tv[0] / grid->parent_grid->dx;
					}
				}
			} else if (!ismin && ((idx+5 > ts->mylasnumber*grid->nvtx-1) &&
					   			  (idx <= ts->mylasnumber*grid->nvtx-1))) {
				for (int i=0; i<11; i++)
					cols[i] = ts->mylasnumber*grid->nvtx-1-i+shift;
				//
				if (idx==(ts->mylasnumber*grid->nvtx-1)){
					values = (PetscScalar [11]) {-2.391602219538, 5.832490322294, -7.650218001182+0.000000000001, 7.907810563576, -5.922599052629,  3.071037015445, -1.014956769726, 0.170022256519, 0.002819958377, -0.004791009708, -0.000013063429};
				} else if (idx==(ts->mylasnumber*grid->nvtx-2)){
					values = (PetscScalar [11]) {-0.180022054228, -1.237550583044, 2.484731692990, -1.810320814061, 1.112990048440, -0.481086916514, 0.126598690230, -0.015510730165, 0.000024609059-0.000003, 0.000156447571-0.000000000001, -0.000007390277};
				} else if (idx==(ts->mylasnumber*grid->nvtx-3)){
					values = (PetscScalar [11]) {0.057982271137, -0.536135360383, -0.264089548967+0.000000000002, 0.917445877606-0.000000000002, -0.169688364841, -0.029716326170, 0.029681617641, -0.005222483773, -0.000118806260, -0.000118806260, -0.000020069730};
				} else if (idx==(ts->mylasnumber*grid->nvtx-4)){
					values = (PetscScalar [11]) {-0.013277273810, 0.115976072920, -0.617479187931, -0.274113948206+0.000000000002, 1.086208764655-0.000000000002, -0.402951626982, 0.131066986242, -0.028154858354, 0.002596328316, 0.000128743150, 0.};
				} else if (idx==(ts->mylasnumber*grid->nvtx-5)){
					values = (PetscScalar [11]) {0.016756572303, -0.117478455239, 0.411034935097, -1.130286765151, 0.341435872100-0.000000000001, 0.556396830543, -0.082525734207, 0.003565834658, 0.001173034777, -0.000071772671+0.000000000064, -0.000000352273};
				}
				//
				for (int i=0; i<11; i++) {
					values[i] = -values[i];
					if (ts->super_las != NULL) {
						values[i] = values[i] * ts->tv[0] / grid->parent_grid->dx;
					}
				}
			} else {
				continue;
			}
			if (ts->super_las != NULL) {
				ierr = MatSetValues(ts->super_las->rhs,1,&idx,11,cols,values,INSERT_VALUES); CHKERRQ(ierr);
			} else {
				ierr = MatSetValues(ts->rhs,1,&idx,11,cols,values,INSERT_VALUES); CHKERRQ(ierr);
			}
		}
	} else if ((strcmp(ts->parent_solver->scheme_name, "RK4o6s_CS") == 0) ||
	    	   (strcmp(ts->parent_solver->scheme_name, "RK4CS") == 0) ||
	    	   (strcmp(ts->parent_solver->scheme_name, "RK4_CS") == 0)) {
		// Fill the vector of coefficients
		cols = (PetscInt*) malloc(2 * sizeof(PetscInt));
		// Now loop on the points and stop only if close to the right boundary
		PetscInt shift = 0;
		if (ts->super_las != NULL) shift = (ts->mylasnumber==1)?grid->nvtx:-grid->nvtx;
		for (idx = rstart; idx < rend; idx ++){
			if (ismin && ((idx-1 < (ts->mylasnumber-1)*grid->nvtx) &&
						  (idx >= (ts->mylasnumber-1)*grid->nvtx))) {
				for (int i=0; i<2; i++)
					cols[i] = (ts->mylasnumber-1)*grid->nvtx+i+shift;
				//
				values = (PetscScalar [2]) {-1.0, 1.0};
				//
				if (ts->super_las != NULL) {
					for (int i=0; i<2; i++) {
						values[i] = values[i] * ts->tv[0] / grid->parent_grid->dx;
					}
				}
			} else if (!ismin && ((idx+1 > ts->mylasnumber*grid->nvtx-1) &&
					   			  (idx <= ts->mylasnumber*grid->nvtx-1))) {
				for (int i=0; i<2; i++)
					cols[i] = ts->mylasnumber*grid->nvtx-1-i+shift;
				//
				values = (PetscScalar [2]) {-1.0, 1.0};
				//
				for (int i=0; i<2; i++) {
					values[i] = -values[i];
					if (ts->super_las != NULL) {
						values[i] = values[i] * ts->tv[0] / grid->parent_grid->dx;
					}
				}
			} else {
				continue;
			}
			if (ts->super_las != NULL) {
				ierr = MatSetValues(ts->super_las->rhs,1,&idx,2,cols,values,INSERT_VALUES); CHKERRQ(ierr);
			} else {
				ierr = MatSetValues(ts->rhs,1,&idx,2,cols,values,INSERT_VALUES); CHKERRQ(ierr);
			}
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown spatial scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}

	free(cols);

	return ierr;
}

/* Method to set the RHS matrix of a 1D transport solver for an
   homogeneous dirichlet boundary condition
*/
PetscErrorCode set_dirichlet_ts1D(TransportSolver1D* ts, Grid1D* grid, PetscBool ismin)
{
	PetscErrorCode ierr = 0;

	// Start by applying an open boundary condition to have the
	// right schemes close to the boundary points
	ierr = set_open_ts1D(ts, grid, ismin);

	// Increment the number of rows to zero later during execution
	ts->parent_solver->ndir_rows ++;
	if (ismin) {
		ts->parent_solver->dir_rows[ts->parent_solver->ndir_rows] = grid->ixmin;
	} else {
		ts->parent_solver->dir_rows[ts->parent_solver->ndir_rows] = grid->ixmax;
	}

	return ierr;
}

/* Method to set the RHS matrix of a 1D transport solver for an
   homogeneous neumann boundary condition
*/
PetscErrorCode set_neumann_ts1D(TransportSolver1D* ts, Grid1D* grid, PetscBool ismin)
{
	PetscInt *rows = NULL;
	PetscErrorCode ierr = 0;
	PetscScalar *values = NULL;

	// Start by applying an open boundary condition to have the
	// right schemes close to the boundary points
	ierr = set_open_ts1D(ts, grid, ismin);

	// Increment the number of rows to zero later during execution
	if (ismin) {
		ts->parent_solver->neu_rows[0] = grid->ixmin;
	} else {
		ts->parent_solver->neu_rows[1] = grid->ixmax;
	}

	// Condition on the scheme for the proper computation of homogeneous Neumann corrections
	if ((strcmp(ts->parent_solver->scheme_name, "RK4o6s_CSo11p") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4CSo11p") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4_CSo11p") == 0)) {
		// Fill the vector of coefficients
		rows = (PetscInt*) malloc(11 * sizeof(PetscInt));
		// Now fix the right boundary
		if (ismin) {
			for (int i=0; i<11; i++)
				rows[i] = i;
			//
			values = (PetscScalar [11]) {-2.391602219538, 5.832490322294, -7.650218001182+0.000000000001, 7.907810563576, -5.922599052629,  3.071037015445, -1.014956769726, 0.170022256519, 0.002819958377, -0.004791009708, -0.000013063429};
			//
			for (int i=1; i<11; i++) {
				values[i] = - values[i] / values[0];
			}
			values[0] = 0.0;
			//
			ierr = VecCreate(PETSC_COMM_WORLD, &(ts->neumanns[0])); CHKERRQ(ierr);
			ierr = VecSetSizes(ts->neumanns[0], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
			ierr = VecSetUp(ts->neumanns[0]); CHKERRQ(ierr);
			ierr = VecSet(ts->neumanns[0], 0.0); CHKERRQ(ierr);
			ierr = VecSetValues(ts->neumanns[0],11,rows,values,INSERT_VALUES); CHKERRQ(ierr);
			ierr = VecAssemblyBegin(ts->neumanns[0]); CHKERRQ(ierr);
			ierr = VecAssemblyEnd(ts->neumanns[0]); CHKERRQ(ierr);
		} else {
			for (int i=0; i<11; i++)
				rows[i] = grid->nvtx-1-i;
			//
			values = (PetscScalar [11]) {-2.391602219538, 5.832490322294, -7.650218001182+0.000000000001, 7.907810563576, -5.922599052629,  3.071037015445, -1.014956769726, 0.170022256519, 0.002819958377, -0.004791009708, -0.000013063429};
			//
			for (int i=1; i<11; i++) {
				values[i] = - values[i] / values[0];
			}
			values[0] = 0.0;
			//
			ierr = VecCreate(PETSC_COMM_WORLD, &(ts->neumanns[1])); CHKERRQ(ierr);
			ierr = VecSetSizes(ts->neumanns[1], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
			ierr = VecSetUp(ts->neumanns[1]); CHKERRQ(ierr);
			ierr = VecSet(ts->neumanns[1], 0.0); CHKERRQ(ierr);
			ierr = VecSetValues(ts->neumanns[1],11,rows,values,INSERT_VALUES); CHKERRQ(ierr);
			ierr = VecAssemblyBegin(ts->neumanns[1]); CHKERRQ(ierr);
			ierr = VecAssemblyEnd(ts->neumanns[1]); CHKERRQ(ierr);
		}
	} else if ((strcmp(ts->parent_solver->scheme_name, "RK4o6s_CS") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4CS") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4_CS") == 0)) {
		// Fill the vector of coefficients
		rows = (PetscInt*) malloc(2 * sizeof(PetscInt));
		// Now fix the right boundary
		if (ismin) {
			for (int i=0; i<2; i++)
				rows[i] = i;
			//
			values = (PetscScalar [2]) {-1.0, 1.0};
			//
			for (int i=1; i<2; i++) {
				values[i] = - values[i] / values[0];
			}
			values[0] = 0.0;
			//
			ierr = VecCreate(PETSC_COMM_WORLD, &(ts->neumanns[0])); CHKERRQ(ierr);
			ierr = VecSetSizes(ts->neumanns[0], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
			ierr = VecSetUp(ts->neumanns[0]); CHKERRQ(ierr);
			ierr = VecSet(ts->neumanns[0], 0.0); CHKERRQ(ierr);
			ierr = VecSetValues(ts->neumanns[0],2,rows,values,INSERT_VALUES); CHKERRQ(ierr);
			ierr = VecAssemblyBegin(ts->neumanns[0]); CHKERRQ(ierr);
			ierr = VecAssemblyEnd(ts->neumanns[0]); CHKERRQ(ierr);
		} else {
			for (int i=0; i<2; i++)
				rows[i] = grid->nvtx-1-i;
			//
			values = (PetscScalar [2]) {-1.0, 1.0};
			//
			for (int i=1; i<2; i++) {
				values[i] = - values[i] / values[0];
			}
			values[0] = 0.0;
			//
			ierr = VecCreate(PETSC_COMM_WORLD, &(ts->neumanns[1])); CHKERRQ(ierr);
			ierr = VecSetSizes(ts->neumanns[1], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
			ierr = VecSetUp(ts->neumanns[1]); CHKERRQ(ierr);
			ierr = VecSet(ts->neumanns[1], 0.0); CHKERRQ(ierr);
			ierr = VecSetValues(ts->neumanns[1],2,rows,values,INSERT_VALUES); CHKERRQ(ierr);
			ierr = VecAssemblyBegin(ts->neumanns[1]); CHKERRQ(ierr);
			ierr = VecAssemblyEnd(ts->neumanns[1]); CHKERRQ(ierr);
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown spatial scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}

	free(rows);

	return ierr;
}

/* Method to set the RHS matrix of a 1D transport solver for an
   homogeneous robin boundary condition
*/
PetscErrorCode set_robin_ts1D(TransportSolver1D* ts, Grid1D* grid, PetscBool ismin, PetscScalar robA, PetscScalar robB)
{
	PetscInt *rows = NULL;
	PetscErrorCode ierr = 0;
	PetscScalar *values = NULL;

	// Start by applying an open boundary condition to have the
	// right schemes close to the boundary points
	ierr = set_open_ts1D(ts, grid, ismin);

	// Increment the number of rows to zero later during execution
	if (ismin) {
		ts->parent_solver->rob_rows[0] = grid->ixmin;
	} else {
		ts->parent_solver->rob_rows[1] = grid->ixmax;
	}

	// Condition on the scheme for the proper computation of homogeneous Neumann corrections
	if ((strcmp(ts->parent_solver->scheme_name, "RK4o6s_CSo11p") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4CSo11p") == 0) ||
	    (strcmp(ts->parent_solver->scheme_name, "RK4_CSo11p") == 0)) {
		// Fill the vector of coefficients
		rows = (PetscInt*) malloc(11 * sizeof(PetscInt));
		// Now fix the right boundary
		if (ismin) {
			for (int i=0; i<11; i++)
				rows[i] = i;
			//
			values = (PetscScalar [11]) {-2.391602219538, 5.832490322294, -7.650218001182+0.000000000001, 7.907810563576, -5.922599052629,  3.071037015445, -1.014956769726, 0.170022256519, 0.002819958377, -0.004791009708, -0.000013063429};
			for (int i=1; i<11; i++)
				values[i] *= robB;
			values[0] += grid->parent_grid->dx * robA;
			//
			for (int i=1; i<11; i++) {
				values[i] = - values[i] / values[0];
			}
			values[0] = 0.0;
			//
			ierr = VecCreate(PETSC_COMM_WORLD, &(ts->robins[0])); CHKERRQ(ierr);
			ierr = VecSetSizes(ts->robins[0], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
			ierr = VecSetUp(ts->robins[0]); CHKERRQ(ierr);
			ierr = VecSet(ts->robins[0], 0.0); CHKERRQ(ierr);
			ierr = VecSetValues(ts->robins[0],11,rows,values,INSERT_VALUES); CHKERRQ(ierr);
			ierr = VecAssemblyBegin(ts->robins[0]); CHKERRQ(ierr);
			ierr = VecAssemblyEnd(ts->robins[0]); CHKERRQ(ierr);
		} else {
			for (int i=0; i<11; i++)
				rows[i] = grid->nvtx-1-i;
			//
			values = (PetscScalar [11]) {-2.391602219538+grid->parent_grid->dx, 5.832490322294, -7.650218001182+0.000000000001, 7.907810563576, -5.922599052629,  3.071037015445, -1.014956769726, 0.170022256519, 0.002819958377, -0.004791009708, -0.000013063429};
			//
			for (int i=1; i<11; i++) {
				values[i] = - values[i] / values[0];
			}
			values[0] = 0.0;
			//
			ierr = VecCreate(PETSC_COMM_WORLD, &(ts->robins[1])); CHKERRQ(ierr);
			ierr = VecSetSizes(ts->robins[1], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
			ierr = VecSetUp(ts->robins[1]); CHKERRQ(ierr);
			ierr = VecSet(ts->robins[1], 0.0); CHKERRQ(ierr);
			ierr = VecSetValues(ts->robins[1],11,rows,values,INSERT_VALUES); CHKERRQ(ierr);
			ierr = VecAssemblyBegin(ts->robins[1]); CHKERRQ(ierr);
			ierr = VecAssemblyEnd(ts->robins[1]); CHKERRQ(ierr);
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown spatial scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}

	free(rows);

	return ierr;
}

/* Method for the execution of a one dimensional transport solver
*/
PetscErrorCode execute_ts1D(TransportSolver1D* ts, Grid1D* grid)
{
	Mat identity, R, mlist1, mlist2;
	PetscErrorCode ierr = 0;
	PetscScalar factor = 0.0, ukmax = 0.0, ukmin = 0.0;
	PetscScalar nL2diff = 0.0, nL2sol = 0.0, errorL2 = 0.0;
	PetscScalar neuscal = 0.0, robscal = 0.0;
	PetscInt step, idx, rstart, rend, nlocal, vecsize;
	Vec b, c;
	PetscMPIInt size;

	// Get info on the MPI layout
	ierr = MPI_Comm_size(PETSC_COMM_WORLD, &size); CHKERRQ(ierr);

	// Get parallel layout of the grid->unknowns vector
	ierr = VecGetSize(grid->unknowns, &vecsize); CHKERRQ(ierr);
	if (vecsize == grid->nvtx) {
		ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend); CHKERRQ(ierr);
		ierr = VecGetLocalSize(grid->unknowns, &nlocal); CHKERRQ(ierr);
	} else {
		ierr = MatGetOwnershipRange(ts->rhs, &rstart, &rend); CHKERRQ(ierr);
		ierr = MatGetLocalSize(ts->rhs, &nlocal, &nlocal); CHKERRQ(ierr);
	}

	// Create an identity matrix, it can come handy
	ierr = MatCreate(PETSC_COMM_WORLD, &identity); CHKERRQ(ierr);
	ierr = MatSetSizes(identity, nlocal, nlocal, grid->nvtx, grid->nvtx); CHKERRQ(ierr);
	ierr = MatSetType(identity, MATAIJ); CHKERRQ(ierr);
	ierr = MatSetUp(identity); CHKERRQ(ierr);
	for (idx = rstart; idx < rend ; idx++) {
		ierr = MatSetValue(identity, idx, idx, 1.0, INSERT_VALUES); CHKERRQ(ierr);
	}
	ierr = MatAssemblyBegin(identity, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
	ierr = MatAssemblyEnd(identity, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

	// Prepare a few other objects before the big loop
	// First pass the RHS actually on the right hand side and scale it by the space step
	ierr = MatScale(ts->rhs, -ts->tv[0]/grid->parent_grid->dx); CHKERRQ(ierr);
	// Then handle the preprocessing for the RK matrix
	if (ts->parent_solver->rk_params->has_gammas) {
		PetscPrintf(PETSC_COMM_WORLD, "\tPreparing Runge-Kutta matrix ...\n");
		// Create the matrix and copy the values from the RHS and assemble
		ierr = MatDuplicate(ts->rhs, MAT_COPY_VALUES, &R); CHKERRQ(ierr);
		ierr = MatScale(R, ts->parent_solver->dt); CHKERRQ(ierr);
		ierr = MatAssemblyBegin(R, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
		ierr = MatAssemblyEnd(R, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
		// First element of mlist is the RHS matrix
		ierr = MatDuplicate(ts->rhs, MAT_COPY_VALUES, &mlist1); CHKERRQ(ierr);
		// Now loop over the remaining RK stages to simulate matrix power
		for (idx = 1; idx < ts->parent_solver->rk_params->steps; idx++) {
			factor = PetscPowScalar(ts->parent_solver->dt, idx+1)*ts->parent_solver->rk_params->gammas[idx];
			ierr = MatMatMult(ts->rhs, mlist1, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &mlist2); CHKERRQ(ierr);
			ierr = MatDestroy(&mlist1); CHKERRQ(ierr);
			ierr = MatAXPY(R, factor, mlist2, DIFFERENT_NONZERO_PATTERN); CHKERRQ(ierr);
			ierr = MatDuplicate(mlist2, MAT_COPY_VALUES, &mlist1); CHKERRQ(ierr);
			ierr = MatDestroy(&mlist2); CHKERRQ(ierr);
		}
		// Destroy the mlist object
		MatDestroy(&mlist1);
		MatDestroy(&mlist2);
		// Add identity to R
		ierr = MatAXPY(R, 1.0, identity, DIFFERENT_NONZERO_PATTERN); CHKERRQ(ierr);
		// Change R structure in case of dirichlet boco
		{
			PetscInt *rows = (PetscInt*) malloc(sizeof(PetscInt) * (ts->parent_solver->ndir_rows+1));
			for (int i=0; i<(ts->parent_solver->ndir_rows+1); i++) {
				rows[i] = ts->parent_solver->dir_rows[i];
			}
			ierr = MatZeroRowsColumns(R, ts->parent_solver->ndir_rows+1, rows, 0.0, 0, 0); CHKERRQ(ierr);
			if (ts->parent_solver->hasFilter) {
				ierr = MatZeroRowsColumns(ts->filter, ts->parent_solver->ndir_rows+1, rows, 0.0, 0, 0); CHKERRQ(ierr);
			}
			free(rows);
		}
		// Change filter structure in case of Neumann boco or Robin boco
		if (ts->parent_solver->hasFilter) {
			for (int i=0; i<DIM2; i++) {
				if (ts->neumanns[i] != NULL) {
					ierr = MatZeroRows(ts->filter, 1, ts->parent_solver->neu_rows + i, 0.0, 0, 0); CHKERRQ(ierr);
				}
				if (ts->robins[i] != NULL) {
					ierr = MatZeroRows(ts->filter, 1, ts->parent_solver->rob_rows + i, 0.0, 0, 0); CHKERRQ(ierr);
				}
			}
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown definition of the Runge-Kutta scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}
	// Scale the filter by the strength input by the user
	if (ts->parent_solver->hasFilter) {
		ierr = MatScale(ts->filter, ts->parent_solver->filter_strength); CHKERRQ(ierr);
	}

	// Export the initial solution
	{
		step = 0;
		// Create the viewers and save the vectors
		PetscViewer Viewer = NULL;
		PetscChar filename[WORDSZ];
		if (grid->parent_grid->ndim == DIM2) {
			sprintf(filename, "sol_%06d.h5", step);
			ierr = PetscViewerHDF5Open(PETSC_COMM_WORLD, filename, FILE_MODE_WRITE, &Viewer); CHKERRQ(ierr);
		} else if (grid->parent_grid->ndim == DIM1) {
			sprintf(filename, "sol_%06d.dat", step);
			ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, filename, &Viewer); CHKERRQ(ierr);
			ierr = PetscViewerPushFormat(Viewer, PETSC_VIEWER_ASCII_MATLAB); CHKERRQ(ierr);
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Wrong dimension for solution output.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
		ierr = VecView(grid->x, Viewer); CHKERRQ(ierr);
		ierr = VecView(grid->unknowns, Viewer); CHKERRQ(ierr);
		ierr = PetscViewerDestroy(&Viewer); CHKERRQ(ierr);
	}

	// Loop on iterations
	ierr = PetscPrintf(PETSC_COMM_WORLD, "\tMarching the equation in time ...\n"); CHKERRQ(ierr);
	for (step = 1; step <= ts->parent_solver->nit; step ++) {
		// Implementation differs function of the way the RK scheme is made
		if (ts->parent_solver->rk_params->has_gammas) {
			// Get an auxiliary vector
			ierr = VecDuplicate(grid->unknowns, &b); CHKERRQ(ierr);
			// Apply the temporal scheme
			ierr = MatMult(R, grid->unknowns, b); CHKERRQ(ierr);
			// Save the result
			ierr = VecCopy(b, grid->unknowns); CHKERRQ(ierr);
			// Destroy b
			ierr = VecDestroy(&b); CHKERRQ(ierr);
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown definition of the Runge-Kutta scheme.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
		// Apply Neumann boundary conditions
		for (int i=0; i<DIM2; i++) {
			if (ts->neumanns[i] != NULL) {
				ierr = VecDot(grid->unknowns, ts->neumanns[i], &neuscal); CHKERRQ(ierr);
				ierr = VecSetValue(grid->unknowns, ts->parent_solver->neu_rows[i], neuscal, INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(grid->unknowns); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(grid->unknowns); CHKERRQ(ierr);
			}
		}
		// Apply Robin boundary conditions
		for (int i=0; i<DIM2; i++) {
			if (ts->robins[i] != NULL) {
				ierr = VecDot(grid->unknowns, ts->robins[i], &robscal); CHKERRQ(ierr);
				ierr = VecSetValue(grid->unknowns, ts->parent_solver->rob_rows[i], robscal, INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(grid->unknowns); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(grid->unknowns); CHKERRQ(ierr);
			}
		}
		// Filter the signal
		if (ts->parent_solver->hasFilter) {
			ierr = VecDuplicate(grid->unknowns, &b); CHKERRQ(ierr);
			ierr = MatMult(ts->filter, grid->unknowns, b); CHKERRQ(ierr);
			ierr = VecAXPY(grid->unknowns, -1.0, b); CHKERRQ(ierr);
			ierr = VecDestroy(&b); CHKERRQ(ierr);
		}
		// Tell a few things to the user
		ierr = VecMax(grid->unknowns, NULL, &ukmax); CHKERRQ(ierr);
		ierr = VecMin(grid->unknowns, NULL, &ukmin); CHKERRQ(ierr);
		ierr = PetscPrintf(PETSC_COMM_WORLD,
					"Iter. %05d\t Time %.6e\t Min/Max uk (%.4e,%.4e)\t\n",
					step, step*ts->parent_solver->dt, ukmin, ukmax); CHKERRQ(ierr);
		// Export the data
		if (ts->parent_solver->export_frequency > 0) {
			if ((step % ts->parent_solver->export_frequency) == 0) {
				// Create the viewers and save the vectors
				PetscViewer Viewer = NULL;
				PetscChar filename[WORDSZ];
				if (grid->parent_grid->ndim == DIM2) {
					sprintf(filename, "sol_%06d.h5", step);
					ierr = PetscViewerHDF5Open(PETSC_COMM_WORLD, filename, FILE_MODE_WRITE, &Viewer); CHKERRQ(ierr);
				} else if (grid->parent_grid->ndim == DIM1) {
					sprintf(filename, "sol_%06d.dat", step);
					ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, filename, &Viewer); CHKERRQ(ierr);
					ierr = PetscViewerPushFormat(Viewer, PETSC_VIEWER_ASCII_MATLAB); CHKERRQ(ierr);
				} else {
					ierr = PetscPrintf(PETSC_COMM_WORLD, "Wrong dimension for solution output.\n"); CHKERRQ(ierr);
					ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
				}
				ierr = VecView(grid->x, Viewer); CHKERRQ(ierr);
				ierr = VecView(grid->unknowns, Viewer); CHKERRQ(ierr);
				ierr = PetscViewerDestroy(&Viewer); CHKERRQ(ierr);
				// Compute at this frequency too an L2 error if an exact solution exists
				if (grid->hasExact) {
					ierr = VecDuplicate(grid->unknowns, &c); CHKERRQ(ierr);
					ierr = VecCopy(grid->unknowns, c); CHKERRQ(ierr);
					ierr = VecAXPY(c, -1.0, grid->exact); CHKERRQ(ierr);
					ierr = VecNorm(c, NORM_2, &nL2diff); CHKERRQ(ierr);
					ierr = VecNorm(grid->unknowns, NORM_2, &nL2sol); CHKERRQ(ierr);
					errorL2 = nL2diff / nL2sol;
					ierr = PetscPrintf(PETSC_COMM_WORLD, "\t --> At this time, error L2 is %.2e\t\n", errorL2); CHKERRQ(ierr);
					ierr = VecDestroy(&c); CHKERRQ(ierr);
				}
			}
		}
	} // end of loop on iterations

	ierr = MatDestroy(&R); CHKERRQ(ierr);
	ierr = MatDestroy(&identity); CHKERRQ(ierr);

	return ierr;
}

/* Method to destroy, i.e. deallocate, a one-dimensional transport solver
*/
void destroy_transportSolver1D(TransportSolver1D *ts)
{
	destroy_rkParams(ts->parent_solver->rk_params);
	free(ts->parent_solver->dir_rows);
	free(ts->parent_solver->neu_rows);
	if ((ts->parent_solver->hasFilter) &&
		(ts->super_las == NULL)) {
		MatDestroy(&(ts->filter));
	}
	free(ts->parent_solver);
	for (int i=0; i<DIM2; i++) {
		if (ts->neumanns[i] != NULL) {
			VecDestroy(&(ts->neumanns[i]));
		}
		if (ts->robins[i] != NULL) {
			VecDestroy(&(ts->robins[i]));
		}
	}
	free(ts->neumanns);
	free(ts->robins);
	if (ts->super_las == NULL) MatDestroy(&(ts->rhs));
	free(ts);
}
