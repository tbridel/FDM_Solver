/* This file contains the methods handling the filters for a
   two-dimensional solver for a transport equation
   @author : Thibault Bridel-Bertomeu
   @license: MIT
   @status : development
*/

#include "fdmsolver.h"
#ifndef LARGEST_STENCIL
#define LARGEST_STENCIL 21
#endif

/* This method yields the filter matrix for a two dimensional transport solver based
   on the name of the filter scheme.
*/
void get_ts2D_filter(TransportSolver2D *ts, Grid2D *grid)
{
	PetscInt idx, jdx, rstart, rend, nlocal, vecsize;
	PetscInt *cols = NULL;
	PetscInt nx = grid->parent_grid->nx, ny = grid->ny;
	PetscInt *result = (PetscInt*) malloc(2*sizeof(PetscInt)), ii, jj;
	PetscMPIInt size;

	// Get info on the MPI layout
	ts->ierr = MPI_Comm_size(PETSC_COMM_WORLD, &size);

	// Get parallel layout of the grid->unknowns vector
	ts->ierr = VecGetSize(grid->unknowns, &vecsize);
	if ((vecsize == grid->nvtx) ||
	    (ts->super_las != NULL)) {
		ts->ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend);
		ts->ierr = VecGetLocalSize(grid->unknowns, &nlocal);
	} else {
		ts->ierr = MatGetOwnershipRange(ts->rhs, &rstart, &rend);
		ts->ierr = MatGetLocalSize(ts->rhs, &nlocal, &nlocal);
	}

	// Prepare the RHS matrix
	if (ts->super_las == NULL) {
		PetscPrintf(PETSC_COMM_WORLD, "\tCreating filter distributed AIJ matrix ...\n");
		ts->ierr = MatCreate(PETSC_COMM_WORLD, &(ts->filter));
		ts->ierr = MatSetSizes(ts->filter, nlocal, nlocal, grid->nvtx, grid->nvtx);
		ts->ierr = MatSetType(ts->filter, MATAIJ);
	}

	// Condition on the scheme now
	if (ts->super_las == NULL) PetscPrintf(PETSC_COMM_WORLD, "\tFilling the diagonals of the filter matrix ...\n");
	if ((strcmp(ts->parent_solver->filter_name, "SFo11p") == 0)) {
		// Complete the matrix setup
		if (ts->super_las == NULL) {
			if (size==1) {
				ts->ierr = MatSeqAIJSetPreallocation(ts->filter, LARGEST_STENCIL, NULL);
			} else {
				ts->ierr = MatMPIAIJSetPreallocation(ts->filter, LARGEST_STENCIL, NULL, LARGEST_STENCIL, NULL);
			}
		}
		// Now loop on the inner points, skip the boundaries for now
		for (idx = rstart; idx < rend; idx ++) {
			if (idx <= ts->mylasnumber*grid->nvtx-1) {
				jdx = idx;
				if ((idx >= grid->nvtx) && (ts->super_las != NULL)) {
					jdx = idx - grid->nvtx;
				}
				unravel_index(jdx, nx, result); ii = result[0]; jj = result[1];
				//
				if ((ii-5 >= 0) && (ii+5 <= nx-1) &&
					(jj-5 >= 0) && (jj+5 <= ny-1)) {
					PetscScalar values[21] = {-0.002999540835, 0.018721609157, -0.059227575576, 0.123755948787, -0.187772883589,
									  		  -0.002999540835, 0.018721609157, -0.059227575576, 0.123755948787, -0.187772883589,
									  		  0.215044884112+0.215044884112,
									  		  -0.187772883589, 0.123755948787, -0.059227575576, 0.018721609157, -0.002999540835,
									  		  -0.187772883589, 0.123755948787, -0.059227575576, 0.018721609157, -0.002999540835};
					//
					cols = (PetscInt*) malloc(21 * sizeof(PetscInt));
					cols[0]=idx-5*nx; cols[1]=idx-4*nx; cols[2]=idx-3*nx; cols[3]=idx-2*nx; cols[4]=idx-1*nx;
					cols[5]=idx-5; cols[6]=idx-4; cols[7]=idx-3; cols[8]=idx-2; cols[9]=idx-1;
					cols[10]=idx;
					cols[11]=idx+1; cols[12]=idx+2; cols[13]=idx+3; cols[14]=idx+4; cols[15]=idx+5;
					cols[16]=idx+1*nx; cols[17]=idx+2*nx; cols[18]=idx+3*nx; cols[19]=idx+4*nx; cols[20]=idx+5*nx;
					//
					if (ts->super_las != NULL) {
						ts->super_las->ierr = MatSetValues(ts->super_las->filter, 1, &idx, 21, cols, values, INSERT_VALUES);
					} else {
						ts->ierr = MatSetValues(ts->filter, 1, &idx, 21, cols, values, INSERT_VALUES);
					}
					//
					free(cols); cols=NULL;
				} else if ((jj-5 >= 0) && (jj+5 <= ny-1)) {
					PetscScalar values[11] = {-0.002999540835, 0.018721609157, -0.059227575576, 0.123755948787, -0.187772883589,
									  		  0.215044884112,
									  		  -0.187772883589, 0.123755948787, -0.059227575576, 0.018721609157, -0.002999540835};
					//
					cols = (PetscInt*) malloc(11 * sizeof(PetscInt));
					cols[0]=idx-5*nx; cols[1]=idx-4*nx; cols[2]=idx-3*nx; cols[3]=idx-2*nx; cols[4]=idx-1*nx;
					cols[5]=idx;
					cols[6]=idx+1*nx; cols[7]=idx+2*nx; cols[8]=idx+3*nx; cols[9]=idx+4*nx; cols[10]=idx+5*nx;
					//
					if (ts->super_las != NULL) {
						ts->super_las->ierr = MatSetValues(ts->super_las->filter, 1, &idx, 11, cols, values, INSERT_VALUES);
					} else {
						ts->ierr = MatSetValues(ts->filter, 1, &idx, 11, cols, values, INSERT_VALUES);
					}
					//
					free(cols); cols=NULL;
				} else if ((ii-5 >= 0) && (ii+5 <= nx-1)) {
					PetscScalar values[11] = {-0.002999540835, 0.018721609157, -0.059227575576, 0.123755948787, -0.187772883589,
									  		  0.215044884112,
									  		  -0.187772883589, 0.123755948787, -0.059227575576, 0.018721609157, -0.002999540835};
					//
					cols = (PetscInt*) malloc(11 * sizeof(PetscInt));
					cols[0]=idx-5; cols[1]=idx-4; cols[2]=idx-3; cols[3]=idx-2; cols[4]=idx-1;
					cols[5]=idx;
					cols[6]=idx+1; cols[7]=idx+2; cols[8]=idx+3; cols[9]=idx+4; cols[10]=idx+5;
					//
					if (ts->super_las != NULL) {
						ts->super_las->ierr = MatSetValues(ts->super_las->filter, 1, &idx, 11, cols, values, INSERT_VALUES);
					} else {
						ts->ierr = MatSetValues(ts->filter, 1, &idx, 11, cols, values, INSERT_VALUES);
					}
					//
					free(cols); cols=NULL;
				}
			}
		} // end of for(idx = rstart; idx < rend ; idx++)
	} else {
		PetscPrintf(PETSC_COMM_WORLD, "Unknown filtering scheme.\n");
		ts->ierr = PETSC_ERR_ARG_WRONG;
	}

	if (cols != NULL) free(cols);
	free(result);
}

/* Method to set the filter matrix of a 2D transport solver for periodicity
*/
PetscErrorCode set_periodic_filter_ts2D(TransportSolver2D* ts, Grid2D* grid, PetscBool isx, PetscBool bothdirections)
{
	PetscErrorCode ierr = 0;
	PetscInt idx, jdx, rstart, rend, vecsize = 0;
	PetscInt nx = grid->parent_grid->nx, ny = grid->ny;
	PetscInt *cols = NULL;
	PetscInt *result = (PetscInt*)malloc(2*sizeof(PetscInt)), ii, jj;

	// Get parallel layout of the grid->unknowns vector
	ierr = VecGetSize(grid->unknowns, &vecsize); CHKERRQ(ierr);
	if ((vecsize == grid->nvtx) ||
		(ts->super_las != NULL)) {
		ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend); CHKERRQ(ierr);
	} else {
		ierr = MatGetOwnershipRange(ts->rhs, &rstart, &rend); CHKERRQ(ierr);
	}

	if ((strcmp(ts->parent_solver->filter_name, "SFo11p") == 0)) {
		// Fill the vector of coefficients
		cols = (PetscInt*) malloc(11 * sizeof(PetscInt));
		PetscScalar values[11] = {-0.002999540835, 0.018721609157, -0.059227575576, 0.123755948787, -0.187772883589,
								  0.215044884112,
								  -0.187772883589, 0.123755948787, -0.059227575576, 0.018721609157, -0.002999540835};
		if (bothdirections) values[5] = values[5] * 2.0;
		// Now loop on the points closest to the boundaries
		for (idx = rstart; idx < rend; idx ++){
			if (idx <= ts->mylasnumber*grid->nvtx-1) {
				jdx = idx;
				if ((idx >= grid->nvtx) && (ts->super_las != NULL)) {
					jdx = idx - grid->nvtx;
				}
				unravel_index(jdx, nx, result); ii = result[0]; jj = result[1];
				// Default assignement columns
				if (isx) {
					cols[0]=idx-5; cols[1]=idx-4; cols[2]=idx-3; cols[3]=idx-2; cols[4]=idx-1;
					cols[5]=idx;
					cols[6]=idx+1; cols[7]=idx+2; cols[8]=idx+3; cols[9]=idx+4; cols[10]=idx+5;
				} else {
					cols[0]=idx-5*nx; cols[1]=idx-4*nx; cols[2]=idx-3*nx; cols[3]=idx-2*nx; cols[4]=idx-1*nx;
					cols[5]=idx;
					cols[6]=idx+1*nx; cols[7]=idx+2*nx; cols[8]=idx+3*nx; cols[9]=idx+4*nx; cols[10]=idx+5*nx;
				}
				// Change function of the location
				if (isx && (ii < 5)) {
					if (ii == 0) {
						cols[0]=idx+nx-5; cols[1]=idx+nx-4; cols[2]=idx+nx-3; cols[3]=idx+nx-2; cols[4]=idx+nx-1;
					} else if (ii == 1) {
						cols[0]=idx+nx-5; cols[1]=idx+nx-4; cols[2]=idx+nx-3; cols[3]=idx+nx-2;
					} else if (ii == 2) {
						cols[0]=idx+nx-5; cols[1]=idx+nx-4; cols[2]=idx+nx-3;
					} else if (ii == 3) {
						cols[0]=idx+nx-5; cols[1]=idx+nx-4;
					} else if (ii == 4) {
						cols[0]=idx+nx-5;
					}
				} else if (isx && (ii > nx-6)) {
					if (ii == nx-1) {
						cols[6]=idx-nx+1; cols[7]=idx-nx+2; cols[8]=idx-nx+3; cols[9]=idx-nx+4; cols[10]=idx-nx+5;
					} else if (ii == nx-2) {
						cols[7]=idx-nx+2; cols[8]=idx-nx+3; cols[9]=idx-nx+4; cols[10]=idx-nx+5;
					} else if (ii == nx-3) {
						cols[8]=idx-nx+3; cols[9]=idx-nx+4; cols[10]=idx-nx+5;
					} else if (ii == nx-4) {
						cols[9]=idx-nx+4; cols[10]=idx-nx+5;
					} else if (ii == nx-5) {
						cols[10]=idx-nx+5;
					}
				} else if (!isx && (jj < 5)) {
					if (jj == 0) {
						cols[0]=idx+(ny-5)*nx; cols[1]=idx+(ny-4)*nx; cols[2]=idx+(ny-3)*nx; cols[3]=idx+(ny-2)*nx; cols[4]=idx+(ny-1)*nx;
					} else if (jj == 1) {
						cols[0]=idx+(ny-5)*nx; cols[1]=idx+(ny-4)*nx; cols[2]=idx+(ny-3)*nx; cols[3]=idx+(ny-2)*nx;
					} else if (jj == 2) {
						cols[0]=idx+(ny-5)*nx; cols[1]=idx+(ny-4)*nx; cols[2]=idx+(ny-3)*nx;
					} else if (jj == 3) {
						cols[0]=idx+(ny-5)*nx; cols[1]=idx+(ny-4)*nx;
					} else if (jj == 4) {
						cols[0]=idx+(ny-5)*nx;
					}
				} else if (!isx && (jj > ny-6)) {
					if (jj == ny-1) {
						cols[6]=idx+(1-ny)*nx; cols[7]=idx+(2-ny)*nx; cols[8]=idx+(3-ny)*nx; cols[9]=idx+(4-ny)*nx; cols[10]=idx+(5-ny)*nx;
					} else if (jj == ny-2) {
						cols[7]=idx+(2-ny)*nx; cols[8]=idx+(3-ny)*nx; cols[9]=idx+(4-ny)*nx; cols[10]=idx+(5-ny)*nx;
					} else if (jj == ny-3) {
						cols[8]=idx+(3-ny)*nx; cols[9]=idx+(4-ny)*nx; cols[10]=idx+(5-ny)*nx;
					} else if (jj == ny-4) {
						cols[9]=idx+(4-ny)*nx; cols[10]=idx+(5-ny)*nx;
					} else if (jj == ny-5) {
						cols[10]=idx+(5-ny)*nx;
					}
				} else {
					continue;
				}
				if (ts->super_las != NULL) {
					ierr = MatSetValues(ts->super_las->filter, 1, &idx, 11, cols, values, INSERT_VALUES); CHKERRQ(ierr);
				} else {
					ierr = MatSetValues(ts->filter, 1, &idx, 11, cols, values, INSERT_VALUES); CHKERRQ(ierr);
				}
			}
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown filter scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}

	free(cols);
	free(result);

	return ierr;
}

/* Method to set the filter matrix of a 2D transport solver for open boundary condition
*/
PetscErrorCode set_open_filter_ts2D(TransportSolver2D* ts, Grid2D* grid, PetscBool ismin, PetscBool isx)
{
	PetscErrorCode ierr = 0;
	PetscInt idx, jdx, rstart, rend, vecsize = 0;
	PetscInt *cols = NULL;
	PetscScalar *values = NULL;
	PetscInt *result = (PetscInt*)malloc(2*sizeof(PetscInt)), ii, jj;
	PetscInt nx = grid->parent_grid->nx, ny = grid->ny;

	// Get parallel layout of the grid->unknowns vector
	ierr = VecGetSize(grid->unknowns, &vecsize); CHKERRQ(ierr);
	if ((vecsize == grid->nvtx) ||
		(ts->super_las != NULL)) {
		ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend); CHKERRQ(ierr);
	} else {
		ierr = MatGetOwnershipRange(ts->rhs, &rstart, &rend); CHKERRQ(ierr);
	}

	if ((strcmp(ts->parent_solver->filter_name, "SFo11p") == 0)) {
		// Fill the vector of coefficients
		cols = (PetscInt*) malloc(11 * sizeof(PetscInt));
		// Now loop on the points and stop only if close to the right boundary
		for (idx = rstart; idx < rend; idx ++){
			if (idx <= ts->mylasnumber*grid->nvtx-1) {
				jdx = idx;
				if ((idx >= grid->nvtx) && (ts->super_las != NULL)) {
					jdx = idx - grid->nvtx;
				}
				unravel_index(jdx, nx, result); ii = result[0]; jj = result[1];
				if (ismin && isx && (ii < 5)) {
					if (ii == 0){
						for (int i=0; i<11; i++)
							cols[i] = idx+i;
						values = (PetscScalar [11]) {0.320882352941, -0.465, 0.179117647059, -0.035, 0., 0., 0., 0., 0., 0., 0.};
					} else if (ii == 1){
						for (int i=0; i<11; i++)
							cols[i] = idx+(i-1);
						values = (PetscScalar [11]) {-0.085777408970+0.000000000001, 0.277628171524, -0.356848072173, 0.223119093072, -0.057347064865, -0.000747264596, -0.000027453993, 0., 0., 0., 0.};
					} else if (ii == 2){
						for (int i=0; i<11; i++)
							cols[i] = idx+(i-2);
						values = (PetscScalar [11]) {0.0307159855992469, -0.148395705486028 , 0.312055385963757, -0.363202245195514, 0.230145457063431, -0.0412316564605079, -0.0531024700805787, 0.0494343261171287, -0.0198143585458560, 0.00339528102492129, 0.};
					} else if (ii == 3){
						for (int i=0; i<11; i++)
							cols[i] = idx+(i-3);
						values = (PetscScalar [11]) {-0.000054596010, 0.042124772446, -0.173103107841, 0.299615871352, -0.276543612935, 0.131223506571, -0.023424966418, 0.013937561779, -0.024565095706, 0.013098287852, -0.002308621090};
					} else if (ii == 4){
						for (int i=0; i<11; i++)
							cols[i] = idx+(i-4);
						values = (PetscScalar [11]) {0.008391235145, -0.047402506444, 0.121438547725, -0.200063042812, 0.240069047836, -0.207269200140-0.000000000001, 0.122263107844-0.000000000001, -0.047121062819, 0.009014891495, 0.001855812216, -0.001176830044};
					}
				} else if (!ismin && isx && (ii > nx-6)) {
					if (ii == nx-1) {
						for (int i=0; i<11; i++)
							cols[i] = idx-i;
						values = (PetscScalar [11]) {0.320882352941, -0.465, 0.179117647059, -0.035, 0., 0., 0., 0., 0., 0., 0.};
					} else if (ii == nx-2) {
						for (int i=0; i<11; i++)
							cols[i] = idx-(i-1);
						values = (PetscScalar [11]) {-0.085777408970+0.000000000001, 0.277628171524, -0.356848072173, 0.223119093072, -0.057347064865, -0.000747264596, -0.000027453993, 0., 0., 0., 0.};
					} else if (ii == nx-3) {
						for (int i=0; i<11; i++)
							cols[i] = idx-(i-2);
						values = (PetscScalar [11]) {0.0307159855992469, -0.148395705486028 , 0.312055385963757, -0.363202245195514, 0.230145457063431, -0.0412316564605079, -0.0531024700805787, 0.0494343261171287, -0.0198143585458560, 0.00339528102492129, 0.};
					} else if (ii == nx-4) {
						for (int i=0; i<11; i++)
							cols[i] = idx-(i-3);
						values = (PetscScalar [11]) {-0.000054596010, 0.042124772446, -0.173103107841, 0.299615871352, -0.276543612935, 0.131223506571, -0.023424966418, 0.013937561779, -0.024565095706, 0.013098287852, -0.002308621090};
					} else if (ii == nx-5) {
						for (int i=0; i<11; i++)
							cols[i] = idx-(i-4);
						values = (PetscScalar [11]) {0.008391235145, -0.047402506444, 0.121438547725, -0.200063042812, 0.240069047836, -0.207269200140-0.000000000001, 0.122263107844-0.000000000001, -0.047121062819, 0.009014891495, 0.001855812216, -0.001176830044};
					}
				} else if (ismin && !isx && (jj < 5)) {
					if (jj == 0){
						for (int i=0; i<11; i++)
							cols[i] = idx+i*nx;
						values = (PetscScalar [11]) {0.320882352941, -0.465, 0.179117647059, -0.035, 0., 0., 0., 0., 0., 0., 0.};
					} else if (jj == 1){
						for (int i=0; i<11; i++)
							cols[i] = idx+(i-1)*nx;
						values = (PetscScalar [11]) {-0.085777408970+0.000000000001, 0.277628171524, -0.356848072173, 0.223119093072, -0.057347064865, -0.000747264596, -0.000027453993, 0., 0., 0., 0.};
					} else if (jj == 2){
						for (int i=0; i<11; i++)
							cols[i] = idx+(i-2)*nx;
						values = (PetscScalar [11]) {0.0307159855992469, -0.148395705486028 , 0.312055385963757, -0.363202245195514, 0.230145457063431, -0.0412316564605079, -0.0531024700805787, 0.0494343261171287, -0.0198143585458560, 0.00339528102492129, 0.};
					} else if (jj == 3){
						for (int i=0; i<11; i++)
							cols[i] = idx+(i-3)*nx;
						values = (PetscScalar [11]) {-0.000054596010, 0.042124772446, -0.173103107841, 0.299615871352, -0.276543612935, 0.131223506571, -0.023424966418, 0.013937561779, -0.024565095706, 0.013098287852, -0.002308621090};
					} else if (jj == 4){
						for (int i=0; i<11; i++)
							cols[i] = idx+(i-4)*nx;
						values = (PetscScalar [11]) {0.008391235145, -0.047402506444, 0.121438547725, -0.200063042812, 0.240069047836, -0.207269200140-0.000000000001, 0.122263107844-0.000000000001, -0.047121062819, 0.009014891495, 0.001855812216, -0.001176830044};
					}
				} else if (!ismin && !isx && (jj > ny-6)) {
					if (jj == ny-1) {
						for (int i=0; i<11; i++)
							cols[i] = idx-i*nx;
						values = (PetscScalar [11]) {0.320882352941, -0.465, 0.179117647059, -0.035, 0., 0., 0., 0., 0., 0., 0.};
					} else if (jj == ny-2) {
						for (int i=0; i<11; i++)
							cols[i] = idx-(i-1)*nx;
						values = (PetscScalar [11]) {-0.085777408970+0.000000000001, 0.277628171524, -0.356848072173, 0.223119093072, -0.057347064865, -0.000747264596, -0.000027453993, 0., 0., 0., 0.};
					} else if (jj == ny-3) {
						for (int i=0; i<11; i++)
							cols[i] = idx-(i-2)*nx;
						values = (PetscScalar [11]) {0.0307159855992469, -0.148395705486028 , 0.312055385963757, -0.363202245195514, 0.230145457063431, -0.0412316564605079, -0.0531024700805787, 0.0494343261171287, -0.0198143585458560, 0.00339528102492129, 0.};
					} else if (jj == ny-4) {
						for (int i=0; i<11; i++)
							cols[i] = idx-(i-3)*nx;
						values = (PetscScalar [11]) {-0.000054596010, 0.042124772446, -0.173103107841, 0.299615871352, -0.276543612935, 0.131223506571, -0.023424966418, 0.013937561779, -0.024565095706, 0.013098287852, -0.002308621090};
					} else if (jj == ny-5) {
						for (int i=0; i<11; i++)
							cols[i] = idx-(i-4)*nx;
						values = (PetscScalar [11]) {0.008391235145, -0.047402506444, 0.121438547725, -0.200063042812, 0.240069047836, -0.207269200140-0.000000000001, 0.122263107844-0.000000000001, -0.047121062819, 0.009014891495, 0.001855812216, -0.001176830044};
					}
				} else {
					continue;
				}
				if (ts->super_las != NULL) {
					ierr = MatSetValues(ts->super_las->filter, 1, &idx, 11, cols, values, ADD_VALUES); CHKERRQ(ierr);
				} else {
					ierr = MatSetValues(ts->filter, 1, &idx, 11, cols, values, ADD_VALUES); CHKERRQ(ierr);
				}
			}
		}
 	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown filter scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}

	free(cols);
	free(result);

	return ierr;
}
