/* This file contains the methods handling the filters for a
   one-dimensional solver for a transport equation
   @author : Thibault Bridel-Bertomeu
   @license: MIT
   @status : development
*/

#include "fdmsolver.h"
#ifndef LARGEST_STENCIL
#define LARGEST_STENCIL 11
#endif

/* This method yields the filter matrix for a one dimensional transport solver based
   on the name of the filter scheme.
*/
void get_ts1D_filter(TransportSolver1D *ts, Grid1D *grid)
{
	PetscInt idx, rstart, rend, nlocal, vecsize;
	PetscInt *cols = NULL;
	PetscMPIInt size;

	// Get info on the MPI layout
	ts->ierr = MPI_Comm_size(PETSC_COMM_WORLD, &size);

	// Get parallel layout of the grid->unknowns vector
	ts->ierr = VecGetSize(grid->unknowns, &vecsize);
	if ((vecsize == grid->nvtx) ||
		(ts->super_las != NULL)) {
		ts->ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend);
		ts->ierr = VecGetLocalSize(grid->unknowns, &nlocal);
	} else {
		ts->ierr = MatGetOwnershipRange(ts->rhs, &rstart, &rend);
		ts->ierr = MatGetLocalSize(ts->rhs, &nlocal, &nlocal);
	}

	// Prepare the RHS matrix
	if (ts->super_las == NULL) {
		PetscPrintf(PETSC_COMM_WORLD, "\tCreating filter distributed AIJ matrix ...\n");
		ts->ierr = MatCreate(PETSC_COMM_WORLD, &(ts->filter));
		ts->ierr = MatSetSizes(ts->filter, nlocal, nlocal, grid->nvtx, grid->nvtx);
		ts->ierr = MatSetType(ts->filter, MATAIJ);
		if (size==1) {
			ts->ierr = MatSeqAIJSetPreallocation(ts->filter, LARGEST_STENCIL, NULL);
		} else {
			ts->ierr = MatMPIAIJSetPreallocation(ts->filter, LARGEST_STENCIL, NULL, LARGEST_STENCIL, NULL);
		}
	}

	// Condition on the scheme now
	if (ts->super_las == NULL) PetscPrintf(PETSC_COMM_WORLD, "\tFilling the diagonals of the filter matrix ...\n");
	if ((strcmp(ts->parent_solver->filter_name, "SFo11p") == 0)) {
		// Fill the vector of coefficients
		cols = (PetscInt*) malloc(11 * sizeof(PetscInt));
		PetscScalar values[11] = {-0.002999540835, 0.018721609157, -0.059227575576, 0.123755948787, -0.187772883589,
								  0.215044884112,
								  -0.187772883589, 0.123755948787, -0.059227575576, 0.018721609157, -0.002999540835};
		// Now loop on the inner points, skip the boundaries for now
		for (idx = rstart; idx < rend; idx ++){
			if (ts->super_las != NULL) {
				if ((idx-5 >= (ts->mylasnumber-1)*grid->nvtx) && (idx+5 <= ts->mylasnumber*grid->nvtx-1)) {
					cols[0]=idx-5; cols[1]=idx-4; cols[2]=idx-3; cols[3]=idx-2; cols[4]=idx-1;
					cols[5]=idx;
					cols[6]=idx+1; cols[7]=idx+2; cols[8]=idx+3; cols[9]=idx+4; cols[10]=idx+5;
					//
					ts->super_las->ierr = MatSetValues(ts->super_las->filter,1,&idx,11,cols,values,INSERT_VALUES);
				}
			} else {
				if ((idx-5 >= 0) && (idx+5 <= grid->nvtx-1)) {
					cols[0]=idx-5; cols[1]=idx-4; cols[2]=idx-3; cols[3]=idx-2; cols[4]=idx-1;
					cols[5]=idx;
					cols[6]=idx+1; cols[7]=idx+2; cols[8]=idx+3; cols[9]=idx+4; cols[10]=idx+5;
					ts->ierr = MatSetValues(ts->filter,1,&idx,11,cols,values,INSERT_VALUES);
				}
			}
		}
	} else {
		PetscPrintf(PETSC_COMM_WORLD, "Unknown filtering scheme.\n");
		ts->ierr = PETSC_ERR_ARG_WRONG;
	}

	free(cols);
}

/* Method to set the filter matrix of a 1D transport solver for periodicity
*/
PetscErrorCode set_periodic_filter_ts1D(TransportSolver1D* ts, Grid1D* grid)
{
	PetscErrorCode ierr = 0;
	PetscInt idx, rstart, rend, vecsize = 0;
	PetscInt *cols = NULL;

	// Get parallel layout of the grid->unknowns vector
	ierr = VecGetSize(grid->unknowns, &vecsize); CHKERRQ(ierr);
	if ((vecsize == grid->nvtx) ||
		(ts->super_las != NULL)) {
		ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend); CHKERRQ(ierr);
	} else {
		ierr = MatGetOwnershipRange(ts->rhs, &rstart, &rend); CHKERRQ(ierr);
	}

	if ((strcmp(ts->parent_solver->filter_name, "SFo11p") == 0)) {
		// Fill the vector of coefficients
		cols = (PetscInt*) malloc(11 * sizeof(PetscInt));
		PetscScalar values[11] = {-0.002999540835, 0.018721609157, -0.059227575576, 0.123755948787, -0.187772883589,
								  0.215044884112,
								  -0.187772883589, 0.123755948787, -0.059227575576, 0.018721609157, -0.002999540835};
		// Now loop on the points closest to the boundaries
		for (idx = rstart; idx < rend; idx ++){
			if ((idx-5 < (ts->mylasnumber-1)*grid->nvtx) &&
				(idx >= (ts->mylasnumber-1)*grid->nvtx)) {
				if (idx == 0+(ts->mylasnumber-1)*grid->nvtx){
					cols[0]=ts->mylasnumber*grid->nvtx-5; cols[1]=ts->mylasnumber*grid->nvtx-4; cols[2]=ts->mylasnumber*grid->nvtx-3; cols[3]=ts->mylasnumber*grid->nvtx-2; cols[4]=ts->mylasnumber*grid->nvtx-1;
				} else if (idx == 1+(ts->mylasnumber-1)*grid->nvtx){
					cols[0]=ts->mylasnumber*grid->nvtx-4; cols[1]=ts->mylasnumber*grid->nvtx-3; cols[2]=ts->mylasnumber*grid->nvtx-2; cols[3]=ts->mylasnumber*grid->nvtx-1; cols[4]=idx-1;
				} else if (idx == 2+(ts->mylasnumber-1)*grid->nvtx){
					cols[0]=ts->mylasnumber*grid->nvtx-3; cols[1]=ts->mylasnumber*grid->nvtx-2; cols[2]=ts->mylasnumber*grid->nvtx-1; cols[3]=idx-2; cols[4]=idx-1;
				} else if (idx == 3+(ts->mylasnumber-1)*grid->nvtx){
					cols[0]=ts->mylasnumber*grid->nvtx-2; cols[1]=ts->mylasnumber*grid->nvtx-1; cols[2]=idx-3; cols[3]=idx-2; cols[4]=idx-1;
				} else if (idx == 4+(ts->mylasnumber-1)*grid->nvtx){
					cols[0]=ts->mylasnumber*grid->nvtx-1; cols[1]=idx-4; cols[2]=idx-3; cols[3]=idx-2; cols[4]=idx-1;
				}
				cols[5]=idx;
				cols[6]=idx+1; cols[7]=idx+2; cols[8]=idx+3; cols[9]=idx+4; cols[10]=idx+5;
			} else if ((idx+5 > ts->mylasnumber*grid->nvtx-1) &&
					   (idx <= ts->mylasnumber*grid->nvtx-1)) {
				cols[0]=idx-5; cols[1]=idx-4; cols[2]=idx-3; cols[3]=idx-2; cols[4]=idx-1;
				cols[5]=idx;
				if (idx==(ts->mylasnumber*grid->nvtx-5)){
					cols[6]=idx+1; cols[7]=idx+2; cols[8]=idx+3; cols[9]=idx+4; cols[10]=(ts->mylasnumber-1)*grid->nvtx+0;
				} else if (idx==(ts->mylasnumber*grid->nvtx-4)){
					cols[6]=idx+1; cols[7]=idx+2; cols[8]=idx+3; cols[9]=(ts->mylasnumber-1)*grid->nvtx+0; cols[10]=(ts->mylasnumber-1)*grid->nvtx+1;
				} else if (idx==(ts->mylasnumber*grid->nvtx-3)){
					cols[6]=idx+1; cols[7]=idx+2; cols[8]=(ts->mylasnumber-1)*grid->nvtx+0; cols[9]=(ts->mylasnumber-1)*grid->nvtx+1; cols[10]=(ts->mylasnumber-1)*grid->nvtx+2;
				} else if (idx==(ts->mylasnumber*grid->nvtx-2)){
					cols[6]=idx+1; cols[7]=(ts->mylasnumber-1)*grid->nvtx+0; cols[8]=(ts->mylasnumber-1)*grid->nvtx+1; cols[9]=(ts->mylasnumber-1)*grid->nvtx+2; cols[10]=(ts->mylasnumber-1)*grid->nvtx+3;
				} else if (idx==(ts->mylasnumber*grid->nvtx-1)){
					cols[6]=(ts->mylasnumber-1)*grid->nvtx+0; cols[7]=(ts->mylasnumber-1)*grid->nvtx+1; cols[8]=(ts->mylasnumber-1)*grid->nvtx+2; cols[9]=(ts->mylasnumber-1)*grid->nvtx+3; cols[10]=(ts->mylasnumber-1)*grid->nvtx+4;
				}
			} else {
				continue;
			}
			if (ts->super_las != NULL) {
				ierr = MatSetValues(ts->super_las->filter, 1, &idx, 11, cols, values, INSERT_VALUES); CHKERRQ(ierr);
			} else {
				ierr = MatSetValues(ts->filter, 1, &idx, 11, cols, values, INSERT_VALUES); CHKERRQ(ierr);
			}
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown filter scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}

	free(cols);

	return ierr;
}

/* Method to set the filter matrix of a 1D transport solver for open boundary condition
*/
PetscErrorCode set_open_filter_ts1D(TransportSolver1D* ts, Grid1D* grid, PetscBool ismin)
{
	PetscErrorCode ierr = 0;
	PetscInt idx, rstart, rend, vecsize = 0;
	PetscInt *cols = NULL;
	PetscScalar *values = NULL;

	// Get parallel layout of the grid->unknowns vector
	ierr = VecGetSize(grid->unknowns, &vecsize); CHKERRQ(ierr);
	if ((vecsize == grid->nvtx) ||
		(ts->super_las != NULL)) {
		ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend); CHKERRQ(ierr);
	} else {
		ierr = MatGetOwnershipRange(ts->rhs, &rstart, &rend); CHKERRQ(ierr);
	}

	if ((strcmp(ts->parent_solver->filter_name, "SFo11p") == 0)) {
		// Fill the vector of coefficients
		cols = (PetscInt*) malloc(11 * sizeof(PetscInt));
		// Now loop on the points and stop only if close to the right boundary
		for (idx = rstart; idx < rend; idx ++){
			if (ismin && ((idx-5 < (ts->mylasnumber-1)*grid->nvtx) &&
						  (idx >= (ts->mylasnumber-1)*grid->nvtx))) {
				for (int i=0; i<11; i++)
					cols[i] = (ts->mylasnumber-1)*grid->nvtx+i;
				//
				if (idx == 0+(ts->mylasnumber-1)*grid->nvtx){
					values = (PetscScalar [11]) {0.320882352941, -0.465, 0.179117647059, -0.035, 0., 0., 0., 0., 0., 0., 0.};
				} else if (idx == 1+(ts->mylasnumber-1)*grid->nvtx){
					values = (PetscScalar [11]) {-0.085777408970+0.000000000001, 0.277628171524, -0.356848072173, 0.223119093072, -0.057347064865, -0.000747264596, -0.000027453993, 0., 0., 0., 0.};
				} else if (idx == 2+(ts->mylasnumber-1)*grid->nvtx){
					values = (PetscScalar [11]) {0.0307159855992469, -0.148395705486028 , 0.312055385963757, -0.363202245195514, 0.230145457063431, -0.0412316564605079, -0.0531024700805787, 0.0494343261171287, -0.0198143585458560, 0.00339528102492129, 0.};
				} else if (idx == 3+(ts->mylasnumber-1)*grid->nvtx){
					values = (PetscScalar [11]) {-0.000054596010, 0.042124772446, -0.173103107841, 0.299615871352, -0.276543612935, 0.131223506571, -0.023424966418, 0.013937561779, -0.024565095706, 0.013098287852, -0.002308621090};
				} else if (idx == 4+(ts->mylasnumber-1)*grid->nvtx){
					values = (PetscScalar [11]) {0.008391235145, -0.047402506444, 0.121438547725, -0.200063042812, 0.240069047836, -0.207269200140-0.000000000001, 0.122263107844-0.000000000001, -0.047121062819, 0.009014891495, 0.001855812216, -0.001176830044};
				}
			} else if (!ismin && ((idx+5 > ts->mylasnumber*grid->nvtx-1) &&
					   			  (idx <= ts->mylasnumber*grid->nvtx-1))) {
				for (int i=0; i<11; i++)
					cols[i] = ts->mylasnumber*grid->nvtx-1-i;
				//
				if (idx==(ts->mylasnumber*grid->nvtx-5)){
					values = (PetscScalar [11]) {0.320882352941, -0.465, 0.179117647059, -0.035, 0., 0., 0., 0., 0., 0., 0.};
				} else if (idx==(ts->mylasnumber*grid->nvtx-4)){
					values = (PetscScalar [11]) {-0.085777408970+0.000000000001, 0.277628171524, -0.356848072173, 0.223119093072, -0.057347064865, -0.000747264596, -0.000027453993, 0., 0., 0., 0.};
				} else if (idx==(ts->mylasnumber*grid->nvtx-3)){
					values = (PetscScalar [11]) {0.0307159855992469, -0.148395705486028 , 0.312055385963757, -0.363202245195514, 0.230145457063431, -0.0412316564605079, -0.0531024700805787, 0.0494343261171287, -0.0198143585458560, 0.00339528102492129, 0.};
				} else if (idx==(ts->mylasnumber*grid->nvtx-2)){
					values = (PetscScalar [11]) {-0.000054596010, 0.042124772446, -0.173103107841, 0.299615871352, -0.276543612935, 0.131223506571, -0.023424966418, 0.013937561779, -0.024565095706, 0.013098287852, -0.002308621090};
				} else if (idx==(ts->mylasnumber*grid->nvtx-1)){
					values = (PetscScalar [11]) {0.008391235145, -0.047402506444, 0.121438547725, -0.200063042812, 0.240069047836, -0.207269200140-0.000000000001, 0.122263107844-0.000000000001, -0.047121062819, 0.009014891495, 0.001855812216, -0.001176830044};
				}
			} else {
				continue;
			}
			if (ts->super_las != NULL) {
				ierr = MatSetValues(ts->super_las->filter, 1, &idx, 11, cols, values, INSERT_VALUES); CHKERRQ(ierr);
			} else {
				ierr = MatSetValues(ts->filter, 1, &idx, 11, cols, values, INSERT_VALUES); CHKERRQ(ierr);
			}
		}
 	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown filter scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}

	free(cols);

	return ierr;
}
