/* This file contains the methods handling the one-dimensional solver for an diffusion equation
   @author : Thibault Bridel-Bertomeu
   @license: MIT
   @status : development
*/

#include "fdmsolver.h"
#ifdef LARGEST_STENCIL
#undef LARGEST_STENCIL
#endif
#define LARGEST_STENCIL 3

/* This method initializes a solver for the diffusion equation in 1D
*/
DiffusionSolver1D* init_diffusionSolver1D(PetscInt nequation_args, PetscChar equation_arguments[TWENTY*2][WORDSZ], Grid1D *grid)
{
	int eq_index;
	PetscChar con_string[WORDSZ] = "";
	DiffusionSolver1D *ds = (DiffusionSolver1D*) malloc(sizeof(DiffusionSolver1D));

	// Initialize the parent
	PetscPrintf(PETSC_COMM_WORLD, "\tInitializing parent solver ...\n");
	ds->parent_solver = init_Solver(nequation_args, equation_arguments, (PetscInt)2);
	ds->ierr = ds->parent_solver->ierr;

	// Force filtering out for this category of solver
	ds->parent_solver->hasFilter = false;

	// Initialize the vectors for the Neumann boundary conditions
	ds->neumanns = (Vec*) malloc(DIM2 * sizeof(Vec));
	ds->neumanns[0] = NULL; ds->neumanns[1] = NULL;

	// Initialize the vectors for the Robin boundary conditions
	ds->robins = (Vec*) malloc(DIM2 * sizeof(Vec));
	ds->robins[0] = NULL; ds->robins[1] = NULL;

	// Get the conductivity from the user inputs
	PetscPrintf(PETSC_COMM_WORLD, "\tAcquiring conductivity...\n");
	for (int i=0; i < nequation_args; i++) {
		if (strstr(equation_arguments[i], "parameters") != NULL) {
			eq_index = ( strchr(equation_arguments[i], '=') - equation_arguments[i] ) + 1;
			strcpy(con_string, equation_arguments[i] + (eq_index + 1));
		}
	}
	if (sscanf(con_string, "[%lf,]", ds->cond) != 1) {
		PetscPrintf(PETSC_COMM_WORLD, "For a 1D diffusion equation, a 1-elements conductivity vector such as [X,] must be input.\n");
		ds->ierr = PETSC_ERR_ARG_WRONG;
	}

	// Store the CFL number
	ds->cfl[0] = ds->cond[0] * ds->parent_solver->dt / PetscPowScalar(grid->parent_grid->dx, 2.0);

	// Call the routine to build the RHS matrix based on numerical scheme
	get_ds1D_rhs(ds, grid);

	// Call the routine to build the filter matrix based on numerical scheme
	if (ds->parent_solver->hasFilter) {
		get_ds1D_filter(ds, grid);
	}

	// Return the solver
	return ds;
}

/* This method yields the RHS matrix for a one dimensional diffusion solver based
   on the name of the scheme.
*/
void get_ds1D_rhs(DiffusionSolver1D *ds, Grid1D *grid)
{
	PetscInt idx, rstart, rend, nlocal;
	PetscInt *cols = NULL, vecsize = 0;
	PetscMPIInt size;

	// Get info on the MPI layout
	ds->ierr = MPI_Comm_size(PETSC_COMM_WORLD, &size);

	// Get parallel layout of the grid->unknowns vector
	ds->ierr = VecGetSize(grid->unknowns, &vecsize);
	if (vecsize == grid->nvtx) {
		ds->ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend);
		ds->ierr = VecGetLocalSize(grid->unknowns, &nlocal);
	}

	// Prepare the RHS matrix
	PetscPrintf(PETSC_COMM_WORLD, "\tCreating RHS distributed AIJ matrix ...\n");
	ds->ierr = MatCreate(PETSC_COMM_WORLD, &(ds->rhs));
	if (vecsize == grid->nvtx) {
		ds->ierr = MatSetSizes(ds->rhs, nlocal, nlocal, grid->nvtx, grid->nvtx);
	} else {
		ds->ierr = MatSetSizes(ds->rhs, PETSC_DECIDE, PETSC_DECIDE, grid->nvtx, grid->nvtx);
	}
	ds->ierr = MatSetType(ds->rhs, MATAIJ);
	if (size==1) {
		ds->ierr = MatSeqAIJSetPreallocation(ds->rhs, LARGEST_STENCIL, NULL);
	} else {
		ds->ierr = MatMPIAIJSetPreallocation(ds->rhs, LARGEST_STENCIL, NULL, LARGEST_STENCIL, NULL);
	}
	if (vecsize != grid->nvtx) {
		ds->ierr = MatGetOwnershipRange(ds->rhs, &rstart, &rend);
	}

	// Condition on the scheme now
	PetscPrintf(PETSC_COMM_WORLD, "\tFilling the diagonals of the RHS matrix ...\n");
	if ((strcmp(ds->parent_solver->scheme_name, "RK4o6s_CS") == 0) ||
	    (strcmp(ds->parent_solver->scheme_name, "RK4CS") == 0) ||
	    (strcmp(ds->parent_solver->scheme_name, "RK4_CS") == 0)) {
		// Fill the vector of coefficients
		cols = (PetscInt*) malloc(3 * sizeof(PetscInt));
		PetscScalar values[3] = {1.0,
								 -2.0,
								 1.0};
		// Now loop on the inner points, skip the boundaries for now
		for (idx = rstart; idx < rend; idx ++){
			if ((idx-1 >= 0) && (idx+1 <= grid->nvtx-1)) {
				cols[0]=idx-1;
				cols[1]=idx;
				cols[2]=idx+1;
				ds->ierr = MatSetValues(ds->rhs,1,&idx,3,cols,values,INSERT_VALUES);
			}
		}
	} else {
		PetscPrintf(PETSC_COMM_WORLD, "Unknown spatial scheme.\n");
		ds->ierr = PETSC_ERR_ARG_WRONG;
	}

	free(cols);
}

/* This method is used to apply all kind of boundary condition onto
   the matrix(ces) of a 1D diffusion solver
*/
PetscErrorCode applyBocos_ds1D(PetscInt nboco_arguments, PetscChar boco_arguments[2*TWENTY][WORDSZ], Grid1D *grid, DiffusionSolver1D *ds)
{
	int eq_index;
	PetscBool ismin = true;
	PetscChar bocotype[WORDSZ] = "";
	PetscErrorCode ierr = 0;

	// Parse the boundary condition arguments for proper assignment
	for (int i=0; i < nboco_arguments; i++) {
		// Get the type of boundary condition for the current boundary
		if (strstr(boco_arguments[i], "xmin") != NULL) {
			ismin = true;
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else if (strstr(boco_arguments[i], "xmax") != NULL) {
			ismin = false;
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown boundary assignement.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
		// Switch on the type of boundary condition required by the user
		if (strcmp(bocotype, "periodic") == 0) {
			ierr = set_periodic_ds1D(ds, grid); CHKERRQ(ierr);
			if (ds->parent_solver->hasFilter) {
				ierr = set_periodic_filter_ds1D(ds, grid); CHKERRQ(ierr);
			}
			// Once periodic has been called, can break the for loop
			break;
		} else if (strcmp(bocotype, "dirichlet") == 0) {
			ierr = set_dirichlet_ds1D(ds, grid, ismin); CHKERRQ(ierr);
			if (ds->parent_solver->hasFilter) {
				ierr = set_open_filter_ds1D(ds, grid, ismin); CHKERRQ(ierr);
			}
		} else if (strcmp(bocotype, "neumann") == 0) {
			ierr = set_neumann_ds1D(ds, grid, ismin); CHKERRQ(ierr);
			if (ds->parent_solver->hasFilter) {
				ierr = set_open_filter_ds1D(ds, grid, ismin); CHKERRQ(ierr);
			}
		} else if (startsWith(bocotype, "robin")) {
			PetscScalar robA = 0.0, robB = 0.0;
			sscanf(bocotype, "robin,%lf,%lf", &robA, &robB);
			ierr = set_robin_ds1D(ds, grid, ismin, robA, robB); CHKERRQ(ierr);
			if (ds->parent_solver->hasFilter) {
				ierr = set_open_filter_ds1D(ds, grid, ismin); CHKERRQ(ierr);
			}
		} else if (strcmp(bocotype, "open") == 0) {
			ierr = set_open_ds1D(ds, grid, ismin); CHKERRQ(ierr);
			if (ds->parent_solver->hasFilter) {
				ierr = set_open_filter_ds1D(ds, grid, ismin); CHKERRQ(ierr);
			}
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown boundary condition type.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
	}

	// Assemble the RHS matrix
	ierr = PetscPrintf(PETSC_COMM_WORLD, "\tAssembling the RHS matrix ...\n"); CHKERRQ(ierr);
	ierr = MatAssemblyBegin(ds->rhs, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
	ierr = MatAssemblyEnd(ds->rhs, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

	// Assemble the filter matrix
	if (ds->parent_solver->hasFilter) {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "\tAssembling the filter matrix ...\n"); CHKERRQ(ierr);
		ierr = MatAssemblyBegin(ds->filter, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
		ierr = MatAssemblyEnd(ds->filter, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
	}

	return ierr;
}

/* Method to set the RHS matrix of a 1D diffusion solver for periodicity
*/
PetscErrorCode set_periodic_ds1D(DiffusionSolver1D *ds, Grid1D* grid)
{
	PetscErrorCode ierr = 0;
	PetscInt idx, rstart, rend;
	PetscInt *cols = NULL, vecsize = 0;

	// Get parallel layout of the grid->unknowns vector
	ierr = VecGetSize(grid->unknowns, &vecsize); CHKERRQ(ierr);
	if (vecsize == grid->nvtx) {
		ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend); CHKERRQ(ierr);
	} else {
		ierr = MatGetOwnershipRange(ds->rhs, &rstart, &rend); CHKERRQ(ierr);
	}

	ierr = PetscPrintf(PETSC_COMM_WORLD, "\tImposing periodicity conditions ...\n"); CHKERRQ(ierr);
	if ((strcmp(ds->parent_solver->scheme_name, "RK4o6s_CS") == 0) ||
	    (strcmp(ds->parent_solver->scheme_name, "RK4CS") == 0) ||
	    (strcmp(ds->parent_solver->scheme_name, "RK4_CS") == 0)) {
		// Fill the vector of coefficients
		cols = (PetscInt*) malloc(3 * sizeof(PetscInt));
		PetscScalar values[3] = {1.0,
								 -2.0,
								 1.0};
		// Now loop on the points closest to the boundaries
		for (idx = rstart; idx < rend; idx ++){
			if (idx < 1) {
				cols[0]=grid->nvtx-1;
				cols[1]=idx;
				cols[2]=idx+1;
			} else if (idx > grid->nvtx-2) {
				cols[0]=idx-1;
				cols[1]=idx;
				cols[2]=0;
			} else {
				continue;
			}
			ierr = MatSetValues(ds->rhs,1,&idx,3,cols,values,INSERT_VALUES); CHKERRQ(ierr);
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown spatial scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}

	free(cols);

	return ierr;
}

/* Method to set the RHS matrix of a 1D diffusion solver for open boundary condition
*/
PetscErrorCode set_open_ds1D(DiffusionSolver1D *ds, Grid1D* grid, PetscBool ismin)
{
	PetscErrorCode ierr = 0;
	PetscInt idx, rstart, rend;
	PetscInt *cols = NULL, vecsize=0;
	PetscScalar *values = NULL;

	// Get parallel layout of the grid->unknowns vector
	ierr = VecGetSize(grid->unknowns, &vecsize); CHKERRQ(ierr);
	if (vecsize == grid->nvtx) {
		ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend); CHKERRQ(ierr);
	} else {
		ierr = MatGetOwnershipRange(ds->rhs, &rstart, &rend); CHKERRQ(ierr);
	}

	ierr = PetscPrintf(PETSC_COMM_WORLD, "\tImposing open conditions ...\n"); CHKERRQ(ierr);
	if ((strcmp(ds->parent_solver->scheme_name, "RK4o6s_CS") == 0) ||
	    (strcmp(ds->parent_solver->scheme_name, "RK4CS") == 0) ||
	    (strcmp(ds->parent_solver->scheme_name, "RK4_CS") == 0)) {
		// Fill the vector of coefficients
		cols = (PetscInt*) malloc(3 * sizeof(PetscInt));
		// Now loop on the points and stop only if close to the right boundary
		for (idx = rstart; idx < rend; idx ++){
			if (ismin && (idx < 1)) {
				for (int i=0; i<3; i++)
					cols[i] = i;
				//
				values = (PetscScalar [3]) {1.0, -2.0, 1.0};
			} else if (!ismin && (idx > grid->nvtx-2)) {
				for (int i=0; i<3; i++)
					cols[i] = grid->nvtx-1-i;
				//
				values = (PetscScalar [3]) {1.0, -2.0, 1.0};
			} else {
				continue;
			}
			ierr = MatSetValues(ds->rhs,1,&idx,3,cols,values,INSERT_VALUES); CHKERRQ(ierr);
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown spatial scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}

	free(cols);

	return ierr;
}

/* Method to set the RHS matrix of a 1D diffusion solver for an
   homogeneous dirichlet boundary condition
*/
PetscErrorCode set_dirichlet_ds1D(DiffusionSolver1D *ds, Grid1D* grid, PetscBool ismin)
{
	PetscErrorCode ierr = 0;

	// Start by applying an open boundary condition to have the
	// right schemes close to the boundary points
	ierr = set_open_ds1D(ds, grid, ismin);

	// Increment the number of rows to zero later during execution
	ds->parent_solver->ndir_rows ++;
	if (ismin) {
		ds->parent_solver->dir_rows[ds->parent_solver->ndir_rows] = grid->ixmin;
	}
	else {
		ds->parent_solver->dir_rows[ds->parent_solver->ndir_rows] = grid->ixmax;
	}

	return ierr;
}

/* Method to set the RHS matrix of a 1D diffusion solver for an
   homogeneous neumann boundary condition
*/
PetscErrorCode set_neumann_ds1D(DiffusionSolver1D *ds, Grid1D* grid, PetscBool ismin)
{
	PetscInt *rows = NULL;
	PetscErrorCode ierr = 0;
	PetscScalar *values = NULL;

	// Start by applying an open boundary condition to have the
	// right schemes close to the boundary points
	ierr = set_open_ds1D(ds, grid, ismin);

	// Increment the number of rows to zero later during execution
	if (ismin) {
		ds->parent_solver->neu_rows[0] = grid->ixmin;
	}
	else {
		ds->parent_solver->neu_rows[1] = grid->ixmax;
	}

	// Condition on the scheme for the proper computation of homogeneous Neumann corrections
	if ((strcmp(ds->parent_solver->scheme_name, "RK4o6s_CS") == 0) ||
	    (strcmp(ds->parent_solver->scheme_name, "RK4CS") == 0) ||
	    (strcmp(ds->parent_solver->scheme_name, "RK4_CS") == 0)) {
		// Fill the vector of coefficients
		rows = (PetscInt*) malloc(2 * sizeof(PetscInt));
		// Now fix the right boundary
		if (ismin) {
			for (int i=0; i<2; i++)
				rows[i] = i;
			//
			values = (PetscScalar [2]) {-1.0, 1.0};
			//
			for (int i=1; i<2; i++) {
				values[i] = - values[i] / values[0];
			}
			values[0] = 0.0;
			//
			ierr = VecCreate(PETSC_COMM_WORLD, &(ds->neumanns[0])); CHKERRQ(ierr);
			ierr = VecSetSizes(ds->neumanns[0], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
			ierr = VecSetUp(ds->neumanns[0]); CHKERRQ(ierr);
			ierr = VecSet(ds->neumanns[0], 0.0); CHKERRQ(ierr);
			ierr = VecSetValues(ds->neumanns[0],2,rows,values,INSERT_VALUES); CHKERRQ(ierr);
			ierr = VecAssemblyBegin(ds->neumanns[0]); CHKERRQ(ierr);
			ierr = VecAssemblyEnd(ds->neumanns[0]); CHKERRQ(ierr);
		} else {
			for (int i=0; i<2; i++)
				rows[i] = grid->nvtx-1-i;
			//
			values = (PetscScalar [2]) {-1.0, 1.0};
			//
			for (int i=1; i<2; i++) {
				values[i] = - values[i] / values[0];
			}
			values[0] = 0.0;
			//
			ierr = VecCreate(PETSC_COMM_WORLD, &(ds->neumanns[1])); CHKERRQ(ierr);
			ierr = VecSetSizes(ds->neumanns[1], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
			ierr = VecSetUp(ds->neumanns[1]); CHKERRQ(ierr);
			ierr = VecSet(ds->neumanns[1], 0.0); CHKERRQ(ierr);
			ierr = VecSetValues(ds->neumanns[1],2,rows,values,INSERT_VALUES); CHKERRQ(ierr);
			ierr = VecAssemblyBegin(ds->neumanns[1]); CHKERRQ(ierr);
			ierr = VecAssemblyEnd(ds->neumanns[1]); CHKERRQ(ierr);
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown spatial scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}

	free(rows);

	return ierr;
}

/* Method to set the RHS matrix of a 1D diffusion solver for an
   homogeneous robin boundary condition
*/
PetscErrorCode set_robin_ds1D(DiffusionSolver1D *ds, Grid1D* grid, PetscBool ismin, PetscScalar robA, PetscScalar robB)
{
	PetscInt *rows = NULL;
	PetscErrorCode ierr = 0;
	PetscScalar *values = NULL;

	// Start by applying an open boundary condition to have the
	// right schemes close to the boundary points
	ierr = set_open_ds1D(ds, grid, ismin);

	// Increment the number of rows to zero later during execution
	if (ismin) {
		ds->parent_solver->rob_rows[0] = grid->ixmin;
	}
	else {
		ds->parent_solver->rob_rows[1] = grid->ixmax;
	}

	// Condition on the scheme for the proper computation of homogeneous Neumann corrections
	if ((strcmp(ds->parent_solver->scheme_name, "RK4o6s_CS") == 0) ||
	    (strcmp(ds->parent_solver->scheme_name, "RK4CS") == 0) ||
	    (strcmp(ds->parent_solver->scheme_name, "RK4_CS") == 0)) {
		// Fill the vector of coefficients
		rows = (PetscInt*) malloc(2 * sizeof(PetscInt));
		// Now fix the right boundary
		if (ismin) {
			for (int i=0; i<2; i++)
				rows[i] = i;
			//
			values = (PetscScalar [2]) {-1.0*robB+grid->parent_grid->dx*robA, 1.0*robB};
			//
			for (int i=1; i<2; i++) {
				values[i] = - values[i] / values[0];
			}
			values[0] = 0.0;
			//
			ierr = VecCreate(PETSC_COMM_WORLD, &(ds->robins[0])); CHKERRQ(ierr);
			ierr = VecSetSizes(ds->robins[0], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
			ierr = VecSetUp(ds->robins[0]); CHKERRQ(ierr);
			ierr = VecSet(ds->robins[0], 0.0); CHKERRQ(ierr);
			ierr = VecSetValues(ds->robins[0],2,rows,values,INSERT_VALUES); CHKERRQ(ierr);
			ierr = VecAssemblyBegin(ds->robins[0]); CHKERRQ(ierr);
			ierr = VecAssemblyEnd(ds->robins[0]); CHKERRQ(ierr);
		} else {
			for (int i=0; i<2; i++)
				rows[i] = grid->nvtx-1-i;
			//
			values = (PetscScalar [2]) {-1.0+grid->parent_grid->dx, 1.0};
			//
			for (int i=1; i<2; i++) {
				values[i] = - values[i] / values[0];
			}
			values[0] = 0.0;
			//
			ierr = VecCreate(PETSC_COMM_WORLD, &(ds->robins[1])); CHKERRQ(ierr);
			ierr = VecSetSizes(ds->robins[1], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
			ierr = VecSetUp(ds->robins[1]); CHKERRQ(ierr);
			ierr = VecSet(ds->robins[1], 0.0); CHKERRQ(ierr);
			ierr = VecSetValues(ds->robins[1],2,rows,values,INSERT_VALUES); CHKERRQ(ierr);
			ierr = VecAssemblyBegin(ds->robins[1]); CHKERRQ(ierr);
			ierr = VecAssemblyEnd(ds->robins[1]); CHKERRQ(ierr);
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown spatial scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}

	free(rows);

	return ierr;
}

/* Method for the execution of a one dimensional diffusion solver
*/
PetscErrorCode execute_ds1D(DiffusionSolver1D *ds, Grid1D* grid)
{
	Mat identity, R, mlist1, mlist2;
	PetscErrorCode ierr = 0;
	PetscScalar factor = 0.0, ukmax = 0.0, ukmin = 0.0;
	PetscScalar nL2diff = 0.0, nL2sol = 0.0, errorL2 = 0.0;
	PetscScalar neuscal = 0.0, robscal = 0.0;
	PetscInt step, idx, rstart, rend, nlocal, vecsize;
	Vec b, c;
	PetscMPIInt size;

	// Get info on the MPI layout
	ierr = MPI_Comm_size(PETSC_COMM_WORLD, &size); CHKERRQ(ierr);

	// Get parallel layout of the grid->unknowns vector
	ierr = VecGetSize(grid->unknowns, &vecsize); CHKERRQ(ierr);
	if (vecsize == grid->nvtx) {
		ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend); CHKERRQ(ierr);
		ierr = VecGetLocalSize(grid->unknowns, &nlocal); CHKERRQ(ierr);
	} else {
		ierr = MatGetOwnershipRange(ds->rhs, &rstart, &rend); CHKERRQ(ierr);
		ierr = MatGetLocalSize(ds->rhs, &nlocal, &nlocal); CHKERRQ(ierr);
	}

	// Create an identity matrix, it can come handy
	ierr = MatCreate(PETSC_COMM_WORLD, &identity); CHKERRQ(ierr);
	ierr = MatSetSizes(identity, nlocal, nlocal, grid->nvtx, grid->nvtx); CHKERRQ(ierr);
	ierr = MatSetType(identity, MATAIJ); CHKERRQ(ierr);
	ierr = MatSetUp(identity); CHKERRQ(ierr);
	for (idx = rstart; idx < rend ; idx++) {
		ierr = MatSetValue(identity, idx, idx, 1.0, INSERT_VALUES); CHKERRQ(ierr);
	}
	ierr = MatAssemblyBegin(identity, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
	ierr = MatAssemblyEnd(identity, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

	// Prepare a few other objects before the big loop
	// First pass the RHS actually on the right hand side and scale it by the space step
	ierr = MatScale(ds->rhs, ds->cond[0]/PetscPowScalar(grid->parent_grid->dx, 2)); CHKERRQ(ierr);
	// Then handle the preprocessing for the RK matrix
	if (ds->parent_solver->rk_params->has_gammas) {
		PetscPrintf(PETSC_COMM_WORLD, "\tPreparing Runge-Kutta matrix ...\n");
		// Create the matrix and copy the values from the RHS and assemble
		ierr = MatDuplicate(ds->rhs, MAT_COPY_VALUES, &R); CHKERRQ(ierr);
		ierr = MatScale(R, ds->parent_solver->dt); CHKERRQ(ierr);
		ierr = MatAssemblyBegin(R, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
		ierr = MatAssemblyEnd(R, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
		// First element of mlist is the RHS matrix
		ierr = MatDuplicate(ds->rhs, MAT_COPY_VALUES, &mlist1); CHKERRQ(ierr);
		// Now loop over the remaining RK stages to simulate matrix power
		for (idx = 1; idx < ds->parent_solver->rk_params->steps; idx++) {
			factor = PetscPowScalar(ds->parent_solver->dt, idx+1)*ds->parent_solver->rk_params->gammas[idx];
			ierr = MatMatMult(ds->rhs, mlist1, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &mlist2); CHKERRQ(ierr);
			ierr = MatDestroy(&mlist1); CHKERRQ(ierr);
			ierr = MatAXPY(R, factor, mlist2, DIFFERENT_NONZERO_PATTERN); CHKERRQ(ierr);
			ierr = MatDuplicate(mlist2, MAT_COPY_VALUES, &mlist1); CHKERRQ(ierr);
			ierr = MatDestroy(&mlist2); CHKERRQ(ierr);
		}
		// Destroy the mlist object
		MatDestroy(&mlist1);
		MatDestroy(&mlist2);
		// Add identity to R
		ierr = MatAXPY(R, 1.0, identity, DIFFERENT_NONZERO_PATTERN); CHKERRQ(ierr);
		// Change R structure in case of dirichlet boco
		{
			PetscInt *rows = (PetscInt*) malloc(sizeof(PetscInt) * (ds->parent_solver->ndir_rows+1));
			for (int i=0; i<(ds->parent_solver->ndir_rows+1); i++) {
				rows[i] = ds->parent_solver->dir_rows[i];
			}
			ierr = MatZeroRowsColumns(R, ds->parent_solver->ndir_rows+1, rows, 0.0, 0, 0); CHKERRQ(ierr);
			if (ds->parent_solver->hasFilter) {
				ierr = MatZeroRowsColumns(ds->filter, ds->parent_solver->ndir_rows+1, rows, 0.0, 0, 0); CHKERRQ(ierr);
			}
			free(rows);
		}
		// Change filter structure in case of Neumann boco or Robin boco
		if (ds->parent_solver->hasFilter) {
			for (int i=0; i<DIM2; i++) {
				if (ds->neumanns[i] != NULL) {
					ierr = MatZeroRows(ds->filter, 1, ds->parent_solver->neu_rows + i, 0.0, 0, 0); CHKERRQ(ierr);
				}
				if (ds->robins[i] != NULL) {
					ierr = MatZeroRows(ds->filter, 1, ds->parent_solver->rob_rows + i, 0.0, 0, 0); CHKERRQ(ierr);
				}
			}
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown definition of the Runge-Kutta scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}
	// Scale the filter by the strength input by the user
	if (ds->parent_solver->hasFilter) {
		ierr = MatScale(ds->filter, ds->parent_solver->filter_strength); CHKERRQ(ierr);
	}

	// Export the initial solution
	{
		step = 0;
		// Create the viewers and save the vectors
		PetscViewer Viewer = NULL;
		PetscChar filename[WORDSZ];
		if (grid->parent_grid->ndim == DIM2) {
			sprintf(filename, "sol_%06d.h5", step);
			ierr = PetscViewerHDF5Open(PETSC_COMM_WORLD, filename, FILE_MODE_WRITE, &Viewer); CHKERRQ(ierr);
		} else if (grid->parent_grid->ndim == DIM1) {
			sprintf(filename, "sol_%06d.dat", step);
			ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, filename, &Viewer); CHKERRQ(ierr);
			ierr = PetscViewerPushFormat(Viewer, PETSC_VIEWER_ASCII_MATLAB); CHKERRQ(ierr);
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Wrong dimension for solution output.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
		ierr = VecView(grid->x, Viewer); CHKERRQ(ierr);
		ierr = VecView(grid->unknowns, Viewer); CHKERRQ(ierr);
		ierr = PetscViewerDestroy(&Viewer); CHKERRQ(ierr);
	}

	// Loop on iterations
	ierr = PetscPrintf(PETSC_COMM_WORLD, "\tMarching the equation in time ...\n"); CHKERRQ(ierr);
	for (step = 1; step <= ds->parent_solver->nit; step ++) {
		// Implementation differs function of the way the RK scheme is made
		if (ds->parent_solver->rk_params->has_gammas) {
			// Get an auxiliary vector
			ierr = VecDuplicate(grid->unknowns, &b); CHKERRQ(ierr);
			// Apply the temporal scheme
			ierr = MatMult(R, grid->unknowns, b); CHKERRQ(ierr);
			// Save the result
			ierr = VecCopy(b, grid->unknowns); CHKERRQ(ierr);
			// Destroy b
			ierr = VecDestroy(&b); CHKERRQ(ierr);
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown definition of the Runge-Kutta scheme.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
		// Apply Neumann boundary conditions
		for (int i=0; i<DIM2; i++) {
			if (ds->neumanns[i] != NULL) {
				ierr = VecDot(grid->unknowns, ds->neumanns[i], &neuscal); CHKERRQ(ierr);
				ierr = VecSetValue(grid->unknowns, ds->parent_solver->neu_rows[i], neuscal, INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(grid->unknowns); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(grid->unknowns); CHKERRQ(ierr);
			}
		}
		// Apply Robin boundary conditions
		for (int i=0; i<DIM2; i++) {
			if (ds->robins[i] != NULL) {
				ierr = VecDot(grid->unknowns, ds->robins[i], &robscal); CHKERRQ(ierr);
				ierr = VecSetValue(grid->unknowns, ds->parent_solver->rob_rows[i], robscal, INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(grid->unknowns); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(grid->unknowns); CHKERRQ(ierr);
			}
		}
		// Filter the signal
		if (ds->parent_solver->hasFilter) {
			ierr = VecDuplicate(grid->unknowns, &b); CHKERRQ(ierr);
			ierr = MatMult(ds->filter, grid->unknowns, b); CHKERRQ(ierr);
			ierr = VecAXPY(grid->unknowns, -1.0, b); CHKERRQ(ierr);
			ierr = VecDestroy(&b); CHKERRQ(ierr);
		}
		// Tell a few things to the user
		ierr = VecMax(grid->unknowns, NULL, &ukmax); CHKERRQ(ierr);
		ierr = VecMin(grid->unknowns, NULL, &ukmin); CHKERRQ(ierr);
		ierr = PetscPrintf(PETSC_COMM_WORLD,
					"Iter. %05d\t Time %.6e\t Min/Max uk (%.4e,%.4e)\t\n",
					step, step*ds->parent_solver->dt, ukmin, ukmax); CHKERRQ(ierr);
		// Export the data
		if (ds->parent_solver->export_frequency > 0) {
			if ((step % ds->parent_solver->export_frequency) == 0) {
				// Create the viewers and save the vectors
				PetscViewer Viewer = NULL;
				PetscChar filename[WORDSZ];
				if (grid->parent_grid->ndim == DIM2) {
					sprintf(filename, "sol_%06d.h5", step);
					ierr = PetscViewerHDF5Open(PETSC_COMM_WORLD, filename, FILE_MODE_WRITE, &Viewer); CHKERRQ(ierr);
				} else if (grid->parent_grid->ndim == DIM1) {
					sprintf(filename, "sol_%06d.dat", step);
					ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, filename, &Viewer); CHKERRQ(ierr);
					ierr = PetscViewerPushFormat(Viewer, PETSC_VIEWER_ASCII_MATLAB); CHKERRQ(ierr);
				} else {
					ierr = PetscPrintf(PETSC_COMM_WORLD, "Wrong dimension for solution output.\n"); CHKERRQ(ierr);
					ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
				}
				ierr = VecView(grid->x, Viewer); CHKERRQ(ierr);
				ierr = VecView(grid->unknowns, Viewer); CHKERRQ(ierr);
				ierr = PetscViewerDestroy(&Viewer); CHKERRQ(ierr);
				// Compute at this frequency too an L2 error if an exact solution exists
				if (grid->hasExact) {
					ierr = VecDuplicate(grid->unknowns, &c); CHKERRQ(ierr);
					ierr = VecCopy(grid->unknowns, c); CHKERRQ(ierr);
					ierr = VecAXPY(c, -1.0, grid->exact); CHKERRQ(ierr);
					ierr = VecNorm(c, NORM_2, &nL2diff); CHKERRQ(ierr);
					ierr = VecNorm(grid->unknowns, NORM_2, &nL2sol); CHKERRQ(ierr);
					errorL2 = nL2diff / nL2sol;
					ierr = PetscPrintf(PETSC_COMM_WORLD, "\t --> At this time, error L2 is %.2e\t\n", errorL2); CHKERRQ(ierr);
					ierr = VecDestroy(&c); CHKERRQ(ierr);
				}
			}
		}
	} // end of loop on iterations

	ierr = MatDestroy(&R); CHKERRQ(ierr);
	ierr = MatDestroy(&identity); CHKERRQ(ierr);

	return ierr;
}

/* Method to destroy, i.e. deallocate, a one-dimensional diffusion solver
*/
void destroy_diffusionSolver1D(DiffusionSolver1D *ds)
{
	destroy_rkParams(ds->parent_solver->rk_params);
	free(ds->parent_solver->dir_rows);
	free(ds->parent_solver->neu_rows);
	if (ds->parent_solver->hasFilter){
		MatDestroy(&(ds->filter));
	}
	free(ds->parent_solver);
	for (int i=0; i<DIM2; i++) {
		if (ds->neumanns[i] != NULL) {
			VecDestroy(&(ds->neumanns[i]));
		}
	}
	free(ds->neumanns);
	for (int i=0; i<DIM2; i++) {
		if (ds->robins[i] != NULL) {
			VecDestroy(&(ds->robins[i]));
		}
	}
	free(ds->robins);
	MatDestroy(&(ds->rhs));
	free(ds);
}
