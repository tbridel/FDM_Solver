/* This file contains the methods handling the two-dimensional solver for an diffusion equation
   @author : Thibault Bridel-Bertomeu
   @license: MIT
   @status : development
*/

#include "fdmsolver.h"
#ifdef LARGEST_STENCIL
#undef LARGEST_STENCIL
#endif
#define LARGEST_STENCIL 5

/* This method initializes a solver for the diffusion equation in 2D
*/
DiffusionSolver2D* init_diffusionSolver2D(PetscInt nequation_args, PetscChar equation_arguments[TWENTY*2][WORDSZ], Grid2D *grid)
{
	int eq_index;
	PetscChar con_string[WORDSZ] = "";
	PetscInt nBocosPnts = 0;
	DiffusionSolver2D *ds = (DiffusionSolver2D*) malloc(sizeof(DiffusionSolver2D));

	// Initialize the parent
	PetscPrintf(PETSC_COMM_WORLD, "\tInitializing parent solver ...\n");
	nBocosPnts = 2 * grid->parent_grid->nx + 2 * grid->ny;
	ds->parent_solver = init_Solver(nequation_args, equation_arguments, nBocosPnts);
	ds->ierr = ds->parent_solver->ierr;

	// Force filtering out for this category of solver
	ds->parent_solver->hasFilter = false;

	// Initialize the vectors for the Neumann boundary conditions
	ds->neumanns = (Vec*) malloc(nBocosPnts * sizeof(Vec));
	for (int i = 0; i < nBocosPnts; i++) {
		ds->neumanns[i] = NULL;
	}

	// Initialize the vectors for the Robin boundary conditions
	ds->robins = (Vec*) malloc(nBocosPnts * sizeof(Vec));
	for (int i = 0; i < nBocosPnts; i++) {
		ds->robins[i] = NULL;
	}

	// Get the conductivity from the user inputs
	PetscPrintf(PETSC_COMM_WORLD, "\tAcquiring conductivity...\n");
	for (int i=0; i < nequation_args; i++) {
		if (strstr(equation_arguments[i], "parameters") != NULL) {
			eq_index = ( strchr(equation_arguments[i], '=') - equation_arguments[i] ) + 1;
			strcpy(con_string, equation_arguments[i] + (eq_index + 1));
		}
	}
	if (sscanf(con_string, "[%lf,%lf]", &(ds->cond[0]), &(ds->cond[1])) != 2) {
		PetscPrintf(PETSC_COMM_WORLD, "For a 2D diffusion equation, a 2-elements conductivity vector such as [X,Y] must be input.\n");
		ds->ierr = PETSC_ERR_ARG_WRONG;
	}

	// Store the norm of the transport velocity
	ds->cond_norm = PetscSqrtScalar( PetscPowScalar(ds->cond[0], 2) + PetscPowScalar(ds->cond[1], 2) );

	// Store the CFL number
	ds->cfl[0] = ds->cond[0] * ds->parent_solver->dt / PetscPowScalar(grid->parent_grid->dx, 2.0);
	ds->cfl[1] = ds->cond[1] * ds->parent_solver->dt / PetscPowScalar(grid->dy, 2.0);

	// Call the routine to build the RHS matrix based on numerical scheme
	get_ds2D_rhs(ds, grid);

	// Call the routine to build the filter matrix based on numerical scheme
	if (ds->parent_solver->hasFilter) {
		get_ds2D_filter(ds, grid);
	}

	// Return the solver
	return ds;
}

/* This method yields the RHS matrix for a two dimensional diffusion solver based
   on the name of the scheme.
*/
void get_ds2D_rhs(DiffusionSolver2D *ds, Grid2D *grid)
{
	PetscInt idx, rstart, rend, nlocal;
	PetscInt *cols = NULL, vecsize = 0;
	PetscInt nx = grid->parent_grid->nx, ny = grid->ny;
	PetscInt *result = (PetscInt*) malloc(2*sizeof(PetscInt)), ii, jj;
	PetscMPIInt size;

	// Get info on the MPI layout
	ds->ierr = MPI_Comm_size(PETSC_COMM_WORLD, &size);

	// Get parallel layout of the grid->unknowns vector
	ds->ierr = VecGetSize(grid->unknowns, &vecsize);
	if (vecsize == grid->nvtx) {
		ds->ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend);
		ds->ierr = VecGetLocalSize(grid->unknowns, &nlocal);
	}

	// Prepare the RHS matrix
	PetscPrintf(PETSC_COMM_WORLD, "\tCreating RHS distributed AIJ matrix ...\n");
	ds->ierr = MatCreate(PETSC_COMM_WORLD, &(ds->rhs));
	if (vecsize == grid->nvtx) {
		ds->ierr = MatSetSizes(ds->rhs, nlocal, nlocal, grid->nvtx, grid->nvtx);
	} else {
		ds->ierr = MatSetSizes(ds->rhs, PETSC_DECIDE, PETSC_DECIDE, grid->nvtx, grid->nvtx);
	}
	ds->ierr = MatSetType(ds->rhs, MATAIJ);
	if (size==1) {
		ds->ierr = MatSeqAIJSetPreallocation(ds->rhs, LARGEST_STENCIL, NULL);
	} else {
		ds->ierr = MatMPIAIJSetPreallocation(ds->rhs, LARGEST_STENCIL, NULL, LARGEST_STENCIL, NULL);
	}
	if (vecsize != grid->nvtx) {
		ds->ierr = MatGetOwnershipRange(ds->rhs, &rstart, &rend);
	}

	// Condition on the scheme now
	PetscPrintf(PETSC_COMM_WORLD, "\tFilling the diagonals of the RHS matrix ...\n");
	if ((strcmp(ds->parent_solver->scheme_name, "RK4o6s_CS") == 0) ||
	    (strcmp(ds->parent_solver->scheme_name, "RK4CS") == 0) ||
	    (strcmp(ds->parent_solver->scheme_name, "RK4_CS") == 0)) {
		// Now loop on the inner points, skip the boundaries for now
		for (idx = rstart; idx < rend; idx ++){
			unravel_index(idx, nx, result); ii = result[0]; jj = result[1];
			//
			if ((ii-1 >= 0) && (ii+1 <= nx-1) &&
				(jj-1 >= 0) && (jj+1 <= ny-1)) {
				PetscScalar values[5] = {1.0*ds->cfl[1], 1.0*ds->cfl[0],
								 		 -2.0*ds->cfl[0]-2.0*ds->cfl[1],
								 		 1.0*ds->cfl[0], 1.0*ds->cfl[1]};
				//
				cols = (PetscInt*) malloc(5 * sizeof(PetscInt));
				cols[0]=idx-nx; cols[1]=idx-1;
				cols[2]=idx;
				cols[3]=idx+1; cols[4]=idx+nx;
				//
				ds->ierr = MatSetValues(ds->rhs,1,&idx,5,cols,values,INSERT_VALUES);
				//
				free(cols); cols = NULL;
			} else if ((jj-1 >= 0) && (jj+1 <= ny-1)) {
				PetscScalar values[3] = {1.0*ds->cfl[1], -2.0*ds->cfl[1], 1.0*ds->cfl[1]};
				//
				cols = (PetscInt*) malloc(3 * sizeof(PetscInt));
				cols[0]=idx-nx; cols[1]=idx; cols[2]=idx+nx;
				//
				ds->ierr = MatSetValues(ds->rhs,1,&idx,3,cols,values,INSERT_VALUES);
				//
				free(cols); cols = NULL;
			} else if ((ii-1 >= 0) && (ii+1 <= nx-1)) {
				PetscScalar values[3] = {1.0*ds->cfl[0], -2.0*ds->cfl[0], 1.0*ds->cfl[0]};
				//
				cols = (PetscInt*) malloc(3 * sizeof(PetscInt));
				cols[0]=idx-1; cols[1]=idx; cols[2]=idx+1;
				//
				ds->ierr = MatSetValues(ds->rhs,1,&idx,3,cols,values,INSERT_VALUES);
				//
				free(cols); cols = NULL;
			}
		}
	} else {
		PetscPrintf(PETSC_COMM_WORLD, "Unknown spatial scheme.\n");
		ds->ierr = PETSC_ERR_ARG_WRONG;
	}

	if (cols != NULL) free(cols);
	free(result);
}

/* This method is used to apply all kind of boundary condition onto
   the matrix(ces) of a 2D diffusion solver
*/
PetscErrorCode applyBocos_ds2D(PetscInt nboco_arguments, PetscChar boco_arguments[2*TWENTY][WORDSZ], Grid2D *grid, DiffusionSolver2D *ds)
{
	int eq_index;
	PetscBool ismin = true, isx = true, dox = false, doy = false;
	PetscChar bocotype[WORDSZ] = "";
	PetscErrorCode ierr = 0;

	// Parse the boundary condition arguments for proper assignment -- only for perio first
	for (int i=0; i < nboco_arguments; i++) {
		// Get the type of boundary condition for the current boundary
		if (strstr(boco_arguments[i], "xmin") != NULL) {
			ismin = true; isx = true;
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else if (strstr(boco_arguments[i], "xmax") != NULL) {
			ismin = false; isx = true;
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else if (strstr(boco_arguments[i], "ymin") != NULL) {
			ismin = true; isx = false;
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else if (strstr(boco_arguments[i], "ymax") != NULL) {
			ismin = false; isx = false;
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown boundary assignement.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
		// Switch on the type of boundary condition required by the user
		if (strcmp(bocotype, "periodic") == 0) {
			if (isx)
				dox = true;
			else
				doy = true;
			ierr = set_periodic_ds2D(ds, grid, isx); CHKERRQ(ierr);
			if (ds->parent_solver->hasFilter) {
				ierr = set_periodic_filter_ds2D(ds, grid); CHKERRQ(ierr);
			}
		} else if (strcmp(bocotype, "dirichlet") == 0) {
			continue;
		} else if (strcmp(bocotype, "neumann") == 0) {
			continue;
		} else if (startsWith(bocotype, "robin")) {
			continue;
		} else if (strcmp(bocotype, "open") == 0) {
			continue;
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown boundary condition type.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
	}

	// Parse the boundary condition arguments for proper assignment -- all but perio
	for (int i=0; i < nboco_arguments; i++) {
		// Get the type of boundary condition for the current boundary
		if (strstr(boco_arguments[i], "xmin") != NULL) {
			ismin = true; isx = true;
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else if (strstr(boco_arguments[i], "xmax") != NULL) {
			ismin = false; isx = true;
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else if (strstr(boco_arguments[i], "ymin") != NULL) {
			ismin = true; isx = false;
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else if (strstr(boco_arguments[i], "ymax") != NULL) {
			ismin = false; isx = false;
			eq_index = ( strchr(boco_arguments[i], '=') - boco_arguments[i] ) + 1;
			strcpy(bocotype, boco_arguments[i] + (eq_index + 1));
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown boundary assignement.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
		// Switch on the type of boundary condition required by the user
		if (strcmp(bocotype, "periodic") == 0) {
			continue;
		} else if (strcmp(bocotype, "dirichlet") == 0) {
			ierr = set_dirichlet_ds2D(ds, grid, ismin, isx); CHKERRQ(ierr);
			if (ds->parent_solver->hasFilter) {
				ierr = set_open_filter_ds2D(ds, grid, ismin); CHKERRQ(ierr);
			}
		} else if (strcmp(bocotype, "neumann") == 0) {
			ierr = set_neumann_ds2D(ds, grid, ismin, isx); CHKERRQ(ierr);
			if (ds->parent_solver->hasFilter) {
				ierr = set_open_filter_ds2D(ds, grid, ismin); CHKERRQ(ierr);
			}
		} else if (startsWith(bocotype, "robin")) {
			PetscScalar robA = 0.0, robB = 0.0;
			sscanf(bocotype, "robin,%lf,%lf", &robA, &robB);
			ierr = set_robin_ds2D(ds, grid, ismin, isx, robA, robB); CHKERRQ(ierr);
			if (ds->parent_solver->hasFilter) {
				ierr = set_open_filter_ds2D(ds, grid, ismin); CHKERRQ(ierr);
			}
		} else if (strcmp(bocotype, "open") == 0) {
			ierr = set_open_ds2D(ds, grid, ismin, isx); CHKERRQ(ierr);
			if (ds->parent_solver->hasFilter) {
				ierr = set_open_filter_ds2D(ds, grid, ismin); CHKERRQ(ierr);
			}
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown boundary condition type.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
	}

	// Assemble the RHS matrix
	ierr = PetscPrintf(PETSC_COMM_WORLD, "\tAssembling the RHS matrix ...\n"); CHKERRQ(ierr);
	ierr = MatAssemblyBegin(ds->rhs, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
	ierr = MatAssemblyEnd(ds->rhs, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

	// Assemble the filter matrix
	if (ds->parent_solver->hasFilter) {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "\tAssembling the filter matrix ...\n"); CHKERRQ(ierr);
		ierr = MatAssemblyBegin(ds->filter, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
		ierr = MatAssemblyEnd(ds->filter, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
	}

	return ierr;
}

/* Method to set the RHS matrix of a 2D diffusion solver for periodicity
*/
PetscErrorCode set_periodic_ds2D(DiffusionSolver2D *ds, Grid2D* grid, PetscBool isx)
{
	PetscErrorCode ierr = 0;
	PetscInt idx, rstart, rend;
	PetscInt nx = grid->parent_grid->nx, ny = grid->ny;
	PetscInt *cols = NULL, vecsize = 0;
	PetscInt *result = (PetscInt*)malloc(2*sizeof(PetscInt)), ii, jj;

	// Get parallel layout of the grid->unknowns vector
	ierr = VecGetSize(grid->unknowns, &vecsize); CHKERRQ(ierr);
	if (vecsize == grid->nvtx) {
		ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend); CHKERRQ(ierr);
	} else {
		ierr = MatGetOwnershipRange(ds->rhs, &rstart, &rend); CHKERRQ(ierr);
	}

	ierr = PetscPrintf(PETSC_COMM_WORLD, "\tImposing periodicity conditions ...\n"); CHKERRQ(ierr);
	if ((strcmp(ds->parent_solver->scheme_name, "RK4o6s_CS") == 0) ||
	    (strcmp(ds->parent_solver->scheme_name, "RK4CS") == 0) ||
	    (strcmp(ds->parent_solver->scheme_name, "RK4_CS") == 0)) {
		// Fill the vector of coefficients
		cols = (PetscInt*) malloc(3 * sizeof(PetscInt));
		// Now loop on the points closest to the boundaries
		for (idx = rstart; idx < rend; idx ++){
			unravel_index(idx, nx, result); ii = result[0]; jj = result[1];
			// Default assignement columns
			if (isx) {
				cols[0] = idx-1; cols[1] = idx; cols[2] = idx+1;
			} else {
				cols[0] = idx-1*nx; cols[1] = idx; cols[2] = idx+1*nx;
			}
			// Change function of the location
			if (isx && (ii==0)) {
				cols[0] = idx+nx-1;
			}
			if (isx && (ii==nx-1)) {
				cols[2] = idx-nx+1;
			}
			if (!isx && (jj==0)) {
				cols[0] = idx+(ny-1)*nx;
			}
			if (!isx && (jj==ny-1)) {
				cols[2] = idx-(ny-1)*nx;
			}
			//
			if (isx) {
				PetscScalar values[3] = {1.0*ds->cfl[0], -2.0*ds->cfl[0]-2.0*ds->cfl[1], 1.0*ds->cfl[0]};
				ierr = MatSetValues(ds->rhs,1,&idx,3,cols,values,INSERT_VALUES); CHKERRQ(ierr);
			} else {
				PetscScalar values[3] = {1.0*ds->cfl[1], -2.0*ds->cfl[0]-2.0*ds->cfl[1], 1.0*ds->cfl[1]};
				ierr = MatSetValues(ds->rhs,1,&idx,3,cols,values,INSERT_VALUES); CHKERRQ(ierr);
			}
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown spatial scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}

	free(cols);
	free(result);

	return ierr;
}

/* Method to set the RHS matrix of a 2D diffusion solver for open boundary condition
*/
PetscErrorCode set_open_ds2D(DiffusionSolver2D *ds, Grid2D* grid, PetscBool ismin, PetscBool isx)
{
	PetscErrorCode ierr = 0;
	PetscInt idx, rstart, rend;
	PetscInt *cols = NULL, vecsize=0;
	PetscScalar *values = NULL;
	PetscInt *result = (PetscInt*)malloc(2*sizeof(PetscInt)), ii, jj;
	PetscInt nx = grid->parent_grid->nx, ny = grid->ny;

	// Get parallel layout of the grid->unknowns vector
	ierr = VecGetSize(grid->unknowns, &vecsize); CHKERRQ(ierr);
	if (vecsize == grid->nvtx) {
		ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend); CHKERRQ(ierr);
	} else {
		ierr = MatGetOwnershipRange(ds->rhs, &rstart, &rend); CHKERRQ(ierr);
	}

	ierr = PetscPrintf(PETSC_COMM_WORLD, "\tImposing open conditions ...\n"); CHKERRQ(ierr);
	if ((strcmp(ds->parent_solver->scheme_name, "RK4o6s_CS") == 0) ||
	    (strcmp(ds->parent_solver->scheme_name, "RK4CS") == 0) ||
	    (strcmp(ds->parent_solver->scheme_name, "RK4_CS") == 0)) {
		// Fill the vector of coefficients
		cols = (PetscInt*) malloc(3 * sizeof(PetscInt));
		// Now loop on the points and stop only if close to the right boundary
		for (idx = rstart; idx < rend; idx ++){
			unravel_index(idx, nx, result); ii = result[0]; jj = result[1];
			if (ismin && isx && (ii == 0)) {
				for (int i=0; i<3; i++)
					cols[i] = idx+i;
				//
				values = (PetscScalar [3]) {1.0*ds->cfl[0], -2.0*ds->cfl[0], 1.0*ds->cfl[0]};
			} else if (!ismin && isx && (ii == nx-1)) {
				for (int i=0; i<3; i++)
					cols[i] = idx-i;
				//
				values = (PetscScalar [3]) {1.0*ds->cfl[0], -2.0*ds->cfl[0], 1.0*ds->cfl[0]};
			} else if (ismin && !isx && (jj == 0)) {
				for (int i=0; i<3; i++)
					cols[i] = idx+i*nx;
				//
				values = (PetscScalar [3]) {1.0*ds->cfl[1], -2.0*ds->cfl[1], 1.0*ds->cfl[1]};
			} else if (!ismin && !isx && (jj == ny-1)) {
				for (int i=0; i<3; i++)
					cols[i] = idx-i*nx;
				//
				values = (PetscScalar [3]) {1.0*ds->cfl[1], -2.0*ds->cfl[1], 1.0*ds->cfl[1]};
			} else {
				continue;
			}
			ierr = MatSetValues(ds->rhs,1,&idx,3,cols,values,ADD_VALUES); CHKERRQ(ierr);
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown spatial scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}

	free(cols);
	free(result);

	return ierr;
}

/* Method to set the RHS matrix of a 2D diffusion solver for an
   homogeneous dirichlet boundary condition
*/
PetscErrorCode set_dirichlet_ds2D(DiffusionSolver2D *ds, Grid2D* grid, PetscBool ismin, PetscBool isx)
{
	PetscErrorCode ierr = 0;
	PetscInt nx = grid->parent_grid->nx, ny = grid->ny;

	// Start by applying an open boundary condition to have the
	// right schemes close to the boundary points
	ierr = set_open_ds2D(ds, grid, ismin, isx);

	// Increment the number of rows to zero later during execution
	if (ismin && isx) {
		for (int i=0; i<ny; i++) {
			ds->parent_solver->ndir_rows ++;
			ds->parent_solver->dir_rows[ds->parent_solver->ndir_rows] = grid->ixmin[i];
		}
	} else if (!ismin && isx) {
		for (int i=0; i<ny; i++) {
			ds->parent_solver->ndir_rows ++;
			ds->parent_solver->dir_rows[ds->parent_solver->ndir_rows] = grid->ixmax[i];
		}
	} else if (ismin && !isx) {
		for (int i=0; i<nx; i++) {
			ds->parent_solver->ndir_rows ++;
			ds->parent_solver->dir_rows[ds->parent_solver->ndir_rows] = grid->iymin[i];
		}
	} else if (!ismin && !isx) {
		for (int i=0; i<nx; i++) {
			ds->parent_solver->ndir_rows ++;
			ds->parent_solver->dir_rows[ds->parent_solver->ndir_rows] = grid->iymax[i];
		}
	}

	return ierr;
}

/* Method to set the RHS matrix of a 2D diffusion solver for an
   homogeneous neumann boundary condition
*/
PetscErrorCode set_neumann_ds2D(DiffusionSolver2D *ds, Grid2D* grid, PetscBool ismin, PetscBool isx)
{
	PetscInt *rows = NULL;
	PetscErrorCode ierr = 0;
	PetscScalar *values = NULL;
	PetscInt nx = grid->parent_grid->nx, ny = grid->ny;

	// Start by applying an open boundary condition to have the
	// right schemes close to the boundary points
	ierr = set_open_ds2D(ds, grid, ismin, isx);

	// Condition on the scheme for the proper computation of homogeneous Neumann corrections
	if ((strcmp(ds->parent_solver->scheme_name, "RK4o6s_CS") == 0) ||
	    (strcmp(ds->parent_solver->scheme_name, "RK4CS") == 0) ||
	    (strcmp(ds->parent_solver->scheme_name, "RK4_CS") == 0)) {
		// Fill the vector of values
		values = (PetscScalar [2]) {-1.0, 1.0};
		for (int i=1; i<2; i++) {
				values[i] = - values[i] / values[0];
		}
		values[0] = 0.0;
		// Fill the vector of coefficients
		rows = (PetscInt*) malloc(2 * sizeof(PetscInt));
		// Now fix the right boundary
		if (ismin && isx) {
			for (int i=0; i<ny; i++) {
				ds->parent_solver->nneu_rows ++;
				ds->parent_solver->neu_rows[ds->parent_solver->nneu_rows] = grid->ixmin[i];
				//
				for (int j=0; j<2; j++)
					rows[j] = grid->ixmin[i]+j;
				//
				ierr = VecCreate(PETSC_COMM_WORLD, &(ds->neumanns[ds->parent_solver->nneu_rows])); CHKERRQ(ierr);
				ierr = VecSetSizes(ds->neumanns[ds->parent_solver->nneu_rows], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
				ierr = VecSetUp(ds->neumanns[ds->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecSet(ds->neumanns[ds->parent_solver->nneu_rows], 0.0); CHKERRQ(ierr);
				ierr = VecSetValues(ds->neumanns[ds->parent_solver->nneu_rows],2,rows,values,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(ds->neumanns[ds->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(ds->neumanns[ds->parent_solver->nneu_rows]); CHKERRQ(ierr);
			}
		} else if (!ismin && isx) {
			for (int i=0; i<ny; i++) {
				ds->parent_solver->nneu_rows ++;
				ds->parent_solver->neu_rows[ds->parent_solver->nneu_rows] = grid->ixmax[i];
				//
				for (int j=0; j<2; j++)
					rows[j] = grid->ixmax[i]-j;
				//
				ierr = VecCreate(PETSC_COMM_WORLD, &(ds->neumanns[ds->parent_solver->nneu_rows])); CHKERRQ(ierr);
				ierr = VecSetSizes(ds->neumanns[ds->parent_solver->nneu_rows], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
				ierr = VecSetUp(ds->neumanns[ds->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecSet(ds->neumanns[ds->parent_solver->nneu_rows], 0.0); CHKERRQ(ierr);
				ierr = VecSetValues(ds->neumanns[ds->parent_solver->nneu_rows],2,rows,values,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(ds->neumanns[ds->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(ds->neumanns[ds->parent_solver->nneu_rows]); CHKERRQ(ierr);
			}
		} else if (ismin && !isx) {
			for (int i=0; i<nx; i++) {
				ds->parent_solver->nneu_rows ++;
				ds->parent_solver->neu_rows[ds->parent_solver->nneu_rows] = grid->iymin[i];
				//
				for (int j=0; j<2; j++)
					rows[j] = grid->iymin[i]+j*nx;
				//
				ierr = VecCreate(PETSC_COMM_WORLD, &(ds->neumanns[ds->parent_solver->nneu_rows])); CHKERRQ(ierr);
				ierr = VecSetSizes(ds->neumanns[ds->parent_solver->nneu_rows], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
				ierr = VecSetUp(ds->neumanns[ds->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecSet(ds->neumanns[ds->parent_solver->nneu_rows], 0.0); CHKERRQ(ierr);
				ierr = VecSetValues(ds->neumanns[ds->parent_solver->nneu_rows],2,rows,values,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(ds->neumanns[ds->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(ds->neumanns[ds->parent_solver->nneu_rows]); CHKERRQ(ierr);
			}
		} else if (!ismin && !isx) {
			for (int i=0; i<nx; i++) {
				ds->parent_solver->nneu_rows ++;
				ds->parent_solver->neu_rows[ds->parent_solver->nneu_rows] = grid->iymax[i];
				//
				for (int j=0; j<2; j++)
					rows[j] = grid->iymax[i]-j*nx;
				//
				ierr = VecCreate(PETSC_COMM_WORLD, &(ds->neumanns[ds->parent_solver->nneu_rows])); CHKERRQ(ierr);
				ierr = VecSetSizes(ds->neumanns[ds->parent_solver->nneu_rows], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
				ierr = VecSetUp(ds->neumanns[ds->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecSet(ds->neumanns[ds->parent_solver->nneu_rows], 0.0); CHKERRQ(ierr);
				ierr = VecSetValues(ds->neumanns[ds->parent_solver->nneu_rows],2,rows,values,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(ds->neumanns[ds->parent_solver->nneu_rows]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(ds->neumanns[ds->parent_solver->nneu_rows]); CHKERRQ(ierr);
			}
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown spatial scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}

	free(rows);

	return ierr;
}

/* Method to set the RHS matrix of a 2D diffusion solver for an
   homogeneous robin boundary condition
*/
PetscErrorCode set_robin_ds2D(DiffusionSolver2D *ds, Grid2D* grid, PetscBool ismin, PetscBool isx, PetscScalar robA, PetscScalar robB)
{
	PetscInt *rows = NULL;
	PetscErrorCode ierr = 0;
	PetscScalar *values = NULL;
	PetscInt nx = grid->parent_grid->nx, ny = grid->ny;

	// Start by applying an open boundary condition to have the
	// right schemes close to the boundary points
	ierr = set_open_ds2D(ds, grid, ismin, isx);

	// Condition on the scheme for the proper computation of homogeneous Neumann corrections
	if ((strcmp(ds->parent_solver->scheme_name, "RK4o6s_CS") == 0) ||
	    (strcmp(ds->parent_solver->scheme_name, "RK4CS") == 0) ||
	    (strcmp(ds->parent_solver->scheme_name, "RK4_CS") == 0)) {
		// Fill the vector of values
		PetscScalar shift = (isx)?grid->parent_grid->dx:grid->dy;
		values = (PetscScalar [2]) {-1.0*robB+shift*robA, 1.0*robB};
		for (int i=1; i<2; i++) {
				values[i] = - values[i] / values[0];
		}
		values[0] = 0.0;
		// Fill the vector of coefficients
		rows = (PetscInt*) malloc(2 * sizeof(PetscInt));
		// Now fix the right boundary
		if (ismin && isx) {
			for (int i=0; i<ny; i++) {
				ds->parent_solver->nrob_rows ++;
				ds->parent_solver->rob_rows[ds->parent_solver->nrob_rows] = grid->ixmin[i];
				//
				for (int j=0; j<2; j++)
					rows[j] = grid->ixmin[i]+j;
				//
				ierr = VecCreate(PETSC_COMM_WORLD, &(ds->robins[ds->parent_solver->nrob_rows])); CHKERRQ(ierr);
				ierr = VecSetSizes(ds->robins[ds->parent_solver->nrob_rows], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
				ierr = VecSetUp(ds->robins[ds->parent_solver->nrob_rows]); CHKERRQ(ierr);
				ierr = VecSet(ds->robins[ds->parent_solver->nrob_rows], 0.0); CHKERRQ(ierr);
				ierr = VecSetValues(ds->robins[ds->parent_solver->nrob_rows],2,rows,values,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(ds->robins[ds->parent_solver->nrob_rows]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(ds->robins[ds->parent_solver->nrob_rows]); CHKERRQ(ierr);
			}
		} else if (!ismin && isx) {
			for (int i=0; i<ny; i++) {
				ds->parent_solver->nrob_rows ++;
				ds->parent_solver->rob_rows[ds->parent_solver->nrob_rows] = grid->ixmax[i];
				//
				for (int j=0; j<2; j++)
					rows[j] = grid->ixmax[i]-j;
				//
				ierr = VecCreate(PETSC_COMM_WORLD, &(ds->robins[ds->parent_solver->nrob_rows])); CHKERRQ(ierr);
				ierr = VecSetSizes(ds->robins[ds->parent_solver->nrob_rows], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
				ierr = VecSetUp(ds->robins[ds->parent_solver->nrob_rows]); CHKERRQ(ierr);
				ierr = VecSet(ds->robins[ds->parent_solver->nrob_rows], 0.0); CHKERRQ(ierr);
				ierr = VecSetValues(ds->robins[ds->parent_solver->nrob_rows],2,rows,values,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(ds->robins[ds->parent_solver->nrob_rows]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(ds->robins[ds->parent_solver->nrob_rows]); CHKERRQ(ierr);
			}
		} else if (ismin && !isx) {
			for (int i=0; i<nx; i++) {
				ds->parent_solver->nrob_rows ++;
				ds->parent_solver->rob_rows[ds->parent_solver->nrob_rows] = grid->iymin[i];
				//
				for (int j=0; j<2; j++)
					rows[j] = grid->iymin[i]+j*nx;
				//
				ierr = VecCreate(PETSC_COMM_WORLD, &(ds->robins[ds->parent_solver->nrob_rows])); CHKERRQ(ierr);
				ierr = VecSetSizes(ds->robins[ds->parent_solver->nrob_rows], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
				ierr = VecSetUp(ds->robins[ds->parent_solver->nrob_rows]); CHKERRQ(ierr);
				ierr = VecSet(ds->robins[ds->parent_solver->nrob_rows], 0.0); CHKERRQ(ierr);
				ierr = VecSetValues(ds->robins[ds->parent_solver->nrob_rows],2,rows,values,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(ds->robins[ds->parent_solver->nrob_rows]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(ds->robins[ds->parent_solver->nrob_rows]); CHKERRQ(ierr);
			}
		} else if (!ismin && !isx) {
			for (int i=0; i<nx; i++) {
				ds->parent_solver->nrob_rows ++;
				ds->parent_solver->rob_rows[ds->parent_solver->nrob_rows] = grid->iymax[i];
				//
				for (int j=0; j<2; j++)
					rows[j] = grid->iymax[i]-j*nx;
				//
				ierr = VecCreate(PETSC_COMM_WORLD, &(ds->robins[ds->parent_solver->nrob_rows])); CHKERRQ(ierr);
				ierr = VecSetSizes(ds->robins[ds->parent_solver->nrob_rows], PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
				ierr = VecSetUp(ds->robins[ds->parent_solver->nrob_rows]); CHKERRQ(ierr);
				ierr = VecSet(ds->robins[ds->parent_solver->nrob_rows], 0.0); CHKERRQ(ierr);
				ierr = VecSetValues(ds->robins[ds->parent_solver->nrob_rows],2,rows,values,INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(ds->robins[ds->parent_solver->nrob_rows]); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(ds->robins[ds->parent_solver->nrob_rows]); CHKERRQ(ierr);
			}
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown spatial scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}

	free(rows);

	return ierr;
}

/* Method for the execution of a two dimensional diffusion solver
*/
PetscErrorCode execute_ds2D(DiffusionSolver2D *ds, Grid2D* grid)
{
	Mat identity, R, mlist1, mlist2;
	PetscErrorCode ierr = 0;
	PetscScalar factor = 0.0, ukmax = 0.0, ukmin = 0.0;
	PetscScalar nL2diff = 0.0, nL2sol = 0.0, errorL2 = 0.0;
	PetscScalar neuscal = 0.0, robscal = 0.0;
	PetscInt step, idx, rstart, rend, nlocal, vecsize;
	Vec b, c;
	PetscMPIInt size;

	// Get info on the MPI layout
	ierr = MPI_Comm_size(PETSC_COMM_WORLD, &size); CHKERRQ(ierr);

	// Get parallel layout of the grid->unknowns vector
	ierr = VecGetSize(grid->unknowns, &vecsize); CHKERRQ(ierr);
	if (vecsize == grid->nvtx) {
		ierr = VecGetOwnershipRange(grid->unknowns, &rstart, &rend); CHKERRQ(ierr);
		ierr = VecGetLocalSize(grid->unknowns, &nlocal); CHKERRQ(ierr);
	} else {
		ierr = MatGetOwnershipRange(ds->rhs, &rstart, &rend); CHKERRQ(ierr);
		ierr = MatGetLocalSize(ds->rhs, &nlocal, &nlocal); CHKERRQ(ierr);
	}

	// Create an identity matrix, it can come handy
	ierr = MatCreate(PETSC_COMM_WORLD, &identity); CHKERRQ(ierr);
	ierr = MatSetSizes(identity, nlocal, nlocal, grid->nvtx, grid->nvtx); CHKERRQ(ierr);
	ierr = MatSetType(identity, MATAIJ); CHKERRQ(ierr);
	ierr = MatSetUp(identity); CHKERRQ(ierr);
	for (idx = rstart; idx < rend ; idx++) {
		ierr = MatSetValue(identity, idx, idx, 1.0, INSERT_VALUES); CHKERRQ(ierr);
	}
	ierr = MatAssemblyBegin(identity, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
	ierr = MatAssemblyEnd(identity, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

	// Prepare a few other objects before the big loop
	// First pass the RHS actually on the right hand side and scale it by the space step
	ierr = MatScale(ds->rhs, 1.0/ds->parent_solver->dt); CHKERRQ(ierr);
	// Then handle the preprocessing for the RK matrix
	if (ds->parent_solver->rk_params->has_gammas) {
		PetscPrintf(PETSC_COMM_WORLD, "\tPreparing Runge-Kutta matrix ...\n");
		// Create the matrix and copy the values from the RHS and assemble
		ierr = MatDuplicate(ds->rhs, MAT_COPY_VALUES, &R); CHKERRQ(ierr);
		ierr = MatScale(R, ds->parent_solver->dt); CHKERRQ(ierr);
		ierr = MatAssemblyBegin(R, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
		ierr = MatAssemblyEnd(R, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
		// First element of mlist is the RHS matrix
		ierr = MatDuplicate(ds->rhs, MAT_COPY_VALUES, &mlist1); CHKERRQ(ierr);
		// Now loop over the remaining RK stages to simulate matrix power
		for (idx = 1; idx < ds->parent_solver->rk_params->steps; idx++) {
			factor = PetscPowScalar(ds->parent_solver->dt, idx+1)*ds->parent_solver->rk_params->gammas[idx];
			ierr = MatMatMult(ds->rhs, mlist1, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &mlist2); CHKERRQ(ierr);
			ierr = MatDestroy(&mlist1); CHKERRQ(ierr);
			ierr = MatAXPY(R, factor, mlist2, DIFFERENT_NONZERO_PATTERN); CHKERRQ(ierr);
			ierr = MatDuplicate(mlist2, MAT_COPY_VALUES, &mlist1); CHKERRQ(ierr);
			ierr = MatDestroy(&mlist2); CHKERRQ(ierr);
		}
		// Destroy the mlist object
		MatDestroy(&mlist1);
		MatDestroy(&mlist2);
		// Add identity to R
		ierr = MatAXPY(R, 1.0, identity, DIFFERENT_NONZERO_PATTERN); CHKERRQ(ierr);
		// Change R structure in case of dirichlet boco
		{
			PetscInt *rows = (PetscInt*) malloc(sizeof(PetscInt) * (ds->parent_solver->ndir_rows+1));
			for (int i=0; i<(ds->parent_solver->ndir_rows+1); i++) {
				rows[i] = ds->parent_solver->dir_rows[i];
			}
			ierr = MatZeroRowsColumns(R, ds->parent_solver->ndir_rows+1, rows, 0.0, 0, 0); CHKERRQ(ierr);
			if (ds->parent_solver->hasFilter) {
				ierr = MatZeroRowsColumns(ds->filter, ds->parent_solver->ndir_rows+1, rows, 0.0, 0, 0); CHKERRQ(ierr);
			}
			free(rows);
		}
		// Change filter structure in case of Neumann boco or Robin boco
		if (ds->parent_solver->hasFilter) {
			for (int i=0; i<(ds->parent_solver->nneu_rows+1); i++) {
				if (ds->neumanns[i] != NULL) {
					ierr = MatZeroRows(ds->filter, 1, ds->parent_solver->neu_rows + i, 0.0, 0, 0); CHKERRQ(ierr);
				}
			}
			for (int i=0; i<(ds->parent_solver->nrob_rows+1); i++) {
				if (ds->robins[i] != NULL) {
					ierr = MatZeroRows(ds->filter, 1, ds->parent_solver->rob_rows + i, 0.0, 0, 0); CHKERRQ(ierr);
				}
			}
		}
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown definition of the Runge-Kutta scheme.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
	}
	// Scale the filter by the strength input by the user
	if (ds->parent_solver->hasFilter) {
		ierr = MatScale(ds->filter, ds->parent_solver->filter_strength); CHKERRQ(ierr);
	}

	// Export the initial solution
	{
		step = 0;
		// Create the viewers and save the vectors
		PetscViewer Viewer = NULL;
		PetscChar filename[WORDSZ];
		if (grid->parent_grid->ndim == DIM2) {
			sprintf(filename, "sol_%06d.h5", step);
			ierr = PetscViewerHDF5Open(PETSC_COMM_WORLD, filename, FILE_MODE_WRITE, &Viewer); CHKERRQ(ierr);
		} else if (grid->parent_grid->ndim == DIM1) {
			sprintf(filename, "sol_%06d.dat", step);
			ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, filename, &Viewer); CHKERRQ(ierr);
			ierr = PetscViewerPushFormat(Viewer, PETSC_VIEWER_ASCII_MATLAB); CHKERRQ(ierr);
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Wrong dimension for solution output.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
		ierr = VecView(grid->x, Viewer); CHKERRQ(ierr);
		ierr = VecView(grid->y, Viewer); CHKERRQ(ierr);
		ierr = VecView(grid->unknowns, Viewer); CHKERRQ(ierr);
		ierr = PetscViewerDestroy(&Viewer); CHKERRQ(ierr);
	}

	// Loop on iterations
	ierr = PetscPrintf(PETSC_COMM_WORLD, "\tMarching the equation in time ...\n"); CHKERRQ(ierr);
	for (step = 1; step <= ds->parent_solver->nit; step ++) {
		// Implementation differs function of the way the RK scheme is made
		if (ds->parent_solver->rk_params->has_gammas) {
			// Get an auxiliary vector
			ierr = VecDuplicate(grid->unknowns, &b); CHKERRQ(ierr);
			// Apply the temporal scheme
			ierr = MatMult(R, grid->unknowns, b); CHKERRQ(ierr);
			// Save the result
			ierr = VecCopy(b, grid->unknowns); CHKERRQ(ierr);
			// Destroy b
			ierr = VecDestroy(&b); CHKERRQ(ierr);
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown definition of the Runge-Kutta scheme.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
		}
		// Apply Neumann boundary conditions
		for (int i=0; i<(ds->parent_solver->nneu_rows+1); i++) {
			if (ds->neumanns[i] != NULL) {
				ierr = VecDot(grid->unknowns, ds->neumanns[i], &neuscal); CHKERRQ(ierr);
				ierr = VecSetValue(grid->unknowns, ds->parent_solver->neu_rows[i], neuscal, INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(grid->unknowns); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(grid->unknowns); CHKERRQ(ierr);
			}
		}
		// Apply Robin boundary conditions
		for (int i=0; i<(ds->parent_solver->nrob_rows+1); i++) {
			if (ds->robins[i] != NULL) {
				ierr = VecDot(grid->unknowns, ds->robins[i], &robscal); CHKERRQ(ierr);
				ierr = VecSetValue(grid->unknowns, ds->parent_solver->rob_rows[i], robscal, INSERT_VALUES); CHKERRQ(ierr);
				ierr = VecAssemblyBegin(grid->unknowns); CHKERRQ(ierr);
				ierr = VecAssemblyEnd(grid->unknowns); CHKERRQ(ierr);
			}
		}
		// Filter the signal
		if (ds->parent_solver->hasFilter) {
			ierr = VecDuplicate(grid->unknowns, &b); CHKERRQ(ierr);
			ierr = MatMult(ds->filter, grid->unknowns, b); CHKERRQ(ierr);
			ierr = VecAXPY(grid->unknowns, -1.0, b); CHKERRQ(ierr);
			ierr = VecDestroy(&b); CHKERRQ(ierr);
		}
		// Tell a few things to the user
		ierr = VecMax(grid->unknowns, NULL, &ukmax); CHKERRQ(ierr);
		ierr = VecMin(grid->unknowns, NULL, &ukmin); CHKERRQ(ierr);
		ierr = PetscPrintf(PETSC_COMM_WORLD,
					"Iter. %05d\t Time %.6e\t Min/Max uk (%.4e,%.4e)\t\n",
					step, step*ds->parent_solver->dt, ukmin, ukmax); CHKERRQ(ierr);
		// Export the data
		if (ds->parent_solver->export_frequency > 0) {
			if ((step % ds->parent_solver->export_frequency) == 0) {
				// Create the viewers and save the vectors
				PetscViewer Viewer = NULL;
				PetscChar filename[WORDSZ];
				if (grid->parent_grid->ndim == DIM2) {
					sprintf(filename, "sol_%06d.h5", step);
					ierr = PetscViewerHDF5Open(PETSC_COMM_WORLD, filename, FILE_MODE_WRITE, &Viewer); CHKERRQ(ierr);
				} else if (grid->parent_grid->ndim == DIM1) {
					sprintf(filename, "sol_%06d.dat", step);
					ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, filename, &Viewer); CHKERRQ(ierr);
					ierr = PetscViewerPushFormat(Viewer, PETSC_VIEWER_ASCII_MATLAB); CHKERRQ(ierr);
				} else {
					ierr = PetscPrintf(PETSC_COMM_WORLD, "Wrong dimension for solution output.\n"); CHKERRQ(ierr);
					ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
				}
				ierr = VecView(grid->x, Viewer); CHKERRQ(ierr);
				ierr = VecView(grid->y, Viewer); CHKERRQ(ierr);
				ierr = VecView(grid->unknowns, Viewer); CHKERRQ(ierr);
				ierr = PetscViewerDestroy(&Viewer); CHKERRQ(ierr);
				// Compute at this frequency too an L2 error if an exact solution exists
				if (grid->hasExact) {
					ierr = VecDuplicate(grid->unknowns, &c); CHKERRQ(ierr);
					ierr = VecCopy(grid->unknowns, c); CHKERRQ(ierr);
					ierr = VecAXPY(c, -1.0, grid->exact); CHKERRQ(ierr);
					ierr = VecNorm(c, NORM_2, &nL2diff); CHKERRQ(ierr);
					ierr = VecNorm(grid->unknowns, NORM_2, &nL2sol); CHKERRQ(ierr);
					errorL2 = nL2diff / nL2sol;
					ierr = PetscPrintf(PETSC_COMM_WORLD, "\t --> At this time, error L2 is %.2e\t\n", errorL2); CHKERRQ(ierr);
					ierr = VecDestroy(&c); CHKERRQ(ierr);
				}
			}
		}
	} // end of loop on iterations

	ierr = MatDestroy(&R); CHKERRQ(ierr);
	ierr = MatDestroy(&identity); CHKERRQ(ierr);

	return ierr;
}

/* Method to destroy, i.e. deallocate, a two-dimensional diffusion solver
*/
void destroy_diffusionSolver2D(DiffusionSolver2D *ds, Grid2D *grid)
{
	PetscInt nBocosPnts = 2 * grid->parent_grid->nx + 2 * grid->ny;

	destroy_rkParams(ds->parent_solver->rk_params);
	free(ds->parent_solver->dir_rows);
	free(ds->parent_solver->neu_rows);
	if (ds->parent_solver->hasFilter){
		MatDestroy(&(ds->filter));
	}
	free(ds->parent_solver);
	for (int i=0; i<nBocosPnts; i++) {
		if (ds->neumanns[i] != NULL) {
			VecDestroy(&(ds->neumanns[i]));
		}
	}
	free(ds->neumanns);
	for (int i=0; i<nBocosPnts; i++) {
		if (ds->robins[i] != NULL) {
			VecDestroy(&(ds->robins[i]));
		}
	}
	free(ds->robins);
	MatDestroy(&(ds->rhs));
	free(ds);
}
