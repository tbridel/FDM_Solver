/* This file contains the methods handling the filters for a
   two-dimensional solver for the linear acoustics equation
   @author : Thibault Bridel-Bertomeu
   @license: MIT
   @status : development
*/

#include "fdmsolver.h"

/* This method yields the filter matrix for a two dimensional linear acoustics solver based
   on the name of the filter scheme.
*/
void get_las2D_filter(LinearAcousticsSolver2D *las, Grid2D *grid)
{
	PetscInt nlocal, vecsize;

	// Get parallel layout of the grid->unknowns vector
	las->ierr = VecGetSize(grid->unknowns, &vecsize);
	las->ierr = VecGetLocalSize(grid->unknowns, &nlocal);

	// Declare the RHS matrix
	las->ierr = MatCreate(PETSC_COMM_WORLD, &(las->filter));
	las->ierr = MatSetSizes(las->filter, nlocal, nlocal, vecsize, vecsize);
	las->ierr = MatSetType(las->filter, MATAIJ);
	las->ierr = MatSetUp(las->filter);

	// Call the two subroutines
	PetscPrintf(PETSC_COMM_WORLD, "\tFilling the diagonals of the filter matrix ...\n");
	get_ts2D_filter(las->ts_1, grid);
	get_ts2D_filter(las->ts_2, grid);
}
