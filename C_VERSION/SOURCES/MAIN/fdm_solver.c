/* This file is the main file for the C version of the FDM solver.
   All other files in the SOURCES directory are either class
   or methods definition packages.
   @author : Thibault Bridel-Bertomeu
   @license: MIT
   @status : development
*/

// Include petscsys.h: contains the base PETSc routines and definition
#include "../IO/arguments_parser.h"
#include "../GRID/fdmgrid.h"
#include "../SOLVER/fdmsolver.h"
#include <petscsys.h>

/* Main method of the fdm_solver program.
   All other methods are invoked from here.
   (1) Parsing of the argument list
   (2) Building the appropriate grid to work on
   (3) Creating the appropriate solver
   (4) Executing the solver to compute and, if requested, write out the solutions
*/
PetscErrorCode main(PetscInt argc, PetscChar** argv)
{
	// Declaration of the variables
	Argument* args = NULL;
	char *equationName;
	int eq_index;
	PetscChar *fdm_home, path2pp[STRSZ] = "";
	PetscErrorCode ierr = 0;
	PetscInt ndim = 0;
	PetscMPIInt rank, size;

	// Initialize Petsc and MPI at the same time
	ierr = PetscInitialize(&argc, &argv, (char*)0, NULL); if (ierr) return ierr;
	ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank); CHKERRQ(ierr);
	ierr = MPI_Comm_size(PETSC_COMM_WORLD, &size); CHKERRQ(ierr);

	// Get the name of the user parameter file and parse it (outside method)
	args = init_argparser(argc, argv);

	// Get the number of dimensions of the problem
	for (int i=0; i < args->ngrid_arguments; i++) {
		if (strstr(args->grid_arguments[i], "ndim") != NULL) {
			eq_index = ( strchr(args->grid_arguments[i], '=') - args->grid_arguments[i] ) + 1;
			ndim = (PetscInt) atoi(args->grid_arguments[i] + (eq_index + 1));
			break;
		}
	}

	// Get the name of equation to solve
	for (int i=0; i < args->nequation_arguments; i++) {
		if (strstr(args->equation_arguments[i], "equation_name") != NULL) {
			eq_index = ( strchr(args->equation_arguments[i], '=') - args->equation_arguments[i] ) + 1;
			equationName = args->equation_arguments[i] + (eq_index + 1);
		}
	}

	// Switch on the number of dimensions
	/*****************/
	/***** DIM 1 *****/
	/*****************/
	if (ndim == DIM1) {
		// Declare the mesh and make it as well as initialize the solution
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Initializing a 1D grid ...\n"); CHKERRQ(ierr);
		Grid1D *grid = NULL;
		grid = init_grid1D(args->ngrid_arguments, args->grid_arguments, args->nboco_arguments, args->boco_arguments);
		ierr = PetscPrintf(PETSC_COMM_WORLD, "\tInitializing the solution ...\n"); CHKERRQ(ierr);
		ierr = initialize_solution1D(args->ninit_values, args->init_values, args->nexact_values, args->exact_values, grid); CHKERRQ(ierr);
		// Conditional on the equation to solve to invoke the right type of solver
		/*-- TRANSPORT --*/
		if (strcmp(equationName, "transport") == 0) {
			// Initialize the solver
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Initializing a 1D transport solver ...\n"); CHKERRQ(ierr);
			TransportSolver1D* ts = NULL;
			ts = init_transportSolver1D(args->nequation_arguments, args->equation_arguments, grid, NULL, 0);
			CHKERRQ(ts->parent_solver->rk_params->ierr); CHKERRQ(ts->parent_solver->ierr); CHKERRQ(ts->ierr);
			// Apply the boundary condition
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Applying boundary conditions ...\n"); CHKERRQ(ierr);
			ierr = applyBocos_ts1D(args->nboco_arguments, args->boco_arguments, grid, ts); CHKERRQ(ierr);
			// Execute
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Executing ...\n"); CHKERRQ(ierr);
			ierr = execute_ts1D(ts, grid); CHKERRQ(ierr);
			// Post-process solution files if any found
			if (ts->parent_solver->export_frequency > 0) {
				ierr = PetscPrintf(PETSC_COMM_WORLD, "Post-processing solution files ...\n"); CHKERRQ(ierr);
				if (rank == 0) {
					fdm_home = getenv("FDM_HOME");
					sprintf(path2pp, "%s/C_VERSION/SOURCES/POST_PROCESSING/solution_files_pp.py  --dim=%d --nx=%d", fdm_home, ndim, grid->parent_grid->nx);
					if (system(path2pp) == -1) {
						ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
					}
				}
				MPI_Barrier(PETSC_COMM_WORLD);
			}
			// Deallocate everybody
			destroy_transportSolver1D(ts);
		/*-- DIFFUSION --*/
		} else if (strcmp(equationName, "diffusion") == 0) {
			// Initialize the solver
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Initializing a 1D diffusion solver ...\n"); CHKERRQ(ierr);
			DiffusionSolver1D* ds = NULL;
			ds = init_diffusionSolver1D(args->nequation_arguments, args->equation_arguments, grid);
			CHKERRQ(ds->parent_solver->rk_params->ierr); CHKERRQ(ds->parent_solver->ierr); CHKERRQ(ds->ierr);
			// Apply the boundary condition
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Applying boundary conditions ...\n"); CHKERRQ(ierr);
			ierr = applyBocos_ds1D(args->nboco_arguments, args->boco_arguments, grid, ds); CHKERRQ(ierr);
			// Execute
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Executing ...\n"); CHKERRQ(ierr);
			ierr = execute_ds1D(ds, grid); CHKERRQ(ierr);
			// Post-process solution files if any found
			if (ds->parent_solver->export_frequency > 0) {
				ierr = PetscPrintf(PETSC_COMM_WORLD, "Post-processing solution files ...\n"); CHKERRQ(ierr);
				if (rank == 0) {
					fdm_home = getenv("FDM_HOME");
					sprintf(path2pp, "%s/C_VERSION/SOURCES/POST_PROCESSING/solution_files_pp.py  --dim=%d --nx=%d", fdm_home, ndim, grid->parent_grid->nx);
					if (system(path2pp) == -1) {
						ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
					}
				}
				MPI_Barrier(PETSC_COMM_WORLD);
			}
			// Deallocate everybody
			destroy_diffusionSolver1D(ds);
		/*-- ADVECTION DIFFUSION --*/
		} else if (strcmp(equationName, "advection-diffusion") == 0) {
			// Initialize the solver
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Initializing a 1D advection-diffusion solver ...\n"); CHKERRQ(ierr);
			AdvectionDiffusionSolver1D* ads = NULL;
			ads = init_advectionDiffusionSolver1D(args->nequation_arguments, args->equation_arguments, grid);
			CHKERRQ(ads->transport_solver->parent_solver->rk_params->ierr); CHKERRQ(ads->transport_solver->parent_solver->ierr);
			CHKERRQ(ads->diffusion_solver->parent_solver->rk_params->ierr); CHKERRQ(ads->diffusion_solver->parent_solver->ierr);
			CHKERRQ(ads->diffusion_solver->ierr); CHKERRQ(ads->transport_solver->ierr); CHKERRQ(ads->ierr);
			// Apply the boundary condition
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Applying boundary conditions ...\n"); CHKERRQ(ierr);
			ierr = applyBocos_ads1D(args->nboco_arguments, args->boco_arguments, grid, ads); CHKERRQ(ierr);
			// Execute
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Executing ...\n"); CHKERRQ(ierr);
			ierr = execute_ads1D(ads, grid); CHKERRQ(ierr);
			// Post-process solution files if any found
			if (ads->transport_solver->parent_solver->export_frequency > 0) {
				ierr = PetscPrintf(PETSC_COMM_WORLD, "Post-processing solution files ...\n"); CHKERRQ(ierr);
				if (rank == 0) {
					fdm_home = getenv("FDM_HOME");
					sprintf(path2pp, "%s/C_VERSION/SOURCES/POST_PROCESSING/solution_files_pp.py  --dim=%d --nx=%d", fdm_home, ndim, grid->parent_grid->nx);
					if (system(path2pp) == -1) {
						ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
					}
				}
				MPI_Barrier(PETSC_COMM_WORLD);
			}
			// Deallocate everybody
			destroy_advectionDiffusionSolver1D(ads);
		/*-- LINEAR ACOUSTICS --*/
		} else if (strcmp(equationName, "linear-acoustics") == 0) {
			// Initialize the solver
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Initializing a 1D linear-acoustics solver ...\n"); CHKERRQ(ierr);
			LinearAcousticsSolver1D* las = NULL;
			las = init_linearAcousticsSolver1D(args->nequation_arguments, args->equation_arguments, grid);
			CHKERRQ(las->ts_1->parent_solver->rk_params->ierr); CHKERRQ(las->ts_1->parent_solver->ierr);
			CHKERRQ(las->ts_2->parent_solver->rk_params->ierr); CHKERRQ(las->ts_2->parent_solver->ierr);
			CHKERRQ(las->ts_1->ierr); CHKERRQ(las->ts_2->ierr); CHKERRQ(las->ierr);
			// Apply the boundary condition
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Applying boundary conditions ...\n"); CHKERRQ(ierr);
			ierr = applyBocos_las1D(args->nboco_arguments, args->boco_arguments, grid, las); CHKERRQ(ierr);
			// Execute
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Executing ...\n"); CHKERRQ(ierr);
			ierr = execute_las1D(las, grid); CHKERRQ(ierr);
			// Post-process solution files if any found
			if (las->ts_1->parent_solver->export_frequency > 0) {
				ierr = PetscPrintf(PETSC_COMM_WORLD, "Post-processing solution files ...\n"); CHKERRQ(ierr);
				if (rank == 0) {
					fdm_home = getenv("FDM_HOME");
					sprintf(path2pp, "%s/C_VERSION/SOURCES/POST_PROCESSING/solution_files_pp.py  --dim=%d --nx=%d", fdm_home, ndim, grid->parent_grid->nx);
					if (system(path2pp) == -1) {
						ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
					}
				}
				MPI_Barrier(PETSC_COMM_WORLD);
			}
			// Deallocate everybody
			destroy_linearAcousticsSolver1D(las);
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Only transport equation has been implemented.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_OUTOFRANGE; CHKERRQ(ierr);
		}
		// Deallocate everybody
		destroy_grid1D(grid);
	/*****************/
	/***** DIM 2 *****/
	/*****************/
	} else if (ndim == DIM2) {
		// Declare the mesh and make it as well as initialize the solution
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Initializing a 2D grid ...\n"); CHKERRQ(ierr);
		Grid2D *grid = NULL;
		grid = init_grid2D(args->ngrid_arguments, args->grid_arguments, args->nboco_arguments, args->boco_arguments); CHKERRQ(grid->ierr);
		ierr = PetscPrintf(PETSC_COMM_WORLD, "\tInitializing the solution ...\n"); CHKERRQ(ierr);
		ierr = initialize_solution2D(args->ninit_values, args->init_values, args->nexact_values, args->exact_values, grid); CHKERRQ(ierr);
		// Conditional on the equation to solve to invoke the right type of solver
		/*-- TRANSPORT --*/
		if (strcmp(equationName, "transport") == 0) {
			// Initialize the solver
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Initializing a 2D transport solver ...\n"); CHKERRQ(ierr);
			TransportSolver2D* ts = NULL;
			ts = init_transportSolver2D(args->nequation_arguments, args->equation_arguments, grid, NULL, 0);
			CHKERRQ(ts->parent_solver->rk_params->ierr); CHKERRQ(ts->parent_solver->ierr); CHKERRQ(ts->ierr);
			// Apply the boundary condition
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Applying boundary conditions ...\n"); CHKERRQ(ierr);
			ierr = applyBocos_ts2D(args->nboco_arguments, args->boco_arguments, grid, ts); CHKERRQ(ierr);
			// Execute
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Executing ...\n"); CHKERRQ(ierr);
			ierr = execute_ts2D(ts, grid); CHKERRQ(ierr);
			// Post-process solution files if any found
			if (ts->parent_solver->export_frequency > 0) {
				ierr = PetscPrintf(PETSC_COMM_WORLD, "Post-processing solution files ...\n"); CHKERRQ(ierr);
				if (rank == 0) {
					fdm_home = getenv("FDM_HOME");
					sprintf(path2pp, "%s/C_VERSION/SOURCES/POST_PROCESSING/solution_files_pp.py  --dim=%d --nx=%d --ny=%d --dt=%f", fdm_home, ndim, grid->parent_grid->nx, grid->ny, ts->parent_solver->dt);
					if (system(path2pp) == -1) {
						ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
					}
				}
				MPI_Barrier(PETSC_COMM_WORLD);
			}
			// Deallocate everybody
			destroy_transportSolver2D(ts, grid);
		/*-- DIFFUSION --*/
		} else if (strcmp(equationName, "diffusion") == 0) {
			// Initialize the solver
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Initializing a 2D diffusion solver ...\n"); CHKERRQ(ierr);
			DiffusionSolver2D* ds = NULL;
			ds = init_diffusionSolver2D(args->nequation_arguments, args->equation_arguments, grid);
			CHKERRQ(ds->parent_solver->rk_params->ierr); CHKERRQ(ds->parent_solver->ierr); CHKERRQ(ds->ierr);
			// Apply the boundary condition
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Applying boundary conditions ...\n"); CHKERRQ(ierr);
			ierr = applyBocos_ds2D(args->nboco_arguments, args->boco_arguments, grid, ds); CHKERRQ(ierr);
			// Execute
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Executing ...\n"); CHKERRQ(ierr);
			ierr = execute_ds2D(ds, grid); CHKERRQ(ierr);
			// Post-process solution files if any found
			if (ds->parent_solver->export_frequency > 0) {
				ierr = PetscPrintf(PETSC_COMM_WORLD, "Post-processing solution files ...\n"); CHKERRQ(ierr);
				if (rank == 0) {
					fdm_home = getenv("FDM_HOME");
					sprintf(path2pp, "%s/C_VERSION/SOURCES/POST_PROCESSING/solution_files_pp.py  --dim=%d --nx=%d --ny=%d --dt=%f", fdm_home, ndim, grid->parent_grid->nx, grid->ny, ds->parent_solver->dt);
					if (system(path2pp) == -1) {
						ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
					}
				}
				MPI_Barrier(PETSC_COMM_WORLD);
			}
			// Deallocate everybody
			destroy_diffusionSolver2D(ds, grid);
		/*-- ADVECTION-DIFFUSION --*/
		} else if (strcmp(equationName, "advection-diffusion") == 0) {
			// Initialize the solver
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Initializing a 2D advection-diffusion solver ...\n"); CHKERRQ(ierr);
			AdvectionDiffusionSolver2D* ads = NULL;
			ads = init_advectionDiffusionSolver2D(args->nequation_arguments, args->equation_arguments, grid);
			CHKERRQ(ads->transport_solver->parent_solver->rk_params->ierr); CHKERRQ(ads->transport_solver->parent_solver->ierr);
			CHKERRQ(ads->diffusion_solver->parent_solver->rk_params->ierr); CHKERRQ(ads->diffusion_solver->parent_solver->ierr);
			CHKERRQ(ads->diffusion_solver->ierr); CHKERRQ(ads->transport_solver->ierr); CHKERRQ(ads->ierr);
			// Apply the boundary condition
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Applying boundary conditions ...\n"); CHKERRQ(ierr);
			ierr = applyBocos_ads2D(args->nboco_arguments, args->boco_arguments, grid, ads); CHKERRQ(ierr);
			// Execute
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Executing ...\n"); CHKERRQ(ierr);
			ierr = execute_ads2D(ads, grid); CHKERRQ(ierr);
			// Post-process solution files if any found
			if (ads->transport_solver->parent_solver->export_frequency > 0) {
				ierr = PetscPrintf(PETSC_COMM_WORLD, "Post-processing solution files ...\n"); CHKERRQ(ierr);
				if (rank == 0) {
					fdm_home = getenv("FDM_HOME");
					sprintf(path2pp, "%s/C_VERSION/SOURCES/POST_PROCESSING/solution_files_pp.py  --dim=%d --nx=%d --ny=%d --dt=%f", fdm_home, ndim, grid->parent_grid->nx, grid->ny, ads->transport_solver->parent_solver->dt);
					if (system(path2pp) == -1) {
						ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
					}
				}
				MPI_Barrier(PETSC_COMM_WORLD);
			}
			// Deallocate everybody
			destroy_advectionDiffusionSolver2D(ads, grid);
		/*-- LINEAR ACOUSTICS --*/
		} else if (strcmp(equationName, "linear-acoustics") == 0) {
			// Initialize the solver
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Initializing a 2D linear-acoustics solver ...\n"); CHKERRQ(ierr);
			LinearAcousticsSolver2D* las = NULL;
			las = init_linearAcousticsSolver2D(args->nequation_arguments, args->equation_arguments, grid);
			CHKERRQ(las->ts_1->parent_solver->rk_params->ierr); CHKERRQ(las->ts_1->parent_solver->ierr);
			CHKERRQ(las->ts_2->parent_solver->rk_params->ierr); CHKERRQ(las->ts_2->parent_solver->ierr);
			CHKERRQ(las->ts_1->ierr); CHKERRQ(las->ts_2->ierr); CHKERRQ(las->ierr);
			// Apply the boundary condition
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Applying boundary conditions ...\n"); CHKERRQ(ierr);
			ierr = applyBocos_las2D(args->nboco_arguments, args->boco_arguments, grid, las); CHKERRQ(ierr);
			// Execute
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Executing ...\n"); CHKERRQ(ierr);
			ierr = execute_las2D(las, grid); CHKERRQ(ierr);
			// Post-process solution files if any found
			if (las->ts_1->parent_solver->export_frequency > 0) {
				ierr = PetscPrintf(PETSC_COMM_WORLD, "Post-processing solution files ...\n"); CHKERRQ(ierr);
				if (rank == 0) {
					fdm_home = getenv("FDM_HOME");
					sprintf(path2pp, "%s/C_VERSION/SOURCES/POST_PROCESSING/solution_files_pp.py  --dim=%d --nx=%d --ny=%d --dt=%f", fdm_home, ndim, grid->parent_grid->nx, grid->ny, las->ts_1->parent_solver->dt);
					if (system(path2pp) == -1) {
						ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
					}
				}
				MPI_Barrier(PETSC_COMM_WORLD);
			}
			// Deallocate everybody
			destroy_linearAcousticsSolver2D(las, grid);
		} else {
			ierr = PetscPrintf(PETSC_COMM_WORLD, "Only transport equation has been implemented.\n"); CHKERRQ(ierr);
			ierr = PETSC_ERR_ARG_OUTOFRANGE; CHKERRQ(ierr);
		}
		// Deallocate everybody
		destroy_grid2D(grid);
	} else {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Only dimension 1 or 2 problems have been implemented.\n"); CHKERRQ(ierr);
		ierr = PETSC_ERR_ARG_OUTOFRANGE; CHKERRQ(ierr);
	}

	// Always call PetscFinalize() before exiting a program, and all the freeing routines within current scope
	destroy_argparser(args);
	ierr = PetscFinalize();
	return ierr;
}
