/* This file handles reading the command line input and parsing the user input file
   @author : Thibault Bridel-Bertomeu
   @license: MIT
   @status : development
*/

#include "arguments_parser.h"

/* Method to initialize the argparser pseudo-class with the reading of command line option
   and parsing of the file found (if found.)
*/
Argument* init_argparser(PetscInt argc, PetscChar** argv)
{
	Argument* args = NULL;
	PetscChar *fname;
	PetscInt eq_index;
	PetscErrorCode ierr;

	// Initialize the structure and allocate memory
	args = (Argument*) malloc(sizeof(Argument));

	// Parse the command line options
	if (argc == 1) {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "User input file name not found. Defaulting to fdm_user_params.params\n");
		fname = (PetscChar*)"fdm_user_params.params";
	} else {
		for (int i=1; i < argc; i++) {
			if (strstr(argv[i], "--argfile") != NULL) {
				eq_index = (PetscInt)( strchr(argv[i], 'e') - argv[i] ) + 1;
				fname = argv[i] + (eq_index + 1);
				break;
			}
		}
	}

	// Store the user input file name into the structure
	args->filename = (PetscChar*) malloc(sizeof(PetscChar) * (strlen(fname)+1));
	strcpy(args->filename, fname);
	ierr = PetscPrintf(PETSC_COMM_WORLD, "User input file name is %s.\n", args->filename);

	// Check if it exists before trying to parse it
	if (access(args->filename, F_OK) == -1) {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "Found option --argfile but cannot find the file given.\n");
		ierr = PETSC_ERR_FILE_OPEN;
	} else {
		ierr = parse_parameters(args);
	}

	return args;
}

/* Method to parse the user parameter file and store everything properly
   into the designated structure
*/
PetscErrorCode parse_parameters(Argument* args)
{
	// Declarations and some initializations
	PetscChar* line = NULL;
	FILE* fp;
	PetscErrorCode ierr = 0;
	size_t len = 0;

	// Open the file and parse it line by line
	fp = fopen(args->filename, "r");
	if (fp == NULL) {
		ierr = PETSC_ERR_FILE_READ;
	} else {
		// Set to 0 the numbers of arguments in all categories
		args->ngrid_arguments = 0;
		args->ninit_values = 0;
		args->nexact_values = 0;
		args->nboco_arguments = 0;
		args->nequation_arguments = 0;
		// March the file line by line
		while ( getline(&line, &len, fp) != -1 ) {
			// If the # symbol is first on the line, it is a comment to be skipped
			if (line[0] == '#') {
				continue;
			}
			// Else, can read the line, split it w.r.t '=' char and analyze the content
			if (startsWith(line, "\n")) {
				continue;
			}
			else if (startsWith(line, "grid")) {
				strncpy(args->grid_arguments[args->ngrid_arguments], line, strlen(line)-1);
				args->ngrid_arguments++;
			} else if (startsWith(line, "init")) {
				strncpy(args->init_values[args->ninit_values], line, strlen(line)-1);
				args->ninit_values++;
			} else if (startsWith(line, "exact")) {
				strncpy(args->exact_values[args->nexact_values], line, strlen(line)-1);
				args->nexact_values++;
			} else if (startsWith(line, "boco")) {
				strncpy(args->boco_arguments[args->nboco_arguments], line, strlen(line)-1);
				args->nboco_arguments++;
			} else if (startsWith(line, "equation")) {
				strncpy(args->equation_arguments[args->nequation_arguments], line, strlen(line)-1);
				args->nequation_arguments++;
			} else if (startsWith(line, "var_names")) {
				strncpy(args->var_names, line, strlen(line)-1);
			} else {
				ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown argument (%s) found in %s.\n", line, args->filename); CHKERRQ(ierr);
				ierr = PETSC_ERR_FILE_READ;
				break;
			}
		}
	}
	fclose(fp);

	// Free line in case it wasn't before
	if (line) {
		free(line);
	}

	return ierr;
}

/* Method destined to print on the screen the content of the Argument structure
   for user inspection.
*/
PetscErrorCode print_arguments(Argument* args)
{
	PetscErrorCode ierr = 0;

	ierr = PetscPrintf(PETSC_COMM_WORLD, "Content of the argument structure is as follow:\n"); CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD, "Name of the user input file is: %s.\n", args->filename); CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD, "There are %d arguments for the grid:\n", args->ngrid_arguments); CHKERRQ(ierr);
	for (PetscInt i=0; i < args->ngrid_arguments; i++) {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "\t %s\n", args->grid_arguments[i]); CHKERRQ(ierr);
	}
	ierr = PetscPrintf(PETSC_COMM_WORLD, "Variable names are stored as: '%s'\n", args->var_names); CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD, "There are %d variable initialization(s) recorded:\n", args->ninit_values); CHKERRQ(ierr);
	for (PetscInt i=0; i < args->ninit_values; i++) {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "\t %s\n", args->init_values[i]); CHKERRQ(ierr);
	}
	ierr = PetscPrintf(PETSC_COMM_WORLD, "There are %d arguments for the boundary conditions:\n", args->nboco_arguments); CHKERRQ(ierr);
	for (PetscInt i=0; i < args->nboco_arguments; i++) {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "\t %s\n", args->boco_arguments[i]); CHKERRQ(ierr);
	}
	ierr = PetscPrintf(PETSC_COMM_WORLD, "There are %d arguments for the equation:\n", args->nequation_arguments); CHKERRQ(ierr);
	for (PetscInt i=0; i < args->nequation_arguments; i++) {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "\t %s\n", args->equation_arguments[i]); CHKERRQ(ierr);
	}
	ierr = PetscPrintf(PETSC_COMM_WORLD, ""); CHKERRQ(ierr);

	return ierr;
}

/* Method to properly deallocate everything that was dynamically allocated in the scope
   of the argument parser.
*/
void destroy_argparser(Argument* args)
{
	free(args->filename);
	free(args);
}
