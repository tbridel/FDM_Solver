/* Header file for the arguments_parser.c methods file
   @author : Thibault Bridel-Bertomeu
   @license: MIT
   @status : development
*/

#if !defined(__ARGPARSER_H)
#define __ARGPARSER_H

#include "../UTILITARIES/utils.h"
#include <petscsys.h>
#include <unistd.h>

typedef struct Argument Argument;
struct Argument
{
	PetscChar* filename;
	PetscInt ngrid_arguments;
	PetscChar grid_arguments[TEN][WORDSZ];
	PetscChar var_names[WORDSZ];
	PetscInt ninit_values;
	PetscChar init_values[TWENTY][WORDSZ];
	PetscInt nexact_values;
	PetscChar exact_values[TWENTY][WORDSZ];
	PetscInt nboco_arguments;
	PetscChar boco_arguments[2*TWENTY][WORDSZ];
	PetscInt nequation_arguments;
	PetscChar equation_arguments[2*TWENTY][WORDSZ];
};

Argument* init_argparser(int argc, char **argv);
PetscErrorCode parse_parameters(Argument* args);
PetscErrorCode print_arguments(Argument* args);
void destroy_argparser(Argument* args);

#endif
