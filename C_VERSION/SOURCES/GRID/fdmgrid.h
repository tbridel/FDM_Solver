/* Header file for the fdmgrid.c methods file
   @author : Thibault Bridel-Bertomeu
   @license: MIT
   @status : development
*/

#if !defined(__FDMGRID_H)
#define __FDMGRID_H

#include "../UTILITARIES/utils.h"
#include <petscsys.h>
#include <petscvec.h>

#define DIM1 1
#define DIM2 2
#define DIM3 3

/********************************************/
/******************BASE GRID*****************/
/********************************************/
typedef struct Grid Grid;
struct Grid {
	PetscInt ndim;
	PetscScalar xmin;
	PetscScalar xmax;
	PetscInt nx;
	PetscScalar dx;
};

Grid* init_grid(PetscInt ndim);

/********************************************/
/*******************GRID 1D******************/
/********************************************/
#define CONDI_FOR_INIT_1D(B_ADD, B_SUB, B_PROD, BUFFER, SIZE, MATHFUNC, COORDS, PARAM1, PARAM2)\
if (B_ADD) {\
	for (int k=0; k < SIZE ; k++)\
		BUFFER[k] = add(BUFFER[k], MATHFUNC(COORDS[k], PARAM1, PARAM2));\
} else if (B_SUB) {\
	for (int k=0; k < SIZE ; k++)\
		BUFFER[k] = sub(BUFFER[k], MATHFUNC(COORDS[k], PARAM1, PARAM2));\
} else if (B_PROD) {\
	for (int k=0; k < SIZE ; k++)\
		BUFFER[k] = prod(BUFFER[k], MATHFUNC(COORDS[k], PARAM1, PARAM2));\
} else {\
	for (int k=0; k < SIZE ; k++)\
		BUFFER[k] = MATHFUNC(COORDS[k], PARAM1, PARAM2);\
}

typedef struct Grid1D Grid1D;
struct Grid1D {
	Grid* parent_grid;
	PetscInt ixmin;
	PetscInt ixmax;
	PetscScalar steps[DIM1];
	PetscInt nvtx;
	Vec unknowns;
	Vec x;
	PetscBool hasExact;
	Vec exact;
};

Grid1D* init_grid1D(PetscInt ngrid_args, PetscChar grid_arguments[TEN][WORDSZ], PetscInt nboco_args, PetscChar boco_arguments[2*TWENTY][WORDSZ]);
PetscErrorCode initialize_solution1D(PetscInt ninit_values, PetscChar init_values[TWENTY][WORDSZ], PetscInt nexact_values, PetscChar exact_values[TWENTY][WORDSZ], Grid1D* grid);
void destroy_grid1D(Grid1D* grid);

/********************************************/
/*******************GRID 2D******************/
/********************************************/
#define CONDI_FOR_INIT_2D(B_ADD, B_SUB, B_PROD, BUFFER, SIZE, MATHFUNC, COORDSX, COORDSY, PARAM1, PARAM2, PARAM3, PARAM4)\
if (B_ADD) {\
	for (int k=0; k < SIZE ; k++)\
		BUFFER[k] = add(BUFFER[k], MATHFUNC(COORDSX[k], COORDSY[k], PARAM1, PARAM2, PARAM3, PARAM4));\
} else if (B_SUB) {\
	for (int k=0; k < SIZE ; k++)\
		BUFFER[k] = sub(BUFFER[k], MATHFUNC(COORDSX[k], COORDSY[k], PARAM1, PARAM2, PARAM3, PARAM4));\
} else if (B_PROD) {\
	for (int k=0; k < SIZE ; k++)\
		BUFFER[k] = prod(BUFFER[k], MATHFUNC(COORDSX[k], COORDSY[k], PARAM1, PARAM2, PARAM3, PARAM4));\
} else {\
	for (int k=0; k < SIZE ; k++)\
		BUFFER[k] = MATHFUNC(COORDSX[k], COORDSY[k], PARAM1, PARAM2, PARAM3, PARAM4);\
}

typedef struct Grid2D Grid2D;
struct Grid2D {
	PetscErrorCode ierr;
	Grid* parent_grid;
	PetscScalar ymin, ymax;
	PetscInt ny;
	PetscScalar dy;
	PetscInt *ixmin, *ixmax;
	PetscInt *iymin, *iymax;
	PetscScalar steps[DIM2];
	PetscInt nvtx;
	Vec unknowns;
	Vec x, y;
	PetscBool hasExact;
	Vec exact;
};

Grid2D* init_grid2D(PetscInt ngrid_args, PetscChar grid_arguments[TEN][WORDSZ], PetscInt nboco_args, PetscChar boco_arguments[2*TWENTY][WORDSZ]);
PetscErrorCode initialize_solution2D(PetscInt ninit_values, PetscChar init_values[TWENTY][WORDSZ], PetscInt nexact_values, PetscChar exact_values[TWENTY][WORDSZ], Grid2D* grid);
void destroy_grid2D(Grid2D* grid);

#endif
