/* This file contains the methods handling a two-dimensional mesh
   @author : Thibault Bridel-Bertomeu
   @license: MIT
   @status : development
*/

#include "fdmgrid.h"

/* Method to initialize a two-dimensional grid from the arguments given by
   the user in the user_params file.
*/
Grid2D* init_grid2D(PetscInt ngrid_args, PetscChar grid_arguments[TEN][WORDSZ], PetscInt nboco_args, PetscChar boco_arguments[2*TWENTY][WORDSZ])
{
	Grid2D* grid = (Grid2D*) malloc(sizeof(Grid2D));
	int eq_index;
	PetscChar **tokens = NULL, rhsString[WORDSZ] = "";
	PetscInt ntok = 0;

	// Initialize the parent
	grid->parent_grid = init_grid(DIM2);

	// Get mesh number of points, bound and spacing
	for (int i=0; i < ngrid_args; i++) {
		if (strstr(grid_arguments[i], "npts") != NULL) {
			eq_index = ( strchr(grid_arguments[i], '=') - grid_arguments[i] ) + 1;
			strcpy(rhsString, grid_arguments[i] + (eq_index + 1));
			grid->ierr = split(rhsString, ',', &tokens, &ntok);
			for (int j=0; j < ntok; j++) {
				if (j==0){
					grid->parent_grid->nx = atoi(tokens[j]);
				} else if (j==1) {
					grid->ny = atoi(tokens[j]);
				}
			}
		} else if (strstr(grid_arguments[i], "xmin") != NULL) {
			eq_index = ( strchr(grid_arguments[i], '=') - grid_arguments[i] ) + 1;
			grid->parent_grid->xmin = (PetscScalar) atof(grid_arguments[i] + (eq_index + 1));
		} else if (strstr(grid_arguments[i], "xmax") != NULL) {
			eq_index = ( strchr(grid_arguments[i], '=') - grid_arguments[i] ) + 1;
			grid->parent_grid->xmax = (PetscScalar) atof(grid_arguments[i] + (eq_index + 1));
		} else if (strstr(grid_arguments[i], "ymin") != NULL) {
			eq_index = ( strchr(grid_arguments[i], '=') - grid_arguments[i] ) + 1;
			grid->ymin = (PetscScalar) atof(grid_arguments[i] + (eq_index + 1));
		} else if (strstr(grid_arguments[i], "ymax") != NULL) {
			eq_index = ( strchr(grid_arguments[i], '=') - grid_arguments[i] ) + 1;
			grid->ymax = (PetscScalar) atof(grid_arguments[i] + (eq_index + 1));
		} else {
			continue;
		}
	}
	grid->parent_grid->dx = (grid->parent_grid->xmax - grid->parent_grid->xmin) / (grid->parent_grid->nx - 1);
	grid->dy = (grid->ymax - grid->ymin) / (grid->ny - 1);
	grid->steps[0] = grid->parent_grid->dx;
	grid->steps[1] = grid->dy;

	// Get the indices of the boundary conditions
	grid->ixmin = (PetscInt*) malloc(grid->ny * sizeof(PetscInt));
	grid->ixmax = (PetscInt*) malloc(grid->ny * sizeof(PetscInt));
	for (int i=0; i < grid->ny; i++) {
		grid->ixmin[i] = i*grid->parent_grid->nx;
		grid->ixmax[i] = grid->ixmin[i] + (grid->parent_grid->nx - 1);
	}
	grid->iymin = (PetscInt*) malloc(grid->parent_grid->nx * sizeof(PetscInt));
	grid->iymax = (PetscInt*) malloc(grid->parent_grid->nx * sizeof(PetscInt));
	for (int i=0; i < grid->parent_grid->nx; i++) {
		grid->iymin[i] = i;
		grid->iymax[i] = grid->iymin[i] + (grid->ny - 1) * grid->parent_grid->nx;
	}

	// Add a few "generic" parameters so any grid can be passed to the solvers
	grid->nvtx = grid->parent_grid->nx * grid->ny;

	// Free malloc'd objects
	if (tokens != NULL) free(tokens);

	// Return the Grid2D
	return grid;
}

/* Method to initialize the solution on a two-dimensional grid
   from the init_XXX strings passed by the user
*/
PetscErrorCode initialize_solution2D(PetscInt ninit_values, PetscChar init_values[TWENTY][WORDSZ], PetscInt nexact_values, PetscChar exact_values[TWENTY][WORDSZ], Grid2D* grid)
{
	int eq_index = 0;
	PetscBool b_add = false, b_sub = false, b_prod = false;
	PetscChar **tokens = NULL;
	PetscErrorCode ierr = 0;
	PetscInt ntok = 0;
	PetscInt rstartx, rendx, nlocalx, rstartunk, rendunk, nlocalunk;
	PetscScalar aux = 0.0, param1 = 0.0, param2 = 0.0, param3 = 0.0, param4 = 0.0;
	PetscInt *indices=NULL, *result=NULL, ii, jj;
	PetscScalar *buffer, *gridX, *gridY;

	// Fill the vectors of positions
	// X coordinate allocation
	grid->x = NULL;
	ierr = VecCreate(PETSC_COMM_WORLD, &(grid->x)); CHKERRQ(ierr);
	ierr = VecSetSizes(grid->x, PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
	ierr = VecSetUp(grid->x); CHKERRQ(ierr);
	ierr = VecGetOwnershipRange(grid->x, &rstartx, &rendx); CHKERRQ(ierr);
	ierr = VecGetLocalSize(grid->x, &nlocalx); CHKERRQ(ierr);
	// Y coordinate allocation with the same MPI structure
	grid->y = NULL;
	ierr = VecCreate(PETSC_COMM_WORLD, &(grid->y)); CHKERRQ(ierr);
	ierr = VecSetSizes(grid->y, nlocalx, grid->nvtx); CHKERRQ(ierr);
	ierr = VecSetUp(grid->y); CHKERRQ(ierr);
	// Fill both in a single pass
	result = (PetscInt*) malloc(2 * sizeof(PetscInt));
	for (int i=rstartx; i<rendx; i++) {
		unravel_index(i, grid->parent_grid->nx, result); ii=result[0]; jj=result[1];
		// The x coordinate is fast varying
		aux = grid->parent_grid->xmin + ii * grid->parent_grid->dx;
		ierr = VecSetValue(grid->x, i, aux, INSERT_VALUES); CHKERRQ(ierr);
		// The y coordinate is slow varying
		aux = grid->ymin + jj * grid->dy;
		ierr = VecSetValue(grid->y, i, aux, INSERT_VALUES); CHKERRQ(ierr);
	}
	// Assemble everybody
	ierr = VecAssemblyBegin(grid->x); CHKERRQ(ierr);
	ierr = VecAssemblyEnd(grid->x); CHKERRQ(ierr);
	ierr = VecAssemblyBegin(grid->y); CHKERRQ(ierr);
	ierr = VecAssemblyEnd(grid->y); CHKERRQ(ierr);

	// Malloc the space for unknowns on this grid
	grid->unknowns = NULL;
	ierr = VecCreate(PETSC_COMM_WORLD, &(grid->unknowns)); CHKERRQ(ierr);
	ierr = VecSetSizes(grid->unknowns, PETSC_DECIDE, ninit_values * grid->nvtx); CHKERRQ(ierr);
	ierr = VecSetUp(grid->unknowns); CHKERRQ(ierr);
	ierr = VecGetOwnershipRange(grid->unknowns, &rstartunk, &rendunk); CHKERRQ(ierr);
	ierr = VecGetLocalSize(grid->unknowns, &nlocalunk); CHKERRQ(ierr);

	// Malloc the space for exact values on this grid
	grid->hasExact = false; grid->exact = NULL;
	if (nexact_values > 0) {
		grid->hasExact = true;
		ierr = VecCreate(PETSC_COMM_WORLD, &(grid->exact)); CHKERRQ(ierr);
		ierr = VecSetSizes(grid->exact, nlocalunk, ninit_values * grid->nvtx); CHKERRQ(ierr);
		ierr = VecSetUp(grid->exact); CHKERRQ(ierr);
		ierr = VecSet(grid->exact, 1); CHKERRQ(ierr);
	}

	// Prepare the buffers for the initialization and exact values
	buffer = (PetscScalar*) malloc(nlocalx * sizeof(PetscScalar));
	for (int i = 0; i < nlocalx; i++){
		buffer[i] = 0.0;
	}

	// Store the mesh coordinates in C arrays to access them quickly during initialization
	gridX = (PetscScalar*) malloc(nlocalx * sizeof(PetscScalar));
	gridY = (PetscScalar*) malloc(nlocalx * sizeof(PetscScalar));
	indices = (PetscInt*) malloc(nlocalx * sizeof(PetscInt));
	for (int i = rstartx; i < rendx; i++) {
		indices[i-rstartx] = i;
	}
	ierr = VecGetValues(grid->x, nlocalx, indices, gridX); CHKERRQ(ierr);
	ierr = VecGetValues(grid->y, nlocalx, indices, gridY); CHKERRQ(ierr);

	// For each variable ... (assuming there is actually only one, for now)
	for (int i = 0; i < ninit_values; i++) {
		// Reinitialize some objects
		b_add = false; b_sub = false; b_prod = false; ntok = 0; tokens = NULL;
		PetscChar rhsString[WORDSZ] = "";
		// Get the right hand side of the init_X string in the user param file
		eq_index = ( strchr(init_values[i], '=') - init_values[i] ) + 1;
		strcpy(rhsString, init_values[i] + (eq_index + 1));
		// Split the string with the commas to get all the pieces of the initial solution
		ierr = split(rhsString, ',', &tokens, &ntok); CHKERRQ(ierr);
		// Parse the tokens to fill the buffer vector (use preprocessor macros here, refer to fdmgrid.h)
		for (int j=0; j < ntok; j++) {
			if (startsWith(tokens[j], "gaussian2")) {
				sscanf(tokens[j], "gaussian2 %lf %lf %lf %lf", &param1, &param2, &param3, &param4);
				CONDI_FOR_INIT_2D(b_add, b_sub, b_prod, buffer, nlocalx, fdmgaussian2, gridX, gridY, param1, param2, param3, param4)
			} else if (startsWith(tokens[j], "sin")) {
				if (tokens[j][3] == 'x') {
					sscanf(tokens[j], "sinx %lf %lf", &param1, &param2);
					CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmsin, gridX, param1, param2)
				} else if (tokens[j][3] == 'y') {
					sscanf(tokens[j], "siny %lf %lf", &param1, &param2);
					CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmsin, gridY, param1, param2)
				}
			} else if (startsWith(tokens[j], "cos")) {
				if (tokens[j][3] == 'x') {
					sscanf(tokens[j], "cosx %lf %lf", &param1, &param2);
					CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmcos, gridX, param1, param2)
				} else if (tokens[j][3] == 'y') {
					sscanf(tokens[j], "cosy %lf %lf", &param1, &param2);
					CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmcos, gridY, param1, param2)
				}
			} else if (startsWith(tokens[j], "gaussian")) {
				if (tokens[j][8] == 'x') {
					sscanf(tokens[j], "gaussianx %lf %lf", &param1, &param2);
					CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmgaussian, gridX, param1, param2)
				} else if (tokens[j][8] == 'y') {
					sscanf(tokens[j], "gaussiany %lf %lf", &param1, &param2);
					CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmgaussian, gridY, param1, param2)
				}
			} else if (startsWith(tokens[j], "heaviside")) {
				if (tokens[j][9] == 'x') {
					sscanf(tokens[j], "heavisidex %lf", &param1);
					param2 = 0.0;
					CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmheaviside, gridX, param1, param2)
				} else if (tokens[j][9] == 'y') {
					sscanf(tokens[j], "heavisidey %lf", &param1);
					param2 = 0.0;
					CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmheaviside, gridY, param1, param2)
				}
			} else if (startsWith(tokens[j], "erf")) {
				if (tokens[j][3] == 'x') {
					sscanf(tokens[j], "erfx %lf", &param1);
					param2 = 0.0;
					CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmerf, gridX, param1, param2)
				} else if (tokens[j][3] == 'y') {
					sscanf(tokens[j], "erfy %lf", &param1);
					param2 = 0.0;
					CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmerf, gridY, param1, param2)
				}
			} else if (startsWith(tokens[j], "constant")) {
				sscanf(tokens[j], "constant %lf", &param1);
				param2 = 0.0;
				CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmconstant, gridX, param1, param2)
			} else if (startsWith(tokens[j], "product") || startsWith(tokens[j], "times")) {
				b_prod = true; b_add = false; b_sub = false;
			} else if (startsWith(tokens[j], "addition") || startsWith(tokens[j], "plus")) {
				b_add = true; b_prod = false; b_sub = false;
			} else if (startsWith(tokens[j], "subtraction") || startsWith(tokens[j], "minus")) {
				b_sub = true; b_add = false; b_prod = false;
			} else {
				ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown token %s in string %s\n", tokens[j], init_values[i]); CHKERRQ(ierr);
				ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
			}
		}
		// Fill the unknown vector
		for (int j = rstartx; j < rendx; j++) {
			ierr = VecSetValue(grid->unknowns, j+i*grid->nvtx, buffer[j-rstartx], INSERT_VALUES); CHKERRQ(ierr);
		}
	}

	// For each exact ... (assuming there is actually only one, for now)
	if (nexact_values > 0) {
		for (int i = 0; i < nlocalx; i++)
			buffer[i] = 0.0;
	}

	for (int i = 0; i < nexact_values; i++) {
		// Reinitialize some objects
		tokens = NULL; ntok = 0; b_add = false; b_sub = false; b_prod = false;
		PetscChar rhsString[WORDSZ] = "";
		// Get the right hand side of the init_X string in the user param file
		eq_index = ( strchr(exact_values[i], '=') - exact_values[i] ) + 1;
		strcpy(rhsString, exact_values[i] + (eq_index + 1));
		// Split the string with the commas to get all the pieces of the exact solution
		ierr = split(rhsString, ',', &tokens, &ntok); CHKERRQ(ierr);
		// Parse the tokens to fill the buffer vector (use preprocessor macros here, refer to fdmgrid.h)
		for (int j = 0; j < ntok; j++) {
			if (startsWith(tokens[j], "gaussian2")) {
				sscanf(tokens[j], "gaussian2 %lf %lf %lf %lf", &param1, &param2, &param3, &param4);
				CONDI_FOR_INIT_2D(b_add, b_sub, b_prod, buffer, nlocalx, fdmgaussian2, gridX, gridY, param1, param2, param3, param4)
			} else if (startsWith(tokens[j], "sin")) {
				if (tokens[j][3] == 'x') {
					sscanf(tokens[j], "sinx %lf %lf", &param1, &param2);
					CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmsin, gridX, param1, param2)
				} else if (tokens[j][3] == 'y') {
					sscanf(tokens[j], "siny %lf %lf", &param1, &param2);
					CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmsin, gridY, param1, param2)
				}
			} else if (startsWith(tokens[j], "cos")) {
				if (tokens[j][3] == 'x') {
					sscanf(tokens[j], "cosx %lf %lf", &param1, &param2);
					CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmcos, gridX, param1, param2)
				} else if (tokens[j][3] == 'y') {
					sscanf(tokens[j], "cosy %lf %lf", &param1, &param2);
					CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmcos, gridY, param1, param2)
				}
			} else if (startsWith(tokens[j], "gaussian")) {
				if (tokens[j][8] == 'x') {
					sscanf(tokens[j], "gaussianx %lf %lf", &param1, &param2);
					CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmgaussian, gridX, param1, param2)
				} else if (tokens[j][8] == 'y') {
					sscanf(tokens[j], "gaussiany %lf %lf", &param1, &param2);
					CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmgaussian, gridY, param1, param2)
				}
			} else if (startsWith(tokens[j], "heaviside")) {
				if (tokens[j][9] == 'x') {
					sscanf(tokens[j], "heavisidex %lf", &param1);
					param2 = 0.0;
					CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmheaviside, gridX, param1, param2)
				} else if (tokens[j][9] == 'y') {
					sscanf(tokens[j], "heavisidey %lf", &param1);
					param2 = 0.0;
					CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmheaviside, gridY, param1, param2)
				}
			} else if (startsWith(tokens[j], "erf")) {
				if (tokens[j][3] == 'x') {
					sscanf(tokens[j], "erfx %lf", &param1);
					param2 = 0.0;
					CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmerf, gridX, param1, param2)
				} else if (tokens[j][3] == 'y') {
					sscanf(tokens[j], "erfy %lf", &param1);
					param2 = 0.0;
					CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmerf, gridY, param1, param2)
				}
			} else if (startsWith(tokens[j], "constant")) {
				sscanf(tokens[j], "constant %lf", &param1);
				param2 = 0.0;
				CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmconstant, gridX, param1, param2)
			} else if (startsWith(tokens[j], "product") || startsWith(tokens[j], "times")) {
				b_prod = true; b_add = false; b_sub = false;
			} else if (startsWith(tokens[j], "addition") || startsWith(tokens[j], "plus")) {
				b_add = true; b_prod = false; b_sub = false;
			} else if (startsWith(tokens[j], "subtraction") || startsWith(tokens[j], "minus")) {
				b_sub = true; b_add = false; b_prod = false;
			} else {
				ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown token %s in string %s\n", tokens[j], exact_values[i]); CHKERRQ(ierr);
				ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
			}
		}
		// Fill the exact vector
		for (int j = rstartx; j < rendx; j++) {
			ierr = VecSetValue(grid->exact, j+i*grid->nvtx, buffer[j-rstartx], INSERT_VALUES); CHKERRQ(ierr);
		}
	}

	// Assemble the vector of unknowns
	ierr = VecAssemblyBegin(grid->unknowns); CHKERRQ(ierr);
	ierr = VecAssemblyEnd(grid->unknowns); CHKERRQ(ierr);

	// Assemble the vector of exacts
	if (nexact_values > 0) {
		ierr = VecAssemblyBegin(grid->exact); CHKERRQ(ierr);
		ierr = VecAssemblyEnd(grid->exact); CHKERRQ(ierr);
	}

	// Destroy malloc()ed variables
	free(buffer);
	free(gridX);
	free(gridY);
	free(indices);
	if (tokens != NULL) free(tokens);
	if (result != NULL) free(result);

	return ierr;
}

/* Method to destroy, i.e. deallocate, a two-dimensional grid
*/
void destroy_grid2D(Grid2D* grid)
{
	free(grid->parent_grid);
	free(grid->ixmin);
	free(grid->ixmax);
	free(grid->iymin);
	free(grid->iymax);
	if (grid->unknowns != NULL) {
		VecDestroy(&(grid->unknowns));
	}
	if (grid->x != NULL) {
		VecDestroy(&(grid->x));
	}
	if (grid->y != NULL) {
		VecDestroy(&(grid->y));
	}
	if (grid->exact != NULL) {
		VecDestroy(&(grid->exact));
	}
	free(grid);
}
