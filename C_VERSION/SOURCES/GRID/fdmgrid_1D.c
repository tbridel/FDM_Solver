/* This file contains the methods handling a one-dimensional mesh
   @author : Thibault Bridel-Bertomeu
   @license: MIT
   @status : development
*/

#include "fdmgrid.h"

/* Method to initialize a one-dimensional grid from the arguments given by
   the user in the user_params file.
*/
Grid1D* init_grid1D(PetscInt ngrid_args, PetscChar grid_arguments[TEN][WORDSZ], PetscInt nboco_args, PetscChar boco_arguments[2*TWENTY][WORDSZ])
{
	Grid1D* grid = (Grid1D*) malloc(sizeof(Grid1D));
	int eq_index;

	// Initialize the parent
	grid->parent_grid = init_grid(DIM1);

	// Get mesh number of points, bound and spacing
	for (int i=0; i < ngrid_args; i++) {
		if (strstr(grid_arguments[i], "npts") != NULL) {
			eq_index = ( strchr(grid_arguments[i], '=') - grid_arguments[i] ) + 1;
			grid->parent_grid->nx = (PetscInt) atoi(grid_arguments[i] + (eq_index + 1));
		} else if (strstr(grid_arguments[i], "xmin") != NULL) {
			eq_index = ( strchr(grid_arguments[i], '=') - grid_arguments[i] ) + 1;
			grid->parent_grid->xmin = (PetscScalar) atof(grid_arguments[i] + (eq_index + 1));
		} else if (strstr(grid_arguments[i], "xmax") != NULL) {
			eq_index = ( strchr(grid_arguments[i], '=') - grid_arguments[i] ) + 1;
			grid->parent_grid->xmax = (PetscScalar) atof(grid_arguments[i] + (eq_index + 1));
		} else {
			continue;
		}
	}
	grid->parent_grid->dx = (grid->parent_grid->xmax - grid->parent_grid->xmin) / (grid->parent_grid->nx - 1);
	grid->steps[0] = grid->parent_grid->dx;

	// Get the indices of the boundary conditions
	grid->ixmin = (PetscInt) 0;
	grid->ixmax = (PetscInt) (grid->parent_grid->nx - 1);

	// Add a few "generic" parameters so any grid can be passed to the solvers
	grid->nvtx = grid->parent_grid->nx;

	// Return the Grid1D
	return grid;
}

/* Method to initialize the solution on a one-dimensional grid
   from the init_XXX strings passed by the user
*/
PetscErrorCode initialize_solution1D(PetscInt ninit_values, PetscChar init_values[TWENTY][WORDSZ], PetscInt nexact_values, PetscChar exact_values[TWENTY][WORDSZ], Grid1D* grid)
{
	int eq_index = 0;
	PetscBool b_add = false, b_sub = false, b_prod = false;
	PetscChar **tokens = NULL;
	PetscErrorCode ierr = 0;
	PetscInt ntok = 0;
	PetscInt rstartx, rendx, nlocalx, rstartunk, rendunk, nlocalunk;
	PetscScalar aux = 0.0, param1 = 0.0, param2 = 0.0;
	PetscScalar *buffer, *gridX;
	PetscInt *indices;

	// Fill the vector of positions
	grid->x = NULL;
	ierr = VecCreate(PETSC_COMM_WORLD, &(grid->x)); CHKERRQ(ierr);
	ierr = VecSetSizes(grid->x, PETSC_DECIDE, grid->nvtx); CHKERRQ(ierr);
	ierr = VecSetUp(grid->x); CHKERRQ(ierr);
	ierr = VecGetOwnershipRange(grid->x, &rstartx, &rendx); CHKERRQ(ierr);
	ierr = VecGetLocalSize(grid->x, &nlocalx); CHKERRQ(ierr);
	for (int i=rstartx; i<rendx; i++) {
		aux = grid->parent_grid->xmin + i * grid->parent_grid->dx;
		ierr = VecSetValue(grid->x, i, aux, INSERT_VALUES); CHKERRQ(ierr);
	}
	ierr = VecAssemblyBegin(grid->x); CHKERRQ(ierr);
	ierr = VecAssemblyEnd(grid->x); CHKERRQ(ierr);

	// Malloc the space for unknowns on this grid
	grid->unknowns = NULL;
	ierr = VecCreate(PETSC_COMM_WORLD, &(grid->unknowns)); CHKERRQ(ierr);
	ierr = VecSetSizes(grid->unknowns, PETSC_DECIDE, ninit_values * grid->nvtx); CHKERRQ(ierr);
	ierr = VecSetUp(grid->unknowns); CHKERRQ(ierr);
	ierr = VecGetOwnershipRange(grid->unknowns, &rstartunk, &rendunk); CHKERRQ(ierr);
	ierr = VecGetLocalSize(grid->unknowns, &nlocalunk); CHKERRQ(ierr);

	// Malloc the space for exact values on this grid
	grid->hasExact = false; grid->exact = NULL;
	if (nexact_values > 0) {
		grid->hasExact = true;
		ierr = VecCreate(PETSC_COMM_WORLD, &(grid->exact)); CHKERRQ(ierr);
		ierr = VecSetSizes(grid->exact, nlocalunk, ninit_values * grid->nvtx); CHKERRQ(ierr);
		ierr = VecSetUp(grid->exact); CHKERRQ(ierr);
	}

	// Prepare the buffers for the initialization and exact values
	buffer = (PetscScalar*) malloc(nlocalx * sizeof(PetscScalar));
	for (int i = 0; i < nlocalx; i++){
		buffer[i] = 0.0;
	}

	// Store the mesh coordinates in C arrays to access them quickly during initialization
	gridX = (PetscScalar*) malloc(nlocalx * sizeof(PetscScalar));
	indices = (PetscInt*) malloc(nlocalx * sizeof(PetscInt));
	for (int i = rstartx; i < rendx; i++) {
		indices[i-rstartx] = i;
	}
	ierr = VecGetValues(grid->x, nlocalx, indices, gridX); CHKERRQ(ierr);

	// For each variable ... (assuming there is actually only one, for now)
	for (int i=0; i<ninit_values; i++) {
		// Reinitialize some objects
		b_add = false; b_sub = false; b_prod = false; ntok = 0; tokens = NULL;
		PetscChar rhsString[WORDSZ] = "";
		// Get the right hand side of the init_X string in the user param file
		eq_index = ( strchr(init_values[i], '=') - init_values[i] ) + 1;
		strcpy(rhsString, init_values[i] + (eq_index + 1));
		// Split the string with the commas to get all the pieces of the initial solution
		ierr = split(rhsString, ',', &tokens, &ntok); CHKERRQ(ierr);
		// Parse the tokens to fill the buffer vector (use preprocessor macros here, refer to fdmgrid.h)
		for (int j=0; j < ntok; j++) {
			if (startsWith(tokens[j], "sin")) {
				sscanf(tokens[j], "sin %lf %lf", &param1, &param2);
				CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmsin, gridX, param1, param2)
			} else if (startsWith(tokens[j], "cos")) {
				sscanf(tokens[j], "cos %lf %lf", &param1, &param2);
				CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmcos, gridX, param1, param2)
			} else if (startsWith(tokens[j], "gaussian")) {
				sscanf(tokens[j], "gaussian %lf %lf", &param1, &param2);
				CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmgaussian, gridX, param1, param2)
			} else if (startsWith(tokens[j], "heaviside")) {
				sscanf(tokens[j], "heaviside %lf", &param1);
				param2 = 0.0;
				CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmheaviside, gridX, param1, param2)
			} else if (startsWith(tokens[j], "erf")) {
				sscanf(tokens[j], "erf %lf", &param1);
				param2 = 0.0;
				CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmerf, gridX, param1, param2)
			} else if (startsWith(tokens[j], "constant")) {
				sscanf(tokens[j], "constant %lf", &param1);
				param2 = 0.0;
				CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmconstant, gridX, param1, param2)
			} else if (startsWith(tokens[j], "product") || startsWith(tokens[j], "times")) {
				b_prod = true; b_add = false; b_sub = false;
			} else if (startsWith(tokens[j], "addition") || startsWith(tokens[j], "plus")) {
				b_add = true; b_prod = false; b_sub = false;
			} else if (startsWith(tokens[j], "subtraction") || startsWith(tokens[j], "minus")) {
				b_sub = true; b_add = false; b_prod = false;
			} else {
				ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown token %s in string %s\n", tokens[j], init_values[i]); CHKERRQ(ierr);
				ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
			}
		}
		// Fill the unknown vector
		for (int j=rstartx; j<rendx; j++) {
			ierr = VecSetValue(grid->unknowns, j+i*grid->nvtx, buffer[j-rstartx], INSERT_VALUES); CHKERRQ(ierr);
		}
	}

	// For each exact ... (assuming there is actually only one, for now)
	if (nexact_values > 0) {
		for (int i=0; i<nlocalx; i++)
			buffer[i] = 0.0;
	}

	for (int i=0; i<nexact_values; i++) {
		// Reinitialize some objects
		tokens = NULL; ntok = 0; b_add = false; b_sub = false; b_prod = false;
		PetscChar rhsString[WORDSZ] = "";
		// Get the right hand side of the init_X string in the user param file
		eq_index = ( strchr(exact_values[i], '=') - exact_values[i] ) + 1;
		strcpy(rhsString, exact_values[i] + (eq_index + 1));
		// Split the string with the commas to get all the pieces of the exact solution
		ierr = split(rhsString, ',', &tokens, &ntok); CHKERRQ(ierr);
		// Parse the tokens to fill the buffer vector (use preprocessor macros here, refer to fdmgrid.h)
		for (int j=0; j < ntok; j++) {
			if (startsWith(tokens[j], "sin")) {
				sscanf(tokens[j], "sin %lf %lf", &param1, &param2);
				CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmsin, gridX, param1, param2)
			} else if (startsWith(tokens[j], "cos")) {
				sscanf(tokens[j], "cos %lf %lf", &param1, &param2);
				CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmcos, gridX, param1, param2)
			} else if (startsWith(tokens[j], "gaussian")) {
				sscanf(tokens[j], "gaussian %lf %lf", &param1, &param2);
				CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmgaussian, gridX, param1, param2)
			} else if (startsWith(tokens[j], "heaviside")) {
				sscanf(tokens[j], "heaviside %lf", &param1);
				param2 = 0.0;
				CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmheaviside, gridX, param1, param2)
			} else if (startsWith(tokens[j], "erf")) {
				sscanf(tokens[j], "erf %lf", &param1);
				param2 = 0.0;
				CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmerf, gridX, param1, param2)
			} else if (startsWith(tokens[j], "constant")) {
				sscanf(tokens[j], "constant %lf", &param1);
				param2 = 0.0;
				CONDI_FOR_INIT_1D(b_add, b_sub, b_prod, buffer, nlocalx, fdmconstant, gridX, param1, param2)
			} else if (startsWith(tokens[j], "product") || startsWith(tokens[j], "times")) {
				b_prod = true; b_add = false; b_sub = false;
			} else if (startsWith(tokens[j], "addition") || startsWith(tokens[j], "plus")) {
				b_add = true; b_prod = false; b_sub = false;
			} else if (startsWith(tokens[j], "subtraction") || startsWith(tokens[j], "minus")) {
				b_sub = true; b_add = false; b_prod = false;
			} else {
				ierr = PetscPrintf(PETSC_COMM_WORLD, "Unknown token %s in string %s\n", tokens[j], exact_values[i]); CHKERRQ(ierr);
				ierr = PETSC_ERR_ARG_WRONG; CHKERRQ(ierr);
			}
		}
		// Fill the exact vector
		for (int j=rstartx; j<rendx; j++) {
			ierr = VecSetValue(grid->exact, j+i*grid->nvtx, buffer[j-rstartx], INSERT_VALUES); CHKERRQ(ierr);
		}
	}

	// Assemble the vector of unknowns
	ierr = VecAssemblyBegin(grid->unknowns); CHKERRQ(ierr);
	ierr = VecAssemblyEnd(grid->unknowns); CHKERRQ(ierr);

	// Assemble the vector of exacts
	if (nexact_values > 0) {
		ierr = VecAssemblyBegin(grid->exact); CHKERRQ(ierr);
		ierr = VecAssemblyEnd(grid->exact); CHKERRQ(ierr);
	}

	// Destroy malloc()ed variables
	free(buffer);
	free(indices);
	free(gridX);
	if (tokens != NULL) free(tokens);

	return ierr;
}

/* Method to destroy, i.e. deallocate, a one-dimensional grid
*/
void destroy_grid1D(Grid1D* grid)
{
	free(grid->parent_grid);
	if (grid->unknowns != NULL) {
		VecDestroy(&(grid->unknowns));
	}
	if (grid->x != NULL) {
		VecDestroy(&(grid->x));
	}
	if (grid->exact != NULL) {
		VecDestroy(&(grid->exact));
	}
	free(grid);
}
