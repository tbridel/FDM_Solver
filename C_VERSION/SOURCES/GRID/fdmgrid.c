/* This file contains the methods handling the mesh throughout the execution of the
   FDM_SOLVER program
   @author : Thibault Bridel-Bertomeu
   @license: MIT
   @status : development
*/

#include "fdmgrid.h"

/* Method to initialize the generic Grid type
*/
Grid* init_grid(PetscInt ndim)
{
	Grid* grid = (Grid*) malloc(sizeof(Grid));

	// Get number of dimensions stored
	grid->ndim = ndim;

	// Return the Grid
	return grid;
}
