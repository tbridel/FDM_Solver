/* Source file for some utilitaries, defines and all
   @author : Thibault Bridel-Bertomeu
   @license: MIT
   @status : development
*/

#include "utils.h"

/* Method to check whether a given string starts
   with a given substring.
*/
bool startsWith(const char* a, const char* b)
{
	return (strncmp(a, b, strlen(b)) == 0);
}

/* Method to check whether two floats are close enough
   with respect to a given precision.
*/
bool isclose(PetscScalar a, PetscScalar b)
{
	PetscScalar rtol=1e-05, atol=1e-08;
	return (PetscAbsScalar(a-b)<=(atol+rtol*PetscAbsScalar(b)));
}

/* Method to unravel an index based on the sub-dimensions
   of the matrix.
*/
void unravel_index(PetscInt index, PetscInt nx, PetscInt result[2])
{
	result[0] = index%nx;
	result[1] = (PetscInt)((index - result[0])/nx);
}

/* Method to split a string with a given delimiter
   Source: https://stackoverflow.com/questions/9210528/split-string-with-delimiters-in-c
   Author: user1090944
   Date: 04/07/2014
*/
PetscErrorCode split(const PetscChar *str, PetscChar c, PetscChar ***arr, PetscInt *ntok)
{
	PetscErrorCode ierr = 0;
    PetscInt token_len = 1;
    PetscInt i = 0;
    PetscChar *p;
    PetscChar *t;

    p = str;
    while (*p != '\0')
    {
        if (*p == c)
            (*ntok)++;
        p++;
    }

    *arr = (PetscChar**) malloc(sizeof(PetscChar*) * (*ntok));
    if (*arr == NULL) {
        ierr = PETSC_ERR_ARG_BADPTR; CHKERRQ(ierr);
    }

    p = str;
    while (*p != '\0')
    {
        if (*p == c)
        {
            (*arr)[i] = (PetscChar*) malloc( sizeof(PetscChar) * token_len );
            if ((*arr)[i] == NULL){
                ierr = PETSC_ERR_ARG_BADPTR; CHKERRQ(ierr);
            }

            token_len = 0;
            i++;
        }
        p++;
        token_len++;
    }
    (*arr)[i] = (PetscChar*) malloc( sizeof(PetscChar) * token_len );
    if ((*arr)[i] == NULL) {
		ierr = PETSC_ERR_ARG_BADPTR; CHKERRQ(ierr);
    }

    i = 0;
    p = str;
    t = ((*arr)[i]);
    while (*p != '\0')
    {
        if (*p != c && *p != '\0')
        {
            *t = *p;
            t++;
        }
        else
        {
            *t = '\0';
            i++;
            t = ((*arr)[i]);
        }
        p++;
    }

    return ierr;
}

/* Mathematical method for functions to be used in the initialization of variable
   Function constant
*/
PetscScalar fdmconstant(PetscScalar foo, PetscScalar val, PetscScalar bar)
{
	return val;
}

/* Mathematical method for functions to be used in the initialization of variable
   Function gaussian
*/
PetscScalar fdmgaussian(PetscScalar x, PetscScalar mean, PetscScalar var)
{
	PetscScalar ret = 0.0;
	ret = PetscExpScalar(-PetscLogScalar(2.0) * PetscPowScalar((x-mean)/var, 2.0));
	return ret;
}

PetscScalar fdmgaussian2(PetscScalar x, PetscScalar y, PetscScalar meanx, PetscScalar varx, PetscScalar meany, PetscScalar vary)
{
	PetscScalar ret = 0.0;
	ret = PetscExpScalar(-PetscLogScalar(2.0) * PetscPowScalar((x-meanx)/varx, 2.0) - PetscLogScalar(2.0) * PetscPowScalar((y-meany)/vary, 2.0));
	return ret;
}

/* Mathematical method for functions to be used in the initialization of variable
   Function sine
*/
PetscScalar fdmsin(PetscScalar x, PetscScalar mean, PetscScalar factor)
{
	PetscScalar ret = 0.0;
	ret = PetscSinScalar(2.0*PI*(x-mean)*factor);
	return ret;
}

/* Mathematical method for functions to be used in the initialization of variable
   Function cosine
*/
PetscScalar fdmcos(PetscScalar x, PetscScalar mean, PetscScalar factor)
{
	PetscScalar ret = 0.0;
	ret = PetscCosScalar(2.0*PI*(x-mean)*factor);
	return ret;
}

/* Mathematical method for functions to be used in the initialization of variable
   Function erf, masked below by fdmerf
   Source: Abramowitz & Stegun, "Handbook of mathematical functions"
*/
PetscScalar m_erf(PetscScalar x, PetscScalar factor)
{
	// results
	PetscScalar t = 0.0, y = 0.0;
	PetscInt sign = 0;
	// constants
	PetscScalar a1 =  0.254829592;
	PetscScalar a2 = -0.284496736;
	PetscScalar a3 =  1.421413741;
	PetscScalar a4 = -1.453152027;
	PetscScalar a5 =  1.061405429;
	PetscScalar p  =  0.3275911;

	// Save the sign of x
	x = x * factor;
	sign = 1;
	if (x < 0){
		sign = -1;
	}
	x = PetscAbsScalar(x);

	// A&S formula 7.1.26
	t = 1.0/(1.0 + p*x);
	y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*PetscExpScalar(-x*x);

	return sign*y;
}
PetscScalar fdmerf(PetscScalar x, PetscScalar factor, PetscScalar bar) {
	return m_erf(x, factor);
}

/* Mathematical method for functions to be used in the initialization of variable
   Function heaviside, masked below by fdmheaviside
*/
PetscScalar m_heaviside(PetscScalar x, PetscScalar mean)
{
	return (x<mean)?0.0:1.0;
}
PetscScalar fdmheaviside(PetscScalar x, PetscScalar mean, PetscScalar foo) {
	return m_heaviside(x, mean);
}

/* Mathematical method for operators
   Operator product
*/
PetscScalar prod(PetscScalar a, PetscScalar b)
{
	return a * b;
}

/* Mathematical method for operators
   Operator addition
*/
PetscScalar add(PetscScalar a, PetscScalar b)
{
	return a + b;
}

/* Mathematical method for operators
   Operator subtraction
*/
PetscScalar sub(PetscScalar a, PetscScalar b)
{
	return a - b;
}

/* Method generating the correct Runge-Kutta scheme upon
   user request
*/
RKParams* prep_rk(PetscChar scheme_name[WORDSZ])
{
	int symbol_index;
	PetscChar rk_order[WORDSZ] = "";
	RKParams *rk_params = (RKParams*) malloc(sizeof(RKParams));
	rk_params->ierr = 0;

	// Parse the scheme name to find the RK scheme asked by the user
	if (startsWith(scheme_name, "RK")) {
		if (strstr(scheme_name, "_") != NULL) {
			symbol_index = strchr(scheme_name, '_') - scheme_name;
			for (int j=2; j<symbol_index; j++) {
				rk_order[j-2] = scheme_name[j];
			}
		} else {
			rk_order[0] = scheme_name[2];
		}
	} else {
		PetscPrintf(PETSC_COMM_WORLD, "Temporal discretization scheme not found.\n");
		rk_params->ierr = PETSC_ERR_ARG_WRONG;
	}

	// Now switch on the "rk_order" string to build the RK parameters structure
	if (strcmp(rk_order, "4") == 0) {
		rk_params->trueRK = true;
		rk_params->low_storage = false;
		rk_params->has_gammas = true;
		rk_params->steps = 4;
		rk_params->sum_coeffs = NULL;
		rk_params->coeffs = NULL;
		rk_params->alphas = NULL;
		rk_params->betas = NULL;
		rk_params->gammas = (PetscScalar*) malloc(rk_params->steps * sizeof(PetscScalar));
		rk_params->gammas[0] = 1.0;
		rk_params->gammas[1] = 0.5;
		rk_params->gammas[2] = (1.0/6.0);
		rk_params->gammas[3] = (1.0/24.0);
	} else if (strcmp(rk_order, "4o6s") == 0) {
		rk_params->trueRK = true;
		rk_params->low_storage = false;
		rk_params->has_gammas = true;
		rk_params->steps = 6;
		rk_params->sum_coeffs = NULL;
		rk_params->coeffs = NULL;
		rk_params->alphas = NULL;
		rk_params->betas = NULL;
		rk_params->gammas = (PetscScalar*) malloc(rk_params->steps * sizeof(PetscScalar));
		rk_params->gammas[0] = 1.0;
		rk_params->gammas[1] = 0.5;
		rk_params->gammas[2] = 0.165919771368;
		rk_params->gammas[3] = 0.040919732041;
		rk_params->gammas[4] = 0.007555704391;
		rk_params->gammas[5] = 0.000891421261;
	} else {
		PetscPrintf(PETSC_COMM_WORLD, "Unknown Runge-Kutta scheme.\n");
		rk_params->ierr = PETSC_ERR_ARG_WRONG;
	}

	return rk_params;
}

void destroy_rkParams(RKParams *rk_params)
{
	if (rk_params->sum_coeffs != NULL) {
		free(rk_params->sum_coeffs);
	}
	if (rk_params->coeffs != NULL) {
		free(rk_params->coeffs);
	}
	if (rk_params->alphas != NULL) {
		free(rk_params->alphas);
	}
	if (rk_params->betas != NULL) {
		free(rk_params->betas);
	}
	if (rk_params->gammas != NULL) {
		free(rk_params->gammas);
	}
	free(rk_params);
}
