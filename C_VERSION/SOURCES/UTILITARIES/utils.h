/* Header file for some utilitaries, defines and all
   @author : Thibault Bridel-Bertomeu
   @license: MIT
   @status : development
*/

#if !defined(__UTILS_H)
#define __UTILS_H

#include <petscsys.h>
#include <petscmath.h>
#include <stdbool.h>
#include <string.h>

#define TEN 10
#define TWENTY 20
#define WORDSZ 1024
#define STRSZ 2048
#define PI 3.14159265358979323846264338327950288419716939937510582097494459230781640628620899

typedef struct RKParams RKParams;
struct RKParams {
	PetscErrorCode ierr;
	bool trueRK;
	bool low_storage;
	bool has_gammas;
	PetscInt steps;
	PetscScalar *sum_coeffs;
	PetscScalar **coeffs;
	PetscScalar *alphas;
	PetscScalar *betas;
	PetscScalar *gammas;
};

// Scalar utilities
bool isclose(PetscScalar a, PetscScalar b);
void unravel_index(PetscInt index, PetscInt nx, PetscInt result[2]);
// String utilities
bool startsWith(const char* a, const char* b);
PetscErrorCode split(const PetscChar *str, PetscChar c, PetscChar ***arr, PetscInt *ntok);
// Mathematical functions
PetscScalar fdmconstant(PetscScalar foo, PetscScalar val, PetscScalar bar);
PetscScalar fdmgaussian(PetscScalar x, PetscScalar mean, PetscScalar var);
PetscScalar fdmgaussian2(PetscScalar x, PetscScalar y, PetscScalar meanx, PetscScalar varx, PetscScalar meany, PetscScalar vary);
PetscScalar fdmsin(PetscScalar x, PetscScalar mean, PetscScalar factor);
PetscScalar fdmcos(PetscScalar x, PetscScalar mean, PetscScalar factor);
PetscScalar m_erf(PetscScalar x, PetscScalar factor);
PetscScalar fdmerf(PetscScalar x, PetscScalar factor, PetscScalar bar);
PetscScalar m_heaviside(PetscScalar x, PetscScalar mean);
PetscScalar fdmheaviside(PetscScalar x, PetscScalar mean, PetscScalar foo);
// Mathematical operators
PetscScalar prod(PetscScalar a, PetscScalar b);
PetscScalar add(PetscScalar a, PetscScalar b);
PetscScalar sub(PetscScalar a, PetscScalar b);
// Runge-Kutta department
RKParams* prep_rk(PetscChar scheme_name[WORDSZ]);
void destroy_rkParams(RKParams *rk_params);

#endif
