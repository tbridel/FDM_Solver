'''
This file contains the class handling the solver and the variables throughout the execution of the
FDM_SOLVER program
'''
__author__ = "Thibault Bridel-Bertomeu"
__license__ = "MIT"
__status__ = "Development"

# Import modules
from mpi4py import MPI
import numpy as np
import petsc4py
from petsc4py import PETSc
import petsc_utils as pu
import utils
from utils import Print
import sys


class Solver(object):
    '''
    Base class 'Solver' made to handle the solving operations during the execution of the FDM_SOLVER.
    Variables are the streamfunction and its X- and Y- first derivatives
    '''

    def __init__(self, equation, comm=None):
        '''
        Initialization of the base class for the solvers
        '''

        # Read the timestep, the number of iterations and the export frequency
        self._timestep = equation['timestep']
        self._niterations = equation['niterations']
        self._exportfrequency = equation['export_every']
        self._plotfrequency = equation['plot_every']

        # Read the scheme
        self._numScheme = equation['scheme']

        # Read the filter
        self._numFilter = equation['filter']

        # Default to no Runge-Kutta substeps if no further declaration
        self.rk_params = {'trueRK':False, 'steps':0, 'coeffs':[], 'sum_coeffs':[1.,], 'low-storage':False}
        # Else ...
        if 'RK' in self._numScheme['name']:
            if '_' in self._numScheme['name']:
                rk_order = self._numScheme['name'].split('_')[0][2:]
            else:
                rk_order = self._numScheme['name'][self._numScheme['name'].index('RK')+1+1]
            #
            self.rk_params = utils.rk_params[rk_order]

        # Prepare the Runge-Kutta coefficients if it was given under the factored form
        if 'gammas' in self.rk_params:
            self.rk_params['coeffs'] = [ [0.]*(idx+1) for idx in range(self.rk_params['steps']) ]
            for idx in range(self.rk_params['steps']):
                if (self.rk_params['steps']-1)-(idx+1) > 0:
                    self.rk_params['coeffs'][idx][-1] = self.rk_params['gammas'][(self.rk_params['steps']-1)-idx] / self.rk_params['gammas'][(self.rk_params['steps']-1)-(idx+1)]
                else:
                    self.rk_params['coeffs'][idx][-1] = self.rk_params['gammas'][(self.rk_params['steps']-1)-idx]

        # Check also for spatial scheme compacity
        self.isCompact = 'Co' in self._numScheme['name']
        if self.isCompact and not self.rk_params['trueRK']: # trick to use the same conditional statement for compact and non-compact schemes
            self.rk_params['trueRK'] = True

    @property
    def dt(self):
        ''' The time step of the solver '''
        return self._timestep

    @property
    def nit(self):
        ''' Number of iterations required by the user '''
        return self._niterations

    @property
    def scheme(self):
        ''' Name of the numerical scheme '''
        return self._numScheme

    @property
    def filter(self):
        ''' Name of filter '''
        return self._numFilter

    def create_filter(self, grid, comm=None, var=None):
        '''
        Create the filtering matrix if a filter has been defined in the user parameters file
        '''

        if self.filter is not None:
            # Create the matrix
            self.filterMat = pu.Matrix(grid.invtx[var[0]], comm=comm)

            try:
                self.filter['coeffs'] = utils.filter_schemes[self.filter['name']]
            except:
                raise NotImplementedError

            # Fill the matrix
            coefficients = np.concatenate((self.filter['coeffs'][1:][::-1], [self.filter['coeffs'][0],], self.filter['coeffs'][1:], self.filter['coeffs'][1:][::-1], [self.filter['coeffs'][0],], self.filter['coeffs'][1:]))
            #
            dia = np.arange(-len(self.filter['coeffs'])+1, len(self.filter['coeffs']))
            diagonals = np.concatenate((dia*grid.nnx[var[0]], dia))
            #
            self.filterMat.setDiagonal(diagonals, coefficients, safety={'b':grid.ndim>1, 'nx':grid.nnx[var[0]], 'ny':grid.nny[var[0]] if grid.ndim>1 else None})

    def dirichletize_filter(self, grid, indices, offset, c_value):
        '''
        Apply Dirichlet boundary conditions to the filter matrix
        '''

        # Associate the direction to the index of tv and cfl
        idir = 0 if abs(offset) == 1 else 1

        # Condition on equation['filter']
        if self.filter['name'] in ['SFo11p',]:
            fd_schemes = ['SF_1-9', 'SF_2-8', 'SF_3-7', 'SF_4-6']
            stencil = np.arange(-5, 6)
            if offset < 0:
                for idx in xrange(len(fd_schemes)):
                    # Remove botch centered scheme
                    self.filterMat.setValues(np.repeat(indices+idx*offset, stencil.size), np.tile(stencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, stencil.size),
                                       0.0, mode=PETSc.InsertMode.INSERT)
                    # Replace with non-centered scheme
                    nstencil = np.arange(-int(fd_schemes[idx].split('_')[1].split('-')[0]), int(fd_schemes[idx].split('_')[1].split('-')[1])+1)
                    nstencil = -nstencil[::-1]
                    nstencil = nstencil[:-1]
                    self.filterMat.setValues(np.repeat(indices+idx*offset, nstencil.size), np.tile(nstencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, nstencil.size),
                                       np.tile(utils.filter_schemes[fd_schemes[idx]][::-1][:-1], indices.size), mode=PETSc.InsertMode.INSERT)
                    for index in indices:
                        self.filterBocosValues.setValue(index+idx*offset, utils.filter_schemes[fd_schemes[idx]][::-1][-1]*c_value)
            else:
                for idx in xrange(len(fd_schemes)):
                    # Remove botch centered scheme
                    self.filterMat.setValues(np.repeat(indices+idx*offset, stencil.size), np.tile(stencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, stencil.size),
                                       0.0, mode=PETSc.InsertMode.INSERT)
                    # Replace with non-centered scheme
                    nstencil = np.arange(-int(fd_schemes[idx].split('_')[1].split('-')[0]), int(fd_schemes[idx].split('_')[1].split('-')[1])+1)
                    nstencil = nstencil[:-1]
                    self.filterMat.setValues(np.repeat(indices+idx*offset, nstencil.size), np.tile(nstencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, nstencil.size),
                                       np.tile(utils.filter_schemes[fd_schemes[idx]][:-1], indices.size), mode=PETSc.InsertMode.INSERT)
                    for index in indices:
                        self.filterBocosValues.setValue(index+idx*offset, utils.filter_schemes[fd_schemes[idx]][-1]*c_value)
        elif self.filter['name'] in ['SFo13p',]:
            coeN = self.filter['coeffs'][1:][::-1]
            coeP = self.filter['coeffs'][1:]
            for index in indices:
                if offset > 0:
                    for idx in xrange(len(coeN)):
                        self.filterBocosValues.setValue(index+idx, np.sum(coeN[:len(coeN)-idx])*self.cfl[idir]*c_value)
                else:
                    for idx in xrange(len(coeP)):
                        self.filterBocosValues.setValue(index+idx, np.sum(coeP[idx:])*self.cfl[idir]*c_value)            
        else:
            raise NotImplementedError

    def neumann_filter(self, grid, indices, offset, factor, c_value):
        '''
        Set the filter matrix for a neumann boundary condition
        Coherence in the order of the scheme is implemented
        '''

        if abs(c_value) > 0:
            raise NotImplementedError

        # Associate the direction to the index of tv and cfl
        idir = 0 if abs(offset) == 1 else 1

        # Condition on equation['filter']

        if self.filter['name'] in ['SFo11p']:
            fd_schemes = ['SF_0-10', 'SF_1-9', 'SF_2-8', 'SF_3-7', 'SF_4-6']
            stencil = np.arange(-5, 6)
            if offset < 0:
                for idx in xrange(len(fd_schemes)):
                    # Remove botch centered scheme
                    self.filterMat.setValues(np.repeat(indices+idx*offset, stencil.size), np.tile(stencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, stencil.size),
                                       0.0, mode=PETSc.InsertMode.INSERT)
                    if not idx:
                        continue
                    # Replace with non-centered scheme
                    nstencil = np.arange(-int(fd_schemes[idx].split('_')[1].split('-')[0]), int(fd_schemes[idx].split('_')[1].split('-')[1])+1)
                    nstencil = -nstencil[::-1]
                    self.filterMat.setValues(np.repeat(indices+idx*offset, nstencil.size), np.tile(nstencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, nstencil.size),
                                       np.tile(utils.filter_schemes[fd_schemes[idx]][::-1], indices.size), mode=PETSc.InsertMode.INSERT)
            else:
                for idx in xrange(len(fd_schemes)):
                    # Remove botch centered scheme
                    self.filterMat.setValues(np.repeat(indices+idx*offset, stencil.size), np.tile(stencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, stencil.size),
                                       0.0, mode=PETSc.InsertMode.INSERT)
                    if not idx:
                        continue
                    # Replace with non-centered scheme
                    nstencil = np.arange(-int(fd_schemes[idx].split('_')[1].split('-')[0]), int(fd_schemes[idx].split('_')[1].split('-')[1])+1)
                    self.filterMat.setValues(np.repeat(indices+idx*offset, nstencil.size), np.tile(nstencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, nstencil.size),
                                       np.tile(utils.filter_schemes[fd_schemes[idx]], indices.size), mode=PETSc.InsertMode.INSERT)
        else:
            raise NotImplementedError


    def open_filter(self, grid, indices, offset):
        '''
        Set the filter matrix for an open boundary condition
        Coherence in the order of the scheme is implemented
        '''

        # Associate the direction to the index of tv and cfl
        idir = 0 if abs(offset) == 1 else 1

        # Get the other offset to re-establish the proper scheme in the normal direction
        other_offset = grid.nnx[grid.varnames[0]] if abs(offset) == 1 else 1

        # Condition on equation['filter']
        if self.filter['name'] in ['SFo11p',]:
            fd_schemes = ['SF_0-10', 'SF_1-9', 'SF_2-8', 'SF_3-7', 'SF_4-6']
            stencil = np.arange(-5, 6)
            if offset < 0:
                for idx in xrange(len(fd_schemes)):
                    # Remove botch centered scheme
                    self.filterMat.setValues(np.repeat(indices+idx*offset, stencil.size), np.tile(stencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, stencil.size),
                                       0.0, mode=PETSc.InsertMode.INSERT)
                    # if not idx:
                    #     continue
                    # Replace with non-centered scheme
                    nstencil = np.arange(-int(fd_schemes[idx].split('_')[1].split('-')[0]), int(fd_schemes[idx].split('_')[1].split('-')[1])+1)
                    nstencil = -nstencil[::-1]
                    self.filterMat.setValues(np.repeat(indices+idx*offset, nstencil.size), np.tile(nstencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, nstencil.size),
                                       np.tile(utils.filter_schemes[fd_schemes[idx]][::-1], indices.size), mode=PETSc.InsertMode.INSERT)
            else:
                for idx in xrange(len(fd_schemes)):
                    # Remove botch centered scheme
                    self.filterMat.setValues(np.repeat(indices+idx*offset, stencil.size), np.tile(stencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, stencil.size),
                                       0.0, mode=PETSc.InsertMode.INSERT)
                    # if not idx:
                    #     continue
                    # Replace with non-centered scheme
                    nstencil = np.arange(-int(fd_schemes[idx].split('_')[1].split('-')[0]), int(fd_schemes[idx].split('_')[1].split('-')[1])+1)
                    self.filterMat.setValues(np.repeat(indices+idx*offset, nstencil.size), np.tile(nstencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, nstencil.size),
                                       np.tile(utils.filter_schemes[fd_schemes[idx]], indices.size), mode=PETSc.InsertMode.INSERT)            
        else:
            raise NotImplementedError    

    def periodize_filter(self, grid, direction=None, override=True):
        '''
        Apply periodicity conditions to the filter matrix
        '''

        # Some handles
        indL, indR = np.array(grid.bocos[direction+'min']), np.array(grid.bocos[direction+'max'])

        # Associate the direction to the index of tv and cfl
        idir = 0 if direction == 'x' else 1
        #
        key2offset = {'xmin':1, 'xmax':-1, 'ymin':grid.nnx[grid.varnames[0]], 'ymax':-grid.nnx[grid.varnames[0]]}
        offL, offR = key2offset[direction+'min'], key2offset[direction+'max']

        # Condition on equation['filter']
        if self.filter['name'] in ['SFo11p', 'SFo13p']:
            pass
            coeN = self.filter['coeffs'][1:][::-1]
            coeP = self.filter['coeffs'][1:]
            for idx in xrange(len(coeN)):
                self.filterMat.setValues(np.tile(indL+idx*offL, len(coeN)-idx),
                                         np.concatenate([indR+(jdx+1)*offR for jdx in np.arange(len(coeN))[::-1][idx:]]),
                                         np.repeat(coeN[:len(coeN)-idx], len(indL)))
                self.filterMat.setValues(np.tile(indR+idx*offR, len(coeP)-idx),
                                         np.concatenate([indL+(jdx+1)*offL for jdx in np.arange(len(coeP))[:len(coeP)-idx]]),
                                         np.repeat(coeP[idx:], len(indR)))
        else:
            raise NotImplementedError

    def apply_bocos(self, var_names, boco_characteristics, grid, comm=None):
        '''
        Apply the boundary conditions properly
        '''
        if grid.ndim > 2 or grid.ndim < 1:
            raise NotImplementedError

        # Better assemble the matrices now 
        # and call upon PETSc routines rather than loops (esp. for 2D & 3D, expensive++)
        self.lhs.assemble()
        self.rhs.assemble()
        if self.isCompact:
            self.compact_assemble()
        if self.filter is not None:
            self.filterMat.assemble()

        # Initialize the bocos vector
        self.bocosValues = pu.Vector(sum([grid.invtx[var] for var in var_names]), comm=comm)
        if self.filter is not None:
            self.filterBocosValues = pu.Vector(sum([grid.invtx[var] for var in var_names]), comm=comm)

        # To avoid doing periodicity twice
        perioDone = {'x':False, 'y':False}

        # Some offsets and factors for neumann boco
        key2offset = {'xmin':1, 'xmax':-1, 'ymin':grid.nnx[var[0]], 'ymax':-grid.nnx[var[0]]}
        key2factor = {'x':grid.dx, 'y':grid.dy if grid.ndim >= 2 else None}

        # An identity matrix to impose dirichlet bocos a posteriori during the RK substeps in need be
        self.zeroForNeumann = pu.Matrix(grid.invtx[var[0]], comm=comm)
        self.zeroForNeumann.to_eye(assemble=True)

        # Loop on those indices and do the proper operation
        for jdx, name in enumerate(var_names):
            indicesDone = []
            # Get the indices to apply the boundary conditions
            bocoAt = grid.bocos[name]          
            # Start with the periodic bocos
            for key in bocoAt:
                c_type = boco_characteristics[key][name]['type']
                if c_type == 'periodic':
                    if not perioDone[key[0]]:
                        perioDone[key[0]] = True
                        if np.all([perioDone[case] for case in perioDone.keys()]):
                            self.set_periodic(grid, direction=key[0], override=False)
                            if self.filter is not None:
                                self.periodize_filter(grid, direction=key[0], override=False)
                        else:
                            self.set_periodic(grid, direction=key[0])
                            if self.filter is not None:
                                self.periodize_filter(grid, direction=key[0])
                    #
                    indicesDone += bocoAt[key].tolist()

            # Then the Neumann and Dirichlet bocos
            for key in bocoAt:
                indices = np.asarray(bocoAt[key])
                c_type = boco_characteristics[key][name]['type']
                ignore = [ii not in indicesDone for ii in indices]
                #
                if c_type == 'dirichlet':
                    offset = key2offset[key]
                    c_value = boco_characteristics[key][name]['value']
                    #
                    self.set_dirichlet(grid, indices, offset, c_value)
                    #
                    if self.filter is not None:
                        self.dirichletize_filter(grid, indices, offset, c_value)
                elif c_type == 'neumann':
                    offset, factor = key2offset[key], key2factor[key[0]]
                    c_value = boco_characteristics[key][name]['value']
                    #
                    self.set_neumann(grid, indices, offset, factor, c_value)
                elif c_type == 'open':
                    offset = key2offset[key]
                    #
                    self.set_open(grid, indices[ignore], offset)
                    #
                    if self.filter is not None:
                        self.open_filter(grid, indices[ignore], offset)
                elif c_type == 'periodic': # already done above
                    continue
                else:
                    raise NotImplementedError
                #
                indicesDone += indices.tolist()

        # Assemble the bocos vector too
        self.bocosValues.assemble()
        if self.filter is not None:
            self.filterBocosValues.assemble()

        # Get the RHS matrix if the scheme is compact
        if self.isCompact:
            self.compact_combine()

    def execute(self, grid, es=None, comm=None):
        '''
        Call the KSP after creating the RHS vector 
        '''

        # Keep an identity matrix somewhere
        identity = pu.Matrix(sum([grid.invtx[var] for var in grid.varnames]), comm=comm)
        identity.to_eye(assemble=True)

        # Prepare a few objects before the temporal loops to avoid repeating costly operations
        if 'gammas' in self.rk_params:
            R = identity.getMatrix.copy()
            mlist = []
            mlist.append(self.rhs.getMatrix)
            R.axpy(self.rk_params['gammas'][0], mlist[0])
            for idx in xrange(1, self.rk_params['steps']):
                mlist.append(self.rhs.getMatrix.matMult(mlist[-1]))
                R.axpy(self.rk_params['gammas'][idx], mlist[-1])
            del mlist
        elif not self.rk_params['low-storage']:
            # Prepare the linear solver
            ksp = PETSc.KSP()
            ksp.create(comm)
            if self.isCompact: 
                ksp.getPC().setType('jacobi')
            ksp.setFromOptions()
            if self.isCompact:
                ksp.getPC().setUp()
        elif self.rk_params['low-storage']:
            # Turn the factors beta into a diagonal matrix for improved performance
            betaMatrix = [None]*len(self.rk_params['betas'])
            for idx in range(len(self.rk_params['betas'])):
                betaMatrix[idx] = identity.getMatrix.copy()
                betaMatrix[idx].axpy(self.rk_params['betas'][idx]-1.0, self.zeroForNeumann.getMatrix)
        #
        if self.filter is not None:
            self.filterMat.getMatrix.scale(self.filter['strength'])
            self.filterBocosValues.getVector.scale(self.filter['strength'])

        # Loop on iterations
        for step in xrange(self._niterations):
            # The implementation differs function of the way the RK coeffs are stored
            if 'gammas' in self.rk_params:
                # Get some auxiliary vectors
                b = grid.unknowns.getVector.duplicate()
                # Apply the temporal scheme
                R.mult(grid.unknowns.getVector, b)
                # Apply the boundary conditions
                b.axpy(1.0, self.bocosValues.getVector)
                aux = b.duplicate()
                self.zeroForNeumann.getMatrix.mult(b, aux)
                # Save the result
                grid.unknowns.copy(aux) #b)
            elif not self.rk_params['low-storage']:
                # Initialize three auxiliary PETSc vectors for the RK process
                b = grid.unknowns.getVector.duplicate()
                uk = grid.unknowns.getVector.duplicate()
                aux = grid.unknowns.getVector.duplicate()                
                # Initialize the list containing the vectors obtained at the RK substeps
                clist = []
                clist.append(grid.unknowns.getVector)
                # Loop on the RK steps to build the vectors in clist
                for idx in range(self.rk_params['steps']):
                    # Build first the linear combination to be multiplied by the "self.rhs"
                    for jdx in range(idx+1):
                        aux.axpy(self.rk_params['coeffs'][idx][jdx], clist[jdx])
                    # Multiply it to get the spatial discretization in "b"
                    if self.isCompact:
                        self.compact_routine(aux, b, comm=comm)
                    else:
                        self.rhs.getMatrix.mult(aux, b)
                    b.axpy(1.0, self.bocosValues.getVector)                    
                    self.zeroForNeumann.getMatrix.mult(grid.unknowns.getVector, uk)
                    b.axpy(1.0, uk)
                    # Add to the substep vectors list
                    c1 = b.copy()
                    clist.append(c1)
                # Now multiply each vector by the spatial matrix and add with the correct RK weight
                for idx in range(self.rk_params['steps']+1):
                    if self.isCompact:
                        self.compact_routine(clist[idx], b, comm=comm)
                    else:
                        self.rhs.getMatrix.mult(clist[idx], b)
                    #
                    b.scale(self.rk_params['sum_coeffs'][idx])
                    if not idx:
                        c4 = b.copy()
                    else:
                        c4.axpy(1.0, b)
                # Finally add the unknowns from previous timestep
                if self.rk_params['trueRK']:
                    self.zeroForNeumann.getMatrix.mult(grid.unknowns.getVector, uk)
                    c4.axpy(1.0, uk)
                c4.axpy(1.0, self.bocosValues.getVector)
                # Do the last system inversion to get the unknown vector at timestep n+1
                ksp.setOperators(self.lhs.getMatrix)
                ksp.solve(c4, uk)
                # Update the unknowns with the result of the timestepping
                grid.unknowns.copy(uk)
            else:
                # Initialize four auxiliary PETSc vectors for the low-storage RK process
                omega = grid.unknowns.getVector.duplicate()
                b = grid.unknowns.getVector.duplicate()
                aux, aux2 = grid.unknowns.getVector.duplicate(), grid.unknowns.getVector.duplicate()                
                uk = grid.unknowns.getVector.copy()
                #               
                for idx in xrange(self.rk_params['steps']):
                    self.zeroForNeumann.getMatrix.mult(omega, aux)
                    aux.scale(self.rk_params['alphas'][idx])
                    if self.isCompact:
                        self.compact_routine(uk, b, comm=comm)
                    else:
                        self.rhs.getMatrix.mult(uk, b)
                    b.axpy(1.0, self.bocosValues.getVector)
                    b.axpy(1.0, aux)
                    omega = b.copy()
                    #
                    betaMatrix[idx].mult(b, aux2)
                    self.zeroForNeumann.getMatrix.mult(uk, aux)
                    aux2.axpy(1.0, aux)
                    #
                    uk = aux2.copy()
                #
                grid.unknowns.copy(uk)
            # If a filter is to be used to damp oscillations, it is called now
            if self.filter is not None:
                uk = grid.unknowns.getVector.duplicate()
                self.filterMat.getMatrix.mult(grid.unknowns.getVector, uk)
                uk.axpy(1.0, self.filterBocosValues.getVector)
                uk.aypx(-1.0, grid.unknowns.getVector)
                #
                grid.unknowns.copy(uk)

            errorL2 = [np.nan,]
            errorL1 = [np.nan,]
            if es is not None:
                # To reconstruct the exact solution
                # We need to load a few functions from numpy
                from numpy import sin, cos, pi, exp, log
                # And store in memory some variables
                x = np.linspace(grid.xmin, grid.xmax, grid.nx)
                if grid.ndim == 2:
                    y = np.linspace(grid.ymin, grid.ymax, grid.ny)
                t = (step+1)*self._timestep
                # Get the numerical solution
                num = grid.preplot(comm=comm)
                # Then eval the string expression given by the user
                solution = [None]*len(es.keys())
                errorL2 = [None]*len(es.keys())
                errorL1 = [None]*len(es.keys())
                # for c_varidx, c_varname in enumerate(es.keys()):
                for c_varidx in range(len(grid.varnames)):
                    c_varname = grid.varnames[c_varidx]
                    solution[c_varidx] = eval(str(es[c_varname]))
                    if type(solution[c_varidx]) is not np.ndarray:
                        solution[c_varidx] = np.ones(x.size) * solution[c_varidx]
                    errorL2[c_varidx] = np.sqrt(np.sum((solution[c_varidx]-num[c_varidx])**2)/np.sum(solution[c_varidx]**2))
                    errorL1[c_varidx] = np.sum(np.fabs(num[c_varidx]-solution[c_varidx]))/grid.nvtx

            # Tell a few things to the user
            Print("Iter. %05d\t Time %.6e\t Min/Max u (%.4e,%.4e)\t Error L2 %.2e\t Error L1 %.2e\t"
                  % (step+1, (step+1)*self._timestep, 
                     np.real(grid.unknowns.getVector.getArray()).min(), 
                     np.real(grid.unknowns.getVector.getArray()).max(), 
                     errorL2[0], errorL1[0]),
                  stype=None,
                  comm=comm)

            # Export a solution if the right iteration is reached
            if self._exportfrequency is not None and not step % self._exportfrequency:
                grid.export_solution(step, comm=comm)

            # If the grid is one-dimensional, can plot the solution in real time
            if not step:
                ymin, ymax = grid.unknowns.getVector.getArray().min(), grid.unknowns.getVector.getArray().max()
                maxi = max(abs(ymin), abs(ymax))
            #     
            if self._plotfrequency is not None and (not (step+1) % self._plotfrequency or step+1 == self._niterations):
                if 'matplotlib' not in sys.modules:
                    import matplotlib.pyplot as plt
                if grid.ndim == 1:
                    toplot = grid.preplot(comm=comm)
                    if not comm.Get_rank():
                        ax = plt.subplot(111)
                        plt.ion()
                        colors = []*len(grid.varnames)
                        for varidx in range(len(grid.varnames)):
                            ln = ax.plot(np.linspace(grid.xmin, grid.xmax, grid.nx), 
                                    toplot[varidx],
                                    '-', mfc='none', label=r'Variable %s'%grid.varnames[varidx])
                            colors.append(ln[0].get_color())
                        if es is not None:
                            for varidx in range(len(grid.varnames)):
                                ax.plot(np.linspace(grid.xmin, grid.xmax, grid.nx),
                                        solution[varidx],
                                        'o', mfc='none', label=r'Solution %s'%grid.varnames[varidx], color=colors[varidx])
                        ax.legend(loc='best', ncol=int(np.ceil(len(grid.varnames)/2.)))
                        ax.set_xlim(grid.xmin, grid.xmax)
                        # ax.set_xlim(-0.08+self.tv[0]*step*self._timestep, 0.08+self.tv[0]*step*self._timestep)
                        # ax.set_xlim(4, 5)
                        ax.set_ylim(-2.0*maxi, 2.0*maxi)
                        ax.set_xlabel('x')
                        ax.set_title('Iteration %d, time %.4f' % (step+1, (step+1)*self._timestep))
                        ax.grid(b=True)
                elif grid.ndim == 2:
                    toplot = grid.preplot(comm=comm)
                    if not comm.Get_rank():
                        ax1 = plt.subplot(221)
                        plt.ion()                    
                        ct = ax1.contourf(np.linspace(grid.xmin, grid.xmax, grid.nx),
                                          np.linspace(grid.ymin, grid.ymax, grid.ny),
                                          toplot[0],
                                          11, extend='both')
                        cb = plt.colorbar(ct, ax=ax1)
                        cb.set_label('u')
                        ax1.set_xlim(grid.xmin, grid.xmax)
                        ax1.set_ylim(grid.ymin, grid.ymax)
                        ax1.set_xlabel('x')
                        ax1.set_ylabel('y')
                        ax1.set_title('Iteration %d, time %.4f' % (step+1, (step+1)*self._timestep))   
                        ax1.grid(b=True)
                        ax2 = plt.subplot(222)
                        ax2.plot(np.linspace(grid.xmin, grid.xmax, grid.nx), toplot[0][grid.ny//2, :], '-k')
                        ax2.set_xlabel('x')
                        ax2.set_ylabel('u(y=ymax/2)')
                        ax2.grid(b=True)
                        ax3 = plt.subplot(223)
                        ax3.plot(np.linspace(grid.ymin, grid.ymax, grid.ny), toplot[0][:, grid.nx//2], '-k')
                        ax3.set_xlabel('y')
                        ax3.set_ylabel('u(x=xmax/2)')
                        ax3.grid(b=True)
                        plt.subplots_adjust(wspace=0.5, hspace=0.5)
                else:
                    raise NotImplementedError
                #
                if step == self._niterations-1:
                    if not comm.Get_rank(): plt.waitforbuttonpress()
                    comm.Barrier()
                else:
                    if not comm.Get_rank(): plt.pause(0.01)
                if not comm.Get_rank(): plt.close()
