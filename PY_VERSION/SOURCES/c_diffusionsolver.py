'''
This file contains the class handling the solver for the diffusion (or heat) equation
'''
__author__ = "Thibault Bridel-Bertomeu"
__license__ = "MIT"
__status__ = "Development"

# Import modules
from c_fdmsolver import Solver
import numpy as np
from petsc4py import PETSc
import petsc_utils as pu
import utils
from utils import Print

class DiffusionSolver(Solver):
    '''
    Derived class 'DiffusionSolver' to solve the diffusion equation
    Schemes available: 'FTCS', 'BTCS', 'CNCS'
    '''

    def __init__(self, equation, grid, comm=None):
        '''
        Initialization of the class for the diffusion equation solver
        '''

        if grid.ndim > 2:
            raise NotImplementedError

        # Initialize the parent class
        Solver.__init__(self, equation, comm=comm)

        # Get the conductivity from the user inputs with safeties
        self.conductivity = equation['parameters']
        if self.conductivity < 0.0 or np.isclose(self.conductivity, 0.0):
            Print('a strictly positive conductivity is required for the diffusion term. Aborting.', stype='error', comm=comm)
            exit()

        # Initialize constants to be used in the schemes matrices definition
        self.cfl = np.zeros(3)
        self.cfl[:grid.ndim] = self.conductivity * self._timestep / np.array(grid.steps)**2

    @property
    def cond(self):
        ''' Conductivity for the diffusion term '''
        return self.conductivity

    def get_lhs(self, grid, comm=None):
        '''
        Fill the left hand side matrix depending on the numerical scheme
        Multiply the vector at time (n+1)
        '''

        # Prepare LHS matrix
        self.lhs = pu.Matrix(grid.invtx, comm=comm)

        # Conditional on equation['scheme']
        if self.scheme['name'] == 'FTCS':
            self.lhs.to_eye(assemble=False)
        elif self.scheme['name'] == 'BTCS':
            self.lhs.setDiagonal([-grid.nnx, -1, 0, 1, grid.nnx],
                                 [-self.cfl[1], -self.cfl[0], 1.0+2.0*(self.cfl[0]+self.cfl[1]), -self.cfl[0], -self.cfl[1]], 
                                 safety={'b':grid.ndim>1, 'nx':grid.nnx, 'ny':grid.nny if grid.ndim>1 else None})
        elif self.scheme['name'] == 'CNCS':
            self.lhs.setDiagonal([-grid.nnx, -1, 0, 1, grid.nnx],
                                 [-self.cfl[1]/2.0, -self.cfl[0]/2.0, 1.0+1.0*(self.cfl[0]+self.cfl[1]), -self.cfl[0]/2.0, -self.cfl[1]/2.0],
                                 safety={'b':grid.ndim>1, 'nx':grid.nnx, 'ny':grid.nny if grid.ndim>1 else None})
        else:
            raise NotImplementedError

    def get_rhs(self, grid, comm=None):
        '''
        Fill the right hand side matrix depending on the numerical scheme
        Multiply the vector at time (n)
        '''

        # Prepare and identity matrix
        identity = pu.Matrix(grid.invtx, comm=comm)
        identity.to_eye(assemble=True)

        # Prepare RHS matrix
        self.rhs = pu.Matrix(grid.invtx, comm=comm)

        # Conditional on equation['scheme']
        if self.scheme['name'] == 'FTCS':
            self.rhs.setDiagonal([-grid.nnx, -1, 0, 1, grid.nnx],
                                 [self.cfl[1], self.cfl[0], 1.0-2.0*(self.cfl[0]+self.cfl[1]), self.cfl[0], self.cfl[1]],
                                 safety={'b':grid.ndim>1, 'nx':grid.nnx, 'ny':grid.nny if grid.ndim>1 else None})
        elif self.scheme['name'] == 'BTCS':
            self.rhs.to_eye(assemble=False)
        elif self.scheme['name'] == 'CNCS':
            self.lhs.assemble()
            self.rhs.assemble()
            self.rhs.getMatrix.axpy(-1.0, self.lhs.getMatrix)
            self.rhs.getMatrix.axpy(2.0, identity.getMatrix)            
        else:
            raise NotImplementedError

    def set_dirichlet(self, grid, indices, offset, c_value):
        '''
        Set the lhs and rhs matrices for a dirichlet boundary condition
        Coherence in the order of the scheme is implemented
        '''

        # Associate the direction to the index of tv and cfl
        idir = 0 if abs(offset) == 1 else 1

        # Condition on equation['scheme']
        if self.scheme['name'] in ['FTCS', 'BTCS', 'CNCS']:
            for index in indices:
                self.bocosValues.setValue(index, self.cfl[idir]*c_value)
        else:
            raise NotImplementedError

    def set_neumann(self, grid, indices, offset, factor, c_value, bocoVec=None):
        '''
        Set the lhs and rhs matrices for a neumann boundary condition
        Coherence in the order of the scheme is implemented
        '''

        if abs(c_value) > 0:
            raise NotImplementedError

        # Associate the direction to the index of tv and cfl
        idir = 0 if abs(offset) == 1 else 1

        # Condition on equation['scheme']
        if self.scheme['name'] == 'FTCS':
            self.rhs.setValues(indices, indices+offset, self.cfl[idir], mode=PETSc.InsertMode.ADD_VALUES)
        elif self.scheme['name'] == 'BTCS':
            self.lhs.setValues(indices, indices+offset, -self.cfl[idir], mode=PETSc.InsertMode.ADD_VALUES)
        elif self.scheme['name'] == 'CNCS':
            self.lhs.setValues(indices, indices+offset, -self.cfl[idir]/2.0, mode=PETSc.InsertMode.ADD_VALUES)
            self.rhs.setValues(indices, indices+offset, self.cfl[idir]/2.0, mode=PETSc.InsertMode.ADD_VALUES)
        else:
            raise NotImplementedError

    def set_open(self, grid, indices, offset):
        '''
        Set the lhs and rhs matrices for an open boundary condition
        Coherence in the order of the scheme is implemented
        '''

        raise NotImplementedError

    def set_periodic(self, grid, direction=None, override=True):
        ''' 
        Set the lhs and the rhs matrices for periodicity
        Depends on the scheme
        '''

        # Some handles
        indL, indR = np.array(grid.bocos[direction+'min']), np.array(grid.bocos[direction+'max'])

        # Associate the direction to the index of tv and cfl
        idir = 0 if direction == 'x' else 1
        key2offset = {'xmin':1, 'xmax':-1, 'ymin':grid.nnx, 'ymax':-grid.nnx}
        offL, offR = key2offset[direction+'min'], key2offset[direction+'max']

        # Conditional on equation['scheme']
        if self.scheme['name'] == 'FTCS':
            self.rhs.setValues(indL, indR+offR, self.cfl[idir])
            self.rhs.setValues(indR, indL+offL, self.cfl[idir])
        elif self.scheme['name'] == 'BTCS':
            self.lhs.setValues(indL, indR+offR, -self.cfl[idir])
            self.lhs.setValues(indR, indL+offL, -self.cfl[idir])
        elif self.scheme['name'] == 'CNCS':
            self.lhs.setValues(indL, indR+offR, -self.cfl[idir]/2.0)
            self.lhs.setValues(indR, indL+offL, -self.cfl[idir]/2.0)
            #
            self.rhs.setValues(indL, indR+offR, self.cfl[idir]/2.0)
            self.rhs.setValues(indR, indL+offL, self.cfl[idir]/2.0)
        else:
            raise NotImplementedError



