'''
This file is an example of user parameter file for the FDM SOLVER
'''

# Input the characteristics of the grid
# - coordinates bounds, keys 'x', 'y' and 'z'
# - number of points per dimension, key 'npts'
grid_characteristics = {'x':[0.0, 1.0],
						'y':[0.0, 1.0],
						'npts':[100,100]}

# Input the names of the variables
# They will be considered in this order during execution
var_names = ['u',]

# Initial value of the different variables
initial_values = {'u':0.0}
# initial_values = {'u':'np.ones(x.size)*(x<=0.75)*(x>=0.25)*(y<=0.75)*(y>=0.25)'}
# initial_values = {'u':'exp(-0.5*(((x-0.5)/0.1)**2 + ((y-0.5)/0.1)**2))'}
# initial_values = {'u':'sin(2*x*pi)*sin(2*y*pi)'}

# Input the boundary conditions
# - boco locations, keys 'Amin' and 'Amax', for 'A' in 'x', 'y' or 'z'
# - boco values, keys are the variable names or first derivative
boco_characteristics = {'xmin':{'u':{'type':'dirichlet', 'value':0.1}},
						'xmax':{'u':{'type':'dirichlet', 'value':0.1}},
						'ymin':{'u':{'type':'dirichlet', 'value':1}},
						'ymax':{'u':{'type':'neumann', 'value':0}}}

# Input the equation to be solved
# - name can be 'transport', 'diffusion'
# - scheme can be 
# - - - - - - - - for 'transport': 'FTUCS', 'RK3UCS', 'RK4UCS', 'RK4BB_UCS', 'BTUCS', 'LW'
# - - - - - - - - for 'diffusion': 'FTCS', 'BTCS', 'BTCS4', CNCS'
# - - - - - - - - for 'advection-diffusion': 'FTCS', 'BTCS', CNCS', 'RK1CS', 'RK2CS', 'RK3CS', 'RK3K_CS', 'RK3BS_CS', 'RK4CS', 'RK4BB_CS', 'RK5F_CS'
equation = {'name':'diffusion',
			'parameters':2.0,
			'scheme':{'name':'BTCS'},
			'timestep':0.001,
			'niterations':100,
			'export_every':None,
			'plot_every':1}

