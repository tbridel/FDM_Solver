'''
This file contains the handler for all kind of arguments, whether command line
or from user parameters file
'''
__author__ = "Thibault Bridel-Bertomeu"
__license__ = "MIT"
__status__ = "Development"

# Import modules
# import imp
import sys
import utils
from utils import Print


class ArgParser(object):
    '''
    Base class 'ArgParser' made to handle the arguments throughout the program
    '''

    def __init__(self, comm=None):
        '''
        Initialization of the ArgParser class
        '''

        # Define the default name for the user parameter file
        self.filename = 'fdm_user_params.py'

        # Parse the command line to get some extra user flags
        if len(sys.argv) > 1:
            for arg in sys.argv[1:]:
                if '--argfile' in arg:
                    try:
                        argFile = arg.split('=')[1]
                    except IndexError:
                        Print("found option '--argfile' but cannot read the name of the file given", stype='warning', comm=comm)
                        Print("reverting to default %s parameter file" % self.filename, stype='warning', comm=comm)
                    else:
                        self.filename = argFile

        # Parse now the user parameter file
        self.parse_parameters(comm=comm)

    def parse_parameters(self, comm=None):
        '''
        Parser for the user parameter file written as a python module
        '''

        from imp import load_source

        # Load the pythonic module made by the user for their input
        self.uiname = 'userinput'
        self.userinput = load_source(self.uiname, self.filename)

        # Now save the inputs with adequate handles
        self.grid_characteristics = self.load_parameter('grid_characteristics', comm=comm)
        self.var_names = self.load_parameter('var_names', comm=comm)
        self.initial_values = self.load_parameter('initial_values', comm=comm)
        self.exact_solution = self.load_parameter('exact_solution', comm=comm)
        self.boco_characteristics = self.load_parameter('boco_characteristics', comm=comm)
        self.equation = self.load_parameter('equation', comm=comm)

    def load_parameter(self, parameter, comm=None):
        '''
        Utilitarian method to encapsulate a check 'hasattr' and the explicit loading of said parameter.
        In: modulename <string>
        In: module <module>
        In: parameter <string>
        Out: module.parameter <object>
        '''

        if hasattr(self.userinput, parameter):
            return eval('.'.join(('self', self.uiname, parameter)))
        else:
            Print('parameter %s not found in module %s. Aborting.', stype='error', comm=comm)
            exit()
