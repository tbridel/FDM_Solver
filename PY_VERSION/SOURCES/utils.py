'''
This file contains auxiliary tools and definitions used during the execution
of the FDM_SOLVER.
'''
__author__ = "Thibault Bridel-Bertomeu"
__license__ = "MIT"
__status__ = "Development"

# Import modules
from mpi4py import MPI
import numpy as np
import os
from petsc4py import PETSc
import sys

# Define global variables
COORDINATES = ['x', 'y', 'z']
DIM1 = 1
DIM2 = 2
DIM3 = 3

# Define a few console colors
WHITE = '\033[97m'
CYAN = '\033[96m'
PINK = '\033[95m'
BLUE = '\033[94m'
YELLOW = '\033[93m'
GREEN = '\033[92m'
RED = '\033[91m'
BLACK = '\033[90m'
BWHITE = '\033[107m'
BCYAN = '\033[106m'
BPINK = '\033[105m'
BBLUE = '\033[104m'
BYELLOW = '\033[103m'
BGREEN = '\033[102m'
BRED = '\033[101m'
BBLACK = '\033[100m'
BOLD = '\033[1m'
ITALIC = '\033[3m'
UNDERLINE = '\033[4m'
BLINK = '\033[5m'
ENDC = '\033[0m'

# Define the Runge-Kutta schemes Butcher tableaus, gamma coeffs list or alpha-beta coeffs list
rk_params = {'1':{'trueRK':True, 'steps':0, 'coeffs':[], 'sum_coeffs':[1.,], 'low-storage':False},
             '2':{'trueRK':True, 'steps':1, 'coeffs':[[0.5,],], 'sum_coeffs':[0., 1.], 'low-storage':False},
             '3':{'trueRK':True, 'steps':2, 'coeffs':[[2./3.,], [-1./3.,1.]], 'sum_coeffs':[1./4., 1./2., 1./4.], 'low-storage':False},
             '3K':{'trueRK':True, 'steps':2, 'coeffs':[[1./2.,], [-1.,2.]], 'sum_coeffs':[1./6., 2./3., 1./6.], 'low-storage':False},
             '3BS':{'trueRK':True, 'steps':2, 'coeffs':[[1./2.,], [0.,3./4.]], 'sum_coeffs':[2./9., 1./3., 4./9.], 'low-storage':False},
             '4':{'trueRK':True, 'steps':3, 'gammas':[1.0, 1./2., 1./6., 1./24.], 'coeffs':[[0.5,], [0.,0.5], [0.,0.,1.0]], 'sum_coeffs':[1./6., 1./3., 1./3., 1./6.], 'low-storage':False},
             '46NL':{'trueRK':True, 'steps':6, 
                    'alphas':[0.0, -0.737101392796, -1.634740794341, -0.744739003780, -1.469897351522, -2.813971388035], 
                    'betas':[0.032918605146, 0.823256998200, 0.381530948900, 0.200092213184, 1.718581042715, 0.27], 'low-storage':True},
             '4o6s':{'trueRK':True, 'steps':6, 'gammas':[1.0, 0.5, 0.165919771368, 0.040919732041, 0.007555704391, 0.000891421261], 'low-storage':False},
             '5F':{'trueRK':True, 'steps':5, 
                   'coeffs':[[0.25,], [3./32.,9./32.], [1932.0/2197.0,-7200.0/2197.0,7296.0/2197.0], [439.0/216.0,-8.0,3680.0/513.0,-845.0/4104.0], [-8.0/27.0,2.0,-3544.0/2565.0,1859.0/4104.0,-11.0/40.0]],
                   'sum_coeffs':[16.0/135.0,0,6656.0/12825.0,28561.0/56430.0,-9.0/50.0,2.0/55.0], 'low-storage':False}}

# Define the coefficients of the high-order schemes
der1_schemes = {'CSo11p':{'coeN':[-0.002484594688, 0.020779405824, -0.090320001280, 0.286511173973, -0.872756993962], 
                          'coeP':[0.872756993962, -0.286511173973, 0.090320001280, -0.020779405824, 0.002484594688]},
                'CSo13p':{'coeN':[0.001456501759, -0.011169294114, 0.045246480208, -0.133442885327, 0.337048393268, -0.907646591371],
                          'coeP':[0.907646591371, -0.337048393268, 0.133442885327, -0.045246480208, 0.011169294114, -0.001456501759]},
                'FD_0-10':[-2.391602219538, 5.832490322294, -7.650218001182+0.000000000001, 7.907810563576, -5.922599052629,  3.071037015445, -1.014956769726, 0.170022256519, 0.002819958377, -0.004791009708, -0.000013063429],
                'FD_1-9':[-0.180022054228, -1.237550583044, 2.484731692990, -1.810320814061, 1.112990048440, -0.481086916514, 0.126598690230, -0.015510730165, 0.000024609059-0.000003, 0.000156447571-0.000000000001, -0.000007390277],
                'FD_2-8':[0.057982271137, -0.536135360383, -0.264089548967+0.000000000002, 0.917445877606-0.000000000002, -0.169688364841, -0.029716326170, 0.029681617641, -0.005222483773, -0.000118806260, -0.000118806260, -0.000020069730],
                'FD_3-7':[-0.013277273810, 0.115976072920, -0.617479187931, -0.274113948206+0.000000000002, 1.086208764655-0.000000000002, -0.402951626982, 0.131066986242, -0.028154858354, 0.002596328316, 0.000128743150, 0.],
                'FD_4-6':[0.016756572303, -0.117478455239, 0.411034935097, -1.130286765151, 0.341435872100-0.000000000001, 0.556396830543, -0.082525734207, 0.003565834658, 0.001173034777, -0.000071772671+0.000000000064, -0.000000352273]}

# Define the coefficients of the high-order selective filters
filter_schemes = {'SFo11p':[0.215044884112, -0.187772883589, 0.123755948787, -0.059227575576, 0.018721609157, -0.002999540835],
                  'SFo13p':[0.190899511506, -0.171503832236, 0.123632891797, -0.069975429105, 0.029662754736, -0.008520738659, 0.001254597714],
                  'SF_0-10':[0.320882352941, -0.465, 0.179117647059, -0.035, 0., 0., 0., 0., 0., 0., 0.],
                  'SF_1-9':[-0.085777408970+0.000000000001, 0.277628171524, -0.356848072173, 0.223119093072, -0.057347064865, -0.000747264596, -0.000027453993, 0., 0., 0., 0.],
                  'SF_2-8':[0.0307159855992469, -0.148395705486028 , 0.312055385963757, -0.363202245195514, 0.230145457063431, -0.0412316564605079, -0.0531024700805787, 0.0494343261171287, -0.0198143585458560, 0.00339528102492129, 0.],
                  'SF_3-7':[-0.000054596010, 0.042124772446, -0.173103107841, 0.299615871352, -0.276543612935, 0.131223506571, -0.023424966418, 0.013937561779, -0.024565095706, 0.013098287852, -0.002308621090],
                  'SF_4-6':[0.008391235145, -0.047402506444, 0.121438547725, -0.200063042812, 0.240069047836, -0.207269200140-0.000000000001, 0.122263107844-0.000000000001, -0.047121062819, 0.009014891495, 0.001855812216, -0.001176830044]}

def say_hello(comm=None):
    '''
    Print a welcome message to the console
    '''
    if not comm.rank:
        print BOLD
        print 'FROM PYTHON V' + str(sys.version.split(' ')[0])
        print
        print 'WELCOME ON '
        print '    __________  __  ________       __               '
        print '   / ____/ __ \/  |/  / ___/____  / /   _____  _____'
        print '  / /_  / / / / /|_/ /\__ \/ __ \/ / | / / _ \/ ___/'
        print ' / __/ / /_/ / /  / /___/ / /_/ / /| |/ /  __/ /    '
        print '/_/   /_____/_/  /_//____/\____/_/ |___/\___/_/     '
        print ' '
        print 'A FINITE DIFFERENCE SOLVER ON IRREGULAR GRIDS'
        print 'FOR THE INCOMPRESSIBLE NAVIER-STOKES EQUATIONS'
        print ENDC


def say_goodbye(comm=None):
    '''
    Print a goodbye message to the console
    '''
    if not comm.rank:
        print ' '
        print BOLD + ' GOOD BYE ! ' + ENDC
        print ' '


def Print(str, stype=None, comm=None):
    '''
    Overload the printing capability of python to restrict printing
    to the master core of rank 0
    '''
    prefix = BOLD
    suffix = ENDC
    if stype == 'warning':
        prefix += BLUE + 'Warning: '
    elif stype == 'error':
        prefix += RED + 'Fatal error: '

    if comm is not None:
        if not comm.rank:
            print prefix + str + suffix
    else:
        print prefix + str + suffix


def file_present(file, text='', announce=False, comm=None):
    '''
    Function designed to handle file presence and raise an exception if 
    the file is absent
    '''
    if announce:
        print BLUE + BOLD + text + ENDC
    if not os.path.isfile(file):
        Print('file %s is missing.' % file,
              'error', comm)
        exit()


def get_FD_coeffs(stencil=[-1,0,1], nder=1, approxOrder=None, approxKind='centered'):
    '''
    Function yielding the coefficients for a finite approximation of the 
    derivative of order nder upon any kind of stencil
    Reference: http://web.media.mit.edu/~crtaylor/calculator.html
    '''

    # Restrict the useage to the first and second derivatives
    if nder > 2:
        raise NotImplementedError

    # If the user does not provide the stencil and the order, fail
    if stencil is None and approxOrder is None:
        raise KeyError

    # Import the factorial function for the RHS vector
    from scipy.special import factorial

    # If the user does not provide any stencil, default
    def augment_stencil(in_stencil):
        ''' Augment a stencil based on the kind of approximation '''
        if approxKind == 'centered':
            mini, maxi = in_stencil.min(), in_stencil.max()
            out_stencil = [mini-1,] + in_stencil.tolist() + [maxi+1,]
        elif approxKind == 'backward':
            mini = in_stencil.min()
            out_stencil = [mini-1,] + in_stencil.tolist()
        elif approxKind == 'forward':
            maxi = in_stencil.max()
            out_stencil = in_stencil.tolist() + [maxi+1,]
        else:
            raise NotImplementedError
        #
        return out_stencil

    defaultStencils = {'backward':{'1':[-1, 0], '2':[-2, -1, 0]},
                       'centered':{'1':[-1, 0, 1], '2':[-1, 0, 1]},
                       'forward':{'1':[0, 1], '2':[0, 1, 2]}}
    if stencil is None:
        stencil = defaultStencils[approxKind][str(nder)]

    while True:
        # Get the size of the stencil
        stencil = np.asarray(stencil)
        N = len(stencil)

        # The matrix to be inverted has size NxN
        powers = np.arange(N)
        coeffmat = stencil[None, :]**powers[:, None]

        # The vector has only one non-zero element
        rhsvect = np.zeros(N)
        rhsvect[int(nder)] = factorial(int(nder))*1.0

        # Get the FD coeffs
        coeffs = np.dot(np.linalg.inv(coeffmat), rhsvect)

        # Get the order of the approximation (check up to 20 by default)
        powers = np.arange(max(20, approxOrder, len(stencil)))
        matrix = stencil[None, :]**powers[:, None] / factorial(powers)[:, None] * coeffs[None, :]
        summed = np.sum(matrix, axis=1)
        nonzeros = np.where(~np.isclose(np.fabs(summed[nder+1:]),0))[0]
        order = (nder+1+nonzeros[0])-nder

        # Get out of the loop conditionally
        if approxOrder is None:
            break
        elif approxOrder is not None and order >= approxOrder:
            break
        else:
            stencil = augment_stencil(stencil)

    # Return the set of FD coeffs and the order of the approximation
    return stencil, coeffs, order

