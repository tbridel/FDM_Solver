'''
This file contains the class handling the solver for the advection-diffusion equation
'''
__author__ = "Thibault Bridel-Bertomeu"
__license__ = "MIT"
__status__ = "Development"

# Import modules
from c_fdmsolver import Solver
import numpy as np
from petsc4py import PETSc
import petsc_utils as pu
import utils
from utils import Print

class AdvectionDiffusionSolver(Solver):
    '''
    Derived class 'AdvectionDiffusionSolver' to solve the diffusion equation
    Schemes available: 'FTCS', 'BTCS', 'CNCS', 'RK1CS', 'RK2CS', 'RK3CS', 'RK3K_CS', 'RK3BS_CS', 'RK4CS', 'RK46NL_CS', 'RK5F_CS'
    '''

    def __init__(self, equation, grid, comm=None):
        '''
        Initialization of the class for the diffusion equation solver
        '''

        if grid.ndim > 2:
            raise NotImplementedError

        # Initialize the parent class
        Solver.__init__(self, equation, comm=comm)

        # Check the user parameters for safety
        if len(equation['parameters']) < grid.ndim+1:
            Print('for a %dD advection-diffusion equation, %d transport velocities plus 1 conductivity are required as parameters. Aborting.' % (grid.ndim, grid.ndim),
                  stype='error', comm=comm)
            exit()
        if len(equation['parameters']) > grid.ndim+1:
            Print('the parameters list is too long for a %dD advection-diffusion equation. The first %d will be considered as velocity.ies. and the %d+1-nth as conductivity' % (grid.ndim, grid.ndim, grid.ndim),
                  stype='warning', comm=comm)

        # Get the transport velocity from the user inputs
        self.transvel = equation['parameters'][:grid.ndim]
        self.transvel = np.concatenate((self.transvel, np.zeros(3-grid.ndim)))

        # Get the conductivity from the user inputs
        self.conductivity = equation['parameters'][grid.ndim]
        if self.conductivity < 0.0:
            Print('a positive conductivity is required for the diffusion term. Aborting.', stype='error', comm=comm)
            exit()

        # Initialize constants to be used in the schemes matrices definition
        self.cflA = np.zeros(3)
        self.cflA[:grid.ndim] = np.array(self.transvel[:grid.ndim]) * self._timestep / np.array(grid.steps)
        #
        self.cflD = np.zeros(3)
        self.cflD[:grid.ndim] = self.conductivity * self._timestep / np.array(grid.steps)**2

    @property
    def tv(self):
        ''' Transport velocity(ies) '''
        return self.transvel

    @property
    def cond(self):
        ''' Conductivity for the diffusion term '''
        return self.conductivity

    def get_lhs(self, grid, comm=None):
        '''
        Fill the left hand side matrix depending on the numerical scheme
        Multiply the vector at time (n+1)
        '''

        # Prepare LHS matrix
        self.lhs = pu.Matrix(grid.invtx, comm=comm)

        # Conditional on equation['scheme']
        if self.scheme['name'] in ['FTCS', 'RK1CS', 'RK2CS', 
                                   'RK3CS', 'RK3K_CS', 'RK3BS_CS', 
                                   'RK4CS', 'RK46NL_CS', 'RK5F_CS']:
            self.lhs.to_eye(assemble=False)
        elif self.scheme['name'] == 'BTCS':
            self.lhs.setDiagonal([-grid.nnx, -1, 0, 1, grid.nnx],
                                 [-self.cflD[1]-self.cflA[1]/2.0, 
                                  -self.cflD[0]-self.cflA[0]/2.0, 
                                  1+2*(self.cflD[0]+self.cflD[1]), 
                                  -self.cflD[0]+self.cflA[0]/2.0,
                                  -self.cflD[1]+self.cflA[1]/2.0],
                                 safety={'b':grid.ndim>1, 'nx':grid.nnx, 'ny':grid.nny if grid.ndim>1 else None})            
        elif self.scheme['name'] == 'CNCS':
            self.lhs.setDiagonal([-grid.nnx, -1, 0, 1, grid.nnx],
                                 [-self.cflA[1]/4.0-self.cflD[1]/2.0, -self.cflA[0]/4.0-self.cflD[0]/2.0, 
                                  1.0+(self.cflD[0]+self.cflD[1]), 
                                  self.cflA[0]/4.0-self.cflD[0]/2.0, self.cflA[1]/4.0-self.cflD[1]/2.0],
                                 safety={'b':grid.ndim>1, 'nx':grid.nnx, 'ny':grid.nny if grid.ndim>1 else None})
        else:
            raise NotImplementedError        

    def get_rhs(self, grid, comm=None):
        '''
        Fill the right hand side matrix depending on the numerical scheme
        Multiply the vector at time (n)
        '''

        # Prepare and identity matrix
        identity = pu.Matrix(grid.invtx, comm=comm)
        identity.to_eye(assemble=True)

        # Prepare RHS matrix
        self.rhs = pu.Matrix(grid.invtx, comm=comm)

        # Conditional on equation['scheme']
        if self.scheme['name'] == 'FTCS':
            self.rhs.setDiagonal([-grid.nnx, -1, 0, 1, grid.nnx],
                                 [self.cflD[1]+self.cflA[1]/2.0, 
                                  self.cflD[0]+self.cflA[0]/2.0, 
                                  1-2*(self.cflD[0]+self.cflD[1]), 
                                  self.cflD[0]-self.cflA[0]/2.0,
                                  self.cflD[1]-self.cflA[1]/2.0],
                                 safety={'b':grid.ndim>1, 'nx':grid.nnx, 'ny':grid.nny if grid.ndim>1 else None})
        elif self.scheme['name'] == 'BTCS':
            self.rhs.to_eye(assemble=False)
        elif self.scheme['name'] == 'CNCS':
            self.lhs.assemble()
            self.rhs.assemble()
            self.rhs.getMatrix.axpy(-1.0, self.lhs.getMatrix)
            self.rhs.getMatrix.axpy(2.0, identity.getMatrix)
        elif self.scheme['name'] in ['RK1CS', 'RK2CS', 'RK3CS', 'RK3K_CS', 
                                     'RK3BS_CS', 'RK4CS', 'RK46NL_CS', 'RK5F_CS']:
            self.rhs.setDiagonal([-grid.nnx, -1, 0, 1, grid.nnx],
                                 [self.cflD[1]+self.cflA[1]/2.0, 
                                  self.cflD[0]+self.cflA[0]/2.0, 
                                  -2*(self.cflD[0]+self.cflD[1]), 
                                  self.cflD[0]-self.cflA[0]/2.0,
                                  self.cflD[1]-self.cflA[1]/2.0],
                                 safety={'b':grid.ndim>1, 'nx':grid.nnx, 'ny':grid.nny if grid.ndim>1 else None})
        else:
            raise NotImplementedError

    def set_dirichlet(self, grid, indices, offset, c_value):
        '''
        Set the lhs and rhs matrices for a dirichlet boundary condition
        Coherence in the order of the scheme is implemented
        '''

        # Associate the direction to the index of tv and cfl
        idir = 0 if abs(offset) == 1 else 1

        # Conditional on equation['scheme']
        if self.scheme['name'] in ['CNCS', 'BTCS', 'FTCS', 'RK1CS', 'RK2CS', 'RK3CS',
                                   'RK3K_CS', 'RK3BS_CS', 'RK4CS',
                                   'RK46NL_CS', 'RK5F_CS']:
            for index in indices:
                if offset > 0:
                    self.bocosValues.setValue(index, (self.cflD[idir]+self.cflA[idir]/2.0)*c_value)
                else:
                    self.bocosValues.setValue(index, (self.cflD[idir]-self.cflA[idir]/2.0)*c_value)
        else:
            raise NotImplementedError

    def set_neumann(self, grid, indices, offset, factor, c_value, bocoVec=None):
        '''
        Set the lhs and rhs matrices for a neumann boundary condition
        Coherence in the order of the scheme is implemented
        '''

        if abs(c_value) > 0:
            raise NotImplementedError

        # Associate the direction to the index of tv and cfl
        idir = 0 if abs(offset) == 1 else 1

        # Conditional on equation['scheme']    
        if self.scheme['name'] in ['FTCS', 'RK1CS', 'RK2CS', 'RK3CS',
                                   'RK3K_CS', 'RK3BS_CS', 'RK4CS',
                                   'RK46NL_CS', 'RK5F_CS']:
            self.rhs.setValues(indices, indices+offset, 0.5*self.cflA[idir]*np.sign(offset)+self.cflD[idir], mode=PETSc.InsertMode.ADD_VALUES)
        elif self.scheme['name'] == 'BTCS':
            self.lhs.setValues(indices, indices+offset, -0.5*self.cflA[idir]*np.sign(offset)-self.cflD[idir], mode=PETSc.InsertMode.ADD_VALUES)         
        elif self.scheme['name'] == 'CNCS':
            self.lhs.setValues(indices, indices+offset, -0.25*self.cflA[idir]*np.sign(offset)-0.5*self.cflD[idir], mode=PETSc.InsertMode.ADD_VALUES)         
            self.rhs.setValues(indices, indices+offset, 0.25*self.cflA[idir]*np.sign(offset)+0.5*self.cflD[idir], mode=PETSc.InsertMode.ADD_VALUES)
        else:
            raise NotImplementedError

    def set_open(self, grid, indices, offset):
        '''
        Set the lhs and rhs matrices for an open boundary condition
        Coherence in the order of the scheme is implemented
        '''

        # Associate the direction to the index of tv and cfl
        idir = 0 if abs(offset) == 1 else 1

        # Get the other offset to re-establish the proper scheme in the normal direction
        other_offset = grid.nnx if abs(offset) == 1 else 1

        # Condition on equation['scheme']
        if self.scheme['name'] in ['FTCS', 'RK1CS', 'RK2CS', 'RK3CS',
                                   'RK3K_CS', 'RK3BS_CS', 'RK4CS',
                                   'RK46NL_CS', 'RK5F_CS']:
            # Remove the actual scheme
            self.rhs.setValues(indices, indices, 2.0*self.cflD[idir], mode=PETSc.InsertMode.ADD_VALUES)
            # Introduce the up/down-wind scheme
            self.rhs.setValues(indices, indices, self.cflD[idir]+self.cflA[idir]*np.sign(offset), mode=PETSc.InsertMode.ADD_VALUES)
            self.rhs.setValues(indices, indices+offset, -2.0*self.cflD[idir]-self.cflA[idir]*np.sign(offset), mode=PETSc.InsertMode.INSERT)
            self.rhs.setValues(indices, indices+2*offset, self.cflD[idir], mode=PETSc.InsertMode.INSERT)
        elif self.scheme['name'] == 'BTCS':
            # Remove the actual scheme
            self.lhs.setValues(indices, indices, -2.0*self.cflD[idir], mode=PETSc.InsertMode.ADD_VALUES)
            # Introduce the up/down-wind scheme
            self.lhs.setValues(indices, indices, -self.cflD[idir]-self.cflA[idir]*np.sign(offset), mode=PETSc.InsertMode.ADD_VALUES)
            self.lhs.setValues(indices, indices+offset, 2.0*self.cflD[idir]+self.cflA[idir]*np.sign(offset), mode=PETSc.InsertMode.INSERT)
            self.lhs.setValues(indices, indices+2*offset, -self.cflD[idir], mode=PETSc.InsertMode.INSERT)
        elif self.scheme['name'] == 'CNCS':
            # Remove the actual scheme
            self.rhs.setValues(indices, indices, self.cflD[idir], mode=PETSc.InsertMode.ADD_VALUES)
            self.lhs.setValues(indices, indices, -self.cflD[idir], mode=PETSc.InsertMode.ADD_VALUES)
            # Introduce the up/down-wind scheme
            self.lhs.setValues(indices, indices, -0.5*self.cflD[idir]-0.5*self.cflA[idir]*np.sign(offset), mode=PETSc.InsertMode.ADD_VALUES)
            self.lhs.setValues(indices, indices+offset, self.cflD[idir]+0.5*self.cflA[idir]*np.sign(offset), mode=PETSc.InsertMode.INSERT)
            self.lhs.setValues(indices, indices+2*offset, -0.5*self.cflD[idir], mode=PETSc.InsertMode.INSERT)
            self.rhs.setValues(indices, indices, 0.5*self.cflD[idir]+0.5*self.cflA[idir]*np.sign(offset), mode=PETSc.InsertMode.ADD_VALUES)
            self.rhs.setValues(indices, indices+offset, -self.cflD[idir]-0.5*self.cflA[idir]*np.sign(offset), mode=PETSc.InsertMode.INSERT)
            self.rhs.setValues(indices, indices+2*offset, 0.5*self.cflD[idir], mode=PETSc.InsertMode.INSERT)
        else:
            raise NotImplementedError

    def set_periodic(self, grid, direction=None, override=True):
        ''' 
        Set the lhs and the rhs matrices for periodicity
        Depends on the scheme
        '''

        # Some handles
        indL, indR = np.array(grid.bocos[direction+'min']), np.array(grid.bocos[direction+'max'])

        # Associate the direction to the index of tv and cfl
        idir = 0 if direction == 'x' else 1
        key2offset = {'xmin':1, 'xmax':-1, 'ymin':grid.nnx, 'ymax':-grid.nnx}
        offL, offR = key2offset[direction+'min'], key2offset[direction+'max']

        # Conditional on equation['scheme']
        if self.scheme['name'] in ['FTCS', 'RK1CS', 'RK2CS', 
                                   'RK3CS', 'RK3K_CS', 'RK3BS_CS', 
                                   'RK4CS', 'RK46NL_CS', 'RK5F_CS']:
            self.rhs.setValues(indL, indR+offR, self.cflD[idir]+self.cflA[idir]/2.0)
            self.rhs.setValues(indR, indL+offL, self.cflD[idir]-self.cflA[idir]/2.0)
        elif self.scheme['name'] == 'BTCS':
            self.lhs.setValues(indL, indR+offR, -self.cflD[idir]-self.cflA[idir]/2.0)
            self.lhs.setValues(indR, indL+offL, -self.cflD[idir]+self.cflA[idir]/2.0)            
        elif self.scheme['name'] == 'CNCS':
            self.lhs.setValues(indL, indR+offR, -self.cflA[idir]/4.0-self.cflD[idir]/2.0)
            self.lhs.setValues(indR, indL+offL, self.cflA[idir]/4.0-self.cflD[idir]/2.0)
            #
            self.rhs.setValues(indL, indR+offR, self.cflA[idir]/4.0+self.cflD[idir]/2.0)
            self.rhs.setValues(indR, indL+offL, -self.cflA[idir]/4.0+self.cflD[idir]/2.0)            
        else:
            raise NotImplementedError        
                                

