'''
This file contains the class handling the mesh throughout the execution of the
FDM_SOLVER program
'''
__author__ = "Thibault Bridel-Bertomeu"
__license__ = "MIT"
__status__ = "Development"

# Import modules
import copy
import meshio
import numpy as np
from numpy import pi
import petsc_utils as pu
import utils
from utils import Print, COORDINATES, DIM3, DIM2, DIM1


def getGrid(characteristics, bocos, comm=None):
    '''
    Return the right grid instance based on dimensionality
    '''

    # Estimate how many coordinates the mesh has
    hasCoords = [coord in characteristics.keys() for coord in COORDINATES]
    ndim = sum(hasCoords)

    # Generate the appropriate grid
    if ndim == 1:
        return Grid1D(characteristics, bocos, comm=comm)
    elif ndim == 2:
        return Grid2D(characteristics, bocos, comm=comm)
    elif ndim == 3:
        raise NotImplementedError()
    else:
        Print('wrong number of coordinates. Aborting.', stype='error', comm=comm)
        exit()


class Grid(object):
    '''
    Base class 'Grid' made to handle the grids for the FDM solver
    Contains information on the vertices coordinates, the connectivity,
    and the BoCo & interior points.
    '''

    def __init__(self, ndim, comm=None):
        '''
        Initialization of the base class 'Grid'
        '''

        # Attribute de dimension
        self._nDimensions = ndim

    @property
    def ndim(self):
        ''' Dimensionality of the mesh '''
        return self._nDimensions

    @property
    def bocos(self):
        ''' Boundary conditions indices '''
        return self._boundaryIdx

    @property
    def xmin(self):
        ''' Lower bound in X direction '''
        return self._xmin

    @property
    def xmax(self):
        ''' Upper bound in X direction '''
        return self._xmax

    @property
    def nx(self):
        ''' Number of points in X direction '''
        return self._nx

    @property
    def dx(self):
        ''' Grid spacing in X direction '''
        return self._dx


class Grid1D(Grid):
    '''
    Derived class 'Grid1D' made to handle one-dimensonal grids
    for the FDM solver
    '''

    def __init__(self, characteristics, bocos, comm=None):
        '''
        Initialization of the derived class 'Grid1'
        '''

        # Initialize the parent
        Grid.__init__(self, DIM1, comm=comm)

        # Get mesh number of points, bounds and spacing
        self._nx = characteristics['npts'][0]
        self._xmin, self._xmax = characteristics['x']
        self._dx = (self._xmax - self._xmin) / (self._nx-1)

        # Get the indices of the boundary conditions
        self._boundaryIdx = {}
        for var in bocos[bocos.keys()[0]].keys():
            self._boundaryIdx[var] = {'xmin':np.array([0,]), 'xmax':np.array([self._nx-1,])}
        self._oldBoundaryIdx = copy.deepcopy(self._boundaryIdx)

        # Parse the boundary conditions, see if there are any Dirichlet type to restrict the matrix
        # The number of inner vertices (i.e. to be solved for) is total minus all the Dirichlet nodes
        self._innerVertices = {}
        for var in bocos[bocos.keys()[0]].keys():
            self._innerVertices[var] = self._nx
        self._isDirichlet = {'numbers':0}
        for key in bocos:
            boundary = bocos[key]
            self._isDirichlet[key] = {}
            for var in boundary:
                self._isDirichlet[key][var] = boundary[var]['type'] == 'dirichlet'
                self._isDirichlet['numbers'] += self._isDirichlet[key][var]
                if self._isDirichlet[key][var]:
                    self._isDirichlet[key]['value'] = boundary[var]['value']
                    self._innerVertices[var] -= len(self._boundaryIdx[var][key])
                    self._boundaryIdx[var]['xmax'] -= 1

    @property
    def nvtx(self):
        ''' Number of vertices '''
        return self.nx

    @property
    def invtx(self):
        ''' Number of inner vertices '''
        return self._innerVertices

    @property
    def nnx(self):
        ''' New number of points in the X direction '''
        return self._innerVertices

    @property
    def ncell(self):
        ''' Number of cells '''
        return self.nx-1

    @property
    def steps(self):
        ''' Steps in all the dimensions '''
        return [self._dx, ]

    def initialize_solution(self, var_names, initial_values, comm=None):
        '''
        This function initializes the solution vector from the user input
        '''

        # Extra imports from numpy to handle the 'eval' later
        # And define locally the space vector
        from numpy import sin, cos, pi, exp, log
        x = np.linspace(self.xmin, self.xmax, self.nx)

        # Save the variable names locally for the export
        self.varnames = var_names

        # Read the user expressions and evaluate them
        iv = {}
        for idx in xrange(len(var_names)):
            c_varname = self.varnames[idx]
            #
            iv[c_varname] = eval(str(initial_values[c_varname]))
            if type(iv[c_varname]) is not np.ndarray:
                iv[c_varname] = np.ones(x.size) * iv[c_varname]

        # The mask to apply onto iv depends on the dirichlet nodes
        ivmask = {}
        for idx in xrange(len(var_names)):
            c_varname = self.varnames[idx]
            ivmask[c_varname] = np.ones(x.size, dtype=bool)
            for key in self._isDirichlet:
                if key == 'numbers':
                    continue
                if self._isDirichlet[key][c_varname]:
                    ivmask[c_varname][self._boundaryIdx[c_varname][key]] = False
                    iv[c_varname][self._oldBoundaryIdx[c_varname][key]] = self._isDirichlet[key]['value']

        # Prepare a PETSc vector
        self.unknowns = pu.Vector(sum([self.invtx[var] for var in self.invtx.keys()])) #self.invtx * len(var_names))

        # Fill it with the initialization demanded by the user
        for idx in xrange(len(var_names)):
            c_varname = self.varnames[idx]
            #
            for jdx in xrange(self.invtx[c_varname]):
                self.unknowns.setValue(jdx+idx*self.invtx[self.varnames[idx-1]], iv[c_varname][ivmask[c_varname]][jdx])

        # Then finally assemble it for later use
        self.unknowns.assemble()

    def preplot(self, comm=None):
        '''
        This function aims at putting back together the solution before plotting it, 
        in case there are any Dirichlet points to consider
        '''

        # Gather the pieces of distributed vector on a single processor
        vecuk = None
        uk = self.unknowns.local2global(comm=comm)

        # On said processor (rank 0), decompose per variable and add the boundary conditions
        if not comm.Get_rank():
            vecuk = [uk[idx*self.invtx[self.varnames[idx-1]]:(idx+1)*self.invtx[self.varnames[idx]]] for idx in range(len(self.varnames))]

            # Now if the number of inner nodes is less than the total, concatenate
            for idx in xrange(len(self.varnames)):
                if self.invtx[self.varnames[idx]] < self.nvtx:
                    c_varname = self.varnames[idx]
                    if self._isDirichlet['xmin'][c_varname]:
                        vecuk[idx] = np.concatenate(([self._isDirichlet['xmin']['value'],], vecuk[idx]))
                    if self._isDirichlet['xmax'][c_varname]:
                        vecuk[idx] = np.concatenate((vecuk[idx], [self._isDirichlet['xmax']['value'],]))

            # This function returns, for easier further use in a plotting method
            return vecuk

    def export_solution(self, step, comm=None):
        '''
        Export a 1D solution to a csv file
        '''

        Print("Writing solution sol_%d.csv ..." %step, stype=None, comm=comm)

        # Gather the pieces of distributed vector on a single processor
        vecuk = self.preplot(comm=comm)

        # On said processor (rank 0), prepare the data with coordinates and write
        if not comm.Get_rank():
            # Put the coordinates and unknowns in the same array
            c_tuple = [np.linspace(self.xmin, self.xmax, self.nx),] + [np.real(tab) for tab in vecuk]
            dataArray = np.vstack(c_tuple)
            #
            np.savetxt('sol_%06d.csv'%step, dataArray.T, fmt='%.12e', delimiter=',')

        # Every process should wait for completion
        comm.Barrier()

class Grid2D(Grid):
    '''
    Derived class 'Grid2D' made to handle two-dimensonal grids
    for the FDM solver
    '''

    def __init__(self, characteristics, bocos, comm=None):
        '''
        Initialization of the derived class 'Grid2D'
        '''

        # Initialize the parent
        Grid.__init__(self, DIM2, comm=comm)

        # Get mesh number of points, bounds and spacing
        self._nx, self._ny = characteristics['npts']
        #
        self._xmin, self._xmax = characteristics['x']
        self._ymin, self._ymax = characteristics['y']
        #
        self._dx = (self._xmax - self._xmin) / (self._nx-1)
        self._dy = (self._ymax - self._ymin) / (self._ny-1)

        # Get the indices of the boundary conditions
        self._boundaryIdx = {'xmin':np.arange(self._ny)*self._nx,
                             'xmax':np.arange(self._ny)*self._nx+(self._nx-1),
                             'ymin':np.arange(self._nx),
                             'ymax':(self._ny-1)*self._nx+np.arange(self._nx)}

        # Parse the boundary conditions, see if there are any Dirichlet type to restrict the matrix
        # The number of inner vertices (i.e. to be solved for) is total minus all the Dirichlet nodes
        self._innerVertices = self._nx * self._ny
        self._isDirichlet = {'numbers':0}
        self._nx_, self._ny_ = self._nx, self._ny
        for key in bocos:
            direction = key[0]
            boundary = bocos[key]
            self._isDirichlet[key] = {}
            for var in boundary:
                self._isDirichlet[key][var] = boundary[var]['type'] == 'dirichlet'
                self._isDirichlet['numbers'] += self._isDirichlet[key][var]
                if self._isDirichlet[key][var]:
                    self._isDirichlet[key]['value'] = boundary[var]['value']
                    if direction == 'x':
                        self._nx_ -= 1
                    else:
                        self._ny_ -= 1

        # Correct the indices of the boundary conditions if necessary
        if self._nx_ < self._nx or self._ny_ < self._ny:
            self._oldBoundaryIdx = copy.copy(self._boundaryIdx)
            self._boundaryIdx = {'xmin':np.arange(self._ny_)*self._nx_,
                                 'xmax':np.arange(self._ny_)*self._nx_+(self._nx_-1),
                                 'ymin':np.arange(self._nx_),
                                 'ymax':(self._ny_-1)*self._nx_+np.arange(self._nx_)}
            self._innerVertices = self._nx_ * self._ny_

    @property
    def ymin(self):
        ''' Lower bound in Y direction '''
        return self._ymin

    @property
    def ymax(self):
        ''' Upper bound in Y direction '''
        return self._ymax

    @property
    def ny(self):
        ''' Number of points in Y direction '''
        return self._ny

    @property
    def dy(self):
        ''' Grid spacing in Y direction '''
        return self._dy

    @property
    def nvtx(self):
        ''' Number of vertices '''
        return self._nx * self._ny

    @property
    def invtx(self):
        ''' Number of inner vertices '''
        return self._innerVertices

    @property
    def nnx(self):
        ''' New number of points in the x direction '''
        return self._nx_

    @property
    def nny(self):
        ''' New number of points in the y direction '''
        return self._ny_

    @property
    def ncell(self):
        ''' Number of cells '''
        return (self._nx-1)*(self._ny-1)

    @property
    def steps(self):
        ''' Steps in all the dimensions '''
        return [self._dx, self._dy]        

    def initialize_solution(self, var_names, initial_values, comm=None):
        '''
        This function initializes the solution vector from the user input
        '''

        # Extra imports from numpy to handle the 'eval' later
        # And define locally the space vector
        from numpy import sin, cos, pi, exp, log
        xx, yy = np.meshgrid(np.linspace(self.xmin, self.xmax, self.nx),
                             np.linspace(self.ymin, self.ymax, self.ny))
        x = xx.ravel()
        y = yy.ravel()
        del xx, yy

        # Save the variable names locally for the export
        self.varnames = var_names

        # Read the user expressions and evaluate them
        iv = {}
        for idx in xrange(len(var_names)):
            c_varname = self.varnames[idx]
            #
            iv[c_varname] = eval(str(initial_values[c_varname]))
            if type(iv[c_varname]) is not np.ndarray:
                iv[c_varname] = np.ones(self.nvtx) * iv[c_varname]

        # The mask to apply onto iv depends on the dirichlet nodes
        self._ivmask = {}
        for idx in xrange(len(var_names)):
            c_varname = self.varnames[idx]
            self._ivmask[c_varname] = np.ones(x.size, dtype=bool)
            for key in self._isDirichlet:
                if key == 'numbers':
                    continue
                if self._isDirichlet[key][c_varname]:
                    self._ivmask[c_varname][self._oldBoundaryIdx[key]] = False
                    iv[c_varname][self._oldBoundaryIdx[key]] = self._isDirichlet[key]['value']

        # Prepare a PETSc vector
        self.unknowns = pu.Vector(self.invtx * len(var_names))

        # Fill it with the initialization demanded by the user
        for idx in xrange(len(var_names)):
            c_varname = self.varnames[idx]
            #
            for jdx in xrange(self.invtx):
                self.unknowns.setValue(jdx+idx*self.invtx, iv[c_varname][self._ivmask[c_varname]][jdx])

        # Then finally assemble it for later use
        self.unknowns.assemble()

    def preplot(self, comm=None):
        '''
        This function aims at putting back together the solution before plotting it, 
        in case there are any Dirichlet points to consider
        '''

        # Gather the pieces of distributed vector on a single processor
        vecuk = None
        uk = self.unknowns.local2global(comm=comm)

        # On said processor (rank 0), decompose per variable and add the boundary conditions
        if not comm.Get_rank():
            vecuk = [uk[idx*self.invtx:(idx+1)*self.invtx] for idx in range(len(self.varnames))]

            # Now if the number of inner nodes is less than the total, concatenate
            if self.invtx < self.nvtx:
                truevecuk = [np.zeros(self.nvtx) for idx in range(len(vecuk))]
                for idx in range(len(vecuk)):
                    truevecuk[idx][self._ivmask[self.varnames[idx]]] = vecuk[idx]
                #
                for idx in xrange(len(self.varnames)):
                    c_varname = self.varnames[idx]
                    for key in self._oldBoundaryIdx:
                        if self._isDirichlet[key][c_varname]:
                            truevecuk[idx][self._oldBoundaryIdx[key]] = self._isDirichlet[key]['value']
                #
                truevecuk = [tab.reshape((self._ny, self._nx)) for tab in truevecuk]
                return truevecuk
            else:
                vecuk = [tab.reshape((self._ny, self._nx)) for tab in vecuk]
                return vecuk

    def export_solution(self, step, comm=None):
        '''
        This function exports a 2D solution as a vtk file
        '''

        Print("Writing solution sol_%d.vtk ..." %step, stype=None, comm=comm)

        # Gather the pieces of distributed vector on a single processor
        vecuk = self.preplot(comm=comm)

        # On said processor (rank 0), prepare the data with coordinates and write
        if not comm.Get_rank():
            # First generate the list of points properly
            xx, yy =  np.meshgrid(np.linspace(self.xmin, self.xmax, self.nx),
                                  np.linspace(self.ymin, self.ymax, self.ny))
            points = np.vstack((xx.ravel(), yy.ravel(), np.zeros(yy.ravel().size))).T
            del xx, yy

            # Then generate the cells, given they are quads and the mesh is regular
            quadco = np.zeros((self.ncell, 4), dtype=int)
            counter = -1
            for idx in xrange(self.nx - 1):
                for jdx in xrange(self.ny - 1):
                    counter += 1
                    pnt = idx + self.nx * jdx
                    quadco[counter, :] = np.array([pnt,
                                                   pnt + 1,
                                                   pnt + 1 + self.nx,
                                                   pnt + self.nx])
            cells = {'quad': quadco}

            # Store the point data as a dictionary
            point_data = {}
            for idx, varname in enumerate(self.varnames):
                point_data[varname] = np.real(vecuk[idx]).ravel()

            # Finally write the solution out
            meshio.write('sol_%06d.vtk'%step, points, cells, point_data=point_data)

        # Every process should wait for completion
        comm.Barrier()