'''
This file contains the class handling the solver for the linear acoustics equation
'''
__author__ = "Thibault Bridel-Bertomeu"
__license__ = "MIT"
__status__ = "Development"

# Import modules
from c_fdmsolver import Solver
from c_transportsolver import TransportSolver
import numpy as np
import petsc_utils as pu

class LinearAcousticSolver(Solver):
    '''
    Derived class 'LinearAcousticSolver' to solve the equation for linear acoustics {du/dt + grad(P) = 0, dP/dt + div(u) = 0}
    Schemes available: 
    '''

    def __init__(self, equation, grid, comm=None):
        '''
        Initialization of the class for the linear acoustics equation solver
        '''

        if grid.ndim > 2:
            raise NotImplementedError

        # Fake an advection velocity so that the transportsolver computes the right CFL
        equation['parameters'] = [1.0, ]

        # Initialize the parent class
        Solver.__init__(self, equation, comm=comm)

        # Get two sub-solvers for P and U
        self.tsP = TransportSolver(equation, grid, comm=comm)
        self.tsU = TransportSolver(equation, grid, comm=comm)

    def get_lhs(self, grid, comm=None, var=None):
        '''
        Fill the left hand side matrix depending on the numerical scheme
        Multiply the vector at time (n+1)
        '''

        # Call the method from the transport solvers
        self.tsP.get_lhs(grid, comm=comm, var=['p',])
        self.tsU.get_lhs(grid, comm=comm, var=['u',])

    def get_rhs(self, grid, comm=None, var=None):
        '''
        Fill the right hand side matrix depending on the numerical scheme
        Multiply the vector at time (n)
        '''

        # Call the method from the transport solvers
        self.tsP.get_rhs(grid, comm=comm, var=['p',])
        self.tsU.get_rhs(grid, comm=comm, var=['u',])

    def create_filter(self, grid, comm=None, var=None):
        '''
        Create the filtering matrix if a filter has been defined in the user parameters file
        Call it for both transport subsolvers so they know about the filterMat attribute
        later in the apply_bocos step
        '''

        # Call the method from the transport solvers
        self.tsP.create_filter(grid, comm=comm, var=['p',])
        self.tsU.create_filter(grid, comm=comm, var=['u',])

    def apply_bocos(self, var_names, boco_characteristics, grid, comm=None):
        '''
        Overload the method of the parent class to distribute the right 
        boundary conditions to the two transport subsolvers depending
        on the variable.
        '''

        # For variable P, 
        self.tsP.apply_bocos(['p',], boco_characteristics, grid, comm=comm)

        # For variable U,
        self.tsU.apply_bocos(['u',], boco_characteristics, grid, comm=comm)

    def execute(self, grid, es=None, comm=None):
        '''
        Overload the method of the parent class to take time and assemble
        the blocks from the two transport subsolvers
        '''

        # Assemble LHS first
        # Get the values and indices of the two LHS from the transport subsolvers
        (aiP, ajP, avP) = self.tsP.lhs.getMatrix.getValuesCSR()
        del self.tsP.lhs
        (aiU, ajU, avU) = self.tsU.lhs.getMatrix.getValuesCSR()
        del self.tsU.lhs
        # And concatenate them, since LHS will be diagonal blocked
        ai = np.concatenate((aiU, aiP[1:]+aiU[-1]))
        aj = np.concatenate((ajU, grid.invtx['u']+ajP))
        av = np.concatenate((avU, avP))
        del aiP, ajP, avP, aiU, ajU, avU
        # Make a matrix double the size
        self.lhs = pu.Matrix(sum([grid.invtx[var] for var in grid.varnames]), comm=comm)
        # And copy the content of the previous matrices
        self.lhs.getMatrix.setValuesCSR(ai, aj, av)
        self.lhs.assemble()

        # Assemble RHS then
        # Get the values and indices of the two LHS from the transport subsolvers
        (aiP, ajP, avP) = self.tsP.rhs.getMatrix.getValuesCSR()
        del self.tsP.rhs
        (aiU, ajU, avU) = self.tsU.rhs.getMatrix.getValuesCSR()
        del self.tsU.rhs
        # And concatenate them, since RHS will be diagonal blocked
        ai = np.concatenate((aiU, aiP[1:]+aiU[-1]))
        aj = np.concatenate((ajU+grid.invtx['u'], ajP))
        av = np.concatenate((avU, avP))
        del aiP, ajP, avP, aiU, ajU, avU
        # Make a matrix double the size
        self.rhs = pu.Matrix(sum([grid.invtx[var] for var in grid.varnames]), comm=comm)
        # And copy the content of the previous matrices
        self.rhs.getMatrix.setValuesCSR(ai, aj, av)
        self.rhs.assemble()

        # Deal with the boundary conditions matrices and vectors
        # First get the vector for dirichlet bocos
        bcP = self.tsP.bocosValues.getVector.getArray()
        bcU = self.tsU.bocosValues.getVector.getArray()
        del self.tsP.bocosValues, self.tsU.bocosValues
        bc = np.concatenate((bcP, bcU))
        del bcP, bcU
        self.bocosValues = pu.Vector(sum([grid.invtx[var] for var in grid.varnames]), comm=comm)
        self.bocosValues.getVector.setArray(bc)
        self.bocosValues.assemble()
        del bc
        # Then the matrix for Neumann boundary conditions
        (aiP, ajP, avP) = self.tsP.zeroForNeumann.getMatrix.getValuesCSR()
        del self.tsP.zeroForNeumann
        (aiU, ajU, avU) = self.tsU.zeroForNeumann.getMatrix.getValuesCSR()
        del self.tsU.zeroForNeumann
        ai = np.concatenate((aiU, aiP[1:]+aiU[-1]))
        aj = np.concatenate((ajU, grid.invtx['u']+ajP))
        av = np.concatenate((avU, avP))
        del aiP, ajP, avP, aiU, ajU, avU
        self.zeroForNeumann = pu.Matrix(sum([grid.invtx[var] for var in grid.varnames]), comm=comm)
        self.zeroForNeumann.getMatrix.setValuesCSR(ai, aj, av)
        self.zeroForNeumann.assemble()

        # Don't forget the filtering matrix if there is one
        if self.filter is not None:
            # Get the values and indices of the two filterMat from the transport subsolvers
            (aiP, ajP, avP) = self.tsP.filterMat.getMatrix.getValuesCSR()
            del self.tsP.filterMat
            (aiU, ajU, avU) = self.tsU.filterMat.getMatrix.getValuesCSR()
            del self.tsU.filterMat
            # And concatenate them, since filterMat will be diagonal blocked
            ai = np.concatenate((aiU, aiP[1:]+aiU[-1]))
            aj = np.concatenate((ajU, grid.invtx['u']+ajP))
            av = np.concatenate((avU, avP))
            del aiP, ajP, avP, aiU, ajU, avU
            # Make a matrix double the size
            self.filterMat = pu.Matrix(sum([grid.invtx[var] for var in grid.varnames]), comm=comm)
            # And copy the content of the previous matrices
            self.filterMat.getMatrix.setValuesCSR(ai, aj, av)
            self.filterMat.assemble()
            # Do the same for the filterBocoValues
            bcP = self.tsP.filterBocosValues.getVector.getArray()
            bcU = self.tsU.filterBocosValues.getVector.getArray()
            del self.tsP.filterBocosValues, self.tsU.filterBocosValues
            bc = np.concatenate((bcP, bcU))
            del bcP, bcU
            self.filterBocosValues = pu.Vector(sum([grid.invtx[var] for var in grid.varnames]), comm=comm)
            self.filterBocosValues.getVector.setArray(bc)
            self.filterBocosValues.assemble()
            del bc

        # Finish by calling the parent method
        del ai, aj, av
        super(LinearAcousticSolver, self).execute(grid, es=es, comm=comm)
                