'''
This file contains auxiliary definitions of PETSc objects
'''
__author__ = "Thibault Bridel-Bertomeu"
__license__ = "MIT"
__status__ = "Development"

# Import modules
import numpy as np
from petsc4py import PETSc

class Matrix(object):
	'''
	A custom class for the PETSc.Mat(), with basically a specific initialization
	'''

	def __init__(self, size, comm=None):
		'''
		Initialization routine for the Matrix class
		'''

		# Save a few variables privately
		self._size = size

		# Create and set the size of the matrix
		# Careful the matrix is not assembled at this point
		self._matrix = PETSc.Mat()
		self._matrix.create(comm)
		self._matrix.setSizes([self._size, self._size])
		self._matrix.setType('aij')
		self._matrix.setUp()

	@property
	def getMatrix(self):
		''' Distributed array in PETSc format '''
		return self._matrix

	@property
	def size(self):
		''' Size of one dimension of the square matrix '''
		return self._size

	def assemble(self):
		'''
		Assembles the class array
		'''
		self._matrix.assemble()

	def setValue(self, row, column, value, mode=PETSc.InsertMode.INSERT):
		'''
		Set a value at the designated (row, column) pair
		'''

		# If input matrix is assembled, keep it in mind
		wasAssembled = self._matrix.isAssembled()

		# Use the PETSc set value routine with pre-check on feasability
		Istart, Iend = self._matrix.getOwnershipRange()
		if row >= 0 and row < self._size and column >= 0 and column < self._size:
			if row >= Istart and row < Iend:
				self._matrix.setValue(row, column, value, mode)

		# If input matrix was assembled, better re-assemble it
		if wasAssembled:
			self._matrix.assemble()

	def setValues(self, rows, columns, values, mode=PETSc.InsertMode.INSERT):
		'''
		Set a set of values to an equal set of (row, column) pairs
		'''

		# If input matrix is assembled, keep it in mind
		wasAssembled = self._matrix.isAssembled()

		# Duplicate value if it is a scalar
		if type(values) is not np.ndarray and type(values) is not list:
			values = np.ones(len(rows)) * values

		# Use the PETSc set value routine directly
		Istart, Iend = self._matrix.getOwnershipRange()
		for idx in xrange(len(rows)):
			row, column = rows[idx], columns[idx]
			if row >= 0 and row < self._size and column >= 0 and column < self._size:
				if row >= Istart and row < Iend:
					self._matrix.setValue(row, column, values[idx], mode)

		# If input matrix was assembled, better re-assemble it
		if wasAssembled:
			self._matrix.assemble()

	def setDiagonal(self, diag_numbers, diag_values, safety={'b':False, 'nx':None, 'ny':None}):
		'''
		Set diagonals to constant values
		Does not assemble the matrix
		'''

		# Safety first
		# Ensure same number of diagonals as values
		assert(len(diag_numbers) == len(diag_values))
		# Prepare the 'forbidden' diagonals lists
		if safety['b']:
			v_jj = [0, 1, safety['nx']-2, safety['nx']-1]
			v_ii = [0, 1, safety['ny']-2, safety['ny']-1]
			n_jj = [[-2, -1], [-2,], [2,], [1, 2]]
			n_ii = [[-2*safety['nx'], -safety['nx']], [-2*safety['nx'],], [2*safety['nx'],], [safety['nx'], 2*safety['nx']]]

		# Convert the diagonal numbers to integers
		dn = [int(val) for val in diag_numbers]

		# Get the local range of the matrix
		Istart, Iend = self._matrix.getOwnershipRange()
		if safety['b']:
			subcols, subrows = np.unravel_index(np.arange(Istart, Iend), (safety['ny'], safety['nx']))

		# Then loop to assign the values
		for idx, I in enumerate(range(Istart, Iend)):
			for num in xrange(len(dn)):
				# If the safety is on, consider the mesh and not just raw matrix diagonals
				if safety['b']:
					isOkay = True
					if subrows[idx] in v_jj:
						where = v_jj.index(subrows[idx])
						if dn[num] in n_jj[where]:
							isOkay = False
							continue
					if subcols[idx] in v_ii:
						where = v_ii.index(subcols[idx])
						if dn[num] in n_ii[where]:
							isOkay = False
							continue
				else:
					isOkay = True
				#
				if isOkay:
					self.setValue(I, I+dn[num], diag_values[num])

	def zeroRows(self, rows, diag=1.0):
		'''
		Put entire rows to zero except the diagonal entry to diag
		If input matrix is assembled, output also
		If input matrix is not assembled, output is not either
		'''
		
		# Dependence on assembly state
		if self._matrix.isAssembled():
			# Call the PETSc routine straight
			self._matrix.zeroRows(np.asarray(rows).astype(int).tolist(), diag)
		else:
			# Loop on the columns to set all to zero
			Istart, Iend = self._matrix.getOwnershipRange()
			for row in rows:
				if row >= Istart and row < Iend:
					for idx in xrange(self.size):
						self.setValue(row, idx, 0.0)
					if not np.isclose(diag, 0.0):
						self.setValue(row, row, diag)
			PETSc.COMM_WORLD.barrier()

	def to_eye(self, assemble=True, comm=None):
		'''
		Turns the matrix into the identity matrix
		Assembles the matrix
		'''

		# First get a vector full of nones
		D = Vector(self._size)
		D.set(1.0)
		D.assemble()

		# Then set the diagonal of the matrix and assemble
		self._matrix.setDiagonal(D.getVector, PETSc.InsertMode.INSERT)
		if assemble:
			self._matrix.assemble()


class Vector(object):
	'''
	A custom class for the PETSc.Vec(), with basically a specific initialization
	'''

	def __init__(self, size, comm=None):
		'''
		Initialization routine for the Vector class
		'''

		# Save a few variables privately
		self._size = size

		# Create and set the size of the vector
		# Careful the vector is not assembled at this point
		self._vector = PETSc.Vec()
		self._vector.create(comm)
		self._vector.setSizes(self._size)
		self._vector.setUp()

	@property
	def getVector(self):
		''' Distributed array in PETSc format '''
		return self._vector

	@property
	def size(self):
		''' Size of one dimension of the square matrix '''
		return self._size

	def assemble(self):
		'''
		Assembles the class array
		'''
		self._vector.assemble()

	def set(self, value):
		'''
		Set all the values of the vector to the argument
		'''
		self._vector.set(value)

	def copy(self, vec):
		'''
		Copy another vector into the present ._vector
		'''
		self._vector = vec.copy()

	def setValue(self, row, value, mode=PETSc.InsertMode.INSERT):
		'''
		Set a value at the designated row
		Does not assemble the vector
		'''

		# Use the PETSc set value with pre-check on feasability
		if row >= 0 and row < self._size:
			self._vector.setValue(row, value, mode)

	def setValues(self, indices, values, mode=PETSc.InsertMode.INSERT):
		'''
		Set a bunch of values to the right indices in the vector
		Does not assemble the vector
		'''

		# Duplicate value if it is a scalar
		if type(values) is not np.ndarray and type(values) is not list:
			values = np.ones(len(rows)) * values

		# Use the PETSc set value routine directly
		self._vector.setValues(indices, values, addv=mode)

	def local2global(self, comm=None):
		'''
		Gather the pieces of a PETSc distributed vector into 
		a single numpy array on processor with rank 0
		'''

		# A few variables in memory
		rank = comm.Get_rank()

		# Get the rows possessed by the current proc and gather them
		istart, iend = self._vector.getOwnershipRange()
		nloc = iend - istart
		#
		Istart = comm.gather(istart, root=0)
		Iend = comm.gather(iend, root=0)

		# Prepare the recipient on proc with rank 0
		if not rank:
			v = np.zeros(self._size, PETSc.ScalarType)
			v[:nloc] = self._vector.getArray()
		else:
			v = None

		# Then parse the processors rank to send or receive the right piece
		for iproc in xrange(1, comm.size):        
			if not rank:
				i0 = Istart[iproc]
				i1 = Iend[iproc]
				comm.Recv(v[i0:i1], source=iproc, tag=77)
			elif rank == iproc:
				comm.Send(self._vector.getArray(), dest=0, tag=77)

		# Return the result
		return v
