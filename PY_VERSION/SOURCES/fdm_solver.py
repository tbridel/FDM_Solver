#!/usr/bin/env python

'''
This file is the main file for the FDM_SOLVER program. All other files in the
SOURCES directory are appendices, methods, or utilities.
'''
__author__ = "Thibault Bridel-Bertomeu"
__license__ = "MIT"
__status__ = "Development"

# Import modules
import c_arguments_parser as ap
import c_fdmgrid as fg
import c_transportsolver as ts
import c_diffusionsolver as ds
import c_advectiondiffusionsolver as ads
import c_linearacousticsolver as la
import imp
import sys
try:
    from mpi4py import MPI
except ImportError:
    print 'Fatal error: module mpi4py must be installed'
    exit()
import utils
from utils import Print

# Prepare the MPI communicator
comm = MPI.COMM_WORLD
rank = comm.Get_rank()

# Say hello
utils.say_hello(comm=comm)

# Parse command line arguments
args = ap.ArgParser(comm=comm)

# Prepare the grid to solve onto
grid = fg.getGrid(args.grid_characteristics, args.boco_characteristics, comm=comm)

# Initialize the variables
grid.initialize_solution(args.var_names, args.initial_values, comm=comm)

# Prepare the solver 
if args.equation['name'] == 'transport':
    solver = ts.TransportSolver(args.equation, grid, comm=comm)
elif args.equation['name'] == 'diffusion':
    solver = ds.DiffusionSolver(args.equation, grid, comm=comm)
elif args.equation['name'] == 'advection-diffusion':
    solver = ads.AdvectionDiffusionSolver(args.equation, grid, comm=comm)
elif args.equation['name'] == 'linear-acoustics':
    solver = la.LinearAcousticSolver(args.equation, grid, comm=comm)
else:
    raise NotImplementedError

# Get its left and right hand side matrices
solver.get_lhs(grid, comm=comm, var=grid.varnames)
solver.get_rhs(grid, comm=comm, var=grid.varnames)

# Prepare the filtering matrix if it exists
solver.create_filter(grid, comm=comm, var=grid.varnames)

# Apply the boundary conditions
solver.apply_bocos(grid.varnames, args.boco_characteristics, grid, comm=comm)

# Solve the equations
solver.execute(grid, es=args.exact_solution, comm=comm)

# Say goodbye
utils.say_goodbye(comm=comm)
