'''
This file contains the class handling the solver for the transport equation
'''
__author__ = "Thibault Bridel-Bertomeu"
__license__ = "MIT"
__status__ = "Development"

# Import modules
from c_fdmsolver import Solver
import numpy as np
from petsc4py import PETSc
import petsc_utils as pu
import utils
from utils import Print

class TransportSolver(Solver):
    '''
    Derived class 'TransportSolver' to solve the transport equation
    Schemes available: 'LW', 'FTUCS', 'RK2UCS' 'RK2CS', 'RK3UCS', 'RK3CS', 'RK4UCS', 'RK4CS', 'RK46NL_UCS', 'RK46NL_CS', 'RK4Co6S', 'BTUCS', 'RK46NL_CSo11p', 'RK4o6s_CSo11p', 'RK4CSo13p', 'RK46NL_CSo13p', 'RK4o6s_CSo13p'
    '''

    def __init__(self, equation, grid, comm=None):
        '''
        Initialization of the class for the transport equation solver
        '''

        if grid.ndim > 2:
            raise NotImplementedError

        # Initialize the parent class
        Solver.__init__(self, equation, comm=comm)

        # Get the transport velocity from the user inputs with safeties
        self.transvel = equation['parameters']
        if len(self.transvel) < grid.ndim:
            Print('for a %dD transport equation, a %d-elements transport velocity vector is required. Aborting.' % (grid.ndim, grid.ndim),
                  stype='error', comm=comm)
            exit()
        elif len(self.transvel) > grid.ndim:
            Print('truncating the transport velocity vector to meet the requirements of a %dD transport equation.' % (grid.ndim),
                  stype='warning', comm=comm)
            self.transvel = self.transvel[:grid.ndim]
        self.transvel = np.concatenate((self.transvel, np.zeros(3-grid.ndim)))

        # Initialize constants to be used in the schemes matrices definition
        self.cfl = np.zeros(3)
        self.cfl[:grid.ndim] = np.array(self.transvel[:grid.ndim]) * self._timestep / np.array(grid.steps)
        Print('The CFL numbers for each direction are as follow: [%.4f, %.4f, %.4f].' % (self.cfl[0], self.cfl[1], self.cfl[2]),
              stype=None, comm=comm)

    @property
    def tv(self):
        ''' Transport velocity(ies) '''
        return self.transvel

    def get_lhs(self, grid, comm=None, var=None):
        '''
        Fill the left hand side matrix depending on the numerical scheme
        Multiply the vector at time (n+1)
        '''

        # Prepare LHS matrix
        self.lhs = pu.Matrix(grid.invtx[var[0]], comm=comm)

        # Conditional on equation['scheme']
        if self.scheme['name'] in ['LW', 'FTUCS', 'RK2UCS', 'RK2CS', 
                                   'RK3UCS', 'RK3CS', 'RK4UCS', 
                                   'RK4CS', 'RK46NL_UCS', 'RK46NL_CS',
                                   'RK4o6s_UCS', 'RK4o6s_UCS', 'RK4o6s_CSo11p', 
                                   'RK46NL_CSo11p', 'RK4CSo13p', 'RK4o6s_CSo13p',
                                   'RK46NL_CSo13p']:
            self.lhs.to_eye(assemble=False)
        elif self.scheme['name'] == 'BTUCS':
            self.lhs.setDiagonal([-grid.nnx[var[0]], -1, 0, 1, grid.nnx[var[0]]],
                                 [-abs(self.cfl[1])*(self.tv[1]>0),
                                  -abs(self.cfl[0])*(self.tv[0]>0),
                                  1.0+(abs(self.cfl[0])+abs(self.cfl[1])),
                                  -abs(self.cfl[0])*(self.tv[0]<0),
                                  -abs(self.cfl[1])*(self.tv[1]<0)],
                                  safety={'b':grid.ndim>1, 'nx':grid.nnx[var[0]], 'ny':grid.nny[var[0]] if grid.ndim>1 else None})
        elif self.scheme['name'] in ['RK4Co6S', 'RK46NL_Co6S']:
            self.lhs.to_eye(assemble=False)
            self.der1 = [pu.Matrix(grid.nvtx, comm=comm), None]
            self.der1[0].setDiagonal([-1, 0, 1], [1., 3., 1.],
                                     safety={'b':grid.ndim>1, 'nx':grid.nnx[var[0]], 'ny':grid.nny[var[0]] if grid.ndim>1 else None})
            if grid.ndim>1:
                self.der1[1] = pu.Matrix(grid.nvtx, comm=comm)
                self.der1[1].setDiagonal([-grid.nnx[var[0]], 0, grid.nnx[var[0]]], [1., 3., 1.],
                                          safety={'b':grid.ndim>1, 'nx':grid.nnx[var[0]], 'ny':grid.nny[var[0]] if grid.ndim>1 else None})
        else:
            raise NotImplementedError

    def get_rhs(self, grid, comm=None, var=None):
        '''
        Fill the right hand side matrix depending on the numerical scheme
        Multiply the vector at time (n)
        '''

        # Prepare RHS matrix
        self.rhs = pu.Matrix(grid.invtx[var[0]], comm=comm)

        # Condition on equation['scheme']
        if self.scheme['name'] == 'LW':
            self.rhs.setDiagonal([-grid.nnx[var[0]], -1, 0, 1, grid.nnx[var[0]]],
                                 [0.5*self.cfl[1]*(self.cfl[1]+1),
                                  0.5*self.cfl[0]*(self.cfl[0]+1), 
                                  1.0-(self.cfl[0]**2+self.cfl[1]**2), 
                                  0.5*self.cfl[0]*(self.cfl[0]-1),
                                  0.5*self.cfl[1]*(self.cfl[1]-1)],
                                  safety={'b':grid.ndim>1, 'nx':grid.nnx[var[0]], 'ny':grid.nny if grid.ndim>1 else None})        
        elif self.scheme['name'] == 'FTUCS':
            self.rhs.setDiagonal([-grid.nnx[var[0]], -1, 0, 1, grid.nnx[var[0]]],
                                 [abs(self.cfl[1])*(self.tv[1]>0),
                                  abs(self.cfl[0])*(self.tv[0]>0), 
                                  1.0-(abs(self.cfl[0])+abs(self.cfl[1])), 
                                  abs(self.cfl[0])*(self.tv[0]<0),
                                  abs(self.cfl[1])*(self.tv[1]<0)],
                                  safety={'b':grid.ndim>1, 'nx':grid.nnx[var[0]], 'ny':grid.nny if grid.ndim>1 else None})
        elif self.scheme['name'] == 'BTUCS':
            self.rhs.to_eye(assemble=False)            
        elif self.scheme['name'] in ['RK2UCS', 'RK3UCS', 'RK4UCS', 'RK46NL_UCS', 'RK4o6s_UCS']:
            self.rhs.setDiagonal([-grid.nnx[var[0]], -1, 0, 1, grid.nnx[var[0]]],
                                 [abs(self.cfl[1])*(self.tv[1]>0),
                                  abs(self.cfl[0])*(self.tv[0]>0), 
                                  -abs(self.cfl[0])-abs(self.cfl[1]), 
                                  abs(self.cfl[0])*(self.tv[0]<0),
                                  abs(self.cfl[1])*(self.tv[1]<0)],
                                  safety={'b':grid.ndim>1, 'nx':grid.nnx[var[0]], 'ny':grid.nny if grid.ndim>1 else None})
        elif self.scheme['name'] in ['RK2CS', 'RK3CS', 'RK4CS', 'RK46NL_CS', 'RK4o6s_CS']:
            self.rhs.setDiagonal([-grid.nnx[var[0]], -1, 1, grid.nnx[var[0]]],
                                 [self.cfl[1]/2., self.cfl[0]/2., -self.cfl[0]/2., -self.cfl[1]/2.],
                                 safety={'b':grid.ndim>1, 'nx':grid.nnx[var[0]], 'ny':grid.nny if grid.ndim>1 else None})
        elif self.scheme['name'] in ['RK4o6s_CSo11p', 'RK46NL_CSo11p']:
            self.coeN = -np.asarray(utils.der1_schemes['CSo11p']['coeN'])
            self.coeP = -np.asarray(utils.der1_schemes['CSo11p']['coeP'])
            coefficients = np.concatenate((self.coeN*self.cfl[1], self.coeN*self.cfl[0], self.coeP*self.cfl[0], self.coeP*self.cfl[1]))
            #
            diaN = np.arange(-len(self.coeN),0)
            diaP = np.arange(1,len(self.coeP)+1)
            diagonals = np.concatenate((diaN*grid.nnx[var[0]], diaN, diaP, diaP*grid.nnx[var[0]]))
            #
            self.rhs.setDiagonal(diagonals, coefficients, safety={'b':grid.ndim>1, 'nx':grid.nnx[var[0]], 'ny':grid.nny if grid.ndim>1 else None})
        elif self.scheme['name'] in ['RK4CSo13p', 'RK4o6s_CSo13p', 'RK46NL_CSo13p']:
            self.coeN = -np.asarray(utils.der1_schemes['CSo13p']['coeN'])
            self.coeP = -np.asarray(utils.der1_schemes['CSo13p']['coeP'])
            coefficients = np.concatenate((self.coeN*self.cfl[1], self.coeN*self.cfl[0], self.coeP*self.cfl[0], self.coeP*self.cfl[1]))
            #
            diaN = np.arange(-len(self.coeN),0)
            diaP = np.arange(1,len(self.coeP)+1)
            diagonals = np.concatenate((diaN*grid.nnx[var[0]], diaN, diaP, diaP*grid.nnx[var[0]]))
            #
            self.rhs.setDiagonal(diagonals, coefficients, safety={'b':grid.ndim>1, 'nx':grid.nnx[var[0]], 'ny':grid.nny if grid.ndim>1 else None})
        elif self.scheme['name'] in ['RK4Co6S', 'RK46NL_Co6S']:
            self.rhs.to_eye(assemble=False)
            self.der0 = [pu.Matrix(grid.nvtx, comm=comm), None]
            self.der0[0].setDiagonal([-2, -1, 1, 2], 
                                     [-1./(12.*grid.dx), -28./(12.*grid.dx), 28./(12.*grid.dx), 1./(12.*grid.dx)],
                                     safety={'b':grid.ndim>1, 'nx':grid.nnx[var[0]], 'ny':grid.nny if grid.ndim>1 else None})
            if grid.ndim>1:
                self.der0[1] = pu.Matrix(grid.nvtx, comm=comm)
                self.der0[1].setDiagonal([-2*grid.nnx[var[0]], -grid.nnx[var[0]], grid.nnx[var[0]], 2*grid.nnx[var[0]]],
                                         [-1./(12.*grid.dy), -28./(12.*grid.dy), 28./(12.*grid.dy), 1./(12.*grid.dy)],
                                         safety={'b':grid.ndim>1, 'nx':grid.nnx[var[0]], 'ny':grid.nny if grid.ndim>1 else None})
        else:
            raise NotImplementedError

    def compact_assemble(self):
        '''
        Assembles the matrices for the compact space scheme
        as only the child class knows them
        '''

        # Condition on equation['scheme']
        if self.scheme['name'] in ['RK4Co6S', 'RK46NL_Co6S']:
            for idir in range(2):
                try:
                    self.der0[idir].assemble()
                    self.der1[idir].assemble()
                except:
                    pass
        else:
            raise NotImplementedError

    def compact_combine(self):
        '''
        If the scheme is compact, one the bocos have been set in the matrices, combine them
        to limit the number of KSP afterwards
        '''

        # Condition on equation['scheme']
        if self.scheme['name'] in ['RK4Co6S', 'RK46NL_Co6S']:
            self.der0[0].getMatrix.scale(-self.transvel[0])
            if self.der0[1] is not None:
                self.der0[0].getMatrix.axpy(-self.transvel[1], self.der0[1].getMatrix)
                self.der1[0].getMatrix.axpy(1.0, self.der1[1].getMatrix)
        else:
            raise NotImplementedError

    def compact_routine(self, input, output, comm=None):
        '''
        If the scheme is compact, some extra matrix operations are required to get the RHS vector
        '''

        # Condition on equation['scheme']
        if self.scheme['name'] in ['RK4Co6S', 'RK46NL_Co6S']:
            aux = input.duplicate()
            self.der0[0].getMatrix.mult(input, aux)
            #
            ksp = PETSc.KSP()
            ksp.create(comm)
            ksp.getPC().setType('jacobi')
            ksp.setOperators(self.der1[0].getMatrix)
            ksp.setFromOptions()
            ksp.getPC().setUp()
            ksp.solve(aux, output)
            #
            output.scale(self._timestep)
        else:
            raise NotImplementedError

    def set_dirichlet(self, grid, indices, offset, c_value):
        '''
        Set the lhs and rhs matrices for a dirichlet boundary condition
        Coherence in the order of the scheme is implemented
        '''

        # Associate the direction to the index of tv and cfl
        idir = 0 if abs(offset) == 1 else 1

        # Condition on equation['scheme']
        if self.scheme['name'] in ['FTUCS', 'BTUCS', 'RK2UCS', 
                                   'RK3UCS', 'RK4UCS', 'RK46NL_UCS']:
            for index in indices:
                if offset > 0:
                    self.bocosValues.setValue(index, abs(self.cfl[idir])*(self.tv[idir]>0)*c_value)
                else:
                    self.bocosValues.setValue(index, abs(self.cfl[idir])*(self.tv[idir]<0)*c_value)
        elif self.scheme['name'] == 'LW':
            for index in indices:
                if offset > 0:
                    self.bocosValues.setValue(index, 0.5*self.cfl[idir]*(self.cfl[idir]+1)*c_value)
                else:
                    self.bocosValues.setValue(index, 0.5*self.cfl[idir]*(self.cfl[idir]-1)*c_value)
        elif self.scheme['name'] in ['RK2CS', 'RK3CS', 'RK4CS', 'RK46NL_CS']:
            for index in indices:
                if offset > 0:
                    self.bocosValues.setValue(index, self.cfl[idir]/2.*c_value)
                else:
                    self.bocosValues.setValue(index, -self.cfl[idir]/2.*c_value)
        elif self.scheme['name'] in ['RK46NL_CSo11p', 'RK4o6s_CSo11p']:
            fd_schemes = ['FD_1-9', 'FD_2-8', 'FD_3-7', 'FD_4-6']
            stencil = np.arange(-5, 6)
            if offset < 0:
                for idx in xrange(len(fd_schemes)):
                    # Remove botch centered scheme
                    self.rhs.setValues(np.repeat(indices+idx*offset, stencil.size), np.tile(stencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, stencil.size),
                                       0.0, mode=PETSc.InsertMode.INSERT)
                    # Replace with non-centered scheme
                    nstencil = np.arange(-int(fd_schemes[idx].split('_')[1].split('-')[0]), int(fd_schemes[idx].split('_')[1].split('-')[1])+1)
                    nstencil = -nstencil[::-1]
                    nstencil = nstencil[:-1]
                    self.rhs.setValues(np.repeat(indices+idx*offset, nstencil.size), np.tile(nstencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, nstencil.size),
                                       np.sign(self.tv[idir])*np.tile(utils.der1_schemes[fd_schemes[idx]][::-1][:-1], indices.size)*self.cfl[idir], mode=PETSc.InsertMode.INSERT)
                    for index in indices:
                        self.bocosValues.setValue(index+idx*offset, utils.der1_schemes[fd_schemes[idx]][::-1][-1]*self.cfl[idir]*c_value)
            else:
                for idx in xrange(len(fd_schemes)):
                    # Remove botch centered scheme
                    self.rhs.setValues(np.repeat(indices+idx*offset, stencil.size), np.tile(stencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, stencil.size),
                                       0.0, mode=PETSc.InsertMode.INSERT)
                    # Replace with non-centered scheme
                    nstencil = np.arange(-int(fd_schemes[idx].split('_')[1].split('-')[0]), int(fd_schemes[idx].split('_')[1].split('-')[1])+1)
                    nstencil = nstencil[:-1]
                    self.rhs.setValues(np.repeat(indices+idx*offset, nstencil.size), np.tile(nstencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, nstencil.size),
                                       np.sign(self.tv[idir])*np.tile(utils.der1_schemes[fd_schemes[idx]][:-1], indices.size)*self.cfl[idir], mode=PETSc.InsertMode.INSERT)
                    for index in indices:
                        self.bocosValues.setValue(index+idx*offset, -utils.der1_schemes[fd_schemes[idx]][-1]*self.cfl[idir]*c_value)          
        elif self.scheme['name'] in ['RK4CSo13p', 'RK46NL_CSo13p', 'RK4o6s_CSo13p']:
            for index in indices:
                if offset > 0:
                    for idx in xrange(len(self.coeN)):
                        self.bocosValues.setValue(index+idx, np.sum(self.coeN[:len(self.coeN)-idx])*self.cfl[idir]*c_value)
                else:
                    for idx in xrange(len(self.coeP)):
                        self.bocosValues.setValue(index+idx, np.sum(self.coeP[idx:])*self.cfl[idir]*c_value)
        else:
            raise NotImplementedError

    def set_neumann(self, grid, indices, offset, factor, c_value, bocoVec=None):
        '''
        Set the lhs and rhs matrices for a neumann boundary condition
        Coherence in the order of the scheme is implemented
        '''

        if abs(c_value) > 0:
            raise NotImplementedError

        # Associate the direction to the index of tv and cfl
        idir = 0 if abs(offset) == 1 else 1

        # Condition on equation['scheme']
        if self.scheme['name'] in ['FTUCS', 'BTUCS']:
            if self.scheme['name'] == 'BTUCS': self.lhs.zeroRows(indices, diag=1.0)
            self.rhs.setValues(indices, indices, abs(self.cfl[idir])-1, mode=PETSc.InsertMode.ADD_VALUES)
            self.rhs.setValues(indices, indices+offset, 1.0, mode=PETSc.InsertMode.INSERT)
        elif self.scheme['name'] == 'LW':
            self.rhs.zeroRows(indices, diag=1.0-(self.cfl[0]**2+self.cfl[1]**2))
            self.rhs.setValues(indices, indices+offset, self.cfl[idir]**2)
        elif self.scheme['name'] in ['RK2UCS', 'RK3UCS', 'RK4UCS', 'RK46NL_UCS']:
            self.rhs.setValues(indices, indices, abs(self.cfl[idir]), mode=PETSc.InsertMode.ADD_VALUES)
            self.rhs.setValues(indices, indices+offset, 1.0, mode=PETSc.InsertMode.INSERT)
            self.zeroForNeumann.zeroRows(indices, diag=0.0)
        elif self.scheme['name'] in ['RK2CS', 'RK3CS', 'RK4CS', 'RK46NL_CS']:
            self.rhs.setValues(indices, indices+offset, 1.0, mode=PETSc.InsertMode.INSERT)
            self.zeroForNeumann.zeroRows(indices, diag=0.0)
        elif self.scheme['name'] in ['RK46NL_CSo11p', 'RK4o6s_CSo11p']:
            self.set_open(grid, indices, offset)
            self.zeroForNeumann.zeroRows(indices, diag=0.0)
            fd_schemes = ['FD_0-10']
            stencil = np.arange(-5, 6)
            if offset < 0:
                for idx in xrange(len(fd_schemes)):
                    # # Remove botch centered scheme
                    # self.rhs.setValues(np.repeat(indices+idx*offset, stencil.size), np.tile(stencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, stencil.size),
                    #                    0.0, mode=PETSc.InsertMode.INSERT)
                    # Replace with non-centered scheme
                    nstencil = np.arange(-int(fd_schemes[idx].split('_')[1].split('-')[0]), int(fd_schemes[idx].split('_')[1].split('-')[1])+1)
                    nstencil = -nstencil[::-1]
                    nstencil = nstencil[:-1]
                    self.zeroForNeumann.setValues(np.repeat(indices+idx*offset, nstencil.size), np.tile(nstencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, nstencil.size),
                                                  -np.tile(utils.der1_schemes[fd_schemes[idx]][::-1][:-1], indices.size)/utils.der1_schemes[fd_schemes[idx]][0], mode=PETSc.InsertMode.INSERT)               
            else:
                for idx in xrange(len(fd_schemes)):
                    # # Remove botch centered scheme
                    # self.rhs.setValues(np.repeat(indices+idx*offset, stencil.size), np.tile(stencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, stencil.size),
                    #                    0.0, mode=PETSc.InsertMode.INSERT)
                    # Replace with non-centered scheme
                    nstencil = np.arange(-int(fd_schemes[idx].split('_')[1].split('-')[0]), int(fd_schemes[idx].split('_')[1].split('-')[1])+1)
                    nstencil = nstencil[1:]
                    self.zeroForNeumann.setValues(np.repeat(indices+idx*offset, nstencil.size), np.tile(nstencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, nstencil.size),
                                                  -np.tile(utils.der1_schemes[fd_schemes[idx]][1:], indices.size)/utils.der1_schemes[fd_schemes[idx]][0], mode=PETSc.InsertMode.INSERT)
        else:
            raise NotImplementedError

    def set_open(self, grid, indices, offset):
        '''
        Set the lhs and rhs matrices for an open boundary condition
        Coherence in the order of the scheme is implemented
        '''

        # Associate the direction to the index of tv and cfl
        idir = 0 if abs(offset) == 1 else 1

        # Get the other offset to re-establish the proper scheme in the normal direction
        other_offset = grid.nnx if abs(offset) == 1 else 1

        # Condition on equation['scheme']
        if self.scheme['name'] in ['FTUCS', 'BTUCS', 'RK2UCS', 'RK3UCS', 'RK4UCS', 'RK46NL_UCS']:
            if (offset > 0 and self.tv[idir] > 0) or (offset < 0 and self.tv[idir] < 0):
                raise NotImplementedError
            else:
                pass
        elif self.scheme['name'] in ['RK2CS', 'RK3CS', 'RK4CS', 'RK46NL_CS']:
            self.rhs.setValues(indices, indices, self.cfl[idir]*np.sign(offset))
            self.rhs.setValues(indices, indices+offset, -self.cfl[idir]*np.sign(offset))
        elif self.scheme['name'] in ['RK46NL_CSo11p', 'RK4o6s_CSo11p']:
            fd_schemes = ['FD_0-10', 'FD_1-9', 'FD_2-8', 'FD_3-7', 'FD_4-6']
            stencil = np.arange(-5, 6)
            if offset < 0:
                for idx in xrange(len(fd_schemes)):
                    # Remove botch centered scheme
                    self.rhs.setValues(np.repeat(indices+idx*offset, stencil.size), np.tile(stencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, stencil.size),
                                       0.0, mode=PETSc.InsertMode.INSERT)
                    # Replace with non-centered scheme
                    nstencil = np.arange(-int(fd_schemes[idx].split('_')[1].split('-')[0]), int(fd_schemes[idx].split('_')[1].split('-')[1])+1)
                    nstencil = -nstencil[::-1]
                    self.rhs.setValues(np.repeat(indices+idx*offset, nstencil.size), np.tile(nstencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, stencil.size),
                                       np.sign(self.tv[idir])*np.tile(utils.der1_schemes[fd_schemes[idx]][::-1], indices.size)*self.cfl[idir], mode=PETSc.InsertMode.INSERT)
            else:
                for idx in xrange(len(fd_schemes)):
                    # Remove botch centered scheme
                    self.rhs.setValues(np.repeat(indices+idx*offset, stencil.size), np.tile(stencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, stencil.size),
                                       0.0, mode=PETSc.InsertMode.INSERT)
                    # Replace with non-centered scheme
                    nstencil = np.arange(-int(fd_schemes[idx].split('_')[1].split('-')[0]), int(fd_schemes[idx].split('_')[1].split('-')[1])+1)
                    self.rhs.setValues(np.repeat(indices+idx*offset, nstencil.size), np.tile(nstencil*abs(offset), indices.size)+np.repeat(indices+idx*offset, stencil.size),
                                       np.sign(self.tv[idir])*np.tile(utils.der1_schemes[fd_schemes[idx]], indices.size)*self.cfl[idir], mode=PETSc.InsertMode.INSERT)
        elif self.scheme['name'] == 'LW':
            self.rhs.zeroRows(indices, diag=1.0+self.cfl[idir]*np.sign(offset))
            self.rhs.setValues(indices, indices+offset, -self.cfl[idir]*np.sign(offset))
            #
            self.rhs.setValues(indices, indices, -self.cfl[1-idir]**2, mode=PETSc.InsertMode.ADD_VALUES)
            self.rhs.setValues(indices, indices+other_offset, 0.5*self.cfl[1-idir]*(self.cfl[1-idir]-1), mode=PETSc.InsertMode.ADD_VALUES)
            self.rhs.setValues(indices, indices-other_offset, 0.5*self.cfl[1-idir]*(self.cfl[1-idir]+1), mode=PETSc.InsertMode.ADD_VALUES)          
        else:
            raise NotImplementedError

    def set_periodic(self, grid, direction=None, override=True):
        ''' 
        Set the lhs and the rhs matrices for periodicity
        Depends on the scheme
        '''

        # Some handles
        indL, indR = np.array(grid.bocos[direction+'min']), np.array(grid.bocos[direction+'max'])

        # Associate the direction to the index of tv and cfl
        idir = 0 if direction == 'x' else 1
        #
        key2offset = {'xmin':1, 'xmax':-1, 'ymin':grid.nnx[grid.varnames[0]], 'ymax':-grid.nnx[grid.varnames[0]]}
        offL, offR = key2offset[direction+'min'], key2offset[direction+'max']

        # Condition on equation['scheme']
        if self.scheme['name'] == 'LW':
            self.rhs.setValues(indL, indR+offR, 0.5*self.cfl[idir]*(self.cfl[idir]+1))
            self.rhs.setValues(indR, indL+offL, 0.5*self.cfl[idir]*(self.cfl[idir]-1))        
        elif self.scheme['name'] in ['FTUCS', 'RK2UCS', 'RK3UCS', 'RK4UCS', 'RK46NL_UCS', 'RK4o6s_UCS']:
            if self.tv[idir] > 0:
                self.rhs.setValues(indL, indR+offR, abs(self.cfl[idir]))
            else:
                self.rhs.setValues(indR, indL+offL, abs(self.cfl[idir]))
        elif self.scheme['name'] in ['RK2CS', 'RK3CS', 'RK4CS', 'RK46NL_CS', 'RK4o6s_CS']:
            self.rhs.setValues(indL, indR+offR, self.cfl[idir]/2.)
            self.rhs.setValues(indR, indL+offL, -self.cfl[idir]/2.)
        elif self.scheme['name'] in ['RK4o6s_CSo11p', 'RK46NL_CSo11p']:
            self.rhs.setValues(np.zeros(11), [grid.nvtx-5,grid.nvtx-4,grid.nvtx-3,grid.nvtx-2,grid.nvtx-1,0,1,2,3,4,5], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
            self.rhs.setValues(np.ones(11), [grid.nvtx-4,grid.nvtx-3,grid.nvtx-2,grid.nvtx-1,0,1,2,3,4,5,6], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
            self.rhs.setValues(np.ones(11)*2, [grid.nvtx-3,grid.nvtx-2,grid.nvtx-1,0,1,2,3,4,5,6,7], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
            self.rhs.setValues(np.ones(11)*3, [grid.nvtx-2,grid.nvtx-1,0,1,2,3,4,5,6,7,8], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
            self.rhs.setValues(np.ones(11)*4, [grid.nvtx-1,0,1,2,3,4,5,6,7,8,9], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
            #
            self.rhs.setValues(np.ones(11)*(grid.nvtx-1), [grid.nvtx-6,grid.nvtx-5,grid.nvtx-4,grid.nvtx-3,grid.nvtx-2,grid.nvtx-1,0,1,2,3,4], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
            self.rhs.setValues(np.ones(11)*(grid.nvtx-2), [grid.nvtx-7,grid.nvtx-6,grid.nvtx-5,grid.nvtx-4,grid.nvtx-3,grid.nvtx-2,grid.nvtx-1,0,1,2,3], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
            self.rhs.setValues(np.ones(11)*(grid.nvtx-3), [grid.nvtx-8,grid.nvtx-7,grid.nvtx-6,grid.nvtx-5,grid.nvtx-4,grid.nvtx-3,grid.nvtx-2,grid.nvtx-1,0,1,2], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
            self.rhs.setValues(np.ones(11)*(grid.nvtx-4), [grid.nvtx-9,grid.nvtx-8,grid.nvtx-7,grid.nvtx-6,grid.nvtx-5,grid.nvtx-4,grid.nvtx-3,grid.nvtx-2,grid.nvtx-1,0,1], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
            self.rhs.setValues(np.ones(11)*(grid.nvtx-5), [grid.nvtx-10,grid.nvtx-9,grid.nvtx-8,grid.nvtx-7,grid.nvtx-6,grid.nvtx-5,grid.nvtx-4,grid.nvtx-3,grid.nvtx-2,grid.nvtx-1,0], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
        elif self.scheme['name'] in ['RK4CSo13p', 'RK4o6s_CSo13p', 'RK46NL_CSo13p']:
            pass
            self.rhs.setValues(np.zeros(13), [grid.nvtx-6,grid.nvtx-5,grid.nvtx-4,grid.nvtx-3,grid.nvtx-2,grid.nvtx-1,0,1,2,3,4,5,6], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
            self.rhs.setValues(np.ones(13), [grid.nvtx-5,grid.nvtx-4,grid.nvtx-3,grid.nvtx-2,grid.nvtx-1,0,1,2,3,4,5,6,7], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
            self.rhs.setValues(np.ones(13)*2, [grid.nvtx-4,grid.nvtx-3,grid.nvtx-2,grid.nvtx-1,0,1,2,3,4,5,6,7,8], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
            self.rhs.setValues(np.ones(13)*3, [grid.nvtx-3,grid.nvtx-2,grid.nvtx-1,0,1,2,3,4,5,6,7,8,9], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
            self.rhs.setValues(np.ones(13)*4, [grid.nvtx-2,grid.nvtx-1,0,1,2,3,4,5,6,7,8,9,10], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
            self.rhs.setValues(np.ones(13)*5, [grid.nvtx-1,0,1,2,3,4,5,6,7,8,9,10,11], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
            #
            self.rhs.setValues(np.ones(13)*(grid.nvtx-1), [grid.nvtx-7,grid.nvtx-6,grid.nvtx-5,grid.nvtx-4,grid.nvtx-3,grid.nvtx-2,grid.nvtx-1,0,1,2,3,4,5], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
            self.rhs.setValues(np.ones(13)*(grid.nvtx-2), [grid.nvtx-8,grid.nvtx-7,grid.nvtx-6,grid.nvtx-5,grid.nvtx-4,grid.nvtx-3,grid.nvtx-2,grid.nvtx-1,0,1,2,3,4], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
            self.rhs.setValues(np.ones(13)*(grid.nvtx-3), [grid.nvtx-9,grid.nvtx-8,grid.nvtx-7,grid.nvtx-6,grid.nvtx-5,grid.nvtx-4,grid.nvtx-3,grid.nvtx-2,grid.nvtx-1,0,1,2,3], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
            self.rhs.setValues(np.ones(13)*(grid.nvtx-4), [grid.nvtx-10,grid.nvtx-9,grid.nvtx-8,grid.nvtx-7,grid.nvtx-6,grid.nvtx-5,grid.nvtx-4,grid.nvtx-3,grid.nvtx-2,grid.nvtx-1,0,1,2], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
            self.rhs.setValues(np.ones(13)*(grid.nvtx-5), [grid.nvtx-11,grid.nvtx-10,grid.nvtx-9,grid.nvtx-8,grid.nvtx-7,grid.nvtx-6,grid.nvtx-5,grid.nvtx-4,grid.nvtx-3,grid.nvtx-2,grid.nvtx-1,0,1], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
            self.rhs.setValues(np.ones(13)*(grid.nvtx-6), [grid.nvtx-12,grid.nvtx-11,grid.nvtx-10,grid.nvtx-9,grid.nvtx-8,grid.nvtx-7,grid.nvtx-6,grid.nvtx-5,grid.nvtx-4,grid.nvtx-3,grid.nvtx-2,grid.nvtx-1,0], self.cfl[idir] * np.concatenate((self.coeN, [0,], self.coeP)))
            # for idx in xrange(len(self.coeN)):
            #     self.rhs.setValues(np.tile(indL+idx*offL, len(self.coeN)-idx),
            #                        np.concatenate([indR+(jdx+1)*offR for jdx in np.arange(len(self.coeN))[::-1][idx:]]),
            #                        np.repeat(self.coeN[:len(self.coeN)-idx]*self.cfl[idir], len(indL)))
            #     self.rhs.setValues(np.tile(indR+idx*offR, len(self.coeP)-idx),
            #                        np.concatenate([indL+(jdx+1)*offL for jdx in np.arange(len(self.coeP))[:len(self.coeP)-idx]]),
            #                        np.repeat(self.coeP[idx:]*self.cfl[idir], len(indR)))
        elif self.scheme['name'] in ['RK4Co6S', 'RK46NL_Co6S']:
            self.der1[idir].setValues(indL, indR+offR, 1.)
            #-
            self.der1[idir].setValues(indR, indL+offL, 1.)
            ##
            self.der0[idir].setValues(indL, indR+2*offR, -1.0/(12.0*grid.steps[idir]))
            self.der0[idir].setValues(indL, indR+offR, -28.0/(12.0*grid.steps[idir]))
            self.der0[idir].setValues(indL+offL, indR+offR, -1.0/(12.0*grid.steps[idir]))
            #-
            self.der0[idir].setValues(indR, indL+2*offL, 1.0/(12.0*grid.steps[idir]))
            self.der0[idir].setValues(indR, indL+offL, 28.0/(12.0*grid.steps[idir]))
            self.der0[idir].setValues(indR+offR, indL+offL, 1.0/(12.0*grid.steps[idir]))           
        elif self.scheme['name'] == 'BTUCS':
            if self.tv[idir] > 0:
                self.lhs.setValues(indL, indR+offR, -abs(self.cfl[idir]))
            else:
                self.lhs.setValues(indR, indL+offL, -abs(self.cfl[idir]))
        else:
            raise NotImplementedError