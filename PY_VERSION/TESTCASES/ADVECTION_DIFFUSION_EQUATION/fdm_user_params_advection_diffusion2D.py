'''
This file is an example of user parameter file for the FDM SOLVER
'''

# Input the characteristics of the grid
# - coordinates bounds, keys 'x', 'y' and 'z'
# - number of points per dimension, key 'npts'
grid_characteristics = {'x':[0.0, 1.0],
						'y':[0.0, 1.0],
						'npts':[200,200]}

# Input the names of the variables
# They will be considered in this order during execution
var_names = ['u',]

# Initial value of the different variables
# initial_values = {'u':0.0}
# initial_values = {'u':'np.ones(x.size)*(x<=0.75)*(x>=0.25)*(y<=0.75)*(y>=0.25)'}
initial_values = {'u':'exp(-0.5*(((x-0.5)/0.1)**2 + ((y-0.5)/0.1)**2))'}
# initial_values = {'u':'sin(2*x*pi)*sin(2*y*pi)'}

# Input the boundary conditions
# - boco locations, keys 'Amin' and 'Amax', for 'A' in 'x', 'y' or 'z'
# - boco values, keys are the variable names or first derivative
boco_characteristics = {'xmin':{'u':{'type':'dirichlet', 'value':0}},
						'xmax':{'u':{'type':'neumann', 'value':0}},
						'ymin':{'u':{'type':'dirichlet', 'value':0}},
						'ymax':{'u':{'type':'dirichlet', 'value':0}}}

# Input the equation to be solved
# - name can be 'transport', 'diffusion'
# - scheme can be 
# - - - - - - - - for 'transport': 'LW', 'FTUCS', 'RK2UCS' 'RK2CS', 'RK3UCS', 'RK3CS', 'RK4UCS', 'RK4CS', 'RK46NL_UCS', 'RK46NL_CS', 'RK4Co6S', 'BTUCS', 'RK46NL_CSo11p', 'RK4o6s_CSo11p', 'RK4CSo13p', 'RK46NL_CSo13p', 'RK4o6s_CSo13p'
# - - - - - - - - for 'diffusion': 'FTCS', 'BTCS', 'CNCS'
# - - - - - - - - for 'advection-diffusion': 'FTCS', 'BTCS', CNCS', 'RK1CS', 'RK2CS', 'RK3CS', 'RK3K_CS', 'RK3BS_CS', 'RK4CS', 'RK46NL_CS', 'RK5F_CS'
# - filter can be: 'SFo11p', 'SFo13p'
equation = {'name':'advection-diffusion',
			'parameters':[1.0, 0., 0.02],
			'scheme':{'name':'CNCS'},
			'filter':None,
			'timestep':0.0005,
			'niterations':500,
			'export_every':None,
			'plot_every':None}

