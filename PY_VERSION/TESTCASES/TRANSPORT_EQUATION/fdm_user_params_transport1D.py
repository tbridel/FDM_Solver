'''
This file is an example of user parameter file for the FDM SOLVER
'''

# Input the characteristics of the grid
# - coordinates bounds, keys 'x', 'y' and 'z'
# - number of points per dimension, key 'npts'
grid_characteristics = {'x':[-50., 50.],
						'npts':[101,]}

# Input the names of the variables
# They will be considered in this order during execution
var_names = ['u',]

# Initial value of the different variables
# initial_values = {'u':0.0}
# initial_values = {'u':'np.ones(x.size)*(x<=0.5)'}
# initial_values = {'u':'np.ones(x.size)*(x<=0.75)*(x>=0.25)'}
# initial_values = {'u':'exp(-0.5*((x-0.5)/0.1)**2)'}
# initial_values = {'u':'cos(pi/(10*0.005)*x)*exp(-0.5*((x-0.5)/0.1)**2)'}
# initial_values = {'u':'exp(-0.5*((x-0.5)/0.03)**2)'}
# initial_values = {'u':'10*exp(-(x-2000)**2/(2.0*264**2))'}
# initial_values = {'u':'sin(2*pi*x/(8*0.005))*exp(-log(2)*(x/(3*0.005))**2)'}
initial_values = {'u':'cos(2*pi*x/8)*exp(-log(2)*(x/12)**2)'}

# Exact solution given the equation solved and the initial solution
# Has to be defaulted to None if no exact solution exact or if comparison is not to be made
# exact_solution = {'u':'sin(2*pi*x/(8*0.005))*exp(-log(2)*(x/(3*0.005))**2)'}
# exact_solution = {'u':'cos(2*pi*x/(6*0.005))*exp(-log(2)*(x/(12*0.005))**2)'}
# exact_solution = {'u':'exp(-0.5*(((x-t)-0.5)/0.1)**2)'}
exact_solution = {'u':'cos(2*pi*(x-t)/8)*exp(-log(2)*((x-t)/12)**2)'}

# Input the boundary conditions
# - boco locations, keys 'Amin' and 'Amax', for 'A' in 'x', 'y' or 'z'
# - boco values, keys are the variable names or first derivative
boco_characteristics = {'xmin':{'u':{'type':'dirichlet', 'value':0}},
						'xmax':{'u':{'type':'dirichlet', 'value':0}}}

# Input the equation to be solved
# - name can be 'transport', 'diffusion'
# - scheme can be 
# - - - - - - - - for 'transport': 'LW', 'FTUCS', 'RK2UCS' 'RK2CS', 'RK3UCS', 'RK3CS', 'RK4UCS', 'RK4CS', 'RK46NL_UCS', 'RK46NL_CS', 'RK4Co6S', 'BTUCS', 'RK46NL_CSo11p', 'RK4o6s_CSo11p', 'RK4CSo13p', 'RK46NL_CSo13p', 'RK4o6s_CSo13p'
# - - - - - - - - for 'diffusion': 'FTCS', 'BTCS', 'CNCS'
# - - - - - - - - for 'advection-diffusion': 'FTCS', 'BTCS', 'CNCS', 'RK1CS', 'RK2CS', 'RK3CS', 'RK3K_CS', 'RK3BS_CS', 'RK4CS', 'RK46NL_CS', 'RK5F_CS'
# - filter can be: 'SFo11p', 'SFo13p'
equation = {'name':'transport',
			'parameters':[1.0,],
			'scheme':{'name':'RK4o6s_CSo11p'},
			'filter':{'name':'SFo11p', 'strength':0.2},
			'timestep':0.001,
			'niterations':6000,
			'export_every':None,
			'plot_every':10}

